﻿// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <algorithm>
#include <vector>
#include <fstream>

#include "averager.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma resource "*.dfm"

#include "gnugettext.hpp"

TAveragerForm *AveragerForm;
//---------------------------------------------------------------------------


template<class T>
inline T sqr(T a) {
    return a*a;
}


TColor TAveragerForm::color[] = {clBlue, clRed, clLime, clMaroon, clYellow, clPurple};

__fastcall TAveragerForm::TAveragerForm(TComponent* Owner)
    : TForm(Owner)
{
}


void TAveragerForm::initDialog(int chanNum, bool eventChan, LibCore::CMeasurement& meas, LibCore::CursorPositions& curs, int intervalChanNum) {
    measurement = &meas;
	cursors = curs;
    numEventChans = measurement->getNumEventChannels();
    numSignalChans = measurement->getNumChannels();
    protectTimes = false;
    protectNums = false;
    protectGraph = true; // do not redraw during the initializaton
    
    eventChanUpDown->Max = short(numEventChans);
    if (intervalChanNum == -1)
        eventChanUpDown->Position = eventChanUpDown->Max;
    else 
        eventChanUpDown->Position = short(intervalChanNum + 1);
    
    if (eventChan) {
        drawChanUpDown->Max = short(numEventChans);
        radioButtonEvent->Checked = true;
    } else {                                   
        drawChanUpDown->Max = short(numSignalChans);
        radioButtonMeasurement->Checked = true;
    }
    drawChanUpDown->Position = short(chanNum+1);

    // cursors set input event range
    startTime = 1e+100;
    endTime = -1;
    for (int i = 0; i < LibCore::CursorPositions::numCursors; i++) {
	    if (cursors.c[i] != -1)
			startTime = std::min(startTime, cursors.c[i]);
        endTime = std::max(endTime, cursors.c[i]);
    }
    
    startEventNum = endEventNum = 1;
    protectTimes = true;        
    eventChannelSettingsCheck();
    setEventNums();                        
    startEventTime->Text = FormatFloat("#0.000", startTime);
    endEventTime->Text = FormatFloat("#0.000", endTime);
    protectTimes = false;
    
    protectGraph = false; // do not redraw during the initializaton
}


void TAveragerForm::setChartRange(int halfRange) {
    drawRange1UpDown->Position = (short)halfRange;
}


double TAveragerForm::getChartRange() const {
    return drawRange1UpDown->Position;
}


void TAveragerForm::setFilename(const AnsiString& str) {
    filename = str;
}


void TAveragerForm::redrawGraph() {
    if (!protectGraph) {
        chart->BottomAxis->Automatic = false;
        chart->BottomAxis->AutomaticMaximum = false;
        chart->BottomAxis->AxisValuesFormat = "# ##0.#\"ms\"";
        chart->BottomAxis->Minimum = -drawRange1UpDown->Position;
        chart->BottomAxis->Maximum = drawRange1UpDown->Position;
        chart->BottomAxis->Increment = drawRange1UpDown->Position / 5; 

        LibCore::CEventChannel& eventChan = measurement->getEventChannel(eventChanUpDown->Position-1);
        if (eventChan.getNumElems() < 1)
            return;
        double timeHalfRange = drawRange1UpDown->Position * 0.001;
    
        int maxDrawEvents = (1 + endEventUpDown->Position - startEventUpDown->Position);
        if (maxDrawEvents < 1) maxDrawEvents = 1;

        for (TChartSeries* series = chart->GetASeries(); series != 0; series = chart->GetASeries()) {
            chart->RemoveSeries(series);
            delete series;
        }
        
        sum.resize(averageSamplingTrackBar->Position);
        cnt.resize(averageSamplingTrackBar->Position);
        period.resize(averageSamplingTrackBar->Position);
        for (unsigned int i = 0; i < cnt.size(); ++i) {
            sum[i].clear();
            sum[i].resize(801, 0.0);
            cnt[i].clear();
            cnt[i].resize(801, 0);
        }
        meanAverage.clear();

        String emptyString("");

        infoGrid->ColCount = averageSamplingTrackBar->Position + (averageEnabled ? 3 : 2);
        // keep count of number of drawn vertices to dynamically change number of drawn series 
        // thus keeping response time in check
        int numVerticesDrawn = 0;
        const int maxNumVerticesDrawn = 200000;   
        int skipEventDraws = 0;
    
	    if (radioButtonMeasurement->Checked) {
            checkBoxAutoResample->Font->Color = clWindowText;
            
            /// signal channel selected
            int averageIndex = 0;
            int averageIndexEventLimit = maxDrawEvents / averageSamplingTrackBar->Position;
            LibCore::CChannel& drawChan = (*measurement)[drawChanUpDown->Position - 1];
            for (int event = 0; event < maxDrawEvents; ++event) {    
                int absEvent = event + std::max(startEventUpDown->Position - 1, 0);

                TFastLineSeries *series;
                if (skipEventDraws == 0) {
                    series = new TFastLineSeries(chart);
                    series->Title = AnsiString(absEvent+1);
            	    series->ParentChart = chart;
            	    series->LinePen->Color = clGreen;
	                series->LinePen->Width = 1;
                    series->VertAxis = aLeftAxis;
                } else {
                    checkBoxAutoResample->Font->Color = clRed;
                }
        
                int minIndex = measurement->time2SampleIndex(std::max(0.0, eventChan[absEvent].time - 
                    timeHalfRange));
                int maxIndex = std::min(drawChan.getNumElems()-1, measurement->time2SampleIndex(
                    eventChan[absEvent].time + timeHalfRange));

                if (skipEventDraws == 0) {
                    for (int index = minIndex; index <= maxIndex; ++index) {
                        double relativeT = (measurement->sampleIndex2Time(index) - 
                            eventChan[absEvent].time) * 1000.0;
                        series->AddXY(relativeT, drawChan[index], emptyString, clTeeColor);
                    }
                }

                // calculation of average
                {
                    if (event >= averageIndexEventLimit) {
                        ++averageIndex;
                        averageIndexEventLimit = (maxDrawEvents * (averageIndex + 1)) / 
                            averageSamplingTrackBar->Position;
                    }
                    
                    // change min/max index so they point before the start / after the end
                    // of marked time interval
                    if ((measurement->sampleIndex2Time(minIndex) > eventChan[absEvent].time - timeHalfRange) && (minIndex > 0))
                        --minIndex;
                    if ((measurement->sampleIndex2Time(maxIndex) < eventChan[absEvent].time + timeHalfRange) && (maxIndex < (drawChan.getNumElems()-1)))
                        ++maxIndex;
                    
                    if (minIndex < maxIndex) {
                        int index1 = minIndex;
                        double time1 = (measurement->sampleIndex2Time(index1) - 
                            eventChan[absEvent].time) * 1000.0 - chart->BottomAxis->Minimum;
                        double time2 = (measurement->sampleIndex2Time(index1 + 1) - 
                            eventChan[absEvent].time) * 1000.0 - chart->BottomAxis->Minimum;
                            
                        for (int i = 0; i < int(sum[0].size()); ++i) {
                            double iTime = ((chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) 
                                / (sum[0].size()-1) * i);
                            
                            bool breakOut = false;
                            while (iTime > time2) {
                                ++index1;
                                if (index1 == maxIndex) {
                                    breakOut = true;
                                    break;
                                }
                                time1 = time2;
                                time2 = (measurement->sampleIndex2Time(index1 + 1) - 
                                    eventChan[absEvent].time) * 1000.0 - chart->BottomAxis->Minimum;
                            }
                            if (breakOut)
                                break;


                            if ((iTime > time1) && (iTime <= time2)) {
                                sum[averageIndex][i] += (drawChan[index1] * (time2 - iTime) + 
                                    drawChan[index1 + 1] * (iTime - time1)) / (time2 - time1);
                                ++cnt[averageIndex][i];
                            }
                        }
                    }
                }

                // see whether series for the next event should be drawn
                int verticesPerSeries = maxIndex - minIndex;
                if (skipEventDraws == 0)
                    numVerticesDrawn += verticesPerSeries;

                if (maxDrawEvents - event > 1) {
                    double nextRatio = double(maxNumVerticesDrawn - numVerticesDrawn) / 
                        double(maxDrawEvents - event - 1);
                    double pastRatio = double(numVerticesDrawn) / double(event + 1);
                    skipEventDraws = ((nextRatio > pastRatio) || 
                        (verticesPerSeries < nextRatio)) ? 0 : 1;
                }
            }
        } else {
            /// event channel selected
            LibCore::CEventChannel& drawChan = measurement->getEventChannel(drawChanUpDown->Position - 1);
            checkBoxAutoResample->Font->Color = clWindowText;
            
            int averageIndex = 0;
            int averageIndexEventLimit = maxDrawEvents / averageSamplingTrackBar->Position;
            
            for (int event = 0; event < maxDrawEvents; ++event) {
                int absEvent = event + startEventUpDown->Position - 1;
                 
                TChartSeries *series;
                
                if (skipEventDraws > 0) {
                    // skip draw was ordered (too many vertices per event + too many events -> don't draw everything)
                    checkBoxAutoResample->Font->Color = clRed;
                } else {
                    // only init series if it si going to be drawn
                    if (lineDrawCheckBox->Checked) {
                        TFastLineSeries *newSeries = new TFastLineSeries(chart);
                        newSeries->ParentChart = chart;
                	    newSeries->LinePen->Color = clGreen;
        	            newSeries->LinePen->Width = 1;
    		        	series = newSeries;
        	    	} else {
    	    	    	TPointSeries *newSeries = new TPointSeries(chart);      
                        newSeries->ParentChart = chart;
    	        		newSeries->Pointer->Style = psDiagCross;
        			    newSeries->Pointer->Pen->Width = 1;
                        newSeries->Pointer->Pen->Color = clGreen;
		            	series = newSeries;
            		}               
                    
                    series->Title = AnsiString(absEvent+1);
                    series->VertAxis = aLeftAxis;
                }

                // range of vertices to be drawn
                int minIndex = drawChan.getIndexClosestToTime(
                    std::max(0.0, eventChan[absEvent].time - timeHalfRange));
                int maxIndex = std::min(drawChan.getNumElems()-1, 
                    drawChan.getIndexClosestToTime(eventChan[absEvent].time + timeHalfRange));
                
                if (skipEventDraws == 0) {
                    for (int index = minIndex; index <= maxIndex; ++index) {
                        double relativeT = (drawChan[index].time - eventChan[absEvent].time) * 1000.0;
                        series->AddXY(relativeT, drawChan[index].value, emptyString, clGreen);
                    }
                }
                
                // calculation of average
                {
                    if (event >= averageIndexEventLimit) {
                        ++averageIndex;
                        averageIndexEventLimit = (maxDrawEvents * (averageIndex + 1)) / 
                            averageSamplingTrackBar->Position;
                    }
                            
                    // change min/max index so they point before the start / after the end
                    // of marked time interval
                    if ((drawChan[minIndex].time > eventChan[absEvent].time - timeHalfRange) && (minIndex > 0))
                        --minIndex;
                    if ((drawChan[maxIndex].time < eventChan[absEvent].time + timeHalfRange) && (maxIndex < (drawChan.getNumElems()-1)))
                        ++maxIndex;
                    
                    if (minIndex < maxIndex) {
                        int index1 = minIndex;
                        double time1 = (drawChan[index1].time - eventChan[absEvent].time) 
                            * 1000.0 - chart->BottomAxis->Minimum;
                        double time2 = (drawChan[index1 + 1].time - eventChan[absEvent].time) 
                            * 1000.0 - chart->BottomAxis->Minimum;
                        
                        for (int i = 0; i < int(sum[0].size()); ++i) {
                            double iTime = ((chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) 
                                / (sum[0].size()-1) * i);

                            bool breakOut = false;
                            while (iTime > time2) {
                                ++index1;
                                if (index1 == maxIndex) {
                                    breakOut = true;
                                    break;
                                }
                                time1 = time2;
                                time2 = (drawChan[index1 + 1].time - eventChan[absEvent].time) 
                                    * 1000.0 - chart->BottomAxis->Minimum;
                            }
                            if (breakOut)
                                break;

                            if (iTime > time1) {
                                sum[averageIndex][i] += (drawChan[index1].value * (time2 - iTime) + 
                                    drawChan[index1 + 1].value * (iTime - time1)) / (time2 - time1);
                                ++cnt[averageIndex][i];
                            }
                        }
                    }
                }
                    

                // see whether series for the next event should be drawn
                int verticesPerSeries = maxIndex - minIndex;
                if (skipEventDraws == 0)
                    numVerticesDrawn += verticesPerSeries;

                if (maxDrawEvents - event > 1) {
                    double nextRatio = double(maxNumVerticesDrawn - numVerticesDrawn) / 
                        double(maxDrawEvents - event - 1);
                    double pastRatio = double(numVerticesDrawn) / double(event + 1);
                    skipEventDraws = ((nextRatio > pastRatio) || 
                        (verticesPerSeries < nextRatio)) ? 0 : 1;
                }
            }
        }

        {
            averageSamplingTrackBar->Enabled = true;
            infoGrid->ColCount = averageSamplingTrackBar->Position + 2;
            
            for (unsigned int a = 0; a < sum.size(); ++a) {
                TFastLineSeries *averageSeries = new TFastLineSeries(chart);
                averageSeries->Title = AnsiString("Povpreèje ") + AnsiString(a);
                averageSeries->ParentChart = chart;
                averageSeries->LinePen->Color = color[a];
                averageSeries->LinePen->Width = 3;
                averageSeries->VertAxis = aLeftAxis;
            
                for (int i = 0; i < int(sum[a].size()); ++i) {
                    if (cnt[a][i] > 0) {
                        averageSeries->AddXY(chart->BottomAxis->Minimum + i * 
                            (chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) / (sum[a].size()-1),
                            sum[a][i] / cnt[a][i], "", clTeeColor);
                    }
                }
            }
            if (averageEnabled->Checked) {
                meanAverage.resize(cnt[0].size());
                    
                TFastLineSeries *averageSeries = new TFastLineSeries(chart);
                averageSeries->Title = AnsiString("Povpreèje");
                averageSeries->ParentChart = chart;
                averageSeries->LinePen->Color = clBlack;
                averageSeries->LinePen->Width = 3;
                averageSeries->VertAxis = aLeftAxis;
                    
                for (size_t i = 0; i < meanAverage.size(); ++i) {
                    meanAverage[i] = sum[0][i];
                    size_t c = cnt[0][i];
                    for (size_t j = 1; j < sum.size(); ++j) {
                        meanAverage[i] += sum[j][i];
                        c += cnt[j][i];
                    }
                    if (c != 0) {
                        meanAverage[i] /= double(c);

                        averageSeries->AddXY(chart->BottomAxis->Minimum + i * 
                            (chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) / (meanAverage.size()-1),
                            meanAverage[i], "", clTeeColor);
                    }
                }
            }     
            analyseAverages();
        }
    }	
}

void TAveragerForm::drawChannelSettingsCheck() {
    if (radioButtonMeasurement->Checked) {
        drawChanUpDown->Max = short(numSignalChans);
        drawChanName->Text = (*measurement)[drawChanUpDown->Position-1].getChannelName();
        drawChanNum->Text = AnsiString(drawChanUpDown->Position);
        unit = (*measurement)[drawChanUpDown->Position-1].getMeasurementUnit();
    } else {
        drawChanUpDown->Max = short(numEventChans);
        drawChanName->Text = measurement->getEventChannel(drawChanUpDown->Position-1).getChannelName();
        drawChanNum->Text = AnsiString(drawChanUpDown->Position);
        unit = measurement->getEventChannel(drawChanUpDown->Position-1).getMeasurementUnit();
    }
                                
    drawRange1UpDown->Increment = std::max(short(1), short(0.05 * drawRange1UpDown->Position + 0.5));
}

void TAveragerForm::eventChannelSettingsCheck() {
    LibCore::CEventChannel& chan = measurement->getEventChannel(eventChanUpDown->Position - 1);
    
    // controls that depend on selected event channel
    eventChanName->Text = chan.getChannelName();
    startEventUpDown->Max = short(std::min(32767, chan.getNumElems()));
    endEventUpDown->Max = short(std::min(32767, chan.getNumElems()));
    endEventUpDown->Min = 1;
    startEventUpDown->Min = 1;
    
    eventRange->Caption = AnsiString("1 - ")+AnsiString(startEventUpDown->Max);
    // more elements than is the range of our Up/Down control - color this label red
    if (startEventUpDown->Max != chan.getNumElems())
        eventRange->Font->Color = clRed;
    else
        eventRange->Font->Color = clWindowText;

    double maxTime = chan[chan.getNumElems()-1].time;
    double minTime = chan[0].time;
        
    if (endTime == -1) {
        // if no cursors are set before calling Averager, select all events
        startTime = minTime;
        endTime = maxTime;
        startEventNum = 1;
        endEventNum = chan.getNumElems();
    }  

    if (startTime < minTime)
        startTime = minTime;
    if (endTime < minTime)
        endTime = minTime;
    if (startTime > maxTime)
        startTime = maxTime;
    if (endTime > maxTime)
        endTime = maxTime;
        
    if (startEventNum < 1)
        startEventNum = 1;
    if (startEventNum > std::min(32767, chan.getNumElems()))
        startEventNum = std::min(32767, chan.getNumElems());
    if (endEventNum < 1)
        endEventNum = 1;
    if (endEventNum > std::min(32767, chan.getNumElems()))
        endEventNum = std::min(32767, chan.getNumElems());
}

void TAveragerForm::setEventTimes() {
    // times were set outside, check if values are ok and change all control values accordingly
    LibCore::CEventChannel& chan = measurement->getEventChannel(eventChanUpDown->Position - 1);
    eventChannelSettingsCheck();

    if ((startTime != chan[startEventNum - 1].time) && !protectTimes) {
        startTime = chan[startEventNum - 1].time;
        startEventTime->Text = FormatFloat("#0.000", startTime);
    }
    
    if ((endTime != chan[endEventNum - 1].time) && !protectTimes) {
        endTime = chan[endEventNum - 1].time;
        endEventTime->Text = FormatFloat("#0.000", endTime);
    }
}

void TAveragerForm::setEventNums(bool protect) {
    bool oldProtect = protectTimes;
    protectTimes |= protect;
    // times were set outside, check if values are ok and change all control values accordingly
    LibCore::CEventChannel& chan = measurement->getEventChannel(eventChanUpDown->Position - 1);
    
    int newIndex = chan.getIndexClosestToTime(startTime) + 1;
    if ((newIndex != startEventNum) && !protectNums) {
        startEventNum = newIndex;
        startEventUpDown->Position = short(std::min(32767, startEventNum));
    }
    newIndex = chan.getIndexClosestToTime(endTime) + 1;
    if ((newIndex != endEventNum) && !protectNums) {
        endEventNum = newIndex;
        endEventUpDown->Position = short(std::min(32767, endEventNum));
    }
    protectTimes = oldProtect;
}

void TAveragerForm::saveAsText(const AnsiString& fname) {
    std::ofstream file(fname.c_str());

    double valMultiply = (unit == "s") ? 1000.0 : 1.0;
    
    int i = 0;

    //file << valMultiply << unit.c_str() << "\n";
    for (std::vector<double>::const_iterator it = meanAverage.begin(), it1 = it+1;
        it1 != meanAverage.end(); 
        ++it, ++it1) 
    {
        double time = (chart->BottomAxis->Minimum + i * (chart->BottomAxis->Maximum - 
            chart->BottomAxis->Minimum) / (meanAverage.size()-1));
        double time1 = time + (chart->BottomAxis->Maximum - 
            chart->BottomAxis->Minimum) / (meanAverage.size()-1);

        //file << int(time * 0.01) << " " << int(time1 * 0.01) << "\n";

        if (int(time * 0.01) < int(time1 * 0.01)) {
            // if step over 0.1s accours, calculate and output value at 0.1s barrier
            int timeMid = int(0.01 * time1) * 100;
            double value = (*it * (time1 - timeMid) + *it1 * (timeMid - time)) * valMultiply / (time1 - time);

            file << timeMid << "\t" << value << "\n";
        }
        ++i;
    }
}

void TAveragerForm::analyseAverages() {
    // set grid sizes
    for (int i = 0; i < infoGrid->ColCount; ++i) {
        infoGrid->ColWidths[i] = (i != infoGrid->ColCount-1) ? 
            40 : 
            infoGrid->Width - (infoGrid->ColCount * 40) + 40;
    }
            
    // determine period
    LibCore::CEventChannel& eventChan = measurement->getEventChannel(eventChanUpDown->Position-1);
    int maxDrawEvents = (endEventUpDown->Position + 1 - startEventUpDown->Position);
    for (short i = 1; i <= averageSamplingTrackBar->Position; ++i) {
        int lowLimit = ((maxDrawEvents-1) * (i-1)) / averageSamplingTrackBar->Position + 
            startEventUpDown->Position - 1;
        int highLimit = ((maxDrawEvents-1) * (i)) / averageSamplingTrackBar->Position + 
            startEventUpDown->Position - 1;
                            
        infoGrid->Cells[i][1] = FormatFloat("#0.0", eventChan[lowLimit].time); 
        infoGrid->Cells[i][2] = FormatFloat("#0.0", eventChan[highLimit].time);;
            
        period[i-1] = 0;
        if (highLimit - lowLimit - 1 > 0) {
            period[i-1] = 1000.0 * (eventChan[highLimit].time - eventChan[lowLimit+1].time) / 
                (highLimit - lowLimit - 1);
        }
    }
    if (endEventUpDown->Position > startEventUpDown->Position) {
        meanPeriod = 1000.0 * (eventChan[endEventUpDown->Position - 1].time - 
            eventChan[startEventUpDown->Position - 1].time) /
            double(endEventUpDown->Position - startEventUpDown->Position);
    } else {
        meanPeriod = -1.0;
    }

    // find minima & maxima
    minima.resize(cnt.size());
    maxima.resize(cnt.size());
    double valMultiply = 1.0;
    AnsiString newUnit(unit);

    if (unit == "s") {
        newUnit = "ms";
        valMultiply = 1000.0;
    }
    
    int takeMin, takeMax;
    bool takeSet = false;
    for (size_t a = 0; a < cnt.size(); ++a) {
        minima[a].clear();
        maxima[a].clear();

        int p = int(period[a] / (chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) 
            * cnt[a].size());
        if ((period[a] > 0.0) && (p < int(cnt[a].size()))) {

            // search for extremes on mid - one period interval
            double minV1 = 0.0, maxV1 = minV1;
            int minI1 = -1, maxI1 = -1;
            for (int i = std::max(0, int(cnt[a].size() / 2 - p)); i < int(cnt[a].size() / 2); ++i) {
                if (cnt[a][i] > 0) {
                    double v = sum[a][i] / cnt[a][i];
                    if (minI1 < 0) {
                        maxV1 = minV1 = v;
                        maxI1 = minI1 = i;
                    } else if (v < minV1) {
                        minV1 = v;
                        minI1 = i;
                    } else if (v > maxV1) {
                        maxV1 = v;
                        maxI1 = i;
                    }
                }
            }

            // search for extremes on mid + one period interval
            double minV2 = 0.0, maxV2 = minV2;
            int minI2 = -1, maxI2 = -1;
            for (int i = cnt[a].size() / 2; i < std::min(int(cnt[a].size() / 2 + p), int(cnt[a].size())); ++i) {
                if (cnt[a][i] > 0) {
                    double v = sum[a][i] / cnt[a][i];
                    if (minI2 < 0) {
                        maxV2 = minV2 = v;
                        maxI2 = minI2 = i;
                    } else if (v < minV2) {
                        minV2 = v;
                        minI2 = i;
                    } else if (v > maxV2) {
                        maxV2 = v;
                        maxI2 = i;
                    }
                }
            }

            if (2*abs(minI1 - minI2) < p) {
                if (minV1 < minV2) {
                    minI2 = minI1;
                    minV2 = minV1;
                } else {
                    minI1 = minI2;
                    minV1 = minV2;
                }
            }
            
            if (2*abs(maxI1 - maxI2) < p) {
                if (maxV1 > maxV2) {
                    maxI2 = maxI1;
                    maxV2 = maxV1;
                } else {
                    maxI1 = maxI2;
                    maxV1 = maxV2;
                }
            }
             
            double minV, maxV;
            int minI, maxI;
                           
            // take min and max closest to 0 as the absolute min and max
            if (!takeSet) {
				takeMin = (abs((long)(minI1 - cnt[a].size() / 2)) < abs((long)(minI2 - cnt[a].size() / 2))) ? minI1 : minI2;
				takeMax = (abs((long)(maxI1 - cnt[a].size() / 2)) < abs((long)(maxI2 - cnt[a].size() / 2))) ? maxI1 : maxI2;
                takeSet = true;
            }
            
            if (abs(takeMin - minI1) < abs(takeMin - minI2)) {
                minI = minI1;
                minV = minV1;
            } else {
                minI = minI2;
                minV = minV2;
            }
            if (abs(takeMax - maxI1) < abs(takeMax - maxI2)) {
                maxI = maxI1;
                maxV = maxV1;
            } else {
                maxI = maxI2;
                maxV = maxV2;
            }
             
            minima[a].push_back(minI);
            maxima[a].push_back(maxI);
            
            double minT = chart->BottomAxis->Minimum + minI * (chart->BottomAxis->Maximum - 
                chart->BottomAxis->Minimum) / (sum[a].size()-1);
            double maxT = chart->BottomAxis->Minimum + maxI * (chart->BottomAxis->Maximum - 
                chart->BottomAxis->Minimum) / (sum[a].size()-1);

            minV *= valMultiply;
            maxV *= valMultiply;
            
            infoGrid->Cells[infoGrid->ColCount - 1][8] = "ms";
            if (a == 0) {
                infoGrid->Cells[infoGrid->ColCount - 1][3] = newUnit;
                infoGrid->Cells[infoGrid->ColCount - 1][4] = "ms";
                infoGrid->Cells[infoGrid->ColCount - 1][5] = newUnit;  
                infoGrid->Cells[infoGrid->ColCount - 1][6] = "ms";
                infoGrid->Cells[infoGrid->ColCount - 1][7] = newUnit;
                infoGrid->Cells[infoGrid->ColCount - 1][8] = "ms";
            }

            if ((abs(maxV) < 10) && (abs(minV) < 10)) {
                infoGrid->Cells[a+1][3] = FormatFloat("#0.000", minV);
                infoGrid->Cells[a+1][5] = FormatFloat("#0.000", maxV);
            } else if ((abs(maxV) < 100) && (abs(minV) < 100)) {
                infoGrid->Cells[a+1][3] = FormatFloat("#0.00", minV);
                infoGrid->Cells[a+1][5] = FormatFloat("#0.00", maxV);
            } else {
                infoGrid->Cells[a+1][3] = FormatFloat("#0.0", minV);
                infoGrid->Cells[a+1][5] = FormatFloat("#0.0", maxV);
            }

            infoGrid->Cells[a+1][4] = FormatFloat("#0.0", minT);
            infoGrid->Cells[a+1][6] = FormatFloat("#0.0", maxT);

            if ((maxV - minV) < 10)
                infoGrid->Cells[a+1][7] = FormatFloat("#0.000", (maxV - minV));
            else if ((maxV - minV) < 100)
                infoGrid->Cells[a+1][7] = FormatFloat("#0.00", (maxV - minV));
            else
                infoGrid->Cells[a+1][7] = FormatFloat("#0.0", (maxV - minV));

            infoGrid->Cells[a+1][8] = FormatFloat("#0.0", period[a]);
        } else {
            if (a == 0) {
                infoGrid->Cells[infoGrid->ColCount - 1][3] = newUnit;
                infoGrid->Cells[infoGrid->ColCount - 1][4] = "ms";
                infoGrid->Cells[infoGrid->ColCount - 1][5] = newUnit;  
                infoGrid->Cells[infoGrid->ColCount - 1][6] = "ms";
                infoGrid->Cells[infoGrid->ColCount - 1][7] = newUnit;
                infoGrid->Cells[infoGrid->ColCount - 1][8] = "ms";
            }
            infoGrid->Cells[a+1][3] = "--";
            infoGrid->Cells[a+1][4] = "--";
            infoGrid->Cells[a+1][5] = "--";
            infoGrid->Cells[a+1][6] = "--";
            infoGrid->Cells[a+1][7] = "--";
            infoGrid->Cells[a+1][8] = "--";
        }
    } 

    // calculate statistics for mean average (average over the whole of the selected series)
    int p = int(meanPeriod / (chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) 
        * meanAverage.size());
    if ((meanPeriod > 0.0) && (p < int(meanAverage.size()))) {
        double mnPos = minima[0][0];
        double mxPos = maxima[0][0];
        double mn = cnt[0][mnPos] == 0 ? 0 : sum[0][mnPos] / cnt[0][mnPos];
        double mx = cnt[0][mxPos] == 0 ? 0 : sum[0][mxPos] / cnt[0][mxPos];
        for (size_t a = 1; a < sum.size(); ++a) {
            mnPos += minima[a][0];
            mxPos += maxima[a][0];
            mn += cnt[a][minima[a][0]] == 0 ? 0 : sum[a][minima[a][0]] / cnt[a][minima[a][0]], 
            mx += cnt[a][maxima[a][0]] == 0 ? 0 : sum[a][maxima[a][0]] / cnt[a][maxima[a][0]];
        }
        mnPos /= sum.size();
        mxPos /= sum.size();
        mn /= sum.size();
        mx /= sum.size();
           
        meanExtremeValue = mn * valMultiply;
        meanExtremePosition = chart->BottomAxis->Minimum + mnPos * (chart->BottomAxis->Maximum - 
            chart->BottomAxis->Minimum) / (sum[0].size()-1);
        meanExtremePositionStdDev = sqr(minima[0][0] - mnPos);
        for (size_t a = 1; a < sum.size(); ++a)
            meanExtremePositionStdDev += sqr(minima[a][0] - mnPos);
        meanExtremePositionStdDev = sum.size() > 1 ?
            sqrt(meanExtremePositionStdDev / (sum.size()-1)) * 
            (chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) / (sum[0].size()-1)
            : 0;
        meanPP = (mx - mn) * valMultiply;
    } else {
        meanPP = -1;
        meanExtremePosition = -1;
        meanExtremePositionStdDev = -1;
    }

    // draw minima & maxima series
    TPointSeries *mSeries = new TPointSeries(chart);      
    mSeries->ParentChart = chart;
    mSeries->Pointer->Style = psDiagCross;
    mSeries->Pointer->Pen->Width = 2;
    mSeries->Pointer->Pen->Color = clBlack;
    mSeries->Title = "Ekstremi";
    mSeries->VertAxis = aLeftAxis;
    for (size_t a = 0; a < minima.size(); ++a) {
        for (size_t j = 0; j < minima[a].size(); ++j) {
            int m = minima[a][j];
            mSeries->AddXY(chart->BottomAxis->Minimum + m * 
                (chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) / (sum[a].size()-1),
                sum[a][m] / cnt[a][m], "", clBlack);
        }
    } 

    for (size_t a = 0; a < maxima.size(); ++a) {
        for (size_t j = 0; j < maxima[a].size(); ++j) {
            int m = maxima[a][j];
            mSeries->AddXY(chart->BottomAxis->Minimum + m *  
                (chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) / (sum[a].size()-1),
                sum[a][m] / cnt[a][m], "", clBlack);
        }
    }

    infoGrid->Cells[0][1] = "od";
    infoGrid->Cells[infoGrid->ColCount-1][1] = "ms";
    infoGrid->Cells[0][2] = "do";
    infoGrid->Cells[infoGrid->ColCount-1][2] = "ms";
    infoGrid->Cells[0][3] = "min";
	infoGrid->Cells[0][4] = "min cas";
	infoGrid->Cells[0][5] = "max";
    infoGrid->Cells[0][6] = "max cas";
    infoGrid->Cells[0][7] = "p-p";
    infoGrid->Cells[0][8] = "perioda";
}

//---------------------------------------------------------------------------
void __fastcall TAveragerForm::eventChanNumChange(TObject *Sender)
{
    eventChannelSettingsCheck();
    setEventNums(true);
    redrawGraph();
}
//---------------------------------------------------------------------------
void __fastcall TAveragerForm::drawChanNumChange(TObject *Sender)
{
    drawChannelSettingsCheck();
    redrawGraph();
}
//---------------------------------------------------------------------------
void __fastcall TAveragerForm::drawRange1Change(TObject *Sender)
{
    drawChannelSettingsCheck();
    redrawGraph();    
}
//---------------------------------------------------------------------------
void __fastcall TAveragerForm::averageEnabledClick(TObject *Sender)
{
    clipboardButton->Enabled = averageEnabled->Enabled;
    redrawGraph();  
}
//---------------------------------------------------------------------------
void __fastcall TAveragerForm::radioButtonClick(TObject *Sender)
{
    drawChannelSettingsCheck();
    redrawGraph();
}
//---------------------------------------------------------------------------
void __fastcall TAveragerForm::startEventChange(TObject *Sender)
{                                                                   
    if (endEventUpDown->Position < startEventUpDown->Position)
        endEventUpDown->Position = startEventUpDown->Position;

    protectNums = true;
    startEventNum = startEventUpDown->Position;
    eventChannelSettingsCheck();
    setEventTimes();
    protectNums = false;
}
//---------------------------------------------------------------------------
void __fastcall TAveragerForm::endEventChange(TObject *Sender)
{                                                                 
    if (startEventUpDown->Position > endEventUpDown->Position)
        startEventUpDown->Position = endEventUpDown->Position;
        
    protectNums = true;
    endEventNum = endEventUpDown->Position;
    eventChannelSettingsCheck();
    setEventTimes();
    protectNums = false;
}
//---------------------------------------------------------------------------
void __fastcall TAveragerForm::FormResize(TObject *Sender)
{
    chart->Width = std::max(50, ClientWidth - chart->Left - 5);
    chart->Height = std::max(50, ClientHeight - chart->Top - 5);
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::FormShow(TObject *Sender)
{
    redrawGraph();
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::averageWindowingTrackBarChange(
      TObject *Sender)
{
    redrawGraph();
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::lineDrawCheckBoxClick(TObject *Sender)
{
    redrawGraph();
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::startEventTimeChange(TObject *Sender)
{
    startTime = std::max(0.0, LibCore::myAtof(startEventTime->Text.c_str()));
    protectTimes = true;
    eventChannelSettingsCheck();
    setEventNums();
    redrawGraph(); 
    protectTimes = false;
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::endEventTimeChange(TObject *Sender)
{
    endTime = std::max(0.0, LibCore::myAtof(endEventTime->Text.c_str()));
    protectTimes = true;
    eventChannelSettingsCheck();
    setEventNums();
    redrawGraph(); 
    protectTimes = false;
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::infoGridDrawCell(TObject *Sender,
      int ACol, int ARow, TRect &Rect, TGridDrawState State)
{
    if ((ACol > 0) && (ACol < infoGrid->ColCount - 1) && (ARow == 0)) {
		TColor oldColor = infoGrid->Canvas->Brush->Color;
		infoGrid->Canvas->Brush->Color = color[ACol - 1];
		infoGrid->Canvas->Rectangle(Rect);
		infoGrid->Canvas->Brush->Color = oldColor;
	}
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::SaveGraphButtonClick(TObject *Sender)
{
    if (saveDialogGraph->Execute()) {
        bool wasInvis = false;
        if (!Visible) {
            Show();
            wasInvis = true;
        }
    	chart->Width = 1000;
	    chart->Height = 700;
    	chart->Color = clWhite;
        chart->Foot->Text->Text = filename;
    
        if (saveDialogGraph->Options.Contains(ofExtensionDifferent))
            chart->SaveToMetafileEnh(saveDialogGraph->FileName + ".wmf");
        else
            chart->SaveToMetafileEnh(saveDialogGraph->FileName);
                                 
    	chart->Color = clBtnFace; 
        chart->Foot->Text->Text = "";
        if (wasInvis)
            Hide();
	}
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::saveTextButtonClick(TObject *Sender)
{
    if (saveDialogText->Execute()) {
        if (saveDialogText->Options.Contains(ofExtensionDifferent))
            saveAsText(saveDialogText->FileName + ".txt");
        else
            saveAsText(saveDialogText->FileName);
	}
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::autoYScaleCheckClick(TObject *Sender)
{   
    if (autoYScaleCheck->Checked) {
        chart->LeftAxis->Automatic = true;
    } else {                              
        chart->LeftAxis->Automatic = false;
        double mid;
        if (radioButtonMeasurement->Checked) {
            LibCore::CChannel& drawChan = (*measurement)[drawChanUpDown->Position - 1];
            mid = (drawChan.getMaxValue() + drawChan.getMinValue()) * 0.5;
        } else {
            LibCore::CEventChannel& drawChan = measurement->getEventChannel(drawChanUpDown->Position - 1);
            mid = (drawChan.getMaxValue() + drawChan.getMinValue()) * 0.5;
        }
        chart->LeftAxis->Minimum = mid - double(yScaleUpDown->Position) * 0.001;
        chart->LeftAxis->Maximum = mid + double(yScaleUpDown->Position) * 0.001;
    }
}
//---------------------------------------------------------------------------


void __fastcall TAveragerForm::clipboardButtonClick(TObject *Sender)
{
	wchar_t* wbuf;
	AnsiString buffer;
	if (averageEnabled->Checked) {
		buffer = buffer + AnsiString(meanExtremeValue) + "\t" +
		AnsiString(meanExtremePosition) + "\t" +
		AnsiString(meanExtremePositionStdDev) + "\t" +
		AnsiString(meanPeriod) + "\t" +
		AnsiString(meanPP);

		wbuf = (wchar_t*) buffer.c_str();
		Clipboard()->SetTextBuf(wbuf);
	} else
		Clipboard()->SetTextBuf(L"");
}
//---------------------------------------------------------------------------

void __fastcall TAveragerForm::checkBoxAutoResampleClick(TObject *Sender)
{
    redrawGraph();
}
//---------------------------------------------------------------------------


void __fastcall TAveragerForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

