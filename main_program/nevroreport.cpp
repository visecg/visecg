#include "nevroreport.h"

#include <sstream>
#include <iostream>

#include "base64.h"
#include "gitVersion.h"

#include <System.SysUtils.hpp> // get Date...

/*
This file is the reason for 1.2.23 release being late. and should be (as the rest of the code dealing with PDF) rewritten completly
It is a flaming piece of garbage, and is also the reason why header file and "interface" are the way they are.

This comment is a personal rant, but also a WARNING when changing this file!

Be aware, people changing this file in the future when fighting Borland C++ compiler in some weird legacy mode:

 - technicaly it should support C++11, however not the parts of the language or parts of std library that actually matters
   - std::string::to_string and similar helpful functions are not relevant
 - compiler has no idea how to do string + &&string, and it gives you no helpful error messages (error message throws into std lib impl)
   - so avoid doing bar = "foo" + std::string("bar");
   - also, "foo" + string_variable + "bar" sometimes works, sometimes it cannot concat "string" and "char*"
 - currently we have not included "locale" and "wstring" headers, that might be neccessary when dealing with UnicodeString, AnsiString and Utf8String magic
 - DON'T DO TEMPLATES, because debugging and compiler err/warnings make no sense
 - use streams to convert doubles and integers to std::string, since it seems to produce expected sprintf-like results
 - if you are tempted to use "const" for temporaries in functions, DON'T, since compiler apperently has no idea how to concat/forward them to functions via reference

*/

namespace nevroreport{

std::string create_nevroreport_node(std::string const &data, UnicodeString const &date, UnicodeString const &author){
	std::string returnValue;

	std::string uauth(System::UTF8Encode(author).c_str());
	std::string udate(System::UTF8Encode(date).c_str());

	//header
	returnValue = "<nevroreport ";
	returnValue += "version=\"0.5\" date=\"";
	returnValue += base64_encode(udate.c_str(), udate.size());
	returnValue += "\" ";
	returnValue += "author=\"";
	returnValue += base64_encode(uauth.c_str(), uauth.size());
	returnValue += "\" ";
	returnValue += "VisEcgVersion=\"";
	returnValue += __GITVERSION__;
	returnValue += "\" ";
	returnValue += ">\n\n";

	//put data in
	returnValue += data;

	//end
	returnValue += "</nevroreport>\n";

	return returnValue;
}

std::string create_node(std::string const &node_name, std::string const &data, bool const toBase64){
	std::string returnValue;

	returnValue += "<";
	returnValue += node_name;
	returnValue += ">";

	if(toBase64){
		std::string val = base64_encode(data.c_str(), data.size());
		returnValue += val;
	}else{
		returnValue += data;
	}
	returnValue += "</";
	returnValue += node_name;
	returnValue += ">";
	returnValue += "\n";

    return returnValue;
}

void ReportPrinter::print_to_file(FILE* file, UnicodeString const &author){
	// it feels like this should not be so hard, ....
	System::String t_date = Date().CurrentDate().FormatString("d mmm yyyy");

	std::string file_content = create_nevroreport_node(this->report_text, t_date, author);

    //output to file C-style
	fprintf(file, "%s", file_content.c_str());
}

void ReportPrinter::add_root_node(std::string const  &node_name, std::string const &data, bool const toBase64){
	this->report_text += create_node(node_name, data, toBase64);
	this->report_text += "\n";
}

void ReportPrinter::add_root_node_borland(std::string const  &node_name, UnicodeString const &text, bool const toBase64){
	std::string utf8String(System::UTF8Encode(text).c_str());

	this->report_text += create_node(node_name, utf8String, toBase64);
	this->report_text += "\n";
}

std::string create_node_borland(std::string const  &node_name, UnicodeString const &text, bool const toBase64){
	std::string utf8String(System::UTF8Encode(text).c_str());
	return create_node(node_name, utf8String, toBase64);
}

std::string create_node_d(std::string const  &node_name, double const data, bool const toBase64){
	std::ostringstream oss;
    oss << data;
	std::string val = oss.str();
	return create_node(node_name, val, toBase64);
}

}

