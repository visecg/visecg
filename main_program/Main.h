// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------
#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
/*
#include <vcl\sysutils.hpp>
#include <vcl\windows.hpp>
#include <vcl\messages.hpp>
#include <vcl\sysutils.hpp>
#include <vcl\classes.hpp>
#include <vcl\graphics.hpp>
#include <vcl\controls.hpp>
#include <vcl\forms.hpp>
#include <vcl\dialogs.hpp>
#include <vcl\stdctrls.hpp>
#include <vcl\buttons.hpp>
#include <vcl\extctrls.hpp>
#include <vcl\menus.hpp>
*/
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <Dialogs.hpp>
#include <Menus.hpp>

#include <vector>
#include <string>

#include "docForm.h"
#include "simpleDocForm.h"
#include "folViewForm.h"
#include "gnugettext.hpp"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:   
	TMainMenu *MainMenu;
	TMenuItem *FileNewItem;
	TMenuItem *FileOpenItem;
	TMenuItem *FileSaveItem;
	TMenuItem *FileSaveAsItem;
	TMenuItem *FilePrintItem;
	TMenuItem *FilePrintSetupItem;
	TMenuItem *FileExitItem;
	TMenuItem *EditCutStart;
	TMenuItem *EditCutMiddle;
	TMenuItem *EditCutEnd;
	TMenuItem *WindowTileItem;
	TMenuItem *WindowCascadeItem;
	TMenuItem *WindowArrangeItem;
	TMenuItem *HelpAboutItem;
	TStatusBar *StatusLine;
	TOpenDialog *OpenDialog;
	TSaveDialog *SaveDialog;
	TPrintDialog *PrintDialog;
	TPrinterSetupDialog *PrintSetupDialog;
	TMenuItem *N5;
	TMenuItem *MeasurementData1;
	TMenuItem *Analize1;
//	TMenuItem *FindPeaksMenu;
	TMenuItem *EditEventsMenu;
	TMenuItem *Filtriranje1;
	TMenuItem *Baznalinija1;
	TMenuItem *Obraanjesignala1;
	TMenuItem *BRRRas1;
	TMenuItem *Interpolacija1;
	TMenuItem *AM1;
	TMenuItem *Statistika1;
	TMenuItem *EditCropToSelection;
	TMenuItem *FileExportText;
	TMenuItem *FileExportTextAll;
	TMenuItem *FileExportTextSelected;
	TSaveDialog *ExportTextDialog;
	TMenuItem *Standardnameritev1;
	TMenuItem *Mirovanje1;
	TMenuItem *Globokodihanje1;
	TMenuItem *Valsalva1;
	TMenuItem *Omoitevobraza1;
	TMenuItem *Omoitevroke1;
	TMenuItem *Stiskpesti1;
	TMenuItem *Ortostatskitest1;
	TMenuItem *Nagibnamiza1;
	TMenuItem *N7;
	TMenuItem *Dogodkifrekvenca1;
	TMenuItem *N8;
	TMenuItem *Odstranidogodek1;
	TMenuItem *Vstavidogodek1;
	TMenuItem *Premaknidogodek1;
	TMenuItem *Nadomestizekvidistantnimidogodki1;
	TMenuItem *N1dogodek1;
	TMenuItem *N2dogodka1;
	TMenuItem *N3dogodke1;
	TMenuItem *N0dogodkov1;
	TMenuItem *N9;
	TMenuItem *EditMeasurementMenu;
	TMenuItem *N4dogodki1;
	TMenuItem *Odstrani1;
	TMenuItem *Obdelavadogodkov1;
	TMenuItem *N6;
	TMenuItem *DiskretnaFourirovatransf1;
	TMenuItem *Koherenca1;
	TMenuItem *Prevzorenje1;
	TMenuItem *HelpPrirocnik;
	TMenuItem *N10;
	TMenuItem *Razveljavi1;
	TMenuItem *Zamikkanala1;
	TMenuItem *Brisanjekanala1;
	TMenuItem *Podvajanjekanala1;
	TMenuItem *N5dogodkov1;
	TMenuItem *N6dogodkov1;
	TMenuItem *N7dogodkov1;
	TMenuItem *N8dogodkov1;
	TMenuItem *N9dogodkov1;
	TMenuItem *N2;
	TMenuItem *Odtevanjepovprenevrednostidogkanala1;
	TMenuItem *Linearnidetrenddogkanala1;
	TMenuItem *Dogodkovneintervaleppi1;
	TOpenDialog *ImportTextDialog;
    TMenuItem *maxmin1;
	TMenuItem *N12;
	TMenuItem *N13;
	TMenuItem *OdstraniDogodke;
	TMenuItem *N16;
    TMenuItem *DodajDogodekTV;
    TMenuItem *IzvozVSliko;
    TSaveDialog *ExportWmfDialog;
    TMenuItem *PrevzorcenjeSignala;
    TMenuItem *Odvod1;
    TMenuItem *casovnaanaliza1;
    TMenuItem *N21;
    TMenuItem *Odtejpovprejedelautripa1;
    TMenuItem *Linearizirajodsekutripa1;
    TMenuItem *Pritevanjekanala1;
    TMenuItem *Mnoenjekanalaskonstanto1;
    TMenuItem *MobECGdogodki3;
    TMenuItem *N11;
    TMenuItem *N14;
    TMenuItem *Odpripregledmape1;
    TMenuItem *Doloanjeutripovnadmapo1;
    TMenuItem *N15;
    TMenuItem *Nastavitvepregledadatotek1;
    TMenuItem *Absolutniodvod1;
    TMenuItem *Absolutnavrednostkanala1;
    TMenuItem *Zamikanjakanalaskonstanto1;
    TMenuItem *IzvozivExcel1;
    TMenuItem *Okenskofiltriranjesignala1;
    TMenuItem *OkenskoFiltriranjeStriktno;
    TMenuItem *Statistikakanalamedkurzorjema1;
    TMenuItem *Doloanjeasovutripov1;
    TMenuItem *Doloanjeasovutripovver21;
    TMenuItem *Odpripregledmape2;
	TMenuItem *N17;
	TMenuItem *Heartbeats1;
	TMenuItem *Replacewithequidistanteventsaverage1;
	TMenuItem *Adaptivenoderivative1;
	TMenuItem *ESTING1;
	TMenuItem *N18;
	TMenuItem *Fixheartbeatfaults1;
	TMenuItem *Addannotationchannel1;
	TMenuItem *N19;
	TMenuItem *FCOREl1;
	TMenuItem *FX1;
	TMenuItem *AF1;
	TMenuItem *FRMS1;
	TMenuItem *FZ1;
	TMenuItem *SimpleView1;

	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ShowHint(TObject *Sender);
	void __fastcall FileNew(TObject *Sender);
	void __fastcall FileOpen(TObject *Sender);
	void __fastcall FileSave(TObject *Sender);
	void __fastcall FileSaveAs(TObject *Sender);
	void __fastcall FilePrint(TObject *Sender);
	void __fastcall FilePrintSetup(TObject *Sender);
	void __fastcall FileExit(TObject *Sender);
	void __fastcall WindowTile(TObject *Sender);
	void __fastcall WindowCascade(TObject *Sender);
	void __fastcall WindowArrange(TObject *Sender);
	void __fastcall MeasurementData1Click(TObject *Sender);
	void __fastcall Filtriranje1Click(TObject *Sender);
	void __fastcall Baznalinija1Click(TObject *Sender);
	void __fastcall Obraanjesignala1Click(TObject *Sender);
	void __fastcall BRRRas1Click(TObject *Sender);
	void __fastcall Interpolacija1Click(TObject *Sender);
	void __fastcall AM1Click(TObject *Sender);
	void __fastcall AF1Click(TObject *Sender);
	void __fastcall FX1Click(TObject *Sender);
	void __fastcall FCOREL1Click(TObject *Sender);
	void __fastcall FRMS1Click(TObject *Sender);
	void __fastcall Statistika1Click(TObject *Sender);
	void __fastcall FZ1Click(TObject *Sender);
	void __fastcall FFT1Click(TObject *Sender);
	void __fastcall EditCutStartClick(TObject *Sender);
	void __fastcall EditCutMiddleClick(TObject *Sender);
	void __fastcall EditCutEndClick(TObject *Sender);
	void __fastcall EditCropToSelectionClick(TObject *Sender);
	void __fastcall FileExportTextAllClick(TObject *Sender);
	void __fastcall FileExportTextSelectedClick(TObject *Sender);
	void __fastcall Mirovanje1Click(TObject *Sender);
	void __fastcall Globokodihanje1Click(TObject *Sender);
	void __fastcall Valsalva1Click(TObject *Sender);
	void __fastcall Omoitevobraza1Click(TObject *Sender);
	void __fastcall Omoitevroke1Click(TObject *Sender);
	void __fastcall Stiskpesti1Click(TObject *Sender);
    void __fastcall maxmin1Click(TObject *Sender);
	void __fastcall Ortostatskitest1Click(TObject *Sender);
	void __fastcall Nagibnamiza1Click(TObject *Sender);
	void __fastcall MainMenuClick(TObject *Sender);
	void __fastcall Dogodkifrekvenca1Click(TObject *Sender);
	void __fastcall Odstranidogodek1Click(TObject *Sender);
	void __fastcall Vstavidogodek1Click(TObject *Sender);
	void __fastcall Premaknidogodek1Click(TObject *Sender);
	void __fastcall N0dogodkov1Click(TObject *Sender);
	void __fastcall N1dogodek1Click(TObject *Sender);
	void __fastcall N2dogodka1Click(TObject *Sender);
	void __fastcall N3dogodke1Click(TObject *Sender);
	void __fastcall N4dogodki1Click(TObject *Sender);
	void __fastcall Odstrani1Click(TObject *Sender);
	void __fastcall Koherenca1Click(TObject *Sender);
	void __fastcall sequentialBRSClick(TObject *Sender);
	void __fastcall HelpAboutItemClick(TObject *Sender);
	void __fastcall HelpPrirocnikClick(TObject *Sender);
	void __fastcall HelpOpisClick(TObject *Sender);
	void __fastcall Prevzorenje1Click(TObject *Sender);
	void __fastcall Razveljavi1Click(TObject *Sender);
	void __fastcall DiskretnaFourirovatransf1Click(TObject *Sender);
	void __fastcall Sistolinitlak1Click(TObject *Sender);
	void __fastcall Zamikkanala1Click(TObject *Sender);
	void __fastcall Brisanjekanala1Click(TObject *Sender);
	void __fastcall Podvajanjekanala1Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall N5dogodkov1Click(TObject *Sender);
	void __fastcall N6dogodkov1Click(TObject *Sender);
	void __fastcall N7dogodkov1Click(TObject *Sender);
	void __fastcall N8dogodkov1Click(TObject *Sender);
	void __fastcall N9dogodkov1Click(TObject *Sender);
	void __fastcall Linearnidetrenddogkanala1Click(TObject *Sender);
	void __fastcall Odtevanjepovprenevrednostidogkanala1Click(
          TObject *Sender);
	void __fastcall RT1Click(TObject *Sender);
	void __fastcall Dogodkovneintervaleppi1Click(TObject *Sender);
    void __fastcall correlationxBRSClick(TObject *Sender);
    void __fastcall sinhronizedrBRSClick(TObject *Sender);
    void __fastcall frequencyfBRSClick(TObject *Sender);
	void __fastcall IzvozivExcel1Click(TObject *Sender);
	void __fastcall Pripravimeritevnaizvoz1Click(TObject *Sender);
	void __fastcall BRS1Click(TObject *Sender);
	void __fastcall Finapresdogodki1Click(TObject *Sender);
	void __fastcall Analiza1Click(TObject *Sender);
	void __fastcall OdstraniDogodkeClick(TObject *Sender);
	void __fastcall NekgDogodki1Click(TObject *Sender);
	void __fastcall Splonodoloanjedogodkov1Click(TObject *Sender);
	void __fastcall Nariiinshranigrafkoherence1Click(TObject *Sender);
    void __fastcall Kontroladihanja1Click(TObject *Sender);
    void __fastcall DodajDogodekTVClick(TObject *Sender);
    void __fastcall IzvozVSlikoClick(TObject *Sender);
    void __fastcall PovprecenjeTlakaClick(TObject *Sender);
    void __fastcall Izraunajsrednitlak1Click(TObject *Sender);
    void __fastcall PrevzorcenjeSignalaClick(TObject *Sender);
    void __fastcall Odvod1Click(TObject *Sender);
    void __fastcall casovnaanaliza1Click(TObject *Sender);
    void __fastcall ComVisexport1Click(TObject *Sender);
    void __fastcall Tval1Click(TObject *Sender);
    void __fastcall Vstavinakljuendogodkovnikanal1Click(TObject *Sender);
    void __fastcall Odtejpovprejedelautripa1Click(TObject *Sender);
    void __fastcall Linearizirajodsekutripa1Click(TObject *Sender);
    void __fastcall Pritevanjekanala1Click(TObject *Sender);
    void __fastcall Mnoenjekanalaskonstanto1Click(TObject *Sender);
    void __fastcall Calc12LeadClick(TObject *Sender);
    void __fastcall N12kanalniprikaz1Click(TObject *Sender);
    void __fastcall MobECGdogodki1newClick(TObject *Sender);
    void __fastcall OdpripregledmapeForceRescanClick(TObject *Sender);
    void __fastcall Doloanjeutripovnadmapo1Click(TObject *Sender);
    void __fastcall Nastavitvepregledadatotek1Click(TObject *Sender);
    void __fastcall Absolutniodvod1Click(TObject *Sender);
    void __fastcall Absolutnavrednostkanala1Click(TObject *Sender);
    void __fastcall Zamikanjakanalaskonstanto1Click(TObject *Sender);
    void __fastcall Okenskofiltriranjesignala1Click(TObject *Sender);
    void __fastcall OkenskoFiltriranjeStriktnoClick(TObject *Sender);
    void __fastcall Statistikakanalamedkurzorjema1Click(TObject *Sender);
    void __fastcall Doloanjeasovutripov1Click(TObject *Sender);
    void __fastcall MobECGdogodki3Click(TObject *Sender);
    void __fastcall Doloanjeasovutripovver21Click(TObject *Sender);
    void __fastcall Odpripregledmape1Click(TObject *Sender);
	void __fastcall Replacewithequidistanteventsaverage1Click(TObject *Sender);
	void __fastcall Adaptivenoderivative1Click(TObject *Sender);
	void __fastcall ESTING1Click(TObject *Sender);
	void __fastcall Fixheartbeatfaults1Click(TObject *Sender);
	void __fastcall Addannotationchannel1Click(TObject *Sender);
	void __fastcall SimpleView1Click(TObject *Sender);
	void __fastcall SimpleView1DrawItem(TObject *Sender, TCanvas *ACanvas, TRect &ARect,
          bool Selected);
private:        // private user declarations
	void refreshMenuItemStates(TMenuItem *item, bool docOpen, bool docEditable, bool eventChSelected, bool undoPossible, bool cursorPresent[3]);
	bool findHROnOpenedMeasurements;
	long maxLengthConvertedFiles;
	void checkVisibleMenuItems(TMenuItem *item);

public:         // public user declarations
    bool simpleView;
	std::vector<std::string> runTimeFlags;

	virtual __fastcall TMainForm(TComponent* Owner);
	TDocumentForm *getCurrentDocument()
	{
		return dynamic_cast<TDocumentForm*>(ActiveMDIChild);
	}
	TsDocForm *getCurrentSimpleDocument()
	{
		return dynamic_cast<TsDocForm*>(ActiveMDIChild);
	}
    TFolderViewForm *getCurrentFolderOverview()
	{
		return dynamic_cast<TFolderViewForm*>(ActiveMDIChild);
	}
    void CloseDuplicateFolderOverview(TFolderViewForm *newForm, AnsiString folder)
	{
        for(int i = 0; i< MDIChildCount; i++){
            TFolderViewForm* curr = dynamic_cast<TFolderViewForm*>(MDIChildren[i]);
            if(curr && AnsiCompareStr(curr->rootFolder, folder)==0 && curr!=newForm)
                curr->Close(); 
        }
	}
	void refreshMenuItemStates(bool docOpen, bool docEditable, bool eventChSelected, bool cursorPresent[3]);
	bool __fastcall CloseQuery();
	AnsiString autoOpenFile;
	AnsiString runScript;
	AnsiString scriptParams;
	AnsiString exeFile;
    int checkFileAlreadyOpened(UnicodeString path);
	bool runNamedScript(AnsiString scriptName, AnsiString scriptParams);
	void openPath(UnicodeString path, bool forceRescan = false);

	virtual void __fastcall WndProc(TMessage &Msg);
};
//---------------------------------------------------------------------------
extern TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
