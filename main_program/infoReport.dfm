object ReportInfoForm: TReportInfoForm
  Left = 456
  Top = 191
  Width = 481
  Height = 391
  AutoScroll = True
  Caption = 'Report information'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 3
    Width = 31
    Height = 13
    Caption = 'Author'
  end
  object Label2: TLabel
    Left = 22
    Top = 27
    Width = 25
    Height = 13
    Caption = 'User:'
  end
  object Label3: TLabel
    Left = 28
    Top = 51
    Width = 19
    Height = 13
    Caption = 'Age'
  end
  object Label4: TLabel
    Left = 13
    Top = 75
    Width = 34
    Height = 13
    Caption = 'Weight'
  end
  object Label5: TLabel
    Left = 29
    Top = 99
    Width = 18
    Height = 13
    Caption = 'Sex'
  end
  object Label6: TLabel
    Left = 2
    Top = 123
    Width = 45
    Height = 13
    Caption = 'Birth date'
  end
  object Label7: TLabel
    Left = 0
    Top = 155
    Width = 120
    Height = 13
    Caption = 'Show first measurements:'
  end
  object Label8: TLabel
    Left = 0
    Top = 177
    Width = 84
    Height = 13
    Caption = 'Show first events:'
  end
  object Label9: TLabel
    Left = 200
    Top = 3
    Width = 163
    Height = 15
    AutoSize = False
    Caption = 'Additional comments about report: '
  end
  object Label10: TLabel
    Left = 233
    Top = 203
    Width = 105
    Height = 13
    Caption = 'ECG length per line [s]'
  end
  object Label11: TLabel
    Left = 241
    Top = 293
    Width = 97
    Height = 13
    Caption = 'ECG mV height [mm]'
  end
  object Label12: TLabel
    Left = 245
    Top = 263
    Width = 93
    Height = 13
    Caption = 'mV around baseline'
  end
  object Label13: TLabel
    Left = 231
    Top = 232
    Width = 105
    Height = 13
    Caption = 'ECG time scale [cm/s]'
  end
  object Label14: TLabel
    Left = 8
    Top = 293
    Width = 113
    Height = 13
    Caption = 'BPM overview length[h]'
  end
  object Label15: TLabel
    Left = 22
    Top = 224
    Width = 166
    Height = 13
    Caption = 'including activities (ignore set limits)'
  end
  object AuthorInp: TEdit
    Left = 56
    Top = 0
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object UserInp: TEdit
    Left = 56
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object AgeInp: TEdit
    Left = 56
    Top = 48
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 2
    OnChange = Edit6Change
  end
  object WeightInp: TEdit
    Left = 56
    Top = 72
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 3
    OnChange = Edit6Change
  end
  object BitBtn1: TBitBtn
    Left = 45
    Top = 327
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 15
  end
  object BitBtn2: TBitBtn
    Left = 352
    Top = 327
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 14
  end
  object GenderInp: TEdit
    Left = 56
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object BirthInp: TEdit
    Left = 56
    Top = 120
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object MeasurementNumberInput: TEdit
    Left = 126
    Top = 147
    Width = 51
    Height = 21
    NumbersOnly = True
    TabOrder = 7
    OnChange = Edit6Change
  end
  object EventNumberInput: TEdit
    Left = 127
    Top = 174
    Width = 51
    Height = 21
    NumbersOnly = True
    TabOrder = 8
    OnChange = Edit6Change
  end
  object Edit2: TEdit
    Left = 344
    Top = 200
    Width = 121
    Height = 21
    TabOrder = 10
    Text = 'Edit2'
    OnChange = Edit6Change
  end
  object Edit3: TEdit
    Left = 344
    Top = 230
    Width = 121
    Height = 21
    TabOrder = 11
    Text = 'Edit3'
    OnChange = Edit6Change
  end
  object Edit4: TEdit
    Left = 344
    Top = 257
    Width = 121
    Height = 21
    TabOrder = 12
    Text = 'Edit4'
    OnChange = Edit6Change
  end
  object Edit5: TEdit
    Left = 344
    Top = 290
    Width = 121
    Height = 21
    TabOrder = 13
    Text = 'Edit5'
    OnChange = Edit6Change
  end
  object Edit6: TEdit
    Left = 127
    Top = 290
    Width = 50
    Height = 21
    TabOrder = 9
    Text = 'Edit6'
    OnChange = Edit6Change
  end
  object Memo1: TMemo
    Left = 200
    Top = 24
    Width = 265
    Height = 157
    TabOrder = 6
  end
  object reportActivityCheckbox: TCheckBox
    Left = 8
    Top = 201
    Width = 201
    Height = 17
    Caption = 'Report all measurements and events'
    TabOrder = 16
  end
end
