// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------

#include <vcl.h>
#include <utilcls.h>
#pragma hdrstop

#include "excelexporter.h"
#include "base.h"
#include "protocol.h"
#include "gnugettext.hpp"

namespace LibStdMeasurement
{

const double IExcelExporter::cGraphFrequency = 1.0;

const AnsiString IExcelExporter::cBeatrateChannelName = "BR XLS";
const AnsiString IExcelExporter::cSystChannelName = "SBP XLS";
const AnsiString IExcelExporter::cDiastChannelName = "DBP XLS";

using namespace LibCore;

StdMeasurementType findMeasurementType(AnsiString measurementName)
{
	for (StdMeasurementType type = StdMeasurementType(stdmEnumStart+1); type < stdmEnumEnd; type = StdMeasurementType(type+1))
		if (type != stdmNonstandard && measurementName.Pos(getMeasurementProps(type).measurementNamePrefix) == 1)
			return type;
	return stdmNonstandard;
}

IExcelExporter *createSpecificExcelExporter(CMeasurement &measurement)
{
	StdMeasurementType mType = findMeasurementType(measurement.getMeasurementName());
	switch (mType)
	{
	case stdmMirovanje:
		return new ExcelExporterMirovanje(measurement);
	case stdmNonstandard:
		Application->MessageBox(gettext("Not possible export of non-standard measurement.").data(), gettext("Error").data(), MB_OK);
		break;
	default:
		Application->MessageBox(gettext("Not implemented export type.").data(), gettext("Error").data(), MB_OK);
		break;
	}
	return NULL;
}

struct Coords
{
	int row, col;
	Coords(int _row, int _col)
	{
		if ( !(_row >= 1 && _row <= 65536) || !(_col >= 1 && _col <= 26*27) )
			throw new EKGException(__LINE__, "Coords::Coords: invalid coordinates.");
		row = _row;
		col = _col;
	}
	Coords(AnsiString excelCoords) //converts "C7" to (3,7)
	{
		char col1, col2;
		if (isalpha(excelCoords[2]))
		{
			if (sscanf(excelCoords.c_str(), "%c%c%d", &col1, &col2, &row) != 3)
				throw new EKGException(__LINE__, "Coords::Coords: invalid coordinates.");
			col = (col1-'A'+1)*26 + (col2-'A'+1);
		} else {
			if (sscanf(excelCoords.c_str(), "%c%d", &col1, &row) != 2)
				throw new EKGException(__LINE__, "Coords::Coords: invalid coordinates.");
			col = col1-'A'+1;
		}
	}
	AnsiString cellName() //converts (3,7) to "C7"
	{
		char buf[100];
		if (col <= 26)
			sprintf(buf, "%c%d", (char)col-1+'A', row);
		else
			sprintf(buf, "%c%c%d", (char)((col-27)/26)+'A',(char)((col+25)%26)+'A', row);
		return AnsiString(buf);
	}

};

void setMultilineValue(Variant sheet, int row, int col, AnsiString newValue)
{
	//multiple-line string: put first line into cell, put all lines into comment

	while (newValue.Pos("\r")) //delete all occurences of '\r'
		newValue.Delete(newValue.Pos("\r"), 1);
	while (newValue.Length() > 0 && newValue[1] == '\n') //delete leading newlines
		newValue.Delete(1, 1);
	while (newValue.Length() > 0 && newValue[newValue.Length()] == '\n') //delete trailing newlines
		newValue.Delete(newValue.Length(), 1);

	AnsiString firstLine;
	if (newValue.Pos("\n"))
		firstLine = newValue.SubString(1, newValue.Pos("\n")-1);
	else
		firstLine = newValue;
	sheet.OlePropertyGet("Range", Coords(row, col).cellName()).OlePropertySet("Value", firstLine);
	sheet.OlePropertyGet("Range", Coords(row, col).cellName()).OlePropertyGet("Comment").OleProcedure("Text", newValue);
}

template <typename T>
void setValue(Variant sheet, int row, int col, T newValue)
{
	sheet.OlePropertyGet("Range", Coords(row, col).cellName()).OlePropertySet("Value", newValue);
}

AnsiString IExcelExporter::constructFilename()
{
	StdMeasurementType measurementType = findMeasurementType(measurement.getMeasurementName());
	StdMeasurementProps measurementProps = getMeasurementProps(measurementType);

	AnsiString ASCIIpatientName = makeStringASCII(measurement.patientData.name);
	if (ASCIIpatientName.Pos(" "))
		ASCIIpatientName = ASCIIpatientName.SubString(1, ASCIIpatientName.Pos(" ")-1);

	unsigned short mYear, mMonth, mDay, mHour, mMin, mSec, mMsec;
	unsigned short pYear, pMonth, pDay;
	int age;
	measurement.patientData.dateOfBirth.DecodeDate(&pYear, &pMonth, &pDay);
	measurement.getDate().DecodeDate(&mYear, &mMonth, &mDay);
	measurement.getDate().DecodeTime(&mHour, &mMin, &mSec, &mMsec);
	age = (mMonth > pMonth || (mMonth == pMonth && mDay >= pDay)) ? mYear - pYear : mYear - pYear - 1;

	AnsiString patientCommentPrefix = measurement.patientData.comment;
	if (patientCommentPrefix.Length() > 6)
		patientCommentPrefix = patientCommentPrefix.SubString(1, 6);

	char buf[1000];
	if (patientCommentPrefix.Length() > 0)
		sprintf(buf, "%s %s %d %c - %s.xls", measurementProps.excelFileNamePrefix, ASCIIpatientName,
						age, measurement.patientData.SexToChar(measurement.patientData.sex),
						makeStringASCII(patientCommentPrefix));
	else
		sprintf(buf, "%s %s %d %c.xls", measurementProps.excelFileNamePrefix, ASCIIpatientName,
						age, measurement.patientData.SexToChar(measurement.patientData.sex));

	return AnsiString(buf);
}

void IExcelExporter::exportData(AnsiString templateFolder, AnsiString destinationFolder)
{
/*
	if (!checkMeasurementSpecificPreparation())
		return;

	StdMeasurementType measurementType = findMeasurementType(measurement.getMeasurementName());
	StdMeasurementProps measurementProps = getMeasurementProps(measurementType);
	Variant excelApplication, destinationWbook, destinationSheet;
	bool existingExcel;

	//check if destination file exists
	AnsiString filename = destinationFolder + "\\" + constructFilename();
	FILE *f = fopen(filename.c_str(), "rb");
	if (f)
	{
		//exists: fail if already open
		fclose(f);
		if (RenameFile(filename, filename + ".krneki"))
			RenameFile(filename + ".krneki", filename);
		else {
			Application->MessageBox(gettext("The file is already open - close it first!").data(), gettext("Error").data(), MB_OK);
			return;
		}
	} else {
		//does not exist: copy template to new file
		AnsiString templateFilename = templateFolder + "\\" + measurementProps.excelFileNamePrefix + "-priimek-leta-spol.xls";
		if (!CopyFile(templateFilename.c_str(), filename.c_str(), FALSE))
		{
			Application->MessageBox(gettext("Templates can not be copied.").data(), gettext("Error").data(), MB_OK);
			return;
		}
	}

	//connect to existing or new excel application
	Variant XL;
	try {
		XL = XL.GetActiveObject("Excel.Application");
		existingExcel = true;
	} catch (EOleSysError *e) {
		XL = XL.CreateObject("Excel.Application");
		existingExcel = false;
	}

	//open file & show it
	Variant wbook = XL.OlePropertyGet("Workbooks").OleFunction("Open", filename);
	XL.OlePropertySet("Visible", TRUE);

	//export patient data
	if (!exportPatientData(wbook))
	{
		wbook.OleProcedure("Close");
		if (!existingExcel)
			XL.OleProcedure("Quit");
		return;
	}

	//switch to measurement specific sheet & export data
	Variant sheet;
	try {
		sheet = wbook.OlePropertyGet("Worksheets").OlePropertyGet("Item", measurementProps.excelSheetName);
		sheet.OleProcedure("Select");
	} catch (EOleSysError *e) {
		Application->MessageBox("The file does not contain exact worksheet.", "Error", MB_OK);
		wbook.OleProcedure("Close");
		if (!existingExcel)
			XL.OleProcedure("Quit");
		return;
	}
	sheet.OlePropertySet("EnableCalculation", FALSE);
	if (!exportMeasurementSpecificData(sheet))
	{
		wbook.OleProcedure("Close");
		if (!existingExcel)
			XL.OleProcedure("Quit");
		return;
	}
	sheet.OlePropertySet("EnableCalculation", TRUE);

	//save
	wbook.OleProcedure("Save");
	XL.OlePropertySet("Visible", FALSE);

	//ask whether file should be closed
	bool closeFile = (Application->MessageBox(
						"Prior to next export in the same file it is necessary to close it.\nDo yo like to close?",
						"Closing file", MB_YESNO) == IDYES);
	try {
		if (closeFile)
		{
			wbook.OleProcedure("Close");
			if (!existingExcel)
				XL.OleProcedure("Quit");
			else
				XL.OlePropertySet("Visible", TRUE);
		} else
			XL.OlePropertySet("Visible", TRUE);
	} catch (EOleSysError *e) {
	}
    */
}

bool IExcelExporter::prepareForExport(bool warnAndShow) //creates any event channels (e.g., RR) needed for export
{
	int oldNumEventChannels = measurement.getNumEventChannels();
	if (!doMeasurementSpecificPreparation())
		return false;
	for (int i = oldNumEventChannels; i < measurement.getNumEventChannels(); i++)
		measurement.getEventChannel(i).appendComment("Channel created automatically during the preparation of the Excel export.", false);
	return true;		
}

bool IExcelExporter::exportPatientData(Variant wbook)
{
/*
	StdMeasurementType measurementType = findMeasurementType(measurement.getMeasurementName());
	StdMeasurementProps measurementProps = getMeasurementProps(measurementType);

	//check if required sheet exists
	Variant sheet;
	AnsiString sheetName = (measurementProps.excelFileNamePrefix == "taz" ? "Resting" : "Tilting table");
	try {
		sheet = wbook.OlePropertyGet("Worksheets").OlePropertyGet("Item", sheetName);
		sheet.OleProcedure("Select");
	} catch (EOleSysError *e) {
		Application->MessageBox("The file does not contain the exact worksheet.", "Error", MB_OK);
		return false;
	}

	//export data
	AnsiString name, surname;
	name = surname = measurement.patientData.name;
	if (name.Pos(" "))
		name = name.SubString(name.Pos(" ")+1, name.Length() - name.Pos(" "));
	else
		name = "";
	if (surname.Pos(" "))
		surname = surname.SubString(1, surname.Pos(" ")-1);

	const int baseRow = 4, baseCol = 1;	//coordinates where string "Podati o pacientu" stands
	setValue(sheet, baseRow+1, baseCol+1, name);
	setValue(sheet, baseRow+1, baseCol+2, surname);
	setValue(sheet, baseRow+1, baseCol+3, measurement.patientData.dateOfBirth);
	setValue(sheet, baseRow+1, baseCol+4, AnsiString(measurement.patientData.SexToChar(measurement.patientData.sex)));

	setMultilineValue(sheet, baseRow+2, baseCol+1, measurement.patientData.diagnosis);
	setValue(sheet, baseRow+3, baseCol+1, measurement.getDate());
	setValue(sheet, baseRow+3, baseCol+2, measurement.getDate());
	setMultilineValue(sheet, baseRow+4, baseCol+1, measurement.patientData.comment);
	setMultilineValue(sheet, baseRow+5, baseCol+1, measurement.getComment());
*/
	return true;
}

void IExcelExporter::exportEventChStats(Variant sheet, int eventChNum, double timeFrom, double timeTo,
						int baseRow, int baseCol, double multiplyBy, bool exportSDNNandRMSSD)
{
	CEventChannel &eventCh = measurement.getEventChannel(eventChNum);
	CEventChannel::Statistics stat = eventCh.calcStatistics(timeFrom, timeTo);
	setValue(sheet, baseRow,   baseCol, stat.avgValue*multiplyBy);
	setValue(sheet, baseRow+1, baseCol, stat.maxValue*multiplyBy);
	setValue(sheet, baseRow+2, baseCol, stat.minValue*multiplyBy);
	if (exportSDNNandRMSSD)
	{
		setValue(sheet, baseRow+4, baseCol, stat.stDevValue*multiplyBy);
		setValue(sheet, baseRow+5, baseCol, stat.RMSSD*multiplyBy);
	}
}

void IExcelExporter::exportEventChSpectralAnalysis(Variant sheet, int eventChNum, double timeFrom, double timeTo,
						int baseRow, int baseCol)
{
	LibCore::CMeasurement::DFTParams DFTParams = measurement.fourierTransform(eventChNum);
	int DFTChannelNum = measurement.getNumEventChannels()-1;
	CEventChannel &DFTChannel = measurement.getEventChannel(DFTChannelNum);

	double TPSend = 0.4, VLFstart = 0.001, VLFend = 0.04, LFend = 0.15, HFend = TPSend;
	double resol = 1 / (DFTParams.end - DFTParams.start);
	double TTPS = 0, VLF = 0, LF = 0, HF = 0;
	for (int i = 0; i < DFTChannel.getNumElems(); i++)
	{
		TTPS += DFTChannel[i].value;
		if (i+1 >= VLFstart/resol && i+1 <= VLFend/resol)
			VLF += DFTChannel[i].value;
		if (i+1 > VLFend/resol && i+1 <= LFend/resol)
			LF += DFTChannel[i].value;
		if (i+1 > LFend/resol && i+1 <= HFend/resol)
			HF += DFTChannel[i].value;
	}

	setValue(sheet, baseRow  , baseCol, VLF);
	setValue(sheet, baseRow+1, baseCol, LF);
	setValue(sheet, baseRow+2, baseCol, LF/(TTPS-VLF)*100);
	setValue(sheet, baseRow+3, baseCol, HF);
	setValue(sheet, baseRow+4, baseCol, HF/(TTPS-VLF)*100);

	measurement.deleteEventChannel(DFTChannelNum);
}

void IExcelExporter::exportResampledEventChannelForGraph(Variant sheet, int eventChNum, double timeFrom, double timeTo,
						int baseRow, int baseCol)
{
	measurement.resampleEventChannel(eventChNum, cGraphFrequency);
	int resampledChannelNum = measurement.getNumEventChannels()-1;
	CEventChannel &resampledChannel = measurement.getEventChannel(resampledChannelNum);

	int numExported = 0;
	for (int i = 0; i < resampledChannel.getNumElems(); i++)
	{
		if (resampledChannel[i].time > timeTo)
			break;
		if (resampledChannel[i].time >= timeFrom)
		{
			setValue(sheet, baseRow+numExported, cTempCol, resampledChannel[i].value);
			numExported++;
		}
	}
	measurement.deleteEventChannel(resampledChannelNum);

	Variant tempRange = sheet.OlePropertyGet("Range", Coords(baseRow, cTempCol).cellName(),
														Coords(baseRow+numExported-1, cTempCol).cellName());
	Variant graphDataRange = sheet.OlePropertyGet("Range", Coords(baseRow, baseCol).cellName(),
															Coords(baseRow+numExported-1, baseCol).cellName());
	tempRange.OleProcedure("Copy", graphDataRange);
	tempRange.OleProcedure("ClearContents");
}

void IExcelExporter::exportResampledSignalChannelForGraph(Variant sheet, int signalChNum, double timeFrom, double timeTo,
						double zoom, double offset, int baseRow, int baseCol)
{
	measurement.resampleSignalChannel(signalChNum, cGraphFrequency);
	int resampledChannelNum = measurement.getNumEventChannels()-1;
	CEventChannel &resampledChannel = measurement.getEventChannel(resampledChannelNum);

	int numExported = 0;
	for (int i = 0; i < resampledChannel.getNumElems(); i++)
	{
		if (resampledChannel[i].time > timeTo)
			break;
		if (resampledChannel[i].time >= timeFrom)
		{
			setValue(sheet, baseRow+numExported, cTempCol, resampledChannel[i].value*zoom + offset);
			numExported++;
		}
	}
	measurement.deleteEventChannel(resampledChannelNum);

	Variant tempRange = sheet.OlePropertyGet("Range", Coords(baseRow, cTempCol).cellName(),
														Coords(baseRow+numExported-1, cTempCol).cellName());
	Variant graphDataRange = sheet.OlePropertyGet("Range", Coords(baseRow, baseCol).cellName(),
															Coords(baseRow+numExported-1, baseCol).cellName());
	tempRange.OleProcedure("Copy", graphDataRange);
	tempRange.OleProcedure("ClearContents");
}

bool ExcelExporterMirovanje::checkMeasurementSpecificPreparation()
{
	//check if measurement is valid
	CEventChannel &markerChannel = measurement.getEventChannel(measurement.findEventChannelByName("bookmarks"));

	mirovanjeStart = markerChannel.findEventByValue(getProtocolItem(stdmMirovanje, 0).markerEvent);
	mirovanjeEnd = markerChannel.findEventByValue(getProtocolItem(stdmMirovanje, 1).markerEvent);

	if (mirovanjeStart < 15) {
        if (mirovanjeStart < 0) {
			Application->MessageBox(gettext("Missing start.").data(), gettext("Error").data(), MB_OK);
		} else {
			Application->MessageBox(gettext("Too early start (min 15 sec).").data(), gettext("Error").data(), MB_OK);
        }
		return false;
	}
    if (mirovanjeEnd < 0) {
		Application->MessageBox(gettext("Missing end.").data(), gettext("Error").data(), MB_OK);
        return false;
    }
	if (mirovanjeEnd < mirovanjeStart + 300) {
		Application->MessageBox(gettext("Measurement is less than 5 minutes").data(), gettext("Warning").data(), MB_OK);
	}

	//check if all channels exist and none are duplicated
	int rrChannelNum = measurement.findEventChannelByName(cBeatrateChannelName),
		systChannelNum = measurement.findEventChannelByName(cSystChannelName),
		diastChannelNum = measurement.findEventChannelByName(cDiastChannelName);

	try {
		if (rrChannelNum == -1 || systChannelNum == -1 || diastChannelNum == -1)
			throw 1;
		int dupChannel;
		measurement.getEventChannel(rrChannelNum).setChannelName("temp");
		dupChannel = measurement.findEventChannelByName(cBeatrateChannelName);
		measurement.getEventChannel(rrChannelNum).setChannelName(cBeatrateChannelName);
		if (dupChannel != -1)
			throw 2;
		measurement.getEventChannel(systChannelNum).setChannelName("temp");
		dupChannel = measurement.findEventChannelByName(cSystChannelName);
		measurement.getEventChannel(systChannelNum).setChannelName(cSystChannelName);
		if (dupChannel != -1)
			throw 3;
		measurement.getEventChannel(diastChannelNum).setChannelName("temp");
		dupChannel = measurement.findEventChannelByName(cDiastChannelName);
		measurement.getEventChannel(diastChannelNum).setChannelName(cDiastChannelName);
		if (dupChannel != -1)
			throw 4;
	} catch (int exceptionNum) {
		wchar_t buf[1000];
		swprintf(buf, gettext("Missing one of the event channels ('%s', '%s' or '%s') or it is duplicated.").data(),
				cBeatrateChannelName.c_str(), cSystChannelName.c_str(), cDiastChannelName.c_str());
		Application->MessageBox(buf, gettext("Error").data(), MB_OK);
		return false;
	}
	return true;
}

bool ExcelExporterMirovanje::exportMeasurementSpecificData(Variant sheet)
{
	int beatrateChannelNum = measurement.findEventChannelByName(cBeatrateChannelName),
		systChannelNum = measurement.findEventChannelByName(cSystChannelName),
		diastChannelNum = measurement.findEventChannelByName(cDiastChannelName);

	CEventChannel &systChannel = measurement.getEventChannel(systChannelNum);
	CEventChannel &diastChannel = measurement.getEventChannel(diastChannelNum);
	int baseRow, baseCol;

	//RR stats & spectral analysis
	measurement.assignInterEventTimeAsEventValues(beatrateChannelNum);
	int rrChannelNum = measurement.getNumEventChannels()-1;
	CEventChannel &rrChannel = measurement.getEventChannel(rrChannelNum);
	exportEventChStats(sheet, rrChannelNum, mirovanjeStart, mirovanjeEnd, 13, 2, 1000, true);
	exportEventChSpectralAnalysis(sheet, rrChannelNum, mirovanjeStart, mirovanjeEnd, 20, 2);

	//RR values for each minute & graph
	baseRow = 29, baseCol = 2;
	setValue(sheet, baseRow, baseCol, rrChannel.calcStatistics(mirovanjeStart-15, mirovanjeStart).avgValue*1000);
	for (int minute = 1; minute <= 5; minute++)
		setValue(sheet, baseRow+minute, baseCol, rrChannel.calcStatistics(mirovanjeStart + minute*60-5,
														mirovanjeStart + minute*60+5).avgValue*1000);
	measurement.deleteEventChannel(rrChannelNum);
	exportResampledEventChannelForGraph(sheet, beatrateChannelNum, mirovanjeStart, mirovanjeEnd, 3, 27);

	//THE SAME FOR PRESSURE:
	//pressure stats & spectral analysis
	exportEventChStats(sheet, systChannelNum, mirovanjeStart, mirovanjeEnd, 13, 5, 1, true);
	exportEventChStats(sheet, diastChannelNum, mirovanjeStart, mirovanjeEnd, 13, 6, 1, true);
	exportEventChSpectralAnalysis(sheet, systChannelNum, mirovanjeStart, mirovanjeEnd, 20, 5);
	exportEventChSpectralAnalysis(sheet, diastChannelNum, mirovanjeStart, mirovanjeEnd, 20, 6);

	//pressure values for each minute & graph
	baseRow = 29, baseCol = 3;
	setValue(sheet, baseRow, baseCol, systChannel.calcStatistics(mirovanjeStart-15, mirovanjeStart).avgValue);
	setValue(sheet, baseRow, baseCol+1, diastChannel.calcStatistics(mirovanjeStart-15, mirovanjeStart).avgValue);
	for (int minute = 1; minute <= 5; minute++)
	{
		setValue(sheet, baseRow+minute, baseCol, systChannel.calcStatistics(mirovanjeStart + minute*60-5,
														mirovanjeStart + minute*60+5).avgValue);
		setValue(sheet, baseRow+minute, baseCol+1, diastChannel.calcStatistics(mirovanjeStart + minute*60-5,
														mirovanjeStart + minute*60+5).avgValue);
	}

	exportResampledEventChannelForGraph(sheet, systChannelNum, mirovanjeStart, mirovanjeEnd, 3, 28);
	exportResampledEventChannelForGraph(sheet, diastChannelNum, mirovanjeStart, mirovanjeEnd, 3, 29);

	//breathing for graph
	exportResampledSignalChannelForGraph(sheet, measurement.findChannelByName("breathing"),
					mirovanjeStart, mirovanjeEnd, 4, -90, 3, 30);

	return true;
}

bool ExcelExporterMirovanje::doMeasurementSpecificPreparation()
{
	//create beatrate event channel named "BR XLS" (if it doesn't exist yet)
	int beatrateChannelNum = measurement.findEventChannelByName(cBeatrateChannelName);
	if (beatrateChannelNum == -1)
	{
		measurement.calculateBeatTimes(CMeasurement::beatAlgorithmFZ, measurement.findChannelByName("EKG II"), NULL);
		measurement.assignEventFreqAsEventValues(measurement.getNumEventChannels()-1);
		measurement.deleteEventChannel(measurement.getNumEventChannels()-2);
		beatrateChannelNum = measurement.getNumEventChannels()-1;
		measurement.getEventChannel(beatrateChannelNum).setChannelName(cBeatrateChannelName);
	}

	//create pressure event channels named "SBP XLS" and "DBP XLS" (if they don't exist yet)
	int existingSystChNum = measurement.findEventChannelByName(cSystChannelName),
		existingDiastChNum = measurement.findEventChannelByName(cDiastChannelName);

	if (existingSystChNum == -1 || existingDiastChNum == -1)
	{
		measurement.createBPEventChannel(measurement.findChannelByName("pressure COLIN"), beatrateChannelNum);
		measurement.getEventChannel(measurement.getNumEventChannels()-2).setChannelName(cSystChannelName);
		measurement.getEventChannel(measurement.getNumEventChannels()-1).setChannelName(cDiastChannelName);
		if (existingSystChNum == -1 && existingDiastChNum != -1)
		{
			//diast existed before, so we delete the newly created one
			measurement.deleteEventChannel(measurement.getNumEventChannels()-1);
		} else if (existingSystChNum != -1 && existingDiastChNum == -1)
		{
			//syst existed before, so we delete the newly created one
			measurement.deleteEventChannel(measurement.getNumEventChannels()-2);
		}
	}

	return true;
}

}; //end namespace LibStdMeasurement



