﻿// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "base.h"
#include "folViewForm.h"
#include "main.h"
#include "progressForm.h"
#include "measurement.h"
#include "docForm.h"
#include "infoReport.h"
#include "eventReportSettingForm.h"
#include "simpleDocForm.h"

#include <VclTee.TeEngine.hpp>
#include <VclTee.TeeProcs.hpp>
#include <VclTee.GanttCh.hpp>
#include "gitVersion.h"

#include <string.h>

#include "ini.h"
#include "StringSupport.h"
#include <windows.h>
#include "base64.h"
#include "nevroreport.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma resource "*.dfm"

#define SECONDS_IN_A_DAY 86400.0

TFolderViewForm *FolderViewForm;
//---------------------------------------------------------------------------
__fastcall TFolderViewForm::TFolderViewForm(TComponent* Owner)
: TForm(Owner)
{
    Parent = dynamic_cast<TMainForm*>(Owner);
    horiz_scroll_enabled = true;
    horiz_scroll_multiplier = 1.0;
}
//---------------------------------------------------------------------------
void __fastcall TFolderViewForm::FormClose(TObject *Sender,
TCloseAction &Action)
{
	delete[] openedFiles;
    MainForm->StatusLine->SimpleText= "";
	Action = caFree;
}
//---------------------------------------------------------------------------

bool CompareCMeasurements(LibCore::CMeasurement & a, LibCore::CMeasurement & b)
{
	//check name
	int result = AnsiCompareStr(a.patientData.name, b.patientData.name);
	if(result != 0)
	    return (result < 0);
	//if same check mac
	result = AnsiCompareStr(a.getMeasurementName(), b.getMeasurementName());
	if(result != 0)
	    return (result < 0);
	//if same check datetime
	return a.getDate() <= b.getDate();
}

void TFolderViewForm::drawOpenedFiles(int numFiles, bool smallerBoxes ){

    //loads settings for colors
    GanttChart->RemoveAllSeries();
    Series1->Clear();
    GanttChart->AddSeries(Series1);
    GanttChart->LeftAxis->Automatic = false; 
    double acceptableStdDeviationThr = 0.15, lowBPMthr = 40, highBPMthr = 130;
	UnicodeString settingsFname("nastavitve.ini");
	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
	    if (Application->ExeName[i] == '\\') {
            settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
    		break;
        }
  	}
    Ini::File ini(settingsFname.c_str());
	ini.loadVar(lowBPMthr, L"Folder overview - lowBPM");
	ini.loadVar(highBPMthr, L"Folder overview - highBPM");
	ini.loadVar(acceptableStdDeviationThr, L"Folder overview - std dev");

    const TColor colorDeprecated = TColor(RGB(200,100,255));
    const TColor colorBlue = TColor(RGB(100,100,255));
    const TColor colorMissing = TColor(RGB(255,255,100));
    const TColor colorMissingGantt = clGrayText; //TColor(RGB(100,100,100));    


	// bubble sort opened nekg files  with indices of openedFiles
    sortedIndex= new int[numFiles];
    for (int i=0;i<numFiles;i++) sortedIndex[i] = i;
	bool sorted = false;
    int tmpInteger;
 	while(!sorted){
		sorted = true;
		for(int i=0;i<numFiles-1;i++){
			if(!CompareCMeasurements(openedFiles[sortedIndex[i]],openedFiles[sortedIndex[i+1]])){
                tmpInteger = sortedIndex[i];
                sortedIndex[i] = sortedIndex[i+1];
                sortedIndex[i+1] = tmpInteger;
				sorted = false;
			}
		}
	}


	//drawOpenedFiles in order -> y_axis++ if new patient/device/measurements overlap
	Series1->XValues->Order =  loNone; //turn sorting of series off to keep correct indices
	Series1->YValues->Order =  loNone;
	AnsiString previousPatientName = openedFiles[sortedIndex[0]].patientData.name;
	AnsiString previousMeasurementName = openedFiles[sortedIndex[0]].getMeasurementName();
	double start, stop, previousStop = 0, missing_data; // axis x
    double ay = 0, previousay = 0;        // axis y
	AnsiString ChartLbl;

	for(int j=0; j< numFiles;j++){
        int i = sortedIndex[j];
        missing_data = 0;
		if( AnsiCompareStr(openedFiles[i].patientData.name, previousPatientName) != 0 ||
				AnsiCompareStr(openedFiles[i].getMeasurementName(), previousMeasurementName) != 0){
			ay++;
			previousPatientName = openedFiles[i].patientData.name;
			previousMeasurementName = openedFiles[i].getMeasurementName();
		}
		start = (double) openedFiles[i].getDate();
        stop = 0;
		TStringList * ChartLblSquare = new TStringList();

        double percentValidMeasurement = 100;
		bool measurementCut = false;
        for(int k=0; k < openedFiles[i].numStatData; k++){
            if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"part length"))
            {
                stop += start + (openedFiles[i].statDataData[k]-1)/SECONDS_IN_A_DAY;
            }
            if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"part missing data length"))
            {
                missing_data = openedFiles[i].statDataData[k];
                stop -= (openedFiles[i].statDataData[k]/SECONDS_IN_A_DAY);
			}
            if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"mark cut")
                && openedFiles[i].statDataData[k] > 0.0)
            {
                measurementCut = true;
            }
            if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"Beats coverage percentage"))
            {
                percentValidMeasurement = openedFiles[i].statDataData[k];
                //ChartLblSquare->Add("!");
                //ChartLblSquare->Add("Pokritost:");
                //ChartLblSquare->Add(FloatToStrF(openedFiles[i].statDataData[k], ffNumber, 5, 2) +  "\%");
            }
        }
        if(stop<=start){
            if(openedFiles[i].getDurationFromFile() <= 0){
                if(openedFiles[i].getDuration() <= 0)
                    stop = start + 1;
                else
					stop = start + (openedFiles[i].getDuration() - 1)/SECONDS_IN_A_DAY;
            }
            else
                stop = start + (openedFiles[i].getDurationFromFile() - 1)/SECONDS_IN_A_DAY;

        }
		AnsiString fileName = openedFiles[i].getFileLocation();
		fileName.Delete(1,fileName.LastDelimiter("\\\\"));

		if(previousay == ay && (start < previousStop) || //cover overlaps
            (start == stop && start == previousStop))
		    ay++;

  		ChartLbl = UnicodeString(j+1);
        if( !smallerBoxes && missing_data > 0)
        {
                // missing_data_written <- draw boring rectangle
				TChartShape *stdMissingRect = new TChartShape(GanttChart);
                stdMissingRect->Style = chasRectangle;
                stdMissingRect->Pen->Width = 0;
                stdMissingRect->Pen->Color = colorMissingGantt;
                stdMissingRect->SeriesColor = colorMissingGantt;
                stdMissingRect->ParentChart = GanttChart;
                stdMissingRect->HorizAxis = aBottomAxis;
                stdMissingRect->VertAxis = aLeftAxis;
                stdMissingRect->X0 = stop;
                stdMissingRect->X1 = stop+(missing_data/SECONDS_IN_A_DAY);
                stdMissingRect->Y0 = ay-0.40;
                stdMissingRect->Y1 = ay+0.40;
		}
        if(openedFiles[i].numStatData < 1) //no statistical data written - no color based on RR
    		Series1->AddGanttColor(start, stop, ay, ChartLbl ,colorDeprecated);
        else{
            int rrIndex = openedFiles[i].statDataString->IndexOf("BPM AVG");
            if(rrIndex < 0) //no information about RRI
                Series1->AddGanttColor(start, stop, ay, ChartLbl,colorBlue);
            else{
                short r,g,b;
                r = 0; g = 255; b = 255;
                double BpmAvg = openedFiles[i].statDataData[rrIndex];
                if(BpmAvg>lowBPMthr){
                    b = 0;
                }
                if(BpmAvg>highBPMthr){
                    r = 255; g = 0;
                }
                Series1->AddGanttColor(start, stop, ay, ChartLbl,TColor(RGB(r,g,b)));

                //draw percentage of valid measurement
                if(!smallerBoxes){
					TChartShape *stdMissingRect = new TChartShape(GanttChart);
                    stdMissingRect->Style = chasRectangle;
                    stdMissingRect->Pen->Width = 0;
                    stdMissingRect->Pen->Color = colorMissing;
                    stdMissingRect->SeriesColor = colorMissing;
                    stdMissingRect->ParentChart = GanttChart;
                    stdMissingRect->HorizAxis = aBottomAxis;
                    stdMissingRect->VertAxis = aLeftAxis;
					// X0 is calculated below -> to draw the triangle part
                    stdMissingRect->X1 = stop;
                    stdMissingRect->Y0 = ay-0.45;
                    stdMissingRect->Y1 = ay+0.45;
					stdMissingRect->Text = ChartLblSquare;
					stdMissingRect->X0 = std::max(start,std::min(stop,start + ((stop-start)*((percentValidMeasurement)/100))));
                }

                //draws inside of block (bpm std deviation)
                rrIndex = openedFiles[i].statDataString->IndexOf("BPM STD");
                if(rrIndex>=0 && acceptableStdDeviationThr != 0){
                    TChartShape *stdRect = new TChartShape(GanttChart);
					double BpmStd = openedFiles[i].statDataData[rrIndex];
                    TColor stdColor;
                    if(BpmStd<=acceptableStdDeviationThr){
                        double std = 255 * (BpmStd/acceptableStdDeviationThr);
                        stdColor = TColor(RGB((short)std,255,(short)std));
                    }else{
                        BpmStd = std::min(2.0*acceptableStdDeviationThr,BpmStd);
                        double std = 255 * ((BpmStd/acceptableStdDeviationThr)-1);
                        std = 255 - std::min(255.0, std);
                        stdColor = TColor(RGB(255,(short)std,(short)std));
                    }
                    stdRect->Style = chasRectangle;
                    stdRect->ParentChart = GanttChart;
                    stdRect->HorizAxis = aBottomAxis;
                    stdRect->VertAxis = aLeftAxis;
                    stdRect->X0 = start + (stop-start)/10.0;
                    stdRect->X1 = stop - (stop-start)/10.0;
                    stdRect->Y0 = ay-0.35;
                    stdRect->Y1 = ay+0.3;

                    ChartLblSquare->Add(UnicodeString(j+1));
					stdRect->Text = ChartLblSquare;
                    stdRect->Marks->Clip = true;

                    stdRect->SeriesColor = stdColor;
                    stdRect->Pen->Width = 0;
                    stdRect->Pen->Color = stdColor;
                }

                //mark measurement was cut
				if(measurementCut){
                    TChartShape *stdRect = new TChartShape(GanttChart);
                    TColor stdColor = TColor(RGB(128,0,128));
                    stdRect->Style = chasRectangle;
                    stdRect->ParentChart = GanttChart;
                    stdRect->HorizAxis = aBottomAxis;
                    stdRect->VertAxis = aLeftAxis;
                    stdRect->X0 = start;
                    stdRect->X1 = stop;
                    stdRect->Y0 = ay-0.15;
                    stdRect->Y1 = ay+0.15;

                    stdRect->SeriesColor = stdColor;
                    stdRect->Pen->Width = 0;
                    stdRect->Pen->Color = stdColor;
                }


            }
        }

        //draw annotations - be very careful of order -> drawing annotations in report !
		for(int iAnn = 0; iAnn < openedFiles[i].getNumAnnotationChannels(); iAnn++){
			if(openedFiles[i].getAnnotationChannel(iAnn).getChannelName().AnsiCompare(openedFiles[i].markedChName)==0)
                continue;
			int numAnn = openedFiles[i].getAnnotationChannel(iAnn).getNumElems();
            for(int jAnn = 0; jAnn<numAnn; jAnn++){
                if(openedFiles[i].getAnnotationChannel(iAnn)[jAnn].value < 1)
                    continue; 

                double time = openedFiles[i].getAnnotationChannel(iAnn)[jAnn].time;
				time =  start + (time/SECONDS_IN_A_DAY);

                TChartShape *annShape = new TChartShape(GanttChart);
                annShape->Style = chasVertLine;
				annShape->ParentChart = GanttChart;
                annShape->HorizAxis = aBottomAxis;
                annShape->VertAxis = aLeftAxis;
                annShape->X0 = time;
                annShape->X1 = time+(1/SECONDS_IN_A_DAY);
                annShape->Y0 = ay+0.5;
                annShape->Y1 = ay+0.4;


				if(openedFiles[i].getAnnotationChannel(iAnn)[jAnn].value == 1){
					ChartLblSquare->Text = AnsiString(openedFiles[i].getAnnotationChannel(iAnn)[jAnn].annotationNum);
				}
				else if(openedFiles[i].getAnnotationChannel(iAnn)[jAnn].value > 1){
					ChartLblSquare->Text = (char) openedFiles[i].getAnnotationChannel(iAnn)[jAnn].annotationNum;
				}

				annShape->Text = ChartLblSquare;
                annShape->Alignment = taRightJustify;
				annShape->Font->Color = TColor(RGB(1, 1, 1));
				//annShape->Font->Style = TFontStyles() << fsBold;
				annShape->Font->Size = 12;
				annShape->Marks->Clip = false;


                annShape->SeriesColor = TColor(RGB(255, 128, 0));
                annShape->Pen->Width = 2;
				annShape->Pen->Color = TColor(RGB(255, 128, 0));
            }

        }

		previousay = ay;
        previousStop = stop;
	}

    if(Series1->MaxXValue() - Series1->MinXValue() > 0){
        while(horiz_scroll_multiplier*(Series1->MaxXValue() - Series1->MinXValue()) > 1000)
            horiz_scroll_multiplier*=0.1;
        while(horiz_scroll_multiplier*(Series1->MaxXValue() - Series1->MinXValue()) < 1000)
            horiz_scroll_multiplier*=10;
    }else{
        horiz_scroll_multiplier = 1; 
    }

	horiz_scroll_enabled = false;
	ScrollBar1Change(NULL);
	horiz_scroll_enabled = true;

    if(GanttChart->BottomAxis->Maximum < Series1->MinXValue())
        GanttChart->BottomAxis->Maximum = Series1->MinXValue();
    GanttChart->BottomAxis->Minimum = Series1->MinXValue();
    GanttChart->BottomAxis->Maximum = Series1->MaxXValue();
   	GanttChart->LeftAxis->Maximum = 0.75 + Series1->MaxYValue();
	GanttChart->LeftAxis->Minimum = -0.75;
}

int CheckFilenameForPartNum(AnsiString original, AnsiString produced){

    AnsiString temp = "";
    for(int i=original.Length()+7; i<produced.Length() - 5;i++){
        temp += produced[i];
        if( produced[i] < '0' || produced[i] > '9'){
            return -1;
        }
    }
    try{
        return temp.ToInt();
    }catch(Exception *e){
        return -1;
    }
}




void TFolderViewForm::processFile(UnicodeString name, FILETIME* ftCreate, FILETIME* ftAccess, FILETIME* ftWrite){
	const auto ind = name.Pos("_RTM.s2");
	if(ind<=0){
		TDocumentForm *newForm = new TDocumentForm(this);
		//write statistics
		newForm->LoadMeasurement(name);
		newForm->processOpenedFile();
		/*
		This ifdef gives you option to choose between two implementations.
		Release() should be correct, however delete has lower MAX memory usage.
		None of them causes known memory leaks in long term,
		Release promises memory release sometime in the future (less control).
		 */
	#if 0
		newForm->dirty = false;
		newForm->Close();   //closes GUI
		newForm->Release(); //should release memory
	#else
		delete newForm;
	#endif
		newForm = NULL;
	}
	//fix date
	HANDLE hFile = CreateFile(name.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
	if(hFile != INVALID_HANDLE_VALUE){
		SetFileTime( hFile, ftCreate, ftAccess, ftWrite);
		CloseHandle(hFile);
	}
}

void TFolderViewForm::LoadFolder(UnicodeString Directory, boolean force)
{
	this->OnActivate = NULL; //prevent recursive call
	GetSystemTimeAsFileTime(&lastUpdate);
    GanttChart->Title->Text->Clear();

    rootFolder = Directory;
	UnicodeString partialPathToFile = Directory; //Location of files to be opened
	UnicodeString tempLabelBuffer;

	if (*(Directory.LastChar()) == '\\'){
		Directory += "*";
	}else{
		Directory += "\\*";
		partialPathToFile += "\\";
	}
	GanttChart->Title->Text->Add(partialPathToFile);
	TProgressBarForm *progWindow = new TProgressBarForm(this);

	tempLabelBuffer = gettext("Start opening folders!");
	progWindow->Label->SetTextBuf(tempLabelBuffer.c_str());
	progWindow->Refresh();

	//first time over directory -> try to convert txtFiles to nekg
	TSearchRec sr;

	int searchFor = faReadOnly; //beside normal files include read only files too
	if (FindFirst(Directory+"*.txt", searchFor, sr) == 0)
	{
		do{
			tempLabelBuffer = gettext("Converting file")+": ";
			tempLabelBuffer += sr.Name;
			progWindow->Label->SetTextBuf(tempLabelBuffer.c_str());
			progWindow->Refresh();
			progWindow->BringToFront();
			AnsiString fileExtension = LibCore::extensionOfFilename(sr.Name);
			if (fileExtension.AnsiCompareIC("txt") == 0){
				UnicodeString name = partialPathToFile + sr.Name;
				LibCore::CMeasurement::convertS2File(name);

				HANDLE hFile = CreateFile(name.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
				FILETIME ftCreate, ftAccess, ftWrite;
				// Retrieve the file times for the file.
				if(hFile != INVALID_HANDLE_VALUE){
					GetFileTime(hFile, &ftCreate, &ftAccess, &ftWrite);
					CloseHandle(hFile);
				}
				//change time of new files to original time
				TSearchRec newFiles;
				if (FindFirst(Directory+sr.Name+"*.nekge", searchFor, newFiles) == 0)
				{

					do{
						if(!(0 == AnsiCompareStr(newFiles.Name, sr.Name+".nekge"))
							&& (0 >= CheckFilenameForPartNum(sr.Name, newFiles.Name)))
							continue;
						processFile(partialPathToFile+newFiles.Name, &ftCreate, &ftAccess, &ftWrite);
					} while (FindNext(newFiles) == 0);
				}
				FindClose(newFiles);
			}
		} while (FindNext(sr) == 0);
		FindClose(sr);
	}   // end .txt

	if (FindFirst(Directory+"*.s2", searchFor, sr) == 0)
	{
		do{
			tempLabelBuffer = gettext("Converting file")+": ";
			tempLabelBuffer += sr.Name;
			progWindow->Label->SetTextBuf(tempLabelBuffer.c_str());
			progWindow->Refresh();
            progWindow->BringToFront();
			AnsiString fileExtension = LibCore::extensionOfFilename(sr.Name);
			if (fileExtension.AnsiCompareIC("s2") == 0){
				//check if files are already converted
				AnsiString filenameWithoutExtension = sr.Name;
				filenameWithoutExtension.Delete(filenameWithoutExtension.Length()-3,4);
				if(!force && (
				   FileExists(partialPathToFile+sr.Name+".nekg")||
				   FileExists(partialPathToFile+sr.Name+".nekge")||
				   FileExists(partialPathToFile+sr.Name+"_part_1.nekg")||
				   FileExists(partialPathToFile+sr.Name+"_part_1.nekge"))
				   )
				   {
					continue;
				   }

				const std::vector<std::string> result = LibCore::CMeasurement::convertS2File(sr.Name, -1, force);


				for(size_t ind = 0; ind<result.size(); ind++){
					UnicodeString name = partialPathToFile + sr.Name;

					//change time of new files to original time
					HANDLE hFile = CreateFile(name.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
					FILETIME ftCreate, ftAccess, ftWrite;
					if(hFile != INVALID_HANDLE_VALUE){
						GetFileTime(hFile, &ftCreate, &ftAccess, &ftWrite);
						CloseHandle(hFile);
					}

					name = partialPathToFile + UnicodeString(result[ind].c_str());
					processFile(name, &ftCreate, &ftAccess, &ftWrite);
				}
			}
		} while (FindNext(sr) == 0);
		FindClose(sr);
	} // end s2

	progWindow->Label->SetTextBuf(gettext("Counting nekg(e) files").data());
	progWindow->Refresh();
	int countFiles = 0; //count number of nekg files
	if (FindFirst(Directory+"*.nekg*", searchFor, sr) == 0)
	{
		do
		{
			AnsiString fileExtension = LibCore::extensionOfFilename(sr.Name);
			if (fileExtension.AnsiCompareIC("nekg") == 0 ||
					fileExtension.AnsiCompareIC("nekge") == 0)
			countFiles++;
		} while (FindNext(sr) == 0);
		FindClose(sr);
	}
	tempLabelBuffer = gettext("Found")+ " " + IntToStr(countFiles) + " " + gettext("nekg files")+".";
	GanttChart->Title->Text->Add(tempLabelBuffer);

	progWindow->Label->SetTextBuf(gettext("Read nekg file").data());
	progWindow->Refresh();
	openedFiles = new LibCore::CMeasurement[countFiles]; //allocate space
	countFiles = 0; //reset counter
	AnsiString tempLabel = "";
	if (FindFirst(Directory+"*.nekg*", searchFor, sr) == 0)
	{
		do
		{
			tempLabelBuffer = gettext("Opening file: ");
			tempLabelBuffer += sr.Name;
			progWindow->Label->SetTextBuf(tempLabelBuffer.c_str());
			progWindow->Refresh();
			AnsiString fileExtension = LibCore::extensionOfFilename(sr.Name);
			if (fileExtension.AnsiCompareIC("nekg") == 0
				|| fileExtension.AnsiCompareIC("nekge") == 0){
				openedFiles[countFiles].loadHeaderFromFile(partialPathToFile + sr.Name,
				LibCore::CMeasurement::nevroEKG);
				countFiles++;
			}
		} while (FindNext(sr) == 0);
		FindClose(sr);
	}
	numOpenedFiles = countFiles;
	if (countFiles < 1)
	throw new LibCore::EKGException(__LINE__, "TFolderViewForm::loadFolder:"
	" No NEKG or NEKGE files in the selected folder ("+Directory+") !");
	tempLabelBuffer = "Drawing data!";
	progWindow->Label->SetTextBuf(tempLabelBuffer.c_str());
	drawOpenedFiles(countFiles);

    GanttChart->Refresh();
    progWindow->Close();
	delete progWindow;
	this->OnActivate = this->FormActivate;
}

void __fastcall TFolderViewForm::GanttChartClickSeries(
TCustomChart *Sender, TChartSeries *Series, int ValueIndex,
TMouseButton Button, TShiftState Shift, int X, int Y)
{
	ValueIndex = Series1->Clicked(X,Y); // ensures correct index
	if(ValueIndex < 0)
		return;
	if(!Parent->simpleView){
		TDocumentForm *newForm;
		try {
			int openedWin = Parent->checkFileAlreadyOpened(openedFiles[sortedIndex[ValueIndex]].getFileLocation());
			if( openedWin >= 0 ){
				Parent->MDIChildren[openedWin]->WindowState = wsNormal;
				Parent->MDIChildren[openedWin]->BringToFront();
				Application->ProcessMessages();
				Parent->MDIChildren[openedWin]->WindowState = wsNormal;
				Parent->MDIChildren[openedWin]->BringToFront();
				return;
			}
			Cursor = crHourGlass;
			newForm = new TDocumentForm(this->Parent);
			newForm->LoadMeasurement(openedFiles[sortedIndex[ValueIndex]].getFileLocation());
			newForm->Width = newForm->Width+1;
			newForm->openStaggered(GanttChart->BottomAxis->CalcPosPoint(X), 60);
			newForm->Cursor = crDefault;
		} catch (LibCore::EKGException *e) {
			Application->MessageBox(e->Message.c_str(), gettext("Error while opening").data(), MB_OK);
			Cursor = crDefault;
			newForm->Close();
			delete newForm;
		}
	}
	else{
		TsDocForm *newForm;
		try {
			int openedWin = Parent->checkFileAlreadyOpened(openedFiles[sortedIndex[ValueIndex]].getFileLocation());
			if( openedWin >= 0 ){
				Parent->MDIChildren[openedWin]->WindowState = wsNormal;
				Parent->MDIChildren[openedWin]->BringToFront();
				Application->ProcessMessages();
				Parent->MDIChildren[openedWin]->WindowState = wsNormal;
				Parent->MDIChildren[openedWin]->BringToFront();
				return;
			}
			Cursor = crHourGlass;
			newForm = new TsDocForm(this->Parent);
			newForm->LoadMeasurement(openedFiles[sortedIndex[ValueIndex]].getFileLocation());
            newForm->rootFolder = rootFolder;
			newForm->Width = newForm->Width+1;
			newForm->Visible = true;
			Cursor = crDefault;
		} catch (LibCore::EKGException *e) {
			Application->MessageBox(e->Message.c_str(), gettext("Error while opening").data(), MB_OK);
			Cursor = crDefault;
			newForm->Close();
			delete newForm;
		}
	}

	Application->ProcessMessages();
	Cursor = crDefault;
}
//---------------------------------------------------------------------------

int TFolderViewForm::countNewLine(AnsiString stringLines){
    int numLines = 1;

	for(int i = 1; i <= stringLines.Length(); i++){
        if(stringLines[i] == '\n'){
            numLines++;
        }
    }
    return numLines;
}

int TFolderViewForm::getLineWidth(AnsiString str0, AnsiString str1, AnsiString str2){
	int width = Canvas->TextWidth(str0) +45;
    width = std::max(width, Canvas->TextWidth(str1) +45);

    AnsiString substr = "";
    for(int i = 1; i <= str2.Length(); i++){
        substr += str2[i];
        if(str2[i] == '\n'){
            width = std::max(width, Canvas->TextWidth(substr)+45);
            substr = "";
        }
    }
    width = std::max(width, Canvas->TextWidth(substr)+45); //if last line does not end with "\n"
    return width; 
}


int prevMouseX =-5, prevMouseY = -5, prevIndexMouse = -1;
void __fastcall TFolderViewForm::GanttChartMouseMove(TObject *Sender,
TShiftState Shift, int X, int Y)
{
	int clicked = Series1->Clicked(X,Y);
    if((  abs(prevMouseX - X) < 5
        || abs(prevMouseY - Y) < 5)
        && prevIndexMouse  == clicked){
        return;
    }
    prevMouseX = X;
    prevMouseY = Y;
	prevIndexMouse = clicked;

	AnsiString fileLoc = "";
    delete hintRectangleGantt;
    hintRectangleGantt = NULL;

	if(clicked >= 0){
    	fileLoc = openedFiles[sortedIndex[clicked]].getFileLocation();
		fileLoc.Delete(1,fileLoc.LastDelimiter("\\\\"));
        if(openedFiles[sortedIndex[clicked]].numStatData > 0){
			AnsiString stats = " ";
            AnsiString stats2 = " ";
            for(int j=0;j< openedFiles[sortedIndex[clicked]].numStatData;j++){
                if(0 == AnsiCompareStr(openedFiles[sortedIndex[clicked]].statDataString->Strings[j],"BPM AVG"))
                    stats = "Average BPM: "+FormatFloat("0.##",openedFiles[sortedIndex[clicked]].statDataData[j])+stats;
                else if(0 == AnsiCompareStr(openedFiles[sortedIndex[clicked]].statDataString->Strings[j],"BPM STD"))
                    stats += " | "+FormatFloat("0.#%",100.0*openedFiles[sortedIndex[clicked]].statDataData[j]);
                else if(0 == AnsiCompareStr(openedFiles[sortedIndex[clicked]].statDataString->Strings[j],"part length")){
                    //don't add part length to any string
                }
                else if(0 == AnsiCompareStr(openedFiles[sortedIndex[clicked]].statDataString->Strings[j],"part missing data length")){
                    //don't add part misssing data to any string
                }
                else{
                    stats2 += openedFiles[sortedIndex[clicked]].statDataString->Strings[j]+": ";
                    stats2 += FormatFloat("0.##",openedFiles[sortedIndex[clicked]].statDataData[j]) + ", ";
                }
            }
            if(stats2.Length() > 2)
				stats2.SetLength(stats2.Length() - 2);

            stats2 += "\n" + openedFiles[sortedIndex[clicked]].patientData.comment;
            int textWidth = getLineWidth(fileLoc, stats, stats2);
            int numNewLine = 2+countNewLine(stats2);
            int textHeight = (2+Canvas->TextHeight(stats2))*numNewLine;
            if(textWidth > 46){
                stats=fileLoc+"\n"+stats+"\n"+stats2;
                hintRectangleGantt = new TChartShape(GanttChart);
                hintRectangleGantt->Style = chasRectangle;
                hintRectangleGantt->RoundRectangle = true;
				hintRectangleGantt->ParentChart = GanttChart;
				UnicodeString popup_text = UnicodeString(stats);
                hintRectangleGantt->Text->SetText((wchar_t*)popup_text.c_str());
                hintRectangleGantt->HorizAxis = aTopAxis;
                hintRectangleGantt->VertAxis = aRightAxis;
                hintRectangleGantt->XYStyle = xysPixels;
                hintRectangleGantt->Alignment =  taLeftJustify;
                hintRectangleGantt->X0 = (X+textWidth<GanttChart->Width)?X+30:X-30;
                hintRectangleGantt->Y0 = Y-(textHeight/2);
                hintRectangleGantt->X1 = (X+textWidth<GanttChart->Width)?X+textWidth:X-textWidth;
                hintRectangleGantt->Y1 = Y+(textHeight/2);
            }
        }
		MainForm->StatusLine->SimpleText = openedFiles[sortedIndex[clicked]].patientData.name + "; "
		"" + openedFiles[sortedIndex[clicked]].getMeasurementName() + "; "
		"" + openedFiles[sortedIndex[clicked]].getDate().DateTimeString();
	}else{
		setGeneralHint();
	}
    fileNameLabel->Caption = fileLoc;
}
//---------------------------------------------------------------------------




void __fastcall TFolderViewForm::GanttChartMouseWheel(TObject *Sender,
TShiftState Shift, int WheelDelta, TPoint &MousePos, bool &Handled)
{
    Handled = true;
    if(Shift.Contains(ssCtrl)){
        double xpos = Series1->XScreenToValue(GanttChart->GetCursorPos().x);
    	double newMaxX, newMinX;
    	if(WheelDelta > 0){
    		if((GanttChart->BottomAxis->Maximum - GanttChart->BottomAxis->Minimum) < 0.003){
                Handled = true;
                return; // "width" of the graph shows ~ 5 minutes min
            }
    		newMaxX = (xpos + GanttChart->BottomAxis->Maximum) / 2.0;
    		newMinX = (xpos + GanttChart->BottomAxis->Minimum) / 2.0;
	    }
        if(WheelDelta < 0){
    		if((GanttChart->BottomAxis->Maximum - GanttChart->BottomAxis->Minimum) >
                    (Series1->MaxXValue()-Series1->MinXValue())){
                Handled = true;
                GanttChart->BottomAxis->Maximum = Series1->MaxXValue();
                GanttChart->BottomAxis->Minimum = Series1->MinXValue();
				return; // "width" of the graph is 100% of maximum span
			}
            double widenFor = (GanttChart->BottomAxis->Maximum - GanttChart->BottomAxis->Minimum)*0.25;
   	    	newMaxX = GanttChart->BottomAxis->Maximum + widenFor;
    		newMinX = GanttChart->BottomAxis->Minimum - widenFor;
    	}
  
        GanttChart->BottomAxis->Maximum = std::min(newMaxX, Series1->MaxXValue());
        GanttChart->BottomAxis->Minimum = std::max(newMinX, Series1->MinXValue());
    }
    else{
        double delta = (GanttChart->BottomAxis->Maximum - GanttChart->BottomAxis->Minimum) / 10.0;
        if (WheelDelta < 0){
            if(GanttChart->BottomAxis->Maximum >= Series1->MaxXValue())
                return;
            GanttChart->BottomAxis->Maximum += delta;
            double overDelta = std::max(0.0, GanttChart->BottomAxis->Maximum - Series1->MaxXValue());
            GanttChart->BottomAxis->Maximum -= overDelta;
            GanttChart->BottomAxis->Minimum += delta - overDelta;
        }
        else{
            if(GanttChart->BottomAxis->Minimum <= Series1->MinXValue())
                return;
            GanttChart->BottomAxis->Minimum -= delta;
            double overDelta = std::max(0.0, Series1->MinXValue() - GanttChart->BottomAxis->Minimum);
            GanttChart->BottomAxis->Minimum += overDelta;
            GanttChart->BottomAxis->Maximum -= delta + overDelta;
        }
    }
	horiz_scroll_enabled = false;
	ScrollBar1Change(NULL);
	horiz_scroll_enabled = true;

}

//---------------------------------------------------------------------------

void TFolderViewForm::folderFindBeat()
{
	//ToDo - function that applies something to whole folder
	Application->MessageBox((gettext("Not yet implemented")+": ").data(), L"", MB_OK);
}


void __fastcall TFolderViewForm::FormCreate(TObject *Sender)
{
	TranslateComponent (this);
	setGeneralHint();
}
//---------------------------------------------------------------------------

void __fastcall TFolderViewForm::GanttChartAfterDraw(TObject *Sender)
{
    if(GanttChart->ChartHeight > 0 && Series1->MaxYValue() >= 0 ){
        Series1->Pointer->VertSize = (GanttChart->ChartHeight / (Series1->MaxYValue()+1.5)) * 0.45;
        GanttChart->Refresh();
    }
}
//---------------------------------------------------------------------------


void __fastcall TFolderViewForm::FormResize(TObject *Sender)
{
	TFolderViewForm::GanttChartAfterDraw(Sender);
    GanttChart->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TFolderViewForm::ScrollBar1Change(TObject *Sender)
{
    ScrollBar1->Min = 0;
	ScrollBar1->Max = 1000;
    if(horiz_scroll_enabled){ //move graph
		double mid = ScrollBar1->Position/1000.0;
        mid*= (Series1->MaxXValue() - Series1->MinXValue());
        mid+= Series1->MinXValue();
        double span = GanttChart->BottomAxis->Maximum - GanttChart->BottomAxis->Minimum;
        if(mid+span<GanttChart->BottomAxis->Minimum)
            GanttChart->BottomAxis->Minimum = mid;
        GanttChart->BottomAxis->Maximum = mid+ span;
        GanttChart->BottomAxis->Minimum = mid;
    }
	else{ //calc size of scrollbar
		ScrollBar1->Position = 	1000*((GanttChart->BottomAxis->Minimum - Series1->MinXValue())			/	(Series1->MaxXValue() - Series1->MinXValue()));
		ScrollBar1->PageSize = 	1000*((GanttChart->BottomAxis->Maximum-GanttChart->BottomAxis->Minimum)	/	(Series1->MaxXValue() - Series1->MinXValue()));
    }
}
//---------------------------------------------------------------------------

void __fastcall TFolderViewForm::showAllClick(TObject *Sender)
{
    if(GanttChart->BottomAxis->Maximum < Series1->MinXValue())
        GanttChart->BottomAxis->Maximum = Series1->MinXValue();
    GanttChart->BottomAxis->Minimum = Series1->MinXValue();
    GanttChart->BottomAxis->Maximum = Series1->MaxXValue();
}
//---------------------------------------------------------------------------


void secondsToDHMS(double seconds, int *d, int* h, int* m, int*s, int* ms){
    *d = std::max(0,(int)floor(seconds / SECONDS_IN_A_DAY));
    *h = std::max(0,(int)fmod(seconds / 3600.0, 24));
    *m = std::max(0,(int)fmod(seconds / 60.0, 60));
    *s = std::max(0,(int)fmod(seconds , 60));
    *ms = std::max(0,(int)fmod(seconds * 1000.0, 1000));
}

//Converts to UTF8 and prints out
void printBase64Tag(FILE* file, std::string tag, UnicodeString text){
	RawByteString utf8String = System::UTF8Encode(text);
	if(text.Length() > 0){
		std::string result = base64_encode(utf8String.c_str(), utf8String.Length());
		fprintf(file, "<%s>%s</%s>\n", tag.c_str(),result.c_str(), tag.c_str());
	}
	else{
	  fprintf(file, "<%s></%s>\n", tag.c_str(), tag.c_str());
	}
}

//convert to UTF8 in process
std::string unicode2base64(UnicodeString text){
	RawByteString utf8String = System::UTF8Encode(text);
	return base64_encode(utf8String.c_str(), utf8String.Length());
}

void __fastcall TFolderViewForm::GenerateReportClick(TObject *Sender)
{

	UnicodeString rootReportFolder = rootFolder+"\\report_"+TDateTime::CurrentDateTime().FormatString("yyyy_mm_dd-hh_nn_ss");
	UnicodeString indexfileloc = rootReportFolder + "\\index.txt";
	CreateDirectory(rootReportFolder.c_str(), NULL);

    //create report
    double
    allRecordedLength = 0,
    allValidLength = 0,
	allBeatLength = 0,
    allAverageBPM = 0,
    allAverageDeviation = 0;
    long allNumberBeats = 0;
    int allNumEvents = 0, allNumMeasurements = 0;

	AnsiString statFileTable = "";
	int day = 0, hour = 0, minut = 0, second = 0, millisecond = 0;
	//calc folder average and save data of each file
	double minAvgBpm = 250;
	double maxAvgBpm = 0;
	for(int j=0; j<numOpenedFiles;j++){
		double length = 0,
		missing_data = 0,
		percentValidMeasurement = 0,
		averagebpm = 0,
		averagestd = 0;
		int numberBeats = 0;

		int i = sortedIndex[j];
		length = openedFiles[i].getDuration();
		double length_from_stat = 0;
		for(int k=0; k < openedFiles[i].numStatData; k++){
			if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"part length"))
				length_from_stat = openedFiles[i].statDataData[k];
			if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"part missing data length"))
				missing_data = openedFiles[i].statDataData[k];
			if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"Beats coverage percentage"))
				percentValidMeasurement = openedFiles[i].statDataData[k];
			if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"Number of beats"))
				numberBeats = openedFiles[i].statDataData[k];
			if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"BPM AVG")){
				averagebpm = openedFiles[i].statDataData[k];
				if(averagebpm < minAvgBpm)
					minAvgBpm = averagebpm;
				if(averagebpm > maxAvgBpm)
					maxAvgBpm = averagebpm;
			}
			if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"BPM STD"))
				averagestd = openedFiles[i].statDataData[k];
		}
		if(length == 0)
			length = openedFiles[i].getDurationFromFile();

		int allEventsNumber = 0;
		for(int k=0; k<openedFiles[i].getNumAnnotationChannels(); k++){
			if(openedFiles[i].getAnnotationChannel(k).getChannelName().AnsiCompare(openedFiles[i].markedChName) == 0)
				continue; //skip marked events
			for(int evN = 0; evN <  openedFiles[i].getAnnotationChannel(k).getNumElems(); evN++){
				if(openedFiles[i].getAnnotationChannel(k)[evN].value < 1)
                	continue;
				allEventsNumber++;
			}
		}
		allNumEvents = allNumEvents + allEventsNumber;

		double minAvgBPM =0, maxAvgBPM = 0;
        int avgCh = openedFiles[i].findEventChannelByName("Window average",0,true);
		if(avgCh >=0){
            LibCore::CEventChannel::Statistics stats = openedFiles[i].getEventChannel(avgCh).calcStatistics();
			minAvgBPM = stats.minValue;
			maxAvgBPM = stats.maxValue;
        }

		allValidLength += length;
		allBeatLength += length*(percentValidMeasurement/100.0);
		allAverageBPM += length*(percentValidMeasurement/100.0) * averagebpm;
		allAverageDeviation += length*(percentValidMeasurement/100.0) * averagestd;
		allNumberBeats += numberBeats;

		AnsiString fileName =openedFiles[i].getFileLocation();
		int locationOfSlash = 1 + fileName.LastDelimiter("\\");
		fileName = fileName.SubString(locationOfSlash, fileName.Length());

        TDateTime curFileDate = openedFiles[i].getDate();

        statFileTable += "<file>"+AnsiString(j+1) + ": ";
        statFileTable += curFileDate.FormatString("yyyy mmm dd hh:nn:ss") + " | ";
        statFileTable +=  openedFiles[i].electrodePosition + " | ";
        secondsToDHMS(length, &day, &hour, &minut, &second, &millisecond);
        statFileTable +=  AnsiString(day) + "d "+AnsiString(hour)+"h "+AnsiString(minut)+"min " + AnsiString(second)+ "s | ";
        secondsToDHMS(missing_data, &day, &hour, &minut, &second, &millisecond);
        statFileTable +=  AnsiString(day) + "d "+AnsiString(hour)+"h "+AnsiString(minut)+"min " + AnsiString(second)+ "s | ";
        statFileTable +=  FormatFloat("0.## %", percentValidMeasurement) + " | ";
        statFileTable +=  AnsiString(allEventsNumber) + " | ";
        statFileTable +=  FormatFloat("0.##", minAvgBPM) + " | ";
        statFileTable +=  FormatFloat("0.##", maxAvgBPM) + " | ";
        if(averagebpm != NULL)
		   statFileTable +=  FormatFloat("0.##", averagebpm) + " | ";
        else
           statFileTable +=  " | ";
        if(averagestd != NULL)
            statFileTable +=  FormatFloat("0.## %", averagestd*100) + "</file>\n";
        else
            statFileTable += " </file>\n";

    }
    if(allBeatLength > 0){
        allAverageBPM /= allBeatLength;
		allAverageDeviation /= allBeatLength;
    }else{
        allAverageBPM = -1;
        allAverageDeviation = -1; 
    }

    //save to file folder overview
    FILE *file = _wfopen(indexfileloc.c_str(), L"wb");
	if (file == NULL)
    {
        return;
    }

	std::string author;
	std::string user;
	std::string age;
	std::string weight;
	std::string sex;
	std::string born;
	double report_quality = 2.0;
	double report_ecgLineLength = 15;
	double report_ecgLineMvHeight = 10;
	double report_ecgLineNumMv = 3;
    double report_ecgLineSecondLength = 1.25;

	UnicodeString settingsFname("nastavitve.ini");
    for (int i = Application->ExeName.Length()-1; i >= 0; --i){
    	if (Application->ExeName[i] == '\\') {
            settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
        	break;
        }
     }
    Ini::File ini(settingsFname.c_str());
	ini.loadVar(author, L"report author");
	ini.loadVar(user, L"report user");
	ini.loadVar(age, L"report age");
	ini.loadVar(weight, L"report weight");
	ini.loadVar(sex, L"report user_sex");
	ini.loadVar(born, L"report user_born");
    try{
		ini.loadVar(report_quality, L"report quality");
		ini.loadVar(report_ecgLineLength, L"report ecg line length");
		ini.loadVar(report_ecgLineMvHeight, L"report ecg mV height");
		ini.loadVar(report_ecgLineNumMv, L"report ecg mV shown");
		ini.loadVar(report_ecgLineSecondLength, L"report ecg length of second");
	}catch(Exception *e){

    }
    report_quality = std::max(report_quality, 1.0/60.0); // minimum width is 1 minute

    ReportInfoForm->reportActivityCheckbox->Checked = false;
	ReportInfoForm->AuthorInp->Text = 		string2WideString(author);
	ReportInfoForm->UserInp->Text = 		string2WideString(user);
	ReportInfoForm->AgeInp->Text = 			string2WideString(age);
	ReportInfoForm->WeightInp->Text = 		string2WideString(weight);
	ReportInfoForm->BirthInp->Text = 		string2WideString(born);
	ReportInfoForm->GenderInp->Text = 		string2WideString(sex);

	ReportInfoForm->MeasurementNumberInput->Text = UnicodeString(numOpenedFiles);
	ReportInfoForm->EventNumberInput->Text = UnicodeString(allNumEvents);
	ReportInfoForm->Edit2->Text = UnicodeString(report_ecgLineLength);
	ReportInfoForm->Edit3->Text = UnicodeString(report_ecgLineSecondLength);
	ReportInfoForm->Edit4->Text = UnicodeString(report_ecgLineNumMv);
	ReportInfoForm->Edit5->Text = UnicodeString(report_ecgLineMvHeight);
	ReportInfoForm->Edit6->Text = UnicodeString(report_quality);

    if(ReportInfoForm->ShowModal() != mrOk){
		fclose(file);
		TSearchRec sr;
		if (FindFirst(rootReportFolder+"\\*", faReadOnly, sr) == 0)
		{
			do{
				DeleteFile(rootReportFolder+"\\"+sr.Name);
			} while (FindNext(sr) == 0);
			FindClose(sr);
		}
		RemoveDir(rootReportFolder);
        return;
    }

	try{
		allNumMeasurements = ReportInfoForm->MeasurementNumberInput->Text.ToInt();
		if(allNumMeasurements < 0)
			allNumMeasurements = 0;
    }catch(Exception *e){}

    try{
        allNumEvents = ReportInfoForm->EventNumberInput->Text.ToInt();
        if(allNumEvents < 0)
            allNumEvents = 0;
    }catch(Exception *e){}

	try{
		report_ecgLineLength = 			ReportInfoForm->Edit2->Text.ToDouble();
		report_ecgLineSecondLength = 	ReportInfoForm->Edit3->Text.ToDouble();
		report_ecgLineNumMv = 			ReportInfoForm->Edit4->Text.ToDouble();
		report_ecgLineMvHeight = 		ReportInfoForm->Edit5->Text.ToDouble();
		report_quality =  				ReportInfoForm->Edit6->Text.ToDouble();
	}catch(Exception *e){

	}

	ini.storeVar(wideString2String(ReportInfoForm->AuthorInp->Text), L"report author");
	ini.storeVar(wideString2String(ReportInfoForm->UserInp->Text), L"report user");
	ini.storeVar(wideString2String(ReportInfoForm->AgeInp->Text), L"report age");
	ini.storeVar(wideString2String(ReportInfoForm->WeightInp->Text), L"report weight");
	ini.storeVar(wideString2String(ReportInfoForm->GenderInp->Text), L"report user_sex");
	ini.storeVar(wideString2String(ReportInfoForm->BirthInp->Text), L"report user_born");
	ini.storeVar(report_ecgLineLength, L"report ecg line length");
	ini.storeVar(report_ecgLineMvHeight, L"report ecg mV height");
	ini.storeVar(report_ecgLineNumMv, L"report ecg mV shown");
	ini.storeVar(report_ecgLineSecondLength, L"report ecg length of second");
	ini.storeVar(report_quality, L"report quality");
	ini.save();


    //draw leading images
	UnicodeString imgloc = rootReportFolder+"\\overview.wmf";
    UnicodeString imglocdeviation = rootReportFolder+"\\meanAndDev.wmf";
    //redraw chart with special argument, making boxes smaller
    drawOpenedFiles(numOpenedFiles, true);
    if(GanttChart->BottomAxis->Maximum < Series1->MinXValue())
        GanttChart->BottomAxis->Maximum = Series1->MinXValue()+1;
    GanttChart->BottomAxis->Minimum = Series1->MinXValue();
    GanttChart->BottomAxis->Maximum = Series1->MaxXValue();
    int gchartWidth = GanttChart->Width, gchartHeight = GanttChart->Height;
    GanttChart->Align = alNone;
    GanttChart->Width = 3000;
    GanttChart->Height = 375;
	int tempFontSize = GanttChart->BottomAxis->LabelsFont->Size;
    GanttChart->Title->Visible = false;
    GanttChart->BottomAxis->LabelsFont->Size = 24;
    int tempLeftMargin = GanttChart->MarginLeft, tempRightMargin = GanttChart->MarginRight;
    GanttChart->MarginRight = 0;
    GanttChart->LeftAxis->Visible = true;
    GanttChart->LeftAxis->Title->Caption = "  ";
	GanttChart->LeftAxis->TitleSize = 41;

    for(int i = 0; i < GanttChart->SeriesCount(); i++){
        TChartShape * tmpCast = dynamic_cast<TChartShape*>(GanttChart->Series[i]);
        if (tmpCast!= NULL){
            tmpCast->Font->Size = 24;
        }
    }

	GanttChart->SaveToMetafile(imgloc);

    GanttChart->Height = 500;
    drawOpenedFilesSimple(numOpenedFiles);
    GanttChart->MarginLeft = 0;
    GanttChart->SaveToMetafile(imglocdeviation);

    //redraw ganttChart
    GanttChart->Width = gchartWidth;
    GanttChart->Height = gchartHeight;
    GanttChart->Align = alClient;
    GanttChart->BottomAxis->LabelsFont->Size = tempFontSize;
    GanttChart->LeftAxis->Visible = false;
    GanttChart->Title->Visible = true;
    GanttChart->MarginLeft = tempLeftMargin;
    GanttChart->MarginRight = tempRightMargin;
	drawOpenedFiles(numOpenedFiles);

	//TODO CREATE CLASS TO GENERATE XML more easily <name, value, atributes, children>

	// create index file
	nevroreport::ReportPrinter reporter;

	std::string temp;
	//patient
	temp = nevroreport::create_node_borland("name", ReportInfoForm->UserInp->Text);
	temp += nevroreport::create_node_borland("age", ReportInfoForm->AgeInp->Text);
	temp += nevroreport::create_node_borland("weight", ReportInfoForm->WeightInp->Text);
	temp += nevroreport::create_node_borland("born", ReportInfoForm->BirthInp->Text);
	temp += nevroreport::create_node_borland("gender", ReportInfoForm->GenderInp->Text);
	reporter.add_root_node("patient", temp);
    //settings
	temp = nevroreport::create_node_d("ecgLineLength", report_ecgLineLength);
	temp += nevroreport::create_node_d("ecgLineMvHeight", report_ecgLineMvHeight);
	temp += nevroreport::create_node_d("ecgLineNumMv", report_ecgLineNumMv);
	temp += nevroreport::create_node_d("ecgLineSecondLength", report_ecgLineSecondLength);
	reporter.add_root_node("settings", temp);

	reporter.add_root_node_borland("userComment", ReportInfoForm->Memo1->Text);

    //folder
	UnicodeString folName = rootFolder;
	folName = folName.SubString(1+folName.LastDelimiter("\\"), folName.Length());
	temp = nevroreport::create_node_borland("name", folName);

	TDateTime tempTime = TDateTime(Series1->MinXValue());
	temp += nevroreport::create_node_borland("start",  tempTime.FormatString("yyyy mmm dd; hh:nn:ss"));

	tempTime = TDateTime(Series1->MaxXValue());
	temp += nevroreport::create_node_borland("stop",  tempTime.FormatString("yyyy mmm dd; hh:nn:ss"));

	reporter.add_root_node("folder", temp);


	//beats

//    secondsToDHMS(allRecordedLength, &day, &hour, &minut, &second, &millisecond);
//    overviewParagraph += "\n\tcas snemanja: " + AnsiString(day) + " dni "+AnsiString(hour)+" ur "+AnsiString(minut)+" minut " + AnsiString(second)+ " sekund ";
//    secondsToDHMS(allValidLength, &day, &hour, &minut, &second, &millisecond);
//    overviewParagraph += "\n\tcas veljavnih meritev: " + AnsiString(day) + " dni "+AnsiString(hour)+" ur "+AnsiString(minut)+" minut " + AnsiString(second)+ " sekund ";
	secondsToDHMS(allBeatLength, &day, &hour, &minut, &second, &millisecond);
	AnsiString tempStr = AnsiString(day) + "d "+AnsiString(hour)+"h "+AnsiString(minut)+"min " + AnsiString(second)+ "s";
	temp = nevroreport::create_node_borland("time", tempStr);

	tempStr = AnsiString(allNumberBeats);
	temp += nevroreport::create_node_borland("number", tempStr);

	tempStr =  AnsiString(allAverageBPM);
	temp += nevroreport::create_node_d("average", allAverageBPM);

	long BPMMaxFileLocation = ftell(file);
	temp += nevroreport::create_node_d("maxAverage", maxAvgBpm);
	long BPMMinFileLocation = ftell(file);
	temp += nevroreport::create_node_d("minAverage", minAvgBpm);

	temp += nevroreport::create_node_d("deviation", allAverageDeviation*100);
	reporter.add_root_node("beats", temp);

	reporter.add_root_node_borland("table", statFileTable);

	//measurementGraphs
//    double snapLength = 1.0*3600.0;
	temp="";
    int expectedWidth = report_quality*60*60;
	for(int kk=0;kk<numOpenedFiles && (ReportInfoForm->reportActivityCheckbox->Checked || kk < allNumMeasurements);kk++){
        int j = sortedIndex[kk];
		TDocumentForm * newForm = new TDocumentForm(this);
        AnsiString partialFilename = rootReportFolder +"\\"+ AnsiString(kk)+"_";

        newForm->LoadMeasurement(openedFiles[j].getFileLocation());

        if(newForm->getDuration() < expectedWidth/24.0){ //less than 15mins we do not show
            newForm->Close();
			delete newForm;
			continue;
        }

        if(newForm->getNumEventChannel() < 2){
            newForm->Close();
            delete newForm;
            continue;
        }

        LibCore::CMeasurement *meas = newForm->getMeasurementPointer();
		for(int i=0; i < newForm->getDuration(); i+=expectedWidth){
;

			UnicodeString title = AnsiString(kk+1)+": ["+meas->getDate().FormatString("yyyy mmm dd; hh:nn:ss")+"] - ["+ (meas->getDate() + meas->getDuration()/86400.0).FormatString("yyyy mmm dd; hh:nn:ss") +"]";
			std::string graphStr = nevroreport::create_node_borland("title", title);

			graphStr += nevroreport::create_node_d("xmin", i+0.0);
			graphStr += nevroreport::create_node_d("xmax", std::min((double)(i+expectedWidth), newForm->getDuration()));
			graphStr += nevroreport::create_node_d("ymin", 0);
			graphStr += nevroreport::create_node_d("ymax", 250.0);

			graphStr += nevroreport::create_node_d("width", expectedWidth);

			graphStr += nevroreport::create_node_borland("ISOtimestampYear", meas->getDate().FormatString("yyyy"));
			graphStr += nevroreport::create_node_borland("ISOtimestampMonth", meas->getDate().FormatString("mm"));
			graphStr += nevroreport::create_node_borland("ISOtimestampDay", meas->getDate().FormatString("dd"));
			graphStr += nevroreport::create_node_borland("ISOtimestampHour", meas->getDate().FormatString("hh"));
			graphStr += nevroreport::create_node_borland("ISOtimestampMinute", meas->getDate().FormatString("nn"));
			graphStr += nevroreport::create_node_borland("ISOtimestampSecond", meas->getDate().FormatString("ss"));

			//choose frequency channel and pump it out
			//dot
			LibCore::CEventChannel dotChannel = meas->getEventChannel(newForm->getNumEventChannel()-2);

			UnicodeString binFileLocation = rootReportFolder + "\\bin" +kk+"_"+i+"_dot.bin";
			FILE *fileBin = _wfopen(binFileLocation.c_str(), L"wb");
			int sampleMin = dotChannel.getIndexClosestToTime(i);
			int sampleMax = dotChannel.getIndexClosestToTime(i + expectedWidth);
			fwrite(&dotChannel[sampleMin], sizeof(LibCore::Event), sampleMax - sampleMin, fileBin);
			fclose(fileBin);
			graphStr += nevroreport::create_node_borland("dot", binFileLocation);

			//line

			//choose average freq. channel and pump it out
			LibCore::CEventChannel lineChannel = meas->getEventChannel(newForm->getNumEventChannel()-1);

			binFileLocation = rootReportFolder + "\\bin" +j+"_"+i+"_line.bin";
			fileBin = _wfopen(binFileLocation.c_str(), L"wb");
			sampleMin = lineChannel.getIndexClosestToTime(i);
			sampleMax = lineChannel.getIndexClosestToTime(i + expectedWidth);
			fwrite(&lineChannel[sampleMin], sizeof(LibCore::Event), sampleMax - sampleMin, fileBin);
			fclose(fileBin);

			graphStr += nevroreport::create_node_borland("line", binFileLocation);

			//annotation
			//choose annotation channel and pump it out
			std::string annText;
			int numnum = meas->getNumAnnotationChannels();
			for(int annchann = 0; annchann < meas->getNumAnnotationChannels(); annchann++){
				if( meas->getAnnotationChannel(annchann).getChannelName().AnsiCompare(meas->markedChName) == 0)
					continue;
				for(int ann = 0; ann < meas->getAnnotationChannel(annchann).getNumElems(); ann++){
					LibCore::Annotation tempAnn = meas->getAnnotationChannel(annchann)[ann];

					std::ostringstream oss;
					oss << "<ann x=" << tempAnn.time << "\" y=\"" << tempAnn.value << "\">";
					annText += oss.str();
					annText += unicode2base64(tempAnn.annotation);
					annText += "</ann>\n";

				}
			}
			graphStr += nevroreport::create_node("annotation", annText);

			if(i+expectedWidth >= newForm->getDuration())
			{
				graphStr += "<divider/>\n";
			}
			temp += nevroreport::create_node("graph", graphStr, false);
        }
        newForm->Close();
		delete newForm;
    }

	reporter.add_root_node("measurementGraphs", temp);


    int numAnnotationEvents[] ={0,0,0,0,0,0,0}; //0, 0 shown, 1, 1 shown, 2, 2 shown, counter of custom
	UnicodeString commentsString = "";

	//annotationGraphs
    temp = "";
	int annotationChar = 'a'-1;
    int annotationNumbering = 0;
    for(int i=0;i<numOpenedFiles;i++){
		int j = sortedIndex[i];
        TDocumentForm * newForm = new TDocumentForm(this);
		newForm->LoadMeasurement(openedFiles[j].getFileLocation());
        LibCore::CMeasurement *meas = newForm->getMeasurementPointer();

        int previousTime = -10e10;
		double lengthAroundAnnotation = 4*60; //4 minutes around annotation show
		for(int annchann = 0; annchann < meas->getNumAnnotationChannels(); annchann++){
			if(meas->getAnnotationChannel(annchann).getChannelName().AnsiCompare(meas->markedChName) == 0)
					continue;
			for(int ann = 0; ann < meas->getAnnotationChannel(annchann).getNumElems(); ann++){

				if(!ReportInfoForm->reportActivityCheckbox->Checked && annotationNumbering+1 > allNumEvents)
					continue;
				LibCore::Annotation tempAnn = meas->getAnnotationChannel(annchann)[ann];

				int annCounter = (int)(tempAnn.value);
				annCounter = std::max(0,std::min(2, annCounter));
				numAnnotationEvents[2*annCounter]++;
				if(!ReportInfoForm->reportActivityCheckbox->Checked && tempAnn.value < 1)
					continue;
				if(previousTime + lengthAroundAnnotation > tempAnn.time)
					continue;
				annotationNumbering++;
				numAnnotationEvents[1+2*annCounter]++;

				if(tempAnn.value > 1.0 && numAnnotationEvents[6] < 5){
					commentsString += "\n" + tempAnn.annotation;
					numAnnotationEvents[6]++;
				}

				//graph
				UnicodeString title =  "Measurement: " +  UnicodeString(i+1) +
					"" + tempAnn.annotation +
					" at: " + (meas->getDate() + tempAnn.time/86400).FormatString("yyyy mmm dd, hh:nn:ss");
				std::string graphstr = nevroreport::create_node_borland("title", title);

				AnsiString substr, substr1;
				int lenSubstr;
              	AnsiString hw ="", sw="";
				bool certified = false;
				if(meas->getGadgetVersion().Length() > 0){
					hw = meas->getGadgetVersion();
					try{
						hw = hw.SubString(1, hw.Pos("git")-3);
						int x1 =  hw.Pos("fw=")+3, x2 = 5;
						AnsiString fw =  hw.SubString(x1,x2);
						x1 = hw.Pos("hw=")+4;
						x2 = 9;
						AnsiString hw1 =  hw.SubString(x1,x2);
						auto r1 = fw.AnsiCompare("1.0.4"), r2 = hw1.AnsiCompare("Savvy 1.2");
						if( 0 < r1){
							if( 0 < r2){
								certified = true;

							}else{
								certified = false;
							}
						}else{
							certified = false;
						}
					}catch(...){
						certified = false;
					}
					hw = meas->getGadgetMac() + "/" + hw;
					graphstr += nevroreport::create_node_borland("gadgetVersion", hw);
				}
				if(meas->getMobEcgVersion().Length() > 0){
					sw = meas->getMobEcgVersion();
					try{
					  sw = sw.SubString(sw.Pos("MobECG ")+7, 100);
					  sw = sw.SubString(1, sw.Pos("-")-1);
					}catch(...){
						certified = false;
					}

					if(certified && (0 < sw.AnsiCompare("1.8.1"))){
						sw += " [EN ISO 60601-2-47:2012]";
					}
					graphstr += nevroreport::create_node_borland("MobEcgVersion", sw);
				}

				graphstr += nevroreport::create_node_d("frequency", meas->getSamplingRate());
				LibCore::CChannel sampleChannel = (*meas)[0];
				graphstr += nevroreport::create_node_d("errorValue", sampleChannel.getMinValue());

                int startIndex = std::min(sampleChannel.getNumElems(), std::max(0, (int)((tempAnn.time - lengthAroundAnnotation)*meas->getSamplingRate())));
                int stopIndex =  std::min(sampleChannel.getNumElems(), std::max(0, (int)((tempAnn.time + lengthAroundAnnotation)*meas->getSamplingRate())));
				graphstr += nevroreport::create_node_borland("timestamp", (meas->getDate() + tempAnn.time/86400).FormatString("yyyy mm dd hh:nn:ss"));
                graphstr += nevroreport::create_node_d("startOffset", meas->sampleIndex2Time(startIndex) - tempAnn.time);

				UnicodeString binFileLocation = rootReportFolder + "\\bin" +j+"_"+ann+"_samples.bin";
				FILE *fileBin = _wfopen(binFileLocation.c_str(), L"wb");
                fwrite(&sampleChannel[startIndex], sizeof(double), stopIndex-startIndex, fileBin);
                fclose(fileBin);
				graphstr += nevroreport::create_node_borland("data", binFileLocation);

                temp += nevroreport::create_node("graph", graphstr, false);

            }
		}
        newForm->Close();
        delete newForm;
	}
	reporter.add_root_node("annotationGraphs", temp);

	//add annotation
	AnsiString correction =  Format(" <activities num=\"%d\" shown=\"%d\"></activities>\n", ARRAYOFCONST((numAnnotationEvents[0], numAnnotationEvents[1])));
	correction +=  Format(" <health num=\"%d\" shown=\"%d\"></health>\n", ARRAYOFCONST((numAnnotationEvents[2], numAnnotationEvents[3])));
	correction +=  Format(" <user num=\"%d\" shown=\"%d\">%s</user>\n", ARRAYOFCONST((numAnnotationEvents[4], numAnnotationEvents[5], unicode2base64(commentsString).c_str())));
	reporter.add_root_node_borland("annotations", correction);

	reporter.print_to_file(file, ReportInfoForm->AuthorInp->Text);
    fclose(file);

    //   Create pdf
	UnicodeString convFilename; //conversion program name
	convFilename = Application->ExeName;
	convFilename.Delete(convFilename.LastDelimiter("\\\\"),100);
	convFilename +="\\resources\\NevroReportToPDF.exe";
	TSearchRec sr;

    SHELLEXECUTEINFO info = {0};
	info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = NULL;
	info.lpVerb = NULL;
	info.lpFile = convFilename.c_str(); //program

	UnicodeString pathToPDF = rootFolder+"\\report_"+TDateTime::CurrentDateTime().FormatString("yyyy_mm_dd-hh_nn_ss")+".pdf";
	UnicodeString params = "\"" +rootReportFolder+"\"" + " \""+pathToPDF+"\"";
	info.lpParameters = params.c_str(); //file to convert

	UnicodeString rootFolderUnicode = UnicodeString(rootFolder);
	info.lpDirectory = rootFolderUnicode.c_str(); //folder
	info.nShow = SW_HIDE;
	info.hInstApp = NULL;
	ShellExecuteEx(&info);

	WaitForSingleObject(info.hProcess,INFINITE);

	unsigned long exitCode = 0;
	GetExitCodeProcess(info.hProcess, &exitCode);
	TerminateProcess(info.hProcess,0);
	CloseHandle(info.hProcess);

    UnicodeString exitString;
    if(exitCode == 0){
		exitString = gettext("Report successfully created in the file") + " " + pathToPDF + ". " + gettext("Do you want to open it?");
		if(IDYES == Application->MessageBox(exitString.c_str(), gettext("Completed successfully").data(), MB_YESNO)){
             ShellExecute(NULL, L"open", pathToPDF.c_str(), NULL, NULL, SW_SHOWDEFAULT);
        }
        TSearchRec sr;

    	if (FindFirst(rootReportFolder+"\\*", faReadOnly, sr) == 0)
	    {
		    do{
                DeleteFile(rootReportFolder+"\\"+sr.Name);                
			} while (FindNext(sr) == 0);
	    	FindClose(sr);
    	}
        RemoveDir(rootReportFolder);
    }else{
		exitString = gettext("Report not created.")+"\n("+gettext("more info")+": "+rootReportFolder+"/log.txt)";
		Application->MessageBox(exitString.c_str(), L"ERROR", MB_OK);
    }
}

void TFolderViewForm::drawOpenedFilesSimple(int numFiles){
    double xLeft = GanttChart->BottomAxis->Minimum;
    double xRight = GanttChart->BottomAxis->Maximum;
    double circSize = (xRight-xLeft) / 200;

    GanttChart->RemoveAllSeries();
    double start, stop;
	for(int j=0; j< numFiles;j++){
        int i = sortedIndex[j];
		start = (double) openedFiles[i].getDate();
		stop = 0;

        double percentValidMeasurement = 100;
		for(int k=0; k < openedFiles[i].numStatData; k++){
			if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"part length"))
                stop += start + (openedFiles[i].statDataData[k]-1)/SECONDS_IN_A_DAY;
            if(0 == AnsiCompareStr(openedFiles[i].statDataString->Strings[k],"part missing data length"))
                stop -= (openedFiles[i].statDataData[k]/SECONDS_IN_A_DAY);
        }
        if(stop<=start)
            stop = start + (openedFiles[i].getDurationFromFile() - 1)/SECONDS_IN_A_DAY;

		if(openedFiles[i].numStatData < 1) //no statistical data written - no color based on RR
            continue; 
        else{
            int rrIndex = openedFiles[i].statDataString->IndexOf("BPM AVG");
            if(rrIndex < 0) //no information about RRI
                continue;
            else{
                double BpmAvg = openedFiles[i].statDataData[rrIndex];
                rrIndex = openedFiles[i].statDataString->IndexOf("BPM STD");
				if(rrIndex>=0){
                    double BpmStd = openedFiles[i].statDataData[rrIndex];
                    //draw "line"

                    TChartShape *line = new TChartShape(GanttChart);
                    line->XValues->DateTime = true;
                    line->Style = chasRectangle;
                    line->Pen->Color = clBlack;
					line->Brush->Color = clBlack;
                    line->XYStyle = xysAxis;
                    line->X0 = (start+stop)/2 - circSize/20;
                    line->X1 = (start+stop)/2 + circSize/20;
                    double pad = BpmStd * BpmAvg;
                    line->Y0 = BpmAvg - pad;
                    line->Y1 = BpmAvg + pad;
                    line->ShowInLegend = false;
                    line->ParentChart = GanttChart;

                }

                //draw circle
                TChartShape *circle = new TChartShape(GanttChart);
                circle->XValues->DateTime = true;
				circle->Style = chasCircle;
                circle->Brush->Color = clBlack;
                circle->XYStyle = xysAxis;
                circle->X0 = (start+stop)/2 - circSize;
                circle->X1 = (start+stop)/2 + circSize;
                circle->Y0 = BpmAvg - 2;
                circle->Y1 = BpmAvg + 2;
                circle->ShowInLegend = false;
                circle->ParentChart = GanttChart;

            }
        }
	}

	GanttChart->LeftAxis->Automatic = false;
    GanttChart->LeftAxis->Minimum = 0;
    GanttChart->LeftAxis->Maximum = 249;
    GanttChart->LeftAxis->LabelsOnAxis = true;     
	GanttChart->LeftAxis->Visible = true;
    GanttChart->LeftAxis->Labels = true;
    GanttChart->LeftAxis->Title->Caption = ""; 
    GanttChart->LeftAxis->Increment = 25;
    GanttChart->LeftAxis->LabelsFont->Size = 24;
    GanttChart->LeftAxis->Title->Caption = gettext("BPM");
    GanttChart->LeftAxis->Title->Font->Size = 24;

    GanttChart->BottomAxis->Visible = true;
    GanttChart->BottomAxis->ExactDateTime = true;
    GanttChart->BottomAxis->Automatic = false;
    GanttChart->BottomAxis->LabelsFont->Size = 24;
    GanttChart->BottomAxis->DateTimeFormat = "d.m.y h:mm";
    GanttChart->BottomAxis->Minimum = TDateTime(xLeft);
    GanttChart->BottomAxis->Maximum = TDateTime(xRight);

    if(GanttChart->BottomAxis->Maximum < Series1->MinXValue())
        GanttChart->BottomAxis->Maximum = Series1->MinXValue();
    GanttChart->BottomAxis->Minimum = Series1->MinXValue();
    GanttChart->BottomAxis->Maximum = Series1->MaxXValue();    
}


//---------------------------------------------------------------------------



void __fastcall TFolderViewForm::Odprimapo1Click(TObject *Sender)
{

	ShellExecute(NULL, L"open", rootFolder.c_str(), NULL, NULL, SW_SHOWDEFAULT);
}
//---------------------------------------------------------------------------


void __fastcall TFolderViewForm::Generirajporoilo1dogodek1Click(
	  TObject *Sender)
{
	UnicodeString rootReportFolder = rootFolder+"\\reportEvents_"+TDateTime::CurrentDateTime().FormatString("yyyy_mm_dd-hh_nn_ss");
	UnicodeString indexfileloc = rootReportFolder + "\\indexEvent.txt";
	CreateDirectory(rootReportFolder.c_str(), NULL);

	FILE *file = _wfopen(indexfileloc.c_str(), L"wb");
	if (file == NULL)
		return;

	std::string author, user, age, weight, born, sex, electrodePosition, reportLength;

	double timeAround = 4;
	double report_ecgLineLength = 15;
	double report_ecgLineMvHeight = 10;
	double report_ecgLineNumMv = 3;
	double report_ecgLineSecondLength = 1.25;

	UnicodeString settingsFname("nastavitve.ini");
	for (int i = Application->ExeName.Length()-1; i >= 0; --i){
		if (Application->ExeName[i] == '\\') {
			settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
			break;
		}
	 }
	Ini::File ini(settingsFname.c_str());
	ini.loadVar(author, L"report author");
	ini.loadVar(user, L"report user");
	ini.loadVar(age, L"report age");
	ini.loadVar(weight, L"report weight");
	ini.loadVar(sex, L"report user_sex");
	ini.loadVar(born, L"report user_born");
	ini.loadVar(timeAround, L"event time length");
	ini.loadVar(electrodePosition, L"ecg position");
	ini.loadVar(reportLength, L"event time length");

	try{
		ini.loadVar(report_ecgLineLength, L"report ecg line length");
		ini.loadVar(report_ecgLineMvHeight, L"report ecg mV height");
		ini.loadVar(report_ecgLineNumMv, L"report ecg mV shown");
		ini.loadVar(report_ecgLineSecondLength, L"report ecg length of second");
	}catch(Exception *e){
	}
	EventReportSetting->AuthorInp->Text 	= string2WideString(author.c_str());
	EventReportSetting->UserInp->Text   	= string2WideString(user.c_str());
	EventReportSetting->AgeInp->Text    	= string2WideString(age.c_str());
	EventReportSetting->WeightInp->Text 	= string2WideString(weight.c_str());
	EventReportSetting->timeMinutes->Text 	= AnsiString(timeAround);
	EventReportSetting->GenderInp->Text 	= string2WideString(sex.c_str());
	EventReportSetting->BirthInp->Text 		= string2WideString(born.c_str());
	EventReportSetting->PositionInp->Text 	= string2WideString(electrodePosition.c_str());

	EventReportSetting->Edit2->Text = UnicodeString(report_ecgLineLength);
	EventReportSetting->Edit3->Text = UnicodeString(report_ecgLineSecondLength);
	EventReportSetting->Edit4->Text = UnicodeString(report_ecgLineNumMv);
	EventReportSetting->Edit5->Text = UnicodeString(report_ecgLineMvHeight);

	if(EventReportSetting->setFiles(numOpenedFiles, openedFiles, sortedIndex) < 1){
        ShowMessage(gettext("There is no applicable annotations to choose!"));
        fclose(file);
        return;
    }



	std::vector<TEventReportSetting::EventSlot> selected;

	while(true){
		if(EventReportSetting->ShowModal()!=mrOk){
			fclose(file);
			TSearchRec sr;
			if (FindFirst(rootReportFolder+"\\*", faReadOnly, sr) == 0)
			{
				do{
					DeleteFile(rootReportFolder+"\\"+sr.Name);
				} while (FindNext(sr) == 0);
				FindClose(sr);
			}
			RemoveDir(rootReportFolder);
			return;
		}else{
			selected = EventReportSetting->getSelectedEvents();
			if(selected.size() > 0)
				break;
			else
				Application->MessageBox(gettext("Annotation must be selected!").data(), gettext("Warning").data(), MB_OK);
		}
	}

	author = wideString2String(EventReportSetting->AuthorInp->Text);
	user = wideString2String(EventReportSetting->UserInp->Text);

	try {
		sex = 				wideString2String(EventReportSetting->GenderInp->Text);
		born  = 			wideString2String(EventReportSetting->BirthInp->Text);
		electrodePosition = wideString2String(EventReportSetting->PositionInp->Text);
		timeAround = 		std::max(0.25, EventReportSetting->timeMinutes->Text.ToDouble());
		age = 				wideString2String(EventReportSetting->AgeInp->Text);
		weight = 			wideString2String(EventReportSetting->WeightInp->Text);
		reportLength = 		AnsiString(EventReportSetting->timeMinutes->Text).c_str();
	} catch(Exception *e) {
		// wrong version for parse.. use defaults
		age = "-1";
		weight = "-1";
		reportLength = 4;
	}

	try{
		report_ecgLineLength = 			EventReportSetting->Edit2->Text.ToDouble();
		report_ecgLineSecondLength = 	EventReportSetting->Edit3->Text.ToDouble();
		report_ecgLineNumMv = 			EventReportSetting->Edit4->Text.ToDouble();
		report_ecgLineMvHeight = 		EventReportSetting->Edit5->Text.ToDouble();
	}catch(Exception *e){

	}

	ini.storeVar(author, L"report author");
	ini.storeVar(user, L"report user");
	ini.storeVar(age, L"report age");
	ini.storeVar(weight, L"report weight");
	ini.storeVar(reportLength, L"event time length");
	ini.storeVar(sex, L"report user_sex");
	ini.storeVar(born, L"report user_born");
	ini.storeVar(electrodePosition, L"ecg position");
	ini.storeVar(report_ecgLineLength, L"report ecg line length");
	ini.storeVar(report_ecgLineMvHeight, L"report ecg mV height");
	ini.storeVar(report_ecgLineNumMv, L"report ecg mV shown");
	ini.storeVar(report_ecgLineSecondLength, L"report ecg length of second");
	ini.save();

	//draw leading images
	//UnicodeString imgloc = rootReportFolder+"\\overview.wmf";
	//UnicodeString imglocdeviation = rootReportFolder+"\\meanAndDev.wmf";
	//GanttChart->SaveToMetafile(imgloc);
	//GanttChart->SaveToMetafile(imglocdeviation);

	// create index file
	nevroreport::ReportPrinter reporter;
	//patient
	std::string temp = nevroreport::create_node_borland("name", EventReportSetting->UserInp->Text);
	temp += nevroreport::create_node_borland("age", EventReportSetting->AgeInp->Text);
	temp += nevroreport::create_node_borland("weight", EventReportSetting->WeightInp->Text);
	temp += nevroreport::create_node_borland("born", EventReportSetting->BirthInp->Text);
	temp += nevroreport::create_node_borland("gender", EventReportSetting->GenderInp->Text);
	temp += nevroreport::create_node_borland("ECGposition", EventReportSetting->PositionInp->Text);
	reporter.add_root_node("patient", temp);

	//settings
	temp = nevroreport::create_node_d("ecgLineLength", report_ecgLineLength);
	temp += nevroreport::create_node_d("ecgLineMvHeight", report_ecgLineMvHeight);
	temp += nevroreport::create_node_d("ecgLineNumMv", report_ecgLineNumMv);
	temp += nevroreport::create_node_d("ecgLineSecondLength", report_ecgLineSecondLength);
	reporter.add_root_node("settings", temp);

	//comment
	reporter.add_root_node_borland("userComment", EventReportSetting->Memo1->Text);


	//event
	temp = "";
	if(selected.size() == 1){
		int eventIndex = 0; //choose ¸only checked event
		LibCore::CMeasurement * meas = &openedFiles[selected[eventIndex].measurement_index];
		LibCore::Annotation tempAnn = meas->getAnnotationChannel(selected[eventIndex].channel_index)[selected[eventIndex].event_index];

		UnicodeString folName = rootFolder;
		folName = folName.SubString(1+folName.LastDelimiter("\\"), folName.Length());

		temp += nevroreport::create_node_borland("name", tempAnn.annotation);

		temp += nevroreport::create_node_borland("time", (meas->getDate() + tempAnn.time/86400).FormatString("yyyy mmm dd; hh:nn:ss"));
		temp += nevroreport::create_node_d("measurementTime", tempAnn.time);

		UnicodeString fileName = meas->getFileLocation();
		int locationOfSlash = 1 + fileName.LastDelimiter("\\");
		fileName = fileName.SubString(locationOfSlash, fileName.Length());
		temp += nevroreport::create_node_borland("filename", fileName);
		temp += nevroreport::create_node_borland("fileStart", meas->getDate().FormatString("yyyy mmm dd; hh:nn:ss"), false);
	}else{
		temp += nevroreport::create_node_borland("name", UnicodeString('[') + UnicodeString(selected.size()) + " events]");
		temp += nevroreport::create_node("time", " - ");
		temp += nevroreport::create_node("measurementTime", " - ", false);
		temp += nevroreport::create_node("filename", "");
		temp += nevroreport::create_node("fileStart", "");

	}
	reporter.add_root_node("event", temp);

	//annotationGraphs
    temp="";
	for(int eventIndex=0; eventIndex<selected.size(); eventIndex++){
		LibCore::CMeasurement * meas = &openedFiles[selected[eventIndex].measurement_index];
		TDocumentForm * newForm = new TDocumentForm(this);
		newForm->LoadMeasurement(meas->getFileLocation());

		meas = newForm->getMeasurementPointer();

		LibCore::Annotation tempAnn = meas->getAnnotationChannel(selected[eventIndex].channel_index)[selected[eventIndex].event_index];

        //graphs
		std::string graphStr;
		UnicodeString title = "" + tempAnn.annotation +
						" at: " + (meas->getDate() + tempAnn.time/86400).FormatString("yyyy mmm dd, hh:nn:ss");
        graphStr = nevroreport::create_node_borland("title", title);

		AnsiString substr, substr1;
		int lenSubstr;
		AnsiString hw ="", sw="";
		bool certified = false;
		if(meas->getGadgetVersion().Length() > 0){
			hw = meas->getGadgetVersion();
			try{
				hw = hw.SubString(1, hw.Pos("git")-3);
				int x1 =  hw.Pos("fw=")+3, x2 = 5;
				AnsiString fw =  hw.SubString(x1,x2);
				x1 = hw.Pos("hw=")+4;
				x2 = 9;
				AnsiString hw1 =  hw.SubString(x1,x2);
				auto r1 = fw.AnsiCompare("1.0.4"), r2 = hw1.AnsiCompare("Savvy 1.2");
				if( 0 < r1){
					if( 0 < r2){
						certified = true;

					}else{
						certified = false;
					}
				}else{
					certified = false;
				}
			}catch(...){
				certified = false;
			}
			hw = meas->getGadgetMac() + "/" + hw;
	        graphStr += nevroreport::create_node_borland("gadgetVersion", hw);
		}
		if(meas->getMobEcgVersion().Length() > 0){
			sw = meas->getMobEcgVersion();
			try{
			  sw = sw.SubString(sw.Pos("MobECG ")+7, 100);
			  sw = sw.SubString(1, sw.Pos("-")-1);
			}catch(...){
				certified = false;
			}

			if(certified && (0 < sw.AnsiCompare("1.8.1"))){
				sw += " [EN ISO 60601-2-47:2012]";
			}
	        graphStr += nevroreport::create_node_borland("MobEcgVersion", sw);
		}

		graphStr += nevroreport::create_node_d("frequency", meas->getSamplingRate());
		LibCore::CChannel sampleChannel = (*meas)[0];
		graphStr += nevroreport::create_node_d("errorValue", sampleChannel.getMinValue());
		int startIndex = std::min(sampleChannel.getNumElems(), std::max(0, (int)((tempAnn.time - timeAround*60)*meas->getSamplingRate())));
		int stopIndex =  std::min(sampleChannel.getNumElems(), std::max(0, (int)((tempAnn.time + timeAround*60)*meas->getSamplingRate())));
		graphStr += nevroreport::create_node_borland("timestamp", (meas->getDate() + tempAnn.time/86400).FormatString("yyyy mm dd hh:nn:ss"));
		graphStr += nevroreport::create_node_d("startOffset", meas->sampleIndex2Time(startIndex) - tempAnn.time);

		UnicodeString binFileLocation = rootReportFolder + "\\bin" +eventIndex+"_EVENT_SAMPLES_samples.bin";
		FILE *fileBin = _wfopen(binFileLocation.c_str(), L"wb");
		fwrite(&sampleChannel[startIndex], sizeof(double), stopIndex-startIndex, fileBin);
		fclose(fileBin);
		graphStr += nevroreport::create_node_borland("data",binFileLocation);

		newForm->Close();
		delete newForm;

		temp += nevroreport::create_node("graph", graphStr, false);
	}

	reporter.add_root_node("annotationGraphs", temp);

	reporter.print_to_file(file, EventReportSetting->AuthorInp->Text);
	fclose(file);

	//   Create pdf
	UnicodeString convFilename; //conversion program name
	convFilename = Application->ExeName;
	convFilename.Delete(convFilename.LastDelimiter("\\\\"),100);
	convFilename +="\\resources\\NevroReportToPDF.exe";
	TSearchRec sr;

    SHELLEXECUTEINFO info = {0};
	info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = NULL;
	info.lpVerb = NULL;
	info.lpFile = convFilename.c_str(); //program

    AnsiString tempAnnAnnotation_ASCII_ONLY  = "";

	UnicodeString pathToPDF = rootReportFolder + "_document" + ".pdf";

	UnicodeString params = "\"" +rootReportFolder+"\"" + " \""+ pathToPDF + "\"";
	info.lpParameters = params.c_str(); //file to convert

    info.lpDirectory = rootFolder.c_str(); //folder
	info.nShow = SW_HIDE;
	info.hInstApp = NULL;
	ShellExecuteEx(&info);

	WaitForSingleObject(info.hProcess,INFINITE);

	unsigned long exitCode = 0;
	GetExitCodeProcess(info.hProcess, &exitCode);
	TerminateProcess(info.hProcess,0);
	CloseHandle(info.hProcess);

    UnicodeString exitString;
    if(exitCode == 0){

		exitString = gettext("Report successfully created in the file")+ " " + pathToPDF +
			". " + gettext("Do you want to open it?");
		if(IDYES == Application->MessageBox(exitString.c_str(), gettext("Completed successfully").data(), MB_YESNO)){
			 ShellExecute(NULL, L"open", pathToPDF.c_str(), NULL, NULL, SW_SHOWDEFAULT);
        }

        TSearchRec sr;

    	if (FindFirst(rootReportFolder+"\\*", faReadOnly, sr) == 0)
	    {
		    do{
				DeleteFile(rootReportFolder+"\\"+sr.Name);
    		} while (FindNext(sr) == 0);
	    	FindClose(sr);
    	}
        RemoveDir(rootReportFolder);
    }else{
		exitString = gettext("Report was not created in folder") + " " +
			rootReportFolder + gettext(" ;") + gettext("exitCode") + ": " + AnsiString(exitCode);
        if(exitCode)
			exitString = gettext("Report not created. Please close applications that use file!");
		Application->MessageBox(exitString.c_str(), gettext("Error").data(), MB_OK);
	}
}
//---------------------------------------------------------------------------

bool TFolderViewForm::checkFilesChangedAfterDate(UnicodeString rootFolder){

	FILETIME currentTime;
	GetSystemTimeAsFileTime(&currentTime);
    TSearchRec sr;
    int searchFor = faReadOnly;
    if (FindFirst(rootFolder+"\\*.nekge", searchFor, sr) == 0)
	{
		do{
			UnicodeString name = rootFolder + "\\" + sr.Name;
			HANDLE hFile = CreateFile(name.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
			FILETIME ftCreate, ftAccess, ftWrite;
			//Retrieve the file times for the file.
			if(hFile != INVALID_HANDLE_VALUE){
				GetFileTime(hFile, &ftCreate, &ftAccess, &ftWrite);
				CloseHandle(hFile);
			}

			if(CompareFileTime(&lastUpdate, &ftWrite) < 0
			&& CompareFileTime(&currentTime, &ftWrite) >= 0){
				return true;
			}

		} while (FindNext(sr) == 0);
		FindClose(sr);
	}
    return false;
}

void __fastcall TFolderViewForm::setGeneralHint() {
    MainForm->StatusLine->SimpleText = gettext("Use CTRL + mouse wheel to zoom. Navigation in time with mouse wheel.");
}

void __fastcall TFolderViewForm::FormActivate(TObject *Sender)
{
	//check if any file changed after last update
	if(rootFolder == NULL)
		return;
	if(checkFilesChangedAfterDate(rootFolder)){
		LoadFolder(rootFolder);
	}
	GetSystemTimeAsFileTime(&lastUpdate);
    GanttChart->Refresh();
}
//---------------------------------------------------------------------------

