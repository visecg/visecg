// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "fftForm1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "CSPIN"

#pragma resource "*.dfm"

#include <sstream>
#include <fstream>
#include <string>
#include <iomanip>

#include "gnugettext.hpp"

TFFTform *FFTform;
//---------------------------------------------------------------------------
__fastcall TFFTform::TFFTform(TComponent* Owner)
	: TForm(Owner), pMeasurement(NULL)
{
    pDFTChannel1 = pDFTChannel2 = pCoherenceChannel = pPhiChannel = NULL;
    disableSpinEdits = true;
	loadFreqFilterDefaults();
	disableSpinEdits = false;
    graphYScale = 0.0;
}


//---------------------------------------------------------------------------
void __fastcall TFFTform::FormResize(TObject *Sender)
{
	Chart->Width = ClientWidth - Chart->Left -5;
	Chart->Height = ClientHeight - Chart->Top - 5;
    ResizerLabel->Left = ClientWidth - ResizerLabel->Width;
    ResizerLabel->Top = ClientHeight - ResizerLabel->Height;
    AutoSize = true;
}
//---------------------------------------------------------------------------


void __fastcall TFFTform::FormShow(TObject *Sender)
{
	initForm();
}


template <typename T>
AnsiString format(AnsiString formatString, T value)
{
	TVarRec varValue(value);
	return AnsiString::Format(formatString, &varValue, 0);
}


void TFFTform::initForm() {
	if (coherence)
	{
		pDFTChannel1 = &(pMeasurement->getEventChannel(pMeasurement->getNumEventChannels()-5));
		pDFTChannel2 = &(pMeasurement->getEventChannel(pMeasurement->getNumEventChannels()-4));
		pPhiChannel = &(pMeasurement->getEventChannel(pMeasurement->getNumEventChannels()-3));      
        pAlphaChannel = &(pMeasurement->getEventChannel(pMeasurement->getNumEventChannels()-2));
		pCoherenceChannel = &(pMeasurement->getEventChannel(pMeasurement->getNumEventChannels()-1));
        EditScale->Visible = false;
        ResetScale->Visible = false;
        ScaleMaxLabel->Visible = false;
        RadioButtonAngle->Visible = true;
        RadioButtonAlpha->Visible = true;
	} else {
		pDFTChannel1 = &(pMeasurement->getEventChannel(pMeasurement->getNumEventChannels()-1));
        EditScale->Visible = true;
        ResetScale->Visible = true;
        ScaleMaxLabel->Visible = true;
        RadioButtonAngle->Visible = false;
        RadioButtonAlpha->Visible = false;
	}

	// set spin edit controls' limits for VL, L and H frequency bands
	CSpinEdit1->MinValue = 0;
	CSpinEdit1->MaxValue = 2000;
	CSpinEdit2->MinValue = 0;
	CSpinEdit2->MaxValue = 2000;
	CSpinEdit3->MinValue = 0;
	CSpinEdit3->MaxValue = 2000;
	CSpinEdit4->MinValue = 0;
	CSpinEdit4->MaxValue = 2000;
	CSpinEdit5->MinValue = 0;
	CSpinEdit5->MaxValue = 2000;
	CSpinEdit6->MinValue = 0;
	CSpinEdit6->MaxValue = 2000;

	// set spin edit contol's values (from frequency filter parameters variable)
	CSpinEdit5->Value = freqFilterParams.VLFLow*1000.0 + 0.5;
	CSpinEdit6->Value = freqFilterParams.VLFHigh*1000.0 + 0.5;
	CSpinEdit3->Value = freqFilterParams.LFLow*1000.0 + 0.5;
	CSpinEdit4->Value = freqFilterParams.LFHigh*1000.0 + 0.5;
	CSpinEdit1->Value = freqFilterParams.HFLow*1000.0 + 0.5;
	CSpinEdit2->Value = freqFilterParams.HFHigh*1000.0 + 0.5;

	// update graph
	if (Visible) {
		//pMeasurement->redrawFourierTransform(Chart, trackAveragingColumns->Position, coherence);
        redrawMeasurement(*pMeasurement, trackAveragingColumns->Position, true);
	}

	// pdate data table
	fillParamsTable();
}


void TFFTform::setGraphYScale(double scale) {
    EditScale->Text = AnsiString(scale);
}


void TFFTform::drawPhiGraph(LibCore::CMeasurement& measurement) {
    TFastLineSeries *phiSeries = new TFastLineSeries(Chart);
	phiSeries->Title = "as ";
	phiSeries->ParentChart = Chart;
	phiSeries->LinePen->Color = clGreen;
	phiSeries->LinePen->Width = 3;
    phiSeries->VertAxis = aRightAxis;

    Chart->RightAxis->Automatic = false;
    Chart->RightAxis->AutomaticMaximum = false;
    Chart->RightAxis->AxisValuesFormat = "# ##0.#######\"�\"";
    Chart->RightAxis->Minimum = -180.0;
    Chart->RightAxis->Maximum = 180.0;
    Chart->RightAxis->Increment = 30;
    
    LibCore::CEventChannel& PCh = *pPhiChannel;
	for (int bar = 0; (bar < PCh.getNumElems()) && (PCh[bar].time <= pDFTParams->maxFrequency); bar++)
		phiSeries->AddXY(PCh[bar].time, PCh[bar].value*180/M_PI, "", clTeeColor);
}


void TFFTform::drawAlphaGraph(LibCore::CMeasurement& measurement) {
    TFastLineSeries *alphaSeries = new TFastLineSeries(Chart);
	alphaSeries->Title = "Alpha values";
	alphaSeries->ParentChart = Chart;
	alphaSeries->LinePen->Color = clOlive;
	alphaSeries->LinePen->Width = 3;
	alphaSeries->VertAxis = aRightAxis;

//    Chart->RightAxis->Automatic = true;
    Chart->RightAxis->Minimum = 0.0;     
    Chart->RightAxis->AutomaticMaximum = true;
    Chart->RightAxis->Increment = 0;//(Chart->RightAxis->Maximum - Chart->RightAxis->Minimum) / 10.0;
    Chart->RightAxis->AxisValuesFormat = "#,##0.#######";

	for (int bar = 0; (bar < pAlphaChannel->getNumElems()) && ((*pAlphaChannel)[bar].time <= pDFTParams->maxFrequency); bar++)
		alphaSeries->AddXY((*pAlphaChannel)[bar].time, (*pAlphaChannel)[bar].value*0.01, "", clTeeColor);
                                         
}


void TFFTform::redrawMeasurement(LibCore::CMeasurement& measurement, int smoothingWidth, bool onInit)
{
	assert(Chart);
	assert(measurement.getNumEventChannels() > 0);

	Chart->RemoveAllSeries();
	LibCore::CEventChannel DFTCh = measurement.getEventChannel(measurement.getNumEventChannels()-1);

	//draw the channel
	TBarSeries *series = new TBarSeries(Chart);
	series->Title = coherence ? "DFT" : "coherency";
	series->Marks->Visible = false;
	series->ParentChart = Chart;
	series->SeriesColor = clRed;
	series->BarPen->Color = clRed;

    if (onInit) {
        double average = 0;
        channelStdDev = 0;
       	for (int i = 0; i < DFTCh.getNumElems(); i++)
            average += DFTCh[i].value;

    	average /= DFTCh.getNumElems();
   	    for (int i = 0; i < DFTCh.getNumElems(); i++)
    	    channelStdDev += (average - DFTCh[i].value) * (average - DFTCh[i].value);
        channelStdDev = sqrt(channelStdDev / DFTCh.getNumElems());

        if (!coherence) {
            setGraphYScale(4*channelStdDev);
        }
    }

	for (int bar = 0; (bar < DFTCh.getNumElems()) && (DFTCh[bar].time <= pDFTParams->maxFrequency); bar++)
		series->AddXY(DFTCh[bar].time, DFTCh[bar].value, "", clTeeColor);

    //series->CustomBarWidth = chart->ChartWidth * 3 / ((1+DFTCh.getNumElems()) * 4);

	if (smoothingWidth >= 1)   // for smoothingWidth = 2, halfWindow=2, -> sum over 5 bars
								// 1/3*b[i-2]+2/3*b[i-1]+b[i]+2/3b[i+1]+1/3b[i+2].
	{
		TFastLineSeries *series = new TFastLineSeries(Chart);
		series->Title = coherence ? "DFT with average" : "coherence with average";
		series->ParentChart = Chart;
		series->LinePen->Color = clBlue;
		series->LinePen->Width = 3;

		double sum;
		for ( int bar = smoothingWidth; (bar < DFTCh.getNumElems()-smoothingWidth)  && (DFTCh[bar].time <= pDFTParams->maxFrequency); bar++)
		{
			sum = DFTCh[bar].value;
			for (int avgI = 1; avgI <= smoothingWidth; avgI++)  {
				double w = 1.0 - (double)avgI/(smoothingWidth+1);
				sum += w*DFTCh[bar+avgI].value;
				sum += w*DFTCh[bar-avgI].value;
			}
			series->AddXY(DFTCh[bar].time, sum/(smoothingWidth+1), "", clTeeColor);
		}
	}

	if (coherence) {
        if (RadioButtonAlpha->Checked) drawAlphaGraph(measurement);
        else if (RadioButtonAngle->Checked) drawPhiGraph(measurement);
	}

	/*
	chart->BottomAxis->Minimum = - 0.99/(DFTCh.getNumElems()-1);
	chart->BottomAxis->Maximum = 0.5 + 0.49/(DFTCh.getNumElems()-1);
	*/
    if (onInit) {
    	Chart->BottomAxis->Minimum = 0;
        Chart->BottomAxis->Increment = pDFTParams->maxFrequency * 0.1;

	    Chart->LeftAxis->Maximum = (coherence ? 1.0 : graphYScale);
    	if (coherence) {
	    	Chart->LeftAxis->Increment = 0.5;
		    Chart->RightAxis->Visible = true;
            Chart->BottomAxis->Maximum = pDFTParams->maxFrequency;
    	} else {
	    	Chart->RightAxis->Visible = false;
            Chart->BottomAxis->Maximum = pDFTParams->maxFrequency + pDFTParams->maxFrequency/(DFTCh.getNumElems()-1);
    	}
    }
}


void TFFTform::createAndSaveGraph(AnsiString _fileName) {
	// save to file only works if Chart is visible :(
	Show();
	Chart->Width = 1000;
	Chart->Height = 500;
	Chart->Color = clWhite; 
	saveDFTGraph(_fileName);
	Hide();
}


void TFFTform::fillParamsTable()
{
	assert(pDFTChannel1);
	if (coherence)
	assert(pDFTChannel2 && pCoherenceChannel);

	double TTPS = 0, TPS = 0, VLF = 0, LF = 0, HF = 0;
	double TPS2 = 0, VLF2 = 0, LF2 = 0, HF2 = 0;
	double TTPSC = 0, TPSC = 0, VLFC = 0, LFC = 0, HFC = 0;
	int countTPS = 0, countVLF = 0, countLF = 0, countHF = 0;
	double resol = 1 / (pDFTParams->end - pDFTParams->start);

	for (int i = 0; i < pDFTChannel1->getNumElems(); i++)
	{
		TTPS += (*pDFTChannel1)[i].value;
		double freq = (i+1)*resol;

		if (freq >= freqFilterParams.VLFLow && freq <= freqFilterParams.HFHigh)
			TPS += (*pDFTChannel1)[i].value;
		if (freq >= freqFilterParams.VLFLow && freq <= freqFilterParams.VLFHigh)
			VLF += (*pDFTChannel1)[i].value;
   		if (freq > freqFilterParams.LFLow && freq <= freqFilterParams.LFHigh)
			LF += (*pDFTChannel1)[i].value;
		if (freq > freqFilterParams.HFLow && freq <= freqFilterParams.HFHigh)
			HF += (*pDFTChannel1)[i].value;
	}

	for (int i = 0; i < gridStats->RowCount; i++)
	{
		gridStats->Cells[0][i] = "";
		gridStats->Cells[1][i] = "";
	}

	gridStats->Cells[0][0] = "Number of DFT samples ";
	gridStats->Cells[1][0] = format("%d", 2*pDFTChannel1->getNumElems());

	gridStats->Cells[0][1] = "Measurement length (from-to) [s]";
	char buf[1000];
	sprintf(buf, "%.3lf - %.3lf", pDFTParams->start, pDFTParams->end);
	gridStats->Cells[1][1] = AnsiString(buf);

	gridStats->Cells[0][2] = "Frequency DFT [Hz]";
	gridStats->Cells[1][2] = format("%.1f", pDFTParams->samplingRate);

	gridStats->Cells[0][3] = "Resolution DFT [Hz/line]";
	gridStats->Cells[1][3] = format("%.5e", resol);

	gridStats->ColWidths[0] = gridStats->DefaultColWidth+20;
	gridStats->ColWidths[1] = gridStats->DefaultColWidth-20;

	if (coherence)
	{
		double cohResol = pDFTParams->samplingRate / (pDFTParams->windowWidth - 1);

		//calculate coherence specific values
		for (int i = 0; i < pDFTChannel2->getNumElems(); i++) {
			TTPS += (*pDFTChannel2)[i].value;
			double freq = (i+1)*resol;

			if (freq >= freqFilterParams.VLFLow && freq <= freqFilterParams.HFHigh)
				TPS2 += (*pDFTChannel2)[i].value;
			if (freq >= freqFilterParams.VLFLow && freq <= freqFilterParams.VLFHigh)
				VLF2 += (*pDFTChannel2)[i].value;
			if (freq > freqFilterParams.LFLow && freq <= freqFilterParams.LFHigh)
				LF2 += (*pDFTChannel2)[i].value;
			if (freq > freqFilterParams.HFLow && freq <= freqFilterParams.HFHigh)
				HF2 += (*pDFTChannel2)[i].value;
		}

		for (int i = 0; i < pCoherenceChannel->getNumElems(); i++)
		{
			TTPSC += (*pCoherenceChannel)[i].value;
			double freq = (i+1) * cohResol;

			if (freq >= freqFilterParams.VLFLow && freq <= freqFilterParams.HFHigh) {
				TPSC += (*pCoherenceChannel)[i].value;
				++countTPS;
			}
			if (freq >= freqFilterParams.VLFLow && freq <= freqFilterParams.VLFHigh) {
				VLFC += (*pCoherenceChannel)[i].value;
				++countVLF;
			}
			if (freq > freqFilterParams.LFLow && freq <= freqFilterParams.LFHigh) {
				LFC += (*pCoherenceChannel)[i].value;
				++countLF;
			}
			if (freq > freqFilterParams.HFLow && freq <= freqFilterParams.HFHigh) {
				HFC += (*pCoherenceChannel)[i].value;
				++countHF;
			}
		}

		if (countVLF > 0)
			VLFC /= countVLF;
		if (countLF > 0)
			LFC /= countLF;
		if (countHF > 0)
			HFC /= countHF;

		// search for two highest peaks in LF and HF regions
		double peakFreqLF[2];
		double peakValueLF[2];
		double peakFreqHF[2];
		double peakValueHF[2];
		double peakPhaseLF, peakPhaseHF;

		// preset in order to make the first iteration identical to others
		double value = 0.25*(*pCoherenceChannel)[0].value + 0.5*(*pCoherenceChannel)[1].value + 0.25*(*pCoherenceChannel)[2].value;
		peakFreqLF[0] = peakFreqLF[1] = peakFreqHF[0] = peakFreqHF[1] = 0;
		peakValueLF[0] = peakValueHF[0] = peakValueLF[1] = peakValueHF[1] = 0;

		for (int i = 2; i < pCoherenceChannel->getNumElems() - 1; i++) {
			double freq = (i+1) * cohResol;
			value += 0.25 * ((*pCoherenceChannel)[i].value + (*pCoherenceChannel)[i+1].value -
				(*pCoherenceChannel)[i-1].value - (*pCoherenceChannel)[i-2].value);

			if (freq > freqFilterParams.LFLow && freq <= freqFilterParams.LFHigh) {
				if (value > peakValueLF[0]) {
					peakValueLF[1] = peakValueLF[0];
					peakFreqLF[1] = peakFreqLF[0];
					peakPhaseLF = (*pPhiChannel)[i].value;
					peakValueLF[0] = value;
					peakFreqLF[0] = freq;
				} else if (value > peakValueLF[1]) {
					peakValueLF[1] = value;
					peakFreqLF[1] = freq;
				}
			}
			if (freq > freqFilterParams.HFLow && freq <= freqFilterParams.HFHigh) {
				if (value > peakValueHF[0]) {
					peakValueHF[1] = peakValueHF[0];
					peakFreqHF[1] = peakFreqHF[0];
					peakPhaseHF = (*pPhiChannel)[i].value;
					peakValueHF[0] = value;
					peakFreqHF[0] = freq;
				} else if (value > peakValueHF[1]) {
					peakValueHF[1] = value;
					peakFreqHF[1] = freq;
				}
			}
		}

		// fill in rows
		gridStats->RowCount = 24;
		gridStats->Cells[0][4] = "";
		gridStats->Cells[0][5] = gettext("coherence");
		gridStats->Cells[0][6] = gettext("Window number");
		gridStats->Cells[1][6] = format("%d", pDFTParams->numberOfWindows);
		gridStats->Cells[0][7] = gettext("Window width [pattern]");
		gridStats->Cells[1][7] = format("%d", pDFTParams->windowWidth);
		gridStats->Cells[0][8] = gettext("Overlap [window]");
		gridStats->Cells[1][8] = format("%.2f", pDFTParams->overlay);
		gridStats->Cells[0][9] = gettext("Resolution [Hz/line]");
		gridStats->Cells[1][9] = format("%.5f", cohResol);

		gridStats->Cells[0][11] = format("VLF [%0.3f - ", freqFilterParams.VLFLow) + format("%0.3fHz]", freqFilterParams.VLFHigh);
		gridStats->Cells[1][11] = format("%4f", VLFC);
		gridStats->Cells[0][12] = format("LF [%0.3f - ", freqFilterParams.LFLow) + format("%0.3fHz]", freqFilterParams.LFHigh);
		gridStats->Cells[1][12] = format("%4f", LFC);
		gridStats->Cells[0][13] = format("HF [%0.3f - ", freqFilterParams.HFLow) + format("%0.3fHz]", freqFilterParams.HFHigh);
		gridStats->Cells[1][13] = format("%4f", HFC);

		gridStats->Cells[0][14] = gettext("First coherence peak in LF");
		gridStats->Cells[1][14] = format("%4f", peakValueLF[0]);
		gridStats->Cells[0][15] = "   "+gettext("at frequency");
		gridStats->Cells[1][15] = format("%4f", peakFreqLF[0]);
		gridStats->Cells[0][16] = gettext("Second coherence peak inLF");
		gridStats->Cells[1][16] = format("%4f", peakValueLF[1]);
		gridStats->Cells[0][17] = "   "+gettext("at frequency");
		gridStats->Cells[1][17] = format("%4f", peakFreqLF[1]);
		gridStats->Cells[0][18] = gettext("Coherence phase in LF");
		gridStats->Cells[1][18] = format("%.0f", peakPhaseLF * 180 / M_PI);
		gridStats->Cells[0][19] = gettext("First coherence peak in HF");
		gridStats->Cells[1][19] = format("%4f", peakValueHF[0]);
		gridStats->Cells[0][20] = "   "+gettext("at frequency");
		gridStats->Cells[1][20] = format("%4f", peakFreqHF[0]);
		gridStats->Cells[0][21] = gettext("Second coherence peak inHF");
		gridStats->Cells[1][21] = format("%4f", peakValueHF[1]);
		gridStats->Cells[0][22] = "   "+gettext("at frequency");
		gridStats->Cells[1][22] = format("%4f", peakFreqHF[1]);
		gridStats->Cells[0][23] = gettext("Coherence phase in HF");
		gridStats->Cells[1][23] = format("%.0f", peakPhaseHF * 180 / M_PI);
	} else {
		gridStats->RowCount = 14;
		gridStats->Cells[0][4] = "";
		gridStats->Cells[0][5] = gettext("SPECTRAL POWER DFT")+" [" + pDFTChannel1->getMeasurementUnit() + "]";
		gridStats->Cells[0][6] = gettext("complete");
		gridStats->Cells[1][6] = format("%.5e", TTPS);
		gridStats->Cells[0][7] = format("TPS [%0.3f - ", freqFilterParams.VLFLow) + format("%0.3fHz]", freqFilterParams.HFHigh);
		gridStats->Cells[1][7] = format("%.5e", TPS);
		gridStats->Cells[0][8] = format("VLF [%0.3f - ", freqFilterParams.VLFLow) + format("%0.3fHz]", freqFilterParams.VLFHigh);
		gridStats->Cells[1][8] = format("%.5e", VLF);
		gridStats->Cells[0][9] = format("LF [%0.3f - ", freqFilterParams.LFLow) + format("%0.3fHz]", freqFilterParams.LFHigh);
		gridStats->Cells[1][9] = format("%.5e", LF);
		gridStats->Cells[0][10] = format("HF [%0.3f - ", freqFilterParams.HFLow) + format("%0.3fHz]", freqFilterParams.HFHigh);
		gridStats->Cells[1][10] = format("%.5e", HF);
		gridStats->Cells[0][11] = "LFn [n.u.]";
		if (fabs(TTPS-VLF) > 1e-10)
			gridStats->Cells[1][11] = format("%.2f", 100*LF/(TTPS-VLF));
		gridStats->Cells[0][12] = "HFn [n.u.]";
		if (fabs(TTPS-VLF) > 1e-10)
			gridStats->Cells[1][12] = format("%.2f", 100*HF/(TTPS-VLF));
		gridStats->Cells[0][13] = gettext("ratio")+" LF/HF";
		if (fabs(HF) > 1e-10)
			gridStats->Cells[1][13] = format("%.2f", LF/HF);
	}

	if (autoCopyToClipboard) {
		char shortBuff[1000];
		if (coherence)
			sprintf(shortBuff, "%s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s\r\n",
				gridStats->Cells[1][12].c_str(), gridStats->Cells[1][14].c_str(),
				gridStats->Cells[1][15].c_str(), gridStats->Cells[1][16].c_str(),
				gridStats->Cells[1][17].c_str(), gridStats->Cells[1][18].c_str(),
				gridStats->Cells[1][13].c_str(), gridStats->Cells[1][19].c_str(),
				gridStats->Cells[1][20].c_str(), gridStats->Cells[1][21].c_str(),
				gridStats->Cells[1][22].c_str(), gridStats->Cells[1][23].c_str());
		else
			sprintf(shortBuff, "%s \t %s \t %s \t %s \t %s \r\n",
				gridStats->Cells[1][9].c_str(), gridStats->Cells[1][10].c_str(),
				gridStats->Cells[1][11].c_str(), gridStats->Cells[1][12].c_str(),
				gridStats->Cells[1][13].c_str());
				//TODO
		//Clipboard()->SetTextBuf(shortBuff);
	}
}
//---------------------------------------------------------------------------


void TFFTform::loadFreqFilterDefaults() {
   freqFilterParams.VLFLow = 0.001;
   freqFilterParams.VLFHigh = 0.04;
   freqFilterParams.LFLow = 0.04;
   freqFilterParams.LFHigh = 0.15;
   freqFilterParams.HFLow = 0.15;
   freqFilterParams.HFHigh = 0.4;                         

   CSpinEdit5->Value = freqFilterParams.VLFLow*1000.0 + 0.5; 
   CSpinEdit6->Value = freqFilterParams.VLFHigh*1000.0 + 0.5;
   CSpinEdit3->Value = freqFilterParams.LFLow*1000.0 + 0.5;
   CSpinEdit4->Value = freqFilterParams.LFHigh*1000.0 + 0.5;
   CSpinEdit1->Value = freqFilterParams.HFLow*1000.0 + 0.5;
   CSpinEdit2->Value = freqFilterParams.HFHigh*1000.0 + 0.5;
}
//---------------------------------------------------------------------------


void __fastcall TFFTform::trackAveragingColumnsChange(TObject *Sender)
{
	lblAveragingColumns->Caption = gettext("Triangular window width:")+" " + AnsiString(1+2*trackAveragingColumns->Position);
    redrawMeasurement(*pMeasurement, trackAveragingColumns->Position);
}
//---------------------------------------------------------------------------
                                            

void __fastcall TFFTform::CSpinEdit1Change(TObject *Sender)
{
	// called on one of the frequency areas spin edit controls change
	if (disableSpinEdits)
		return;

	TCSpinEdit*                castedSender = dynamic_cast<TCSpinEdit*>(Sender);
	AnsiString                 valueStr = castedSender->Text;
	double                     value;

    try {
        value = valueStr.ToDouble();
		if (castedSender == CSpinEdit1) {
			freqFilterParams.HFLow = value / 1000.0;
		} else if (castedSender == CSpinEdit2) {
			freqFilterParams.HFHigh = value / 1000.0;
		} else if (castedSender == CSpinEdit3) {
			freqFilterParams.LFLow = value / 1000.0;
		} else if (castedSender == CSpinEdit4) {
			freqFilterParams.LFHigh = value / 1000.0;
		} else if (castedSender == CSpinEdit5) {
			freqFilterParams.VLFLow = value / 1000.0;
		} else if (castedSender == CSpinEdit6) {
			freqFilterParams.VLFHigh = value / 1000.0;
		} else {
		// throw ?BorlantException?
    	}
	} catch(...) {
		// invalid value in editbox but no need to panick
	}
   
	fillParamsTable();
   	gridStats->Repaint();
}
//---------------------------------------------------------------------------
void TFFTform::saveDFTResults(AnsiString _fileName) {
	// export DFT results to text file
	std::ofstream outFile(_fileName.c_str());
	if (outFile.is_open()) {
		//double resol = 1 / (pDFTParams->end - pDFTParams->start);

		if (coherence) {
			// coherence

			// header
            AnsiString tmp(pCoherenceChannel->getChannelName());
			outFile << tmp.c_str() << "\n";

			// additional info
			outFile
				<< gridStats->Cells[0][10].c_str() << "\t"      // resolution
				<< gridStats->Cells[0][11].c_str() << "\t"      // VLF
				<< gridStats->Cells[0][12].c_str() << "\t"      // LF
				<< gridStats->Cells[0][13].c_str() << "\n";     // HF

			outFile
				<< gridStats->Cells[1][10].c_str() << "\t"      // resolution
				<< gridStats->Cells[1][11].c_str() << "\t"      // VLF
				<< gridStats->Cells[1][12].c_str() << "\t"      // LF
				<< gridStats->Cells[1][13].c_str() << "\n";     // HF

			// data
			outFile << "frequency\t" << "amplitude\t" << "phase\n";
			for (int i = 0; i < pCoherenceChannel->getNumElems(); ++i) {
				outFile << (*pCoherenceChannel)[i].time <<
					"\t" << (*pCoherenceChannel)[i].value <<
					"\t" << (*pPhiChannel)[i].value << "\n";
			}
		} else {
			// first DFT channel

			// header
            AnsiString tmp(pCoherenceChannel->getChannelName());
			outFile << tmp.c_str() << "\n";

			// additional info
			outFile
				<< gridStats->Cells[0][7].c_str() << "\t"       // TPS
				<< gridStats->Cells[0][8].c_str() << "\t"       // VLF
				<< gridStats->Cells[0][9].c_str() << "\t"       // LF
				<< gridStats->Cells[0][10].c_str() << "\t"      // HF
				<< gridStats->Cells[0][11].c_str() << "\t"      // LFn
				<< gridStats->Cells[0][12].c_str() << "\t"      // HFn
				<< gridStats->Cells[0][13].c_str() << "\n";     // LF/HF

			outFile
				<< gridStats->Cells[1][7].c_str() << "\t"       // TPS
				<< gridStats->Cells[1][8].c_str() << "\t"       // VLF
				<< gridStats->Cells[1][9].c_str() << "\t"       // LF
				<< gridStats->Cells[1][10].c_str() << "\t"      // HF
				<< gridStats->Cells[1][11].c_str() << "\t"      // LFn
				<< gridStats->Cells[1][12].c_str() << "\t"      // HFn
				<< gridStats->Cells[1][13].c_str() << "\n";     // LF/HF

			// data
			outFile << "frequency \t amplitude\n";
			for (int i = 0; i < pDFTChannel1->getNumElems(); ++i) {
				outFile << (*pDFTChannel1)[i].time << " \t " << (*pDFTChannel1)[i].value << "\n";
			}
		}

		// show warning if writing to file fails
		if (outFile.fail())
			Application->MessageBox(gettext("Saving error!").data(), gettext("Warning").data(), MB_OK);
	} else {
		Application->MessageBox(gettext("File was not possible to open, save failed.").data(), gettext("Warning").data(), MB_OK);
	}
}
//---------------------------------------------------------------------------


void TFFTform::saveDFTGraph(AnsiString _fileName) {
	// export DFT graph to wmf file
	Chart->SaveToMetafileEnh(_fileName);
}

//---------------------------------------------------------------------------

void __fastcall TFFTform::ButtonDefaultsClick(TObject *Sender)
{
	disableSpinEdits = true;
	loadFreqFilterDefaults();
	disableSpinEdits = false;

	fillParamsTable();
	gridStats->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall TFFTform::Button1Click(TObject *Sender)
{
	if (SaveDialogText->Execute()) {
        if (SaveDialogText->Options.Contains(ofExtensionDifferent))
            saveDFTResults(SaveDialogText->FileName + ".txt");
        else
    		saveDFTResults(SaveDialogText->FileName);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFFTform::SaveGraphButtonClick(TObject *Sender)
{
	if (SaveDialogGraph->Execute()) {
        if (SaveDialogGraph->Options.Contains(ofExtensionDifferent))
    		saveDFTGraph(SaveDialogGraph->FileName + ".wmf");
        else
            saveDFTGraph(SaveDialogGraph->FileName);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFFTform::ResetScaleClick(TObject *Sender)
{
    // reset drawing scale to 4 times std dev
    setGraphYScale(4*channelStdDev);
}
//---------------------------------------------------------------------------

void __fastcall TFFTform::EditScaleChange(TObject *Sender)
{
    try {
        double val = EditScale->Text.ToDouble();
        graphYScale = fabs(val);

        EditScale->Font->Color = clWindowText;
    } catch(...) {
        graphYScale = channelStdDev * 4.0;

        EditScale->Font->Color = clRed;
    }

    Chart->LeftAxis->Maximum = (coherence ? 1.0 : graphYScale);
    Chart->LeftAxis->MinorTickCount = 4;
}
//---------------------------------------------------------------------------
void __fastcall TFFTform::RadioButtonAngleClick(TObject *Sender)
{
    redrawMeasurement(*pMeasurement, trackAveragingColumns->Position);
}
//---------------------------------------------------------------------------

void __fastcall TFFTform::RadioButtonAlphaClick(TObject *Sender)
{
    redrawMeasurement(*pMeasurement, trackAveragingColumns->Position);
}
//---------------------------------------------------------------------------

void __fastcall TFFTform::FormCanResize(TObject *Sender, int &NewWidth,
      int &NewHeight, bool &Resize)
{
    Resize = true;
    AutoSize = false;
}
//---------------------------------------------------------------------------





void __fastcall TFFTform::FormCreate(TObject *Sender)
{
 TranslateComponent(this);
}
//---------------------------------------------------------------------------

