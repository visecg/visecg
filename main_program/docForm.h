// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------

#ifndef docFormH
#define docFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "measurement.h"
#include "protocol.h"
#include "excelexporter.h"

#include <VclTee.Chart.hpp>
#include <ExtCtrls.hpp>
#include <VclTee.TeEngine.hpp>
#include <VclTee.TeeProcs.hpp>
#include <Grids.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <VclTee.TeeGDIPlus.hpp>

#include <string>
#include <sstream>
#include <iomanip>


class Cursors: public LibCore::CursorPositions
{
private:
	double xvalue2screenApprox(double x, TChart *chart)
	{
		return (x - chart->BottomAxis->Minimum)
					/ (chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) * chart->ChartWidth;
	}
public:
    TChartShape *vertLine[numCursors], *crossLine[numCursors], *hintRectangle;
    
	Cursors() : LibCore::CursorPositions()
	{
		for (int i = 0; i < numCursors; i++)
		{
			vertLine[i] = NULL;
			crossLine[i] = NULL;
		}            
        hintRectangle = NULL;  
	}
    
	Cursors(const Cursors &other) : LibCore::CursorPositions()
	{
		for (int i = 0; i < numCursors; i++)
		{
			c[i] = other.c[i];
			vertLine[i] = NULL;
			crossLine[i] = NULL;
		}
        hintRectangle = NULL;
	}
    
	int getNumSetCursors()
	{
		int numSetCursors = 0;
		for (int i = 0; i < numCursors; i++)
			if (c[i] != -1)
				numSetCursors++;
		return numSetCursors;
	}

	double getLeftmostCursor()
	{
		double leftmost = 1e+100;
		for (int i = 0; i < numCursors; i++)
			if (c[i] != -1)
				leftmost = std::min(leftmost, c[i]);
		return leftmost == 1e+100 ? -1 : leftmost;
	}

	double getRightmostCursor()
	{
		double rightmost = -1;
		for (int i = 0; i < numCursors; i++)
			rightmost = std::max(rightmost, c[i]);
		return rightmost;
	}

	void removeReferencesToLines()
	{
		for (int i = 0; i < numCursors; i++)
		{
			vertLine[i] = NULL;
			crossLine[i] = NULL;
		}
        hintRectangle = NULL;
	}

	void drawCursors(TChart *chart, LibCore::CMeasurement &measurement,
						bool eventChannelSelected, bool annotationChannelSelected, int numSelectedChannel)
	//also moves them to closest sample/event on selected channel
	{
		assert(chart);
        
		static TColor cursorColors[] = {clBlue, clGreen, clRed};
		for (int i = 0; i < numCursors; i++)
		{
			if (c[i] == -1)
			{
				//if a cursor was removed, delete it from screen
				if (vertLine[i] != NULL)
				{
					delete vertLine[i];
					vertLine[i] = NULL;
				}
				if (crossLine[i] != NULL)
				{
					delete crossLine[i];
					crossLine[i] = NULL;
				}
			} else {

				//move to closest sample/event
				c[i] = eventChannelSelected
						? annotationChannelSelected
                            ? measurement.accessAnnotationByTime(numSelectedChannel, c[i]).time
                            : measurement.accessEventByTime(numSelectedChannel, c[i]).time
						: measurement.sampleIndex2Time(measurement.time2SampleIndex(c[i]));

				if (!vertLine[i])
				{
					vertLine[i] = new TChartShape(chart);
					vertLine[i]->Pen->Color = cursorColors[i];
					vertLine[i]->Style = chasVertLine;
					vertLine[i]->ParentChart = chart;
					vertLine[i]->ShowInLegend = false;
				}
				vertLine[i]->Pen->Style = psSolid;
				for (int other = 0; other < numCursors; other++)
					if (fabs(xvalue2screenApprox(c[other], chart) - xvalue2screenApprox(c[i], chart)) < 2
							&& other != i)
						vertLine[i]->Pen->Style = psDot;
				vertLine[i]->X0 = c[i];
				vertLine[i]->X1 = c[i];
				vertLine[i]->Y0 = chart->LeftAxis->Minimum;
				vertLine[i]->Y1 = chart->LeftAxis->Maximum;

				if(!eventChannelSelected){ // avoid this because left axis is no longer for event channels
					if (!crossLine[i])
					{
						crossLine[i] = new TChartShape(chart);
						crossLine[i]->Pen->Color = cursorColors[i];
						crossLine[i]->Style = chasHorizLine;
						crossLine[i]->ParentChart = chart;
						crossLine[i]->ShowInLegend = false;
					}
					double crossY = eventChannelSelected
							? annotationChannelSelected
								? measurement.accessAnnotationByTime(numSelectedChannel, c[i]).value
								: measurement.accessEventByTime(numSelectedChannel, c[i]).value
							: measurement.accessByTime(numSelectedChannel, c[i]);
					double crossWidth = chart->ChartWidth > 0
							? 12.0/chart->ChartWidth*(chart->BottomAxis->Maximum - chart->BottomAxis->Minimum) : 1;
					crossLine[i]->X0 = c[i] - crossWidth/2;
					crossLine[i]->X1 = c[i] + crossWidth/2;
					crossLine[i]->Y0 = crossY;
					crossLine[i]->Y1 = crossY;
				}
			}
		}
	}

	void drawPopup(TChart *chart, LibCore::CMeasurement &measurement, double X, double Y){
		try{
			unsigned short mHour, mMinute, mSecond, mmSecond; //measurement start date and time
			static WideChar buf[250];

			if(!hintRectangle){
				hintRectangle = new TChartShape(chart);
				hintRectangle->Style = chasRectangle;
				hintRectangle->RoundRectangle = true;
				hintRectangle->ParentChart = chart;
				hintRectangle->ShowInLegend = false;
			}

			double mouseX = chart->Series[0]->XScreenToValue(X);
			double mouseYLeft  = 0;
			try{
				mouseYLeft = chart->LeftAxis->InternalCalcPosPoint(Y);
			}catch(...){
				mouseYLeft = -1;
			}

			double mouseYRight = 0;
			try{
				mouseYRight = chart->RightAxis->InternalCalcPosPoint(Y);
			}catch(...){
				mouseYRight = -1;
			}

			TDateTime cursorTime = (mouseX/86400)+double (measurement.getDate());
			cursorTime.DecodeTime(&mHour, &mMinute, &mSecond, &mmSecond);

			UnicodeString leftText = chart->LeftAxis->Title->Caption;
			UnicodeString rightText = chart->RightAxis->Title->Caption;
			swprintf(buf, 250, L"%02u:%02u:%02u.%03u\nx = %.3lf\n%s = %.3lf\n%s = %.3lf",mHour,mMinute,mSecond,mmSecond, mouseX, leftText.c_str(), mouseYLeft, rightText.c_str(), mouseYRight);

			if(hintRectangle){
				hintRectangle->Text->SetText(buf);
				hintRectangle->XYStyle = xysPixels;
				int width = chart->Width;
				int strArrSize = hintRectangle->Text->Count;
				int popupWidth = 50;
				for(int i=0; i< strArrSize; i++)
					popupWidth = std::max(popupWidth, chart->Canvas->TextWidth(hintRectangle->Text->Strings[i]) + 50);
				hintRectangle->X0 = (X+popupWidth < width)?X+30 :X-30 ;
				hintRectangle->Y0 = Y-30;
				hintRectangle->X1 = (X+popupWidth < width)?X+popupWidth:X-popupWidth ;
				hintRectangle->Y1 = Y+40;
			}
		}catch(...){
		}
	}

	void deletePopup(){
        delete hintRectangle;
        hintRectangle = NULL;
    }
};

class CUndoStack
//memorizes a stack of the last few undoable actions
{
public:
	struct UndoableAction
	//description of the action
	{
		enum ActionType {
			actionTypeDeleteEvent,
			actionTypeAddEvent,
			actionTypeNonUndoable
		};

		ActionType actionType;
		LibCore::Event event; //event that was operated on
		int	channelNum; //channel

		UndoableAction()
		{
			//default constructor constructs an non-undoable
			actionType = actionTypeNonUndoable;
			event = LibCore::Event(-1, -1);
			channelNum = -1;
		}
		UndoableAction(ActionType _actionType, LibCore::Event _event, int _channelNum)
		{
			actionType = _actionType;
			event = _event;
			channelNum = _channelNum;
		}
	};

protected:
	static const int MaxUndoLevels = 20;
	int numUndoableActions;				   //num of actions currently in the stack
	UndoableAction stack[MaxUndoLevels]; //the stack itself

public:
	CUndoStack()
	{
		resetStack();
	}
	void resetStack()
	{
		numUndoableActions = 0;
	}
	bool isEmpty()
	{
		return numUndoableActions == 0;
	}
	UndoableAction pop() //if empty, it returns a non-undoable actions
	{
		if (numUndoableActions == 0)
			return UndoableAction();
		else
			return stack[--numUndoableActions];
	}
	void push(UndoableAction action)  //if full, the oldest is removed
	{
		if (numUndoableActions == MaxUndoLevels)
		{
			for (int i = 0; i < MaxUndoLevels-1; i++)
				stack[i] = stack[i+1];
			numUndoableActions--;
		}
		stack[numUndoableActions++] = action;
	}
};


//---------------------------------------------------------------------------
struct TGraphPainter {
    virtual void afterGraphDraw(TChart * const chart) = 0;
};


//--------------------------------------------------------------------------
class TDocumentForm : public TForm
{
friend class TdialogViewY;
friend class TMainForm;

__published:	// IDE-managed Components
	TChart *Chart; //chart that shows signal
	TScrollBar *horzScroll;
	TStringGrid *gridChannelView;
	TToolBar *ToolBar;
	TToolButton *btnZoom;
	TToolButton *btnUnzoom;
	TToolButton *btnViewAll;
	TToolButton *ToolButton1;
	TToolButton *btnShowLegend;
	TToolButton *btnShowChannelTable;
	TToolButton *ToolButton2;
	TToolButton *btnShowAllChannels;
	TToolButton *btnHideAllChannels;
	TToolButton *ToolButton3;
	TToolButton *btnZoomCursor;
	TToolButton *btnTrueYscale;
	TToolButton *ToolButton4;
	TComboBox *comboCursorColor;
	TToolButton *btnRemoveAllCursors;
	TGroupBox *groupCursors;
	TLabel *lblBlueCursor;
	TLabel *lblGreenCursor;
	TLabel *lblRedCursor;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *lblBlueGreenDiff;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *lblBlueRedDiff;
	TLabel *Label6;
	TLabel *Label7;
	TLabel *lblGreenRedDiff;
    TLabel *lblStartDateTime;
    TLabel *lblStartDateTimeVar;
    TButton *show10secBtn;
    TToolButton *ToolButton5;
    TButton *show30secBtn;
    TButton *show60secBtn;
    TButton *show10minbutton;
    TButton *show30minbutton;
    TButton *Button3;
	TButton *show4hbutton;
	TButton *Button1;
	TButton *Button2;
	TButton *Button4;
	TToolButton *ToolButton6;
	void __fastcall FormResize(TObject *Sender);
	void __fastcall btnZoomClick(TObject *Sender);
	void __fastcall btnUnzoomClick(TObject *Sender);
	void __fastcall horzScrollChange(TObject *Sender);
	void __fastcall btnViewAllClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall gridChannelViewDblClick(TObject *Sender);
	void __fastcall gridChannelViewMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
	void __fastcall btnShowAllChannelsClick(TObject *Sender);
	void __fastcall btnHideAllChannelsClick(TObject *Sender);
	void __fastcall gridChannelViewSetEditText(TObject *Sender, int ACol,
          int ARow, const AnsiString Value);
	void __fastcall btnShowLegendClick(TObject *Sender);
	void __fastcall btnShowChannelTableClick(TObject *Sender);
	void __fastcall gridChannelViewMouseWheelDown(TObject *Sender,
          TShiftState Shift, TPoint &MousePos, bool &Handled);
	void __fastcall gridChannelViewMouseWheelUp(TObject *Sender,
		  TShiftState Shift, TPoint &MousePos, bool &Handled);
	void __fastcall FormMouseWheelDown(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled);
	void __fastcall FormMouseWheelUp(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled);
	void __fastcall ChartMouseWheel(TObject *Sender, TShiftState Shift,
          int WheelDelta, TPoint &MousePos, bool &Handled);
	void __fastcall ChartMouseMove(TObject *Sender, TShiftState Shift, int X,
          int Y);
	void __fastcall ChartMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y);
	void __fastcall gridChannelViewDrawCell(TObject *Sender, int ACol,
          int ARow, TRect &Rect, TGridDrawState State);
	void __fastcall btnZoomCursorClick(TObject *Sender);
	void __fastcall btnTrueYscaleClick(TObject *Sender);
	void __fastcall btnRemoveAllCursorsClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
    void __fastcall ChartAfterDraw(TObject *Sender);
    void __fastcall show10secBtnClick(TObject *Sender);
    void __fastcall show30secBtnClick(TObject *Sender);
    void __fastcall show60secBtnClick(TObject *Sender);
    void __fastcall show10minbuttonClick(TObject *Sender);
    void __fastcall show30minbuttonClick(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall show4hbuttonClick(TObject *Sender);
    void __fastcall openStaggered(double time, double duration);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall gridChannelViewClick(TObject *Sender);

private:	// User declarations
    bool horiz_scroll_enabled;
    std::vector<double> view_stack_jump;
	TGridCoord gridCellUnderMouse;
	LibCore::CMeasurement measurement; //measurement-document that this form shows
	bool propDirty; //document is different from the one saved in file
	void setDirty(bool _dirty) //manages showing * in caption for dirty documents
	{
		if (!_dirty && Caption[Caption.Length()] == '*')
			Caption = Caption.SubString(1, Caption.Length()-1);
		else if (_dirty && Caption[Caption.Length()] != '*')
			Caption = Caption + "*";
		propDirty = _dirty;
	}
	void refreshWholeWindow(bool automatic=false);
	void zoomOutX(double factor, double newCentre);
	void fillGridChannelView();

	double xLeft, xRight; //x-range currently shown
	bool scrollbarTrackingEnabled; //true by default; set to false for parts of code that change scrollbar->Position
	bool nextClickDefinesEventTime; //true if ChartMouseUp should add the event
	double valueOfEventToAdd;      //its value is taken from valueOfEventToAdd
								//(unless values are inter-event times or frequencies, in which case the value is calculated as such

	LibCore::CMeasurement::YscaleMode yScaleMode;
	bool trueYscale;

	bool eventChannelSelected; //if true, measurement.eventChannel[numSelectedChannel] is selected; otherwise, measurement.channel[numSelectedChannel]
    bool annotationChannelSelected; // if eventChannelSelected && annotationChannelSelected: measurement.annotationChannel[numSelectedChannel]
	int numSelectedChannel;  //-1 = none (which should not be if a measurement exists)
	double mousePosOnChartX, mousePosOnChartY;
	Cursors cursors;

	CUndoStack undoStack;
	bool autoCopyToClipboard; // set to true (in ctor) if automatic copy of statistics to clipboard is needed (see clipboard.txt)
    TGraphPainter* additionalPainter; // if additional objects have to be put on graph (on its canvas) after the graph has been drawn, implement that within a paninter and supply it to the document

    void pushCurrentViewOnStack(double left, double right);
    void popCurrentViewFromStack();

	void useUserDefaultBeatDetector();
public:		// User declarations

	enum Detectors{
        NO_DETECTOR,
		Version1_static,
		Version2_static_advanced,
		Version3_signal,
		Version3_derivative,
		Version5_multiple_peaks
	};

	static Detectors getUserDefaultBeatDetector();

	UnicodeString fileName; //filename to which document was last saved
	__property bool dirty = {read = propDirty, write = setDirty};

	__fastcall TDocumentForm(TComponent* Owner);
	virtual bool __fastcall CloseQuery();
	void LoadMeasurement(const UnicodeString _fileName, bool findHR = false);
	void SaveMeasurement(UnicodeString _fileName, bool automatic = false);
	void exportText(const AnsiString _fileName, bool allChannels);
	void importText(const AnsiString _fileName);

  	void selectChannel(int numChannel, bool eventChannel, bool annotationChannel);

    // used when loading MECG, after the file is read but before it is displayed; only channels named in parameter are kept
    void filterChannels(const AnsiString& channelsToFilter);

	void prepareForExcelExport(bool warnAndShow = true);
	void exportToExcel();
    void exportToWmf(const AnsiString& filename);

	bool editMeasurementData();
	void dataFromDlgMeasurementData();
	void repaintGraph(bool genReport = false);
	void refreshStatusLine();
	bool measure(LibStdMeasurement::StdMeasurementType measurementType); //receive data from instrument; return true iff successful

	void filterSignal();
	void setBaseline();
	void invertChannel();
    void channelGain();
    void channelOffset();    
	void shiftChannel();
	void offsetChannel();
	void detrendChannel();
	void deleteChannel();
	void duplicateChannel();
	void assignInterEventTimeAsEventValues();
    void assignInterEventTimeAsEventValuesSameChannel();
	void assignEventFreqAsEventValues();
	void assignEventFreqAsEventValuesFromRRI();    
	void refineBRwithInterpolation();
    void differentialChannel(double errorValue = -1, bool absolute = false);
	void resampleSignalChannel();
	void resampleEventChannel();
	void calcBeatTimes(LibCore::CMeasurement::BeatAlgorithm algorithm, double AMparam = -1, bool automatic = false, double AMmaxValue = -1);
  	void calcBeatTimesBeatDetection(LibCore::CMeasurement::BeatAlgorithm algorithm, double AMparam = -1);
	void calcStatistics();
    void calcStatisticsBetweenCursors();
	void maxANDmin();
	void fourierTransform();
	void calcCoherence();
	void sequentialBRS();
	void correlationxBRS();
	void sinhronizedrBRS();
	void frequencyfBRS();
	void everyKnownBRS();
	void createBPEventChannel();
	void createRTEventChannel();
    void absoluteValueOfChannel();
    
	void cutFromTo(double from, double to);
    void sortAllEventChannels();

	void removeEventAtCursor(int cursorIndex);
	void removeEventsAtCursors();
	void moveEventAtFirstCursor();
	void addEvent();            
    void addEventByValue();
	void replaceWithEquidistantEvents(int numReplacementEvents);
    void replaceWithEquidistantAverageEvents();
	void removeExtremeEvents();
	void removeExtremeEventsBetween(double low, double high);
	void undo();

	void addEventChennels();

    void showAveragerWindow();

    int getDocumentVersion();
    void setDocumentVersionAndBits(int ver, int bits);

    // za viktorja
    void averageBeatAndsubtract();
    void removeBeatLinear();
    void addMeasurementChannel();

	// scripts
	void generateEventChannels(int ecgChan = 0, int bpChan = 1, int respChan = -1);
	void standardAnalysis();
	bool findChannelByNameStart(const AnsiString* names, int numOfNames, int& chan, bool eventChan = false);
	void interactiveGenerateEventChannels(bool respirationEnabled = true);
	void saveCoherenceGraph(AnsiString _fileName = "");
	void saveDFTGraph(int channel, AnsiString _fileName = "");
	void saveCoherenceData(AnsiString _fileName = "");
    void exportChannel(int channel, AnsiString channelName = "");
    void resampleAndExportChannel(float frequency, int channel, AnsiString channelName = "");
    void functionPovprecniTlak();
    void calculateMDP();
    void comVisExport();
    void TWaveAnalysis();
    void addRandomChannel();
    void mobEcgGetBeatEvents(bool automatic=false);

	// run predefined script by specifying it's name and optional parameters
	// returns true if script wishes to exit application
	bool runNamedScript(const AnsiString& name, const AnsiString& params = "");

    double getMissingSamplesValue();
    void correctToMiliVolts();
    //IvanT
    void generate12leadMeasurement(const TDocumentForm* source);
    void generate12leadGraph(const TDocumentForm* source);
    void ArrangeDisplay(void);
    void centerCursorView(double duration);
    void windowFilterEventChannel(double windowSize, double step, bool strictTime, bool refresh = true);
    void setCurrentChannelVisible(bool visible);
    int getNumEventChannel();
	//int saveGraphWMFShowAll(AnsiString location, double length,int quality = 2);
    //int saveGraphWMFShowAllAnnotations(AnsiString location, FILE *file, int fileNumber, int * numAnnotations, AnsiString *comStr, int * sequenceChar, int * sequenceNumbering, int quality = 2);
    double getDuration(){
        return measurement.getDuration();
    }
    void mobEcgGetBeatEventsNew(bool automatic=false);
    void findBeatNumAndDev(int numSamples, double threshold, double limit, double errorValue, double* signal, double* derivative, double* number, double* deviation, double* avgValue, bool createChannel = false, bool automatic = false);
    void findBeatNumAndDevConv(int numSamples, double threshold, double limit, double errorValue, double* signal, double* derivative, double* number, double* deviation, double* avgValue, bool createChannel, bool automatic);
    LibCore::CMeasurement * getMeasurementPointer(){
        return &measurement;
	}
	void mobEcgGetBeatEventsv3(bool automatic = false, double filterFreq = 30, double exp1 = 0.5, double amplituteThr = 10, double thau = 0.05, bool derivative = false);
    void findLocalPositivePeak(std::vector<LibCore::Event>* extremePeakIndex, double aroundPeak, LibCore::CChannel samples, double* derivative);
	void mobEcgGetBeatEventsv5(bool automatic = false, double filterFreq = 30, double exp1 = 0.5, double amplituteThr = 10, double thau = 0.05, bool derivative = true);
	void fixBeatVariability();
	void processOpenedFile();
	static void copyFileTimestamps(UnicodeString sourceFile, UnicodeString targetFile);
};
//---------------------------------------------------------------------------
extern PACKAGE TDocumentForm *DocumentForm;
//---------------------------------------------------------------------------

#endif
