object MeasureForm: TMeasureForm
  Left = 372
  Top = 71
  BorderStyle = bsDialog
  Caption = 'New measurement'
  ClientHeight = 440
  ClientWidth = 239
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 113
    Height = 13
    Caption = 'Measurement length (s):'
  end
  object spinDuration: TCSpinEdit
    Left = 128
    Top = 8
    Width = 89
    Height = 22
    MaxValue = 7200
    TabOrder = 0
    Value = 30
  end
  object checkManualStop: TCheckBox
    Left = 112
    Top = 40
    Width = 129
    Height = 17
    Caption = 'manual stop'
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 64
    Width = 225
    Height = 337
    Caption = 'Measured channels'
    TabOrder = 2
    object Label2: TLabel
      Left = 8
      Top = 24
      Width = 28
      Height = 13
      Caption = 'Port 1'
    end
    object Label3: TLabel
      Left = 8
      Top = 80
      Width = 31
      Height = 13
      Caption = 'Port 2:'
    end
    object Label4: TLabel
      Left = 8
      Top = 208
      Width = 31
      Height = 13
      Caption = 'Port 3:'
    end
    object checkColinDigital: TCheckBox
      Left = 24
      Top = 48
      Width = 177
      Height = 17
      Caption = 'COLIN digital'
      Enabled = False
      TabOrder = 0
    end
    object checkPressure: TCheckBox
      Left = 24
      Top = 104
      Width = 185
      Height = 17
      Caption = 'pressure'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object checkEKG: TCheckBox
      Left = 24
      Top = 232
      Width = 193
      Height = 17
      Caption = 'ECG (three channels)'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 2
    end
    object checkP4: TCheckBox
      Left = 8
      Top = 280
      Width = 145
      Height = 17
      Caption = 'aux digital (port 4)'
      Enabled = False
      TabOrder = 3
      Visible = False
    end
    object checkMarkers: TCheckBox
      Left = 8
      Top = 312
      Width = 169
      Height = 17
      Caption = 'bookmarks (keys 1-9)'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object checkCH2: TCheckBox
      Left = 24
      Top = 128
      Width = 137
      Height = 17
      Caption = 'analog channel CH2'
      TabOrder = 5
    end
    object checkCH3: TCheckBox
      Left = 24
      Top = 152
      Width = 121
      Height = 17
      Caption = 'analog channel CH3'
      TabOrder = 6
    end
    object checkCH4: TCheckBox
      Left = 24
      Top = 176
      Width = 121
      Height = 17
      Caption = 'analog channel CH4'
      TabOrder = 7
    end
    object checkBreathing: TCheckBox
      Left = 24
      Top = 256
      Width = 97
      Height = 17
      Caption = 'respiration'
      TabOrder = 8
    end
  end
  object BitBtn1: TBitBtn
    Left = 160
    Top = 408
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 3
  end
  object BitBtn2: TBitBtn
    Left = 80
    Top = 408
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 4
  end
end
