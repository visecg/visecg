object HeartBeatParam: THeartBeatParam
  Left = 244
  Top = 0
  AutoSize = True
  BorderIcons = []
  Caption = 'Assigning beats - parameters'
  ClientHeight = 642
  ClientWidth = 641
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  DesignSize = (
    641
    642)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 104
    Top = 0
    Width = 137
    Height = 13
    Caption = 'Output value to search beats'
  end
  object Label2: TLabel
    Left = 336
    Top = 0
    Width = 89
    Height = 13
    Caption = 'Noise output value'
  end
  object amValueInput: TEdit
    Left = 176
    Top = 24
    Width = 124
    Height = 21
    TabOrder = 0
    Text = 'amValueInput'
  end
  object BitBtn1: TBitBtn
    Left = 344
    Top = 64
    Width = 107
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 192
    Top = 64
    Width = 105
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object Chart1: TChart
    Left = 0
    Top = 144
    Width = 641
    Height = 249
    AllowPanning = pmNone
    BackWall.Brush.Style = bsClear
    BackWall.Pen.Visible = False
    Legend.Visible = False
    MarginBottom = 1
    MarginLeft = 0
    MarginRight = 0
    MarginTop = 0
    Title.Text.Strings = (
      'Standard deviation measurement of beats'#39' output value:'
      'Yellow boxes indicate an average of BMP in the specified value')
    OnClickSeries = Chart1ClickSeries
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.LabelStyle = talValue
    BottomAxis.Maximum = 5.000000000000000000
    BottomAxis.Title.Caption = 'Detection output value'
    Frame.Visible = False
    LeftAxis.AxisValuesFormat = '0%'
    LeftAxis.LabelsSize = 30
    LeftAxis.Title.Caption = 'Standard deviation'
    View3D = False
    View3DWalls = False
    Zoom.Allow = False
    BevelOuter = bvNone
    BevelWidth = 0
    TabOrder = 3
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object Series1: TBarSeries
      Legend.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      BarWidthPercent = 75
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Bar'
      YValues.Order = loNone
    end
  end
  object Chart2: TChart
    Left = 0
    Top = 393
    Width = 641
    Height = 249
    AllowPanning = pmNone
    BackWall.Brush.Style = bsClear
    BackWall.Pen.Visible = False
    Legend.ResizeChart = False
    Legend.Visible = False
    MarginBottom = 1
    MarginLeft = 0
    MarginRight = 0
    MarginTop = 0
    Title.Text.Strings = (
      'Total number of beats')
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.Maximum = 5.000000000000000000
    BottomAxis.Title.Caption = 'Detection output values'
    Frame.Visible = False
    LeftAxis.AxisValuesFormat = '0'
    LeftAxis.LabelsSize = 30
    LeftAxis.LabelStyle = talValue
    LeftAxis.Title.Caption = 'Number of detected beats'
    View3D = False
    View3DWalls = False
    Zoom.Allow = False
    BevelOuter = bvNone
    BevelWidth = 0
    TabOrder = 4
    Anchors = [akRight, akBottom]
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object Series2: TBarSeries
      Marks.Visible = False
      Marks.Callout.Length = 8
      PercentFormat = '##0 %'
      SeriesColor = 16744448
      ValueFormat = '#,#00'
      BarWidthPercent = 75
      MultiBar = mbNone
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Bar'
      YValues.Order = loNone
    end
  end
  object amMaxValue: TEdit
    Left = 345
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '120'
  end
end
