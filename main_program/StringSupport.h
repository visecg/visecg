#ifndef STRING_SUPPORT_H
#define STRING_SUPPORT_H

#include <string>
#include <Classes.hpp>


WideString string2WideString(const std::string& str);
UnicodeString string2UnicodeString(const std::string& str);
std::string wideString2String(const WideString& str);


#endif //STRING_SUPPORT_H
