object filterParamsDlg: TfilterParamsDlg
  Left = 451
  Top = 430
  AutoSize = True
  Caption = 'Filtering'
  ClientHeight = 161
  ClientWidth = 177
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel
    Left = 0
    Top = 0
    Width = 73
    Height = 49
    AutoSize = False
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 117
    Height = 13
    Caption = 'Low pass filter frequency'
  end
  object Label2: TLabel
    Left = 104
    Top = 112
    Width = 73
    Height = 49
    AutoSize = False
  end
  object RadioGroup1: TRadioGroup
    Left = 8
    Top = 56
    Width = 161
    Height = 65
    Caption = 'Window'
    TabOrder = 0
  end
  object radioRectangularWindow: TRadioButton
    Left = 16
    Top = 72
    Width = 113
    Height = 17
    Caption = 'quadratic'
    TabOrder = 1
  end
  object radioTriangularWindow: TRadioButton
    Left = 16
    Top = 96
    Width = 113
    Height = 17
    Caption = 'trinagular'
    Checked = True
    TabOrder = 2
    TabStop = True
  end
  object editFrequency: TEdit
    Left = 88
    Top = 24
    Width = 81
    Height = 21
    TabOrder = 3
    Text = '50'
  end
  object BitBtn1: TBitBtn
    Left = 104
    Top = 128
    Width = 67
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 4
  end
  object BitBtn2: TBitBtn
    Left = 8
    Top = 128
    Width = 67
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 5
  end
end
