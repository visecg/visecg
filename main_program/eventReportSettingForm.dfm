object EventReportSetting: TEventReportSetting
  Left = 336
  Top = 61
  Width = 721
  Height = 593
  AutoScroll = True
  Caption = 'Select event to create report'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 60
    Height = 13
    Caption = 'Select event'
  end
  object Label4: TLabel
    Left = 41
    Top = 213
    Width = 150
    Height = 39
    Caption = 
      'Enter the length of the recorded measurements around event   (in' +
      ' minutes):'
    WordWrap = True
  end
  object Label6: TLabel
    Left = 381
    Top = 383
    Width = 37
    Height = 13
    Caption = 'Author: '
  end
  object Label7: TLabel
    Left = 393
    Top = 221
    Width = 25
    Height = 13
    Caption = 'User:'
  end
  object Label8: TLabel
    Left = 399
    Top = 245
    Width = 19
    Height = 13
    Caption = 'Age'
  end
  object Label9: TLabel
    Left = 387
    Top = 269
    Width = 34
    Height = 13
    Caption = 'Weight'
  end
  object Label5: TLabel
    Left = 400
    Top = 293
    Width = 18
    Height = 13
    Caption = 'Sex'
  end
  object Label10: TLabel
    Left = 373
    Top = 317
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Birth date'
  end
  object Label11: TLabel
    Left = 334
    Top = 341
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Electrode position'
  end
  object Label12: TLabel
    Left = 31
    Top = 269
    Width = 105
    Height = 13
    Caption = 'ECG length per line [s]'
  end
  object Label13: TLabel
    Left = 41
    Top = 344
    Width = 97
    Height = 13
    Caption = 'ECG mV height [mm]'
  end
  object Label14: TLabel
    Left = 45
    Top = 317
    Width = 93
    Height = 13
    Caption = 'mV around baseline'
  end
  object Label15: TLabel
    Left = 31
    Top = 293
    Width = 105
    Height = 13
    Caption = 'ECG time scale [cm/s]'
  end
  object Label16: TLabel
    Left = 8
    Top = 399
    Width = 163
    Height = 13
    Caption = 'Additional comments about report: '
  end
  object OkButton: TButton
    Left = 424
    Top = 514
    Width = 145
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 13
  end
  object CancelButton: TButton
    Left = 88
    Top = 514
    Width = 145
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 14
  end
  object timeMinutes: TEdit
    Left = 224
    Top = 210
    Width = 41
    Height = 21
    TabOrder = 12
    Text = '4'
    OnChange = timeMinutesChange
  end
  object AuthorInp: TEdit
    Left = 432
    Top = 380
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'AuthorInp'
  end
  object UserInp: TEdit
    Left = 432
    Top = 218
    Width = 241
    Height = 21
    TabOrder = 0
  end
  object AgeInp: TEdit
    Left = 432
    Top = 242
    Width = 89
    Height = 21
    MaxLength = 3
    NumbersOnly = True
    TabOrder = 1
    OnChange = timeMinutesChange
  end
  object WeightInp: TEdit
    Left = 432
    Top = 266
    Width = 89
    Height = 21
    MaxLength = 3
    NumbersOnly = True
    TabOrder = 2
    OnChange = timeMinutesChange
  end
  object GenderInp: TEdit
    Left = 432
    Top = 290
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object BirthInp: TEdit
    Left = 432
    Top = 314
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object PositionInp: TEdit
    Left = 432
    Top = 338
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object Edit2: TEdit
    Left = 144
    Top = 266
    Width = 121
    Height = 21
    TabOrder = 8
    Text = 'Edit2'
    OnChange = timeMinutesChange
  end
  object Edit3: TEdit
    Left = 144
    Top = 290
    Width = 121
    Height = 21
    TabOrder = 9
    Text = 'Edit3'
    OnChange = timeMinutesChange
  end
  object Edit4: TEdit
    Left = 144
    Top = 314
    Width = 121
    Height = 21
    TabOrder = 10
    Text = 'Edit4'
    OnChange = timeMinutesChange
  end
  object Edit5: TEdit
    Left = 144
    Top = 341
    Width = 121
    Height = 21
    TabOrder = 11
    Text = 'Edit5'
    OnChange = timeMinutesChange
  end
  object Memo1: TMemo
    Left = 0
    Top = 418
    Width = 705
    Height = 90
    Lines.Strings = (
      '')
    TabOrder = 7
  end
  object CheckListBox1: TCheckListBox
    Left = 0
    Top = 27
    Width = 705
    Height = 113
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'DejaVu Sans Mono'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    ParentShowHint = False
    ScrollWidth = 10
    ShowHint = True
    TabOrder = 15
    OnMouseLeave = CheckListBox1MouseLeave
    OnMouseMove = CheckListBox1MouseMove
  end
  object Button1: TButton
    Left = 16
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Check all'
    TabOrder = 16
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 97
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Check none'
    TabOrder = 17
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 178
    Top = 160
    Width = 137
    Height = 25
    Caption = 'Check VisEcg Annotations'
    TabOrder = 18
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 321
    Top = 160
    Width = 128
    Height = 25
    Caption = 'Check user annotations'
    TabOrder = 19
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 455
    Top = 160
    Width = 114
    Height = 25
    Caption = 'Check reported events'
    TabOrder = 20
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 575
    Top = 160
    Width = 98
    Height = 25
    Caption = 'Check user comfort'
    TabOrder = 21
    OnClick = Button6Click
  end
end
