// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f)
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop
#include "Main.h"
#include "protocol.h"
#include "aboutDialog.h"
#include "measurementDataDlg.h"
#include "folderSettingsDialog.h"
#include <FileCtrl.hpp>
#include <IOUtils.hpp>
#include "gitVersion.h"
#include "teulaShowForm.h"

#include <windows.h>

#include "ini.h"
#include "stringSupport.h"
#include "gnugettext.hpp"
#include <string>

#include <fstream>

#include "gnugettext.hpp"
//---------------------------------------------------------------------------
#pragma resource "*.dfm"

TMainForm *MainForm;
//---------------------------------------------------------------------------
class MenuItemFlags
{
	//if a menu item's tag includes a number then it is only enabled when the corresponding bool is true
	static const int docOpen 			= 1;
	static const int docEditable 		= 10;
	static const int eventChSelected	= 100;
	static const int signalChSelected	= 1000;
	static const int devicePresent		= 10000;

	//if tag includes k*cursorPresence then it is only enabled:
	//when blue cursor set (k=1), green (k=2), red (k=4)
	static const int cursorPresence   	= 100000;

	static const int undoPossible		= 1000000;

	static const int showInSimpleView  = 10000000;
};

void TMainForm::checkVisibleMenuItems(TMenuItem *item){
	item->Visible = true;
	if(simpleView){
		item->Visible = false;
		int remainder = item->Tag / MenuItemFlags::showInSimpleView;
		if (remainder > 0 )
			item->Visible = true;
	}

	//recurse to the rest of the menu
	for (int i = 0; i < item->Count; i++)
		checkVisibleMenuItems(item->Items[i]);
}

__fastcall TMainForm::TMainForm(TComponent* Owner)
	: TForm(Owner)
{
	this->WindowProc = WndProc;
	autoOpenFile = "";
	runScript = "";
	scriptParams = "";
	runTimeFlags.clear();
	findHROnOpenedMeasurements = true;
	maxLengthConvertedFiles = 43200;

	simpleView = SAVING;

	UnicodeString settingsFname("nastavitve.ini");
	for (int i = Application->ExeName.Length()-1; i >= 0; --i){
		if (Application->ExeName[i] == '\\') {
			settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
			break;
		}
	 }
	Ini::File ini(settingsFname.c_str());
	ini.loadVar(simpleView, L"simpleGUI");

	checkVisibleMenuItems(MainMenu->Items);
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::FormCreate(TObject *Sender)
{
	this->WindowProc = WndProc;
	Application->OnHint = ShowHint;
	TranslateComponent (this);

}
//----------------------------------------------------------------------------
void __fastcall TMainForm::ShowHint(TObject *Sender)
{
	StatusLine->SimpleText = Application->Hint;
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::FileNew(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmNonstandard))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::FileOpen(TObject *Sender)
{
	if (OpenDialog->Execute())
		openPath(OpenDialog->FileName);
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::FileSave(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->SaveMeasurement(getCurrentDocument()->fileName);
	else if(getCurrentSimpleDocument())
		getCurrentSimpleDocument()->SaveMeasurement();
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::FileSaveAs(TObject *Sender)
{
	if (getCurrentDocument())
		if (SaveDialog->Execute())
		{
			getCurrentDocument()->SaveMeasurement(SaveDialog->FileName);
		}
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::FilePrint(TObject *Sender)
{
	if (PrintDialog->Execute())
	{
		//---- Add code to print current file ----
	}
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::FilePrintSetup(TObject *Sender)
{
	PrintSetupDialog->Execute();
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::FileExit(TObject *Sender)
{
	Close();
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::WindowTile(TObject *Sender)
{
	Tile();
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::WindowCascade(TObject *Sender)
{
	Cascade();
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::WindowArrange(TObject *Sender)
{
	ArrangeIcons();
}
//----------------------------------------------------------------------------
void __fastcall TMainForm::MeasurementData1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->editMeasurementData();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Filtriranje1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->filterSignal();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Baznalinija1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->setBaseline();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Obraanjesignala1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->invertChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BRRRas1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->assignInterEventTimeAsEventValues();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Interpolacija1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->refineBRwithInterpolation();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AM1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->calcBeatTimes(LibCore::CMeasurement::beatAlgorithmAM);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AF1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->calcBeatTimes(LibCore::CMeasurement::beatAlgorithmAF);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FX1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->calcBeatTimes(LibCore::CMeasurement::beatAlgorithmFX);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FCOREL1Click(TObject *Sender)
{
	if (getCurrentDocument()) {
		getCurrentDocument()->calcBeatTimes(LibCore::CMeasurement::beatAlgorithmFCOREL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FRMS1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->calcBeatTimes(LibCore::CMeasurement::beatAlgorithmFRMS);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Statistika1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->calcStatistics();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::maxmin1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->maxANDmin();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FZ1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->calcBeatTimes(LibCore::CMeasurement::beatAlgorithmFZ);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FFT1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->fourierTransform();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::EditCutStartClick(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->cutFromTo(0, getCurrentDocument()->cursors.c[0]);
}
//---------------------------------------------------------------------------

template <typename T> void swap(T &x1, T &x2)
{
	T temp = x1;
	x1 = x2;
	x2 = temp;
}

void __fastcall TMainForm::EditCutMiddleClick(TObject *Sender)
{
	if (getCurrentDocument())
	{
		double c0 = getCurrentDocument()->cursors.c[0], c1 = getCurrentDocument()->cursors.c[1];
		if (c0 > c1)
			swap(c0, c1);
		getCurrentDocument()->cutFromTo(c0, c1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::EditCutEndClick(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->cutFromTo(getCurrentDocument()->cursors.c[0], getCurrentDocument()->measurement.getDuration());
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::EditCropToSelectionClick(TObject *Sender)
{
	if (getCurrentDocument())
	{
		getCurrentDocument()->cutFromTo(getCurrentDocument()->cursors.c[1], getCurrentDocument()->measurement.getDuration());
		getCurrentDocument()->cutFromTo(0, getCurrentDocument()->cursors.c[0]);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FileExportTextAllClick(TObject *Sender)
{
	if (getCurrentDocument())
		if (ExportTextDialog->Execute())
		{
			getCurrentDocument()->exportText(ExportTextDialog->FileName, true);
		}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FileExportTextSelectedClick(TObject *Sender)
{
	if (getCurrentDocument())
		if (ExportTextDialog->Execute())
		{
			getCurrentDocument()->exportText(ExportTextDialog->FileName, false);
		}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Mirovanje1Click(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmMirovanje))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Globokodihanje1Click(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmGlobokoDihanje))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Valsalva1Click(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmValsalva))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Omoitevobraza1Click(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmOmocitevObraza))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Omoitevroke1Click(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmOmocitevRoke))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Stiskpesti1Click(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmStiskPesti))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Ortostatskitest1Click(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmOrtostatskiTest))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Nagibnamiza1Click(TObject *Sender)
{
	TDocumentForm *newForm = new TDocumentForm(this);
	if (newForm->measure(LibStdMeasurement::stdmNagibnaMiza))
	{
		newForm->Width = newForm->Width+1;
	} else 	{
		newForm->Close();
		delete newForm;
	}
}
//---------------------------------------------------------------------------



void TMainForm::refreshMenuItemStates(TMenuItem *item, bool docOpen, bool docEditable, bool eventChSelected, bool undoPossible, bool cursorPresent[3])
{
	item->Enabled = true;

	int requiredCursors = (item->Tag % (10*MenuItemFlags::cursorPresence)) / MenuItemFlags::cursorPresence;
	if (requiredCursors & 1)
		item->Enabled &= cursorPresent[0];
	if (requiredCursors & 2)
		item->Enabled &= cursorPresent[1];
	if (requiredCursors & 4)
		item->Enabled &= cursorPresent[2];

	if (item->Tag % (10*MenuItemFlags::undoPossible) >= MenuItemFlags::undoPossible)
		item->Enabled &= undoPossible;
	//check for presence of devices
	if (item->Tag % (10*MenuItemFlags::devicePresent) >= MenuItemFlags::devicePresent)
	{
		#ifdef INCLUDE_USB_DRIVERS
			DWORD numDevices;
			ftCheck(FT_ListDevices(&numDevices, NULL, FT_LIST_NUMBER_ONLY), "FT_ListDevices");
			item->Enabled &= (numDevices > 0);
		#else
			item->Enabled = false;
		#endif
	}
	if (item->Tag % (10*MenuItemFlags::signalChSelected) >= MenuItemFlags::signalChSelected)
		item->Enabled &= !eventChSelected;
	if (item->Tag % (10*MenuItemFlags::eventChSelected) >= MenuItemFlags::eventChSelected)
		item->Enabled &= eventChSelected;
/*	if (item->Tag % (10*MenuItemFlags::docEditable) >= MenuItemFlags::docEditable)
		item->Enabled &= docEditable;*/  //this version ignores editable property
	if (item->Tag % (10*MenuItemFlags::docOpen) >= MenuItemFlags::docOpen)
		item->Enabled &= docOpen;

	//recurse to the rest of the menu
	for (int i = 0; i < item->Count; i++)
		refreshMenuItemStates(item->Items[i], docOpen, docEditable, eventChSelected, undoPossible, cursorPresent);
}

void __fastcall TMainForm::MainMenuClick(TObject *Sender)
{
	//on opening the menu, refresh the Enabled property of every item
	TDocumentForm *currentDoc = getCurrentDocument();
	TsDocForm *currSimpleDoc = getCurrentSimpleDocument();

	bool cursorPresent[3];
	if (currentDoc == NULL && currSimpleDoc == NULL)
	{
		for (int i = 0; i < 3; i++)
			cursorPresent[i] = false;
		refreshMenuItemStates(MainMenu->Items, false, false, false, false, cursorPresent);
	} else if(currSimpleDoc != NULL){
		refreshMenuItemStates(MainMenu->Items, true, true, false, true, cursorPresent);
	}else {
		for (int i = 0; i < 3; i++)
			cursorPresent[i] = (currentDoc->cursors.c[i] != -1);
		refreshMenuItemStates(MainMenu->Items, currentDoc != NULL, currentDoc->measurement.getEditable(),
							currentDoc->eventChannelSelected, !currentDoc->undoStack.isEmpty(), cursorPresent);
	}
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Dogodkifrekvenca1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->assignEventFreqAsEventValues();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Odstranidogodek1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->removeEventAtCursor(0);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Vstavidogodek1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->addEvent();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Premaknidogodek1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->moveEventAtFirstCursor();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N0dogodkov1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(0);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N1dogodek1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(1);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N2dogodka1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(2);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N3dogodke1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(3);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::N4dogodki1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(4);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Odstrani1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->removeExtremeEvents();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Koherenca1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->calcCoherence();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::sequentialBRSClick(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->sequentialBRS();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::HelpAboutItemClick(TObject *Sender)
{
    AnsiString version =  __GITVERSION__;
    version += " " __DATE__ " ob " __TIME__;
	aboutDlg->LabelVersion->Caption = version;
	aboutDlg->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::HelpPrirocnikClick(TObject *Sender)
{
	SHELLEXECUTEINFO info;
	UnicodeString filename = Application->ExeName.SubString(0,Application->ExeName.LastDelimiter("\\"));
	filename += L"resources\\" + gettext("User_Interface_EN.pdf");

	ZeroMemory(&info, sizeof(SHELLEXECUTEINFO));
	info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = Handle;
	//wcstombs(helpfilenameAnsi,  helpFilename,1000);
	info.lpFile = filename.c_str();
	ShellExecuteEx(&info);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::HelpOpisClick(TObject *Sender)
{
	SHELLEXECUTEINFO info;
	wchar_t helpFilename[1000];
	wchar_t helpfilenameAnsi[1000];
	wcscpy(helpFilename, Application->ExeName.c_str());
	int i;
	for (i = wcslen(helpFilename)-1; i >= 0; i--)
		if (helpFilename[i] == '\\')
			break;
	wcscpy(helpFilename + i + 1, L"VisECG-opis.pdf");
	ZeroMemory(&info, sizeof(SHELLEXECUTEINFO));
	info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = Handle;
	//wcstombs(helpfilenameAnsi,  helpFilename,1000);
	info.lpFile = helpFilename;
	ShellExecuteEx(&info);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Prevzorenje1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->resampleEventChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Razveljavi1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->undo();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::DiskretnaFourirovatransf1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->fourierTransform();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Sistolinitlak1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->createBPEventChannel();
}
//---------------------------------------------------------------------------

bool __fastcall TMainForm::CloseQuery()
{
	if (TForm::CloseQuery())
	{
		//warn the user to switch off the battery powered EKG device (if it is switched on)
		#ifdef INCLUDE_USB_DRIVERS
			LibCore::CDeviceStatus deviceStatus;
			deviceStatus.getDeviceStatus(false);
			if (deviceStatus.dataAvailable[2])
				Application->MessageBox(gettext("Turn off the device that measures ECG and breathing."), gettext("Warning"), MB_OK);
		#endif
		return true;
	} else
		return false;
}
void __fastcall TMainForm::Zamikkanala1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->shiftChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Brisanjekanala1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->deleteChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Podvajanjekanala1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->duplicateChannel();
}
//---------------------------------------------------------------------------

bool dirExists(const char* dirName_in)
{
  DWORD ftyp = GetFileAttributesA(dirName_in);
  if (ftyp == INVALID_FILE_ATTRIBUTES)
    return false;  //something is wrong with your path!

  if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
	return true;   // this is a directory!

  return false;    // this is not a directory!
}

void TMainForm::openPath(UnicodeString path, bool forceRescan){
	if(TDirectory::Exists(path.c_str())){
		TFolderViewForm *newForm;
		newForm = new TFolderViewForm(this);
		try{
			newForm->LoadFolder(path, forceRescan);
			newForm->Caption = path;
			CloseDuplicateFolderOverview(newForm, path);
		}catch (LibCore::EKGException *e) {
			Application->MessageBox(e->Message.c_str(), gettext("Error while openning folder").data(), MB_OK);
			Cursor = crDefault;
			newForm->Close();
			delete newForm;
			return;
		}
		newForm->Width = newForm->Width+1;
		newForm->Cursor = crDefault;
	}else if(FileExists(path)){
		int openedWin = checkFileAlreadyOpened(path);
		if(openedWin < 0) //check if txt file -> then open converted file in same location
			openedWin = checkFileAlreadyOpened(path+".nekge");
		if(openedWin < 0)
			openedWin = checkFileAlreadyOpened(path+"_part_1.nekge");

		if( openedWin >= 0){
			MDIChildren[openedWin]->BringToFront();
			Application->ProcessMessages();
			return;
		}

		TDocumentForm *newForm;
		try {
			Cursor = crHourGlass;
			newForm = new TDocumentForm(this);
			newForm->LoadMeasurement(path, findHROnOpenedMeasurements);
		} catch (LibCore::EKGException *e) {
			Application->MessageBox(e->Message.c_str(), gettext("Error while openning form").data(), MB_OK);
			Cursor = crDefault;
			newForm->Close();
			delete newForm;
			return;
		}
		newForm->Width = newForm->Width+1;
		newForm->Cursor = crDefault;
	}else{
		Application->MessageBox(gettext("Given path is invalid (does not exist)!").data(), gettext("General error!").data(), MB_OK);
	}
	Application->ProcessMessages();
	Cursor = crDefault;
	autoOpenFile = "";
}

void __fastcall TMainForm::FormShow(TObject *Sender)
{
    //check if eula read or not
	UnicodeString settingsFname("nastavitve.ini");
	AnsiString eulaLocation = AnsiString(L"resources\\" + gettext("EULA_EN.rtf"));
    for (int i = Application->ExeName.Length()-1; i >= 0; --i){
    	if (Application->ExeName[i] == '\\') {
            settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
            eulaLocation = Application->ExeName.SubString(1, i) + eulaLocation;
        	break;
        }
     }

	Ini::File ini(settingsFname.c_str());
    int alreadyRead = 0;
	ini.loadVar(alreadyRead, L"EULA");

    if(alreadyRead != 1){
        // no eula read yet
        EulaForm->Memo1->Lines->LoadFromFile(eulaLocation);
		if(EulaForm->ShowModal() == mrYes){
			ini.storeVar(1,L"EULA");
            ini.save();
        }else{
            Application->Terminate();            
        }
    }

	for (std::vector<std::string>::iterator it = runTimeFlags.begin() ; it != runTimeFlags.end(); ++it){
		if((*it).compare(0, 2, "hr") == 0){
            findHROnOpenedMeasurements = true;
		}
		else if((*it).compare(0, 5, "temp:") == 0){
			alreadyRead = 0;
		}
		else if((*it).compare(0, 7, "length:") == 0){
			try{
				std::string length = (*it).substr(7);
				char *end;
				maxLengthConvertedFiles = std::strtol(length.c_str(), &end, 10);
			}catch(...){
                //default value will be used
			}
		}
	}


	if (autoOpenFile != "")
	{
		this->openPath(autoOpenFile);
		if ((runScript != "") && getCurrentDocument()) {
			if (getCurrentDocument()->runNamedScript(runScript, scriptParams)) {
				// auto close application requested -> discard changes to measurement and
				// terminate
				getCurrentDocument()->dirty = false;
				Close();
			}
		}
	} else if (runScript != "") {
		runNamedScript(runScript, scriptParams);
		Close();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N5dogodkov1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(5);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N6dogodkov1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(6);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N7dogodkov1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(7);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N8dogodkov1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(8);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N9dogodkov1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->replaceWithEquidistantEvents(9);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Linearnidetrenddogkanala1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->detrendChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Odtevanjepovprenevrednostidogkanala1Click(
      TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->offsetChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::RT1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->createRTEventChannel();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Dogodkovneintervaleppi1Click(TObject *Sender)
{
	if (getCurrentDocument())
		if (ImportTextDialog->Execute())
		{
			getCurrentDocument()->importText(ImportTextDialog->FileName);
		}
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::correlationxBRSClick(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->correlationxBRS();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::sinhronizedrBRSClick(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->sinhronizedrBRS();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::frequencyfBRSClick(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->frequencyfBRS();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::IzvozivExcel1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->exportToExcel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Pripravimeritevnaizvoz1Click(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->prepareForExcelExport();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BRS1Click(TObject *Sender)
{
	if (getCurrentDocument()) {
		getCurrentDocument()->everyKnownBRS();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Finapresdogodki1Click(TObject *Sender)
{
	if (getCurrentDocument()) {
		// generate event channels with default settings
		getCurrentDocument()->generateEventChannels();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Analiza1Click(TObject *Sender)
{
	if (getCurrentDocument()) {
		getCurrentDocument()->standardAnalysis();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::OdstraniDogodkeClick(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->removeEventsAtCursors();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::NekgDogodki1Click(TObject *Sender)
{
	if (getCurrentDocument()) {
		getCurrentDocument()->generateEventChannels(2, 0, 4);
	}
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Splonodoloanjedogodkov1Click(TObject *Sender)
{
	if (getCurrentDocument()) {
		getCurrentDocument()->interactiveGenerateEventChannels();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Nariiinshranigrafkoherence1Click(
      TObject *Sender)
{
	if (getCurrentDocument()) {
		getCurrentDocument()->saveCoherenceGraph();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Kontroladihanja1Click(TObject *Sender)
{
/*
    SHELLEXECUTEINFO info;
	char helpFilename[1000];
	strcpy(helpFilename, Application->ExeName.c_str());
	int i;
	for (i = strlen(helpFilename)-1; i >= 0; i--)
		if (helpFilename[i] == '\\')
			break;
	strcpy(helpFilename + i + 1, "kontrola_dihanja.exe");
	ZeroMemory(&info, sizeof(SHELLEXECUTEINFO));
	info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = Handle;
	info.lpFile = helpFilename;
	ShellExecuteEx(&info);
    */
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::DodajDogodekTVClick(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->addEventByValue();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::IzvozVSlikoClick(TObject *Sender)
{
    if (getCurrentDocument() && ExportWmfDialog->Execute()) {
        if (ExportWmfDialog->Options.Contains(ofExtensionDifferent)) {
    		getCurrentDocument()->exportToWmf(ExportWmfDialog->FileName + ".wmf");
        } else
            getCurrentDocument()->exportToWmf(ExportWmfDialog->FileName);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PovprecenjeTlakaClick(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->functionPovprecniTlak();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Izraunajsrednitlak1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->calculateMDP();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PrevzorcenjeSignalaClick(TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->resampleSignalChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Odvod1Click(TObject *Sender)
{
    if (getCurrentDocument()){
  		getCurrentDocument()->differentialChannel(getCurrentDocument()->getMissingSamplesValue(),false);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::casovnaanaliza1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->showAveragerWindow();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ComVisexport1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->comVisExport();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Tval1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->TWaveAnalysis();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Vstavinakljuendogodkovnikanal1Click(
      TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->addRandomChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Odtejpovprejedelautripa1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->averageBeatAndsubtract();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Linearizirajodsekutripa1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->removeBeatLinear();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Pritevanjekanala1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->addMeasurementChannel();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Mnoenjekanalaskonstanto1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->channelGain();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Calc12LeadClick(TObject *Sender)
{
    if (getCurrentDocument())
    {
        //for now just open TDocumentForm with the 12-lead ECG
		TDocumentForm *newForm;
        TDocumentForm *thisForm = getCurrentDocument();
		try {
			Cursor = crHourGlass;
			newForm = new TDocumentForm(this);             
            newForm->generate12leadMeasurement(thisForm);
		} catch (LibCore::EKGException *e) {
			Application->MessageBox(e->Message.c_str(), gettext("Error while openning file").data(), MB_OK);
			Cursor = crDefault;
			newForm->Close();
			delete newForm;
			return;
		}
		newForm->Width = newForm->Width+1;
		newForm->Cursor = crDefault;
		Application->ProcessMessages();
		Cursor = crDefault;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::N12kanalniprikaz1Click(TObject *Sender)
{
    if (getCurrentDocument())
    {
        //for now just opet TDocumentForm with the 12-lead ECG
		TDocumentForm *newForm;
        TDocumentForm *thisForm = getCurrentDocument();
		try {
			Cursor = crHourGlass;
			newForm = new TDocumentForm(this);             
            newForm->generate12leadGraph(thisForm);
		} catch (LibCore::EKGException *e) {
			Application->MessageBox(e->Message.c_str(), gettext("Error while openning file").data(), MB_OK);
			Cursor = crDefault;
			newForm->Close();
			delete newForm;
			return;
		}
		newForm->Width = newForm->Width+1;
		newForm->Cursor = crDefault;
		Application->ProcessMessages();
		Cursor = crDefault;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MobECGdogodki1newClick(TObject *Sender)
{
    TDocumentForm *thisForm = getCurrentDocument();
    if (thisForm) {
        thisForm->mobEcgGetBeatEventsNew();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Odpripregledmape1Click(TObject *Sender)
{
	UnicodeString Directory;
    UnicodeString settingsFname("nastavitve.ini");
   	for (int i = Application->ExeName.Length()-1; i >= 0; --i)
	    if (Application->ExeName[i] == '\\') {
            settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
    		break;
        }
	Ini::File ini(settingsFname.c_str());
    std::string commentText = std::getenv("USERPROFILE");
	ini.loadVar(commentText, L"default folder");
	Directory = string2UnicodeString(commentText);

	if(!DirectoryExists(Directory))
		Directory = UnicodeString(std::getenv("USERPROFILE"));

	if(SelectDirectory(Directory,TSelectDirOpts() << sdAllowCreate ,0))
	{
		openPath(Directory);
	}

}



void __fastcall TMainForm::OdpripregledmapeForceRescanClick(TObject *Sender)
{
	UnicodeString Directory;
	UnicodeString settingsFname("nastavitve.ini");
	for (int i = Application->ExeName.Length()-1; i >= 0; --i)
		if (Application->ExeName[i] == '\\') {
			settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
			break;
		}
	Ini::File ini(settingsFname.c_str());
	std::string commentText = std::getenv("USERPROFILE");
	ini.loadVar(commentText, L"default folder");
	Directory = string2UnicodeString(commentText);

    if(!DirectoryExists(Directory))
        Directory = UnicodeString(std::getenv("USERPROFILE"));

	if(SelectDirectory(Directory,TSelectDirOpts() << sdAllowCreate ,0))
    {
		openPath(Directory, true);
	}

}
//---------------------------------------------------------------------------



void __fastcall TMainForm::Doloanjeutripovnadmapo1Click(TObject *Sender)
{
    TFolderViewForm *currFolder = getCurrentFolderOverview();
    if(currFolder){
        currFolder->folderFindBeat();

    }
}
//---------------------------------------------------------------------------


int TMainForm::checkFileAlreadyOpened(UnicodeString path)
{
    for(int i=0;i<MDIChildCount;i++){
        TDocumentForm *tempForm = dynamic_cast<TDocumentForm*>(MDIChildren[i]);
        if(tempForm){
            AnsiString tempLocation = tempForm->measurement.getFileLocation();
            if(AnsiCompareStr(tempLocation, path) == 0)
                return i;
        }
    }
    return -1;
}

void __fastcall TMainForm::Nastavitvepregledadatotek1Click(TObject *Sender)
{
	double acceptableStdDeviationThr = 0.15, lowBPMthr = 40, highBPMthr = 130, length = 1;
	long chunkLength = 43200;
	size_t selected_detector = TDocumentForm::Version3_derivative;

	UnicodeString settingsFname("nastavitve.ini");
	for (int i = Application->ExeName.Length()-1; i >= 0; --i)
		if (Application->ExeName[i] == '\\') {
			settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
			break;
		}

	Ini::File ini(settingsFname.c_str());
	ini.loadVar(lowBPMthr, L"Folder overview - lowBPM");
	ini.loadVar(highBPMthr, L"Folder overview - highBPM");
	ini.loadVar(acceptableStdDeviationThr, L"Folder overview - std dev");
	ini.loadVar(chunkLength, L"nekgeConversionLength");

	std::string folderLocation;
	ini.loadVar(folderLocation, L"default folder");

	//	UnicodeString unicodeFolder = System::UTF8ToUnicodeString(RawByteString(folderLocation.c_str()));
	UnicodeString unicodeFolder = string2UnicodeString(folderLocation);
	ini.loadVar(length, L"report quality");

	ini.loadVar(selected_detector, L"default detector");



	TFolderOverviewSettings *window = new TFolderOverviewSettings(this);
	window->lowBPMinput->Text = AnsiString(lowBPMthr);
	window->highBPMinput->Text = AnsiString(highBPMthr);
	window->stdInput->Text = AnsiString(acceptableStdDeviationThr*100);
	window->folderLocation->Text = unicodeFolder;
	window->lengthBPM->Text = AnsiString(length);
	window->chunkLength->Text = AnsiString(chunkLength);
	try{
	//TODO
		window->ComboBox1->ItemIndex = selected_detector;
	}   catch(...){
		//do nothing
	}

	if(window->ShowModal() == mrOk){
        ini = Ini::File(settingsFname.c_str());
		ini.storeVar<double>(window->lowBPMinput->Text.ToDouble(),L"Folder overview - lowBPM");
		ini.storeVar<double>(window->highBPMinput->Text.ToDouble(),L"Folder overview - highBPM");
		ini.storeVar<double>(window->stdInput->Text.ToDouble()/100,L"Folder overview - std dev");
		try{
			ini.storeVar<long>((long)window->chunkLength->Text.ToDouble(), L"nekgeConversionLength");
		}catch(...){
			//do nothing...
		}

		try{

		}   catch(...){
//	        do nothing
		}

		//RawByteString utf8String = System::UTF8Encode(window->folderLocation->Text);
		//ini.storeVar(utf8String.c_str(),L"default folder");

		ini.storeVar(wideString2String(window->folderLocation->Text) ,L"default folder");


		double quality = std::max(1.0/60.0, window->lengthBPM->Text.ToDouble());
		ini.storeVar(quality,L"report quality");
		ini.save();
    }
    window->Close();
    delete window;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Absolutniodvod1Click(TObject *Sender)
{
    if (getCurrentDocument()){
  		getCurrentDocument()->differentialChannel(getCurrentDocument()->getMissingSamplesValue(),true);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Absolutnavrednostkanala1Click(TObject *Sender)
{
 if (getCurrentDocument()){
  		getCurrentDocument()->absoluteValueOfChannel();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Zamikanjakanalaskonstanto1Click(TObject *Sender)
{
    if (getCurrentDocument())
		getCurrentDocument()->channelOffset();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Okenskofiltriranjesignala1Click(TObject *Sender)
{
    double window;
    double step;
    try{
        window = InstantDialog::query("VisEcg",	"Window width (seconds): ", "60").ToDouble();
        step = InstantDialog::query("VisECG",	"Filter step: ", "30").ToDouble();
		if(window <=0 || step <= 0){
			Application->MessageBox(gettext("Wrong value (only positive numbers)!").data(), gettext("Error").data(), MB_OK);
            return;
        }
    }catch(Exception * e){
		Application->MessageBox(gettext("Wrong value!").data(), gettext("Error").data(), MB_OK);
        return;
    }

    if (getCurrentDocument())
		getCurrentDocument()->windowFilterEventChannel(window,step, false);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::OkenskoFiltriranjeStriktnoClick(TObject *Sender)
{
    double window;
    double step;
    try{
        window = InstantDialog::query("VisECG",	"Window width (seconds): ", "60").ToDouble();
        step = InstantDialog::query("VisECG",	"Filter step: ", "30").ToDouble();
        if(window <=0 || step <= 0){
			Application->MessageBox(gettext("Wrong value (only positive numbers)!").data(), gettext("Error").data(), MB_OK);
            return;
        }
    }catch(Exception * e){
		Application->MessageBox(gettext("Wrong value!").data(), gettext("Error").data(), MB_OK);
        return;
    }

    if (getCurrentDocument())
		getCurrentDocument()->windowFilterEventChannel(window,step, true);
}
//---------------------------------------------------------------------------

bool TMainForm::runNamedScript(AnsiString scriptName, AnsiString scriptParams) {
    if (scriptName == "--version") {
        std::ofstream outFile("version.txt");
        outFile << __GITVERSION__ << std::flush;
        //printf(__GITVERSION__ "\n");
        return true;
    } else if (scriptName == "--self_check") {
        printf("All right!\n");
        return true;
    }

    printf("Command %s is not supported\n", scriptName.c_str());
    return false;
}

void placeHolderForPhoneTransfer()
{
	UnicodeString convFilename; //conversion program name
	convFilename = Application->ExeName;
	convFilename.Delete(convFilename.LastDelimiter("\\\\"),100);
	convFilename +="\\resources\\acquireFromPhone.exe";

  	SHELLEXECUTEINFO info = {0};
    info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = NULL;
	info.lpVerb = NULL;
	info.lpFile = convFilename.c_str(); //program

    UnicodeString params = "";
	info.lpParameters = params.c_str(); //file to convert

	//info.lpDirectory = partialPathToFile.c_str(); //folder
	info.nShow = SW_HIDE;
	info.hInstApp = NULL;
	ShellExecuteEx(&info);
	WaitForSingleObject(info.hProcess,INFINITE);

	unsigned long exitCode = 0;
	GetExitCodeProcess(info.hProcess, &exitCode);
	TerminateProcess(info.hProcess,0);
	CloseHandle(info.hProcess);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Statistikakanalamedkurzorjema1Click(
      TObject *Sender)
{
	if (getCurrentDocument())
		getCurrentDocument()->calcStatisticsBetweenCursors();    
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Doloanjeasovutripov1Click(TObject *Sender)
{
	TDocumentForm *thisForm = getCurrentDocument();
	if (thisForm) {
		thisForm->mobEcgGetBeatEvents();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MobECGdogodki3Click(TObject *Sender)
{
    TDocumentForm *thisForm = getCurrentDocument();
    if (thisForm) {
        thisForm->mobEcgGetBeatEventsv3();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Doloanjeasovutripovver21Click(TObject *Sender)
{
         TDocumentForm *thisForm = getCurrentDocument();
    if (thisForm) {
        thisForm->mobEcgGetBeatEventsNew();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Replacewithequidistanteventsaverage1Click(TObject *Sender)
{
	if (!getCurrentDocument())
		return;
	getCurrentDocument()->replaceWithEquidistantAverageEvents();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Adaptivenoderivative1Click(TObject *Sender)
{
	TDocumentForm *thisForm = getCurrentDocument();
	if (thisForm) {
		thisForm->mobEcgGetBeatEventsv3(false, 30, 0.5, 10, 0.05, true);
	}

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ESTING1Click(TObject *Sender)
{
    TDocumentForm *thisForm = getCurrentDocument();
	if (thisForm) {
		thisForm->mobEcgGetBeatEventsv5();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Fixheartbeatfaults1Click(TObject *Sender)
{
	TDocumentForm *thisForm = getCurrentDocument();
	if (thisForm) {
		thisForm->fixBeatVariability();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Addannotationchannel1Click(TObject *Sender)
{
	TDocumentForm *thisForm = getCurrentDocument();
	if (thisForm) {
		AnsiString channelName, channelUnit;
		if (!InstantDialog::queryOKCancel(gettext("Enter parameters"),
				gettext("Set channel name"),"", channelName))
			return;
		if (!InstantDialog::queryOKCancel(gettext("Enter parameters"),
				gettext("Set channel measurement unit"), "", channelUnit))
			return;

		int chNum = thisForm->measurement.addAnnotationChannel(channelName, channelUnit);
		thisForm->selectChannel(chNum,true,true);
        thisForm->refreshWholeWindow();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::WndProc(TMessage &Msg)
{
	if (Msg.Msg == WM_COPYDATA){
		try{
			if(Msg.LParam != NULL){
				COPYDATASTRUCT  *strpoint = (COPYDATASTRUCT *) Msg.LParam;
				wchar_t*  ptr = (wchar_t *)strpoint->lpData;

                //TODO what if there are additional flags beside path here?
				UnicodeString uPath = UnicodeString(ptr);
				AnsiString aPath = AnsiString(uPath);
				if(aPath != NULL && aPath.Length() > 0)
					this->openPath(aPath.c_str());
			}
		}catch(...){

		}
		this->Show();
        this->BringToFront();
        this->Focused();
	}

	TForm::WndProc(Msg);
}
void __fastcall TMainForm::SimpleView1Click(TObject *Sender)
{
	simpleView = !simpleView;
	checkVisibleMenuItems(MainMenu->Items);

   	UnicodeString settingsFname("nastavitve.ini");
	for (int i = Application->ExeName.Length()-1; i >= 0; --i){
		if (Application->ExeName[i] == '\\') {
			settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
			break;
		}
	 }
	Ini::File ini(settingsFname.c_str());
	ini.storeVar(simpleView, L"simpleGUI");
    ini.save();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::SimpleView1DrawItem(TObject *Sender, TCanvas *ACanvas,
          TRect &ARect, bool Selected)
{
	TMenuItem *checkButton = (TMenuItem*) Sender;
	checkButton->Checked = simpleView;
}
//---------------------------------------------------------------------------

