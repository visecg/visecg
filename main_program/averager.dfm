object AveragerForm: TAveragerForm
  Left = 247
  Top = 79
  Caption = 'AveragerForm'
  ClientHeight = 625
  ClientWidth = 910
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel2: TBevel
    Left = 8
    Top = 208
    Width = 289
    Height = 161
    Shape = bsFrame
  end
  object Bevel3: TBevel
    Left = 8
    Top = 160
    Width = 289
    Height = 41
    Shape = bsFrame
  end
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 289
    Height = 145
    Shape = bsFrame
  end
  object Label2: TLabel
    Left = 174
    Top = 173
    Width = 47
    Height = 13
    Caption = '+- 0.001 x'
  end
  object eventRange: TLabel
    Left = 192
    Top = 104
    Width = 59
    Height = 13
    Caption = 'eventRange'
  end
  object chart: TChart
    Left = 304
    Top = 5
    Width = 609
    Height = 620
    Cursor = crCross
    AllowPanning = pmVertical
    BackWall.Brush.Style = bsClear
    Foot.Font.Color = clBlack
    Foot.Font.Height = -13
    LeftWall.Pen.Style = psClear
    Legend.Visible = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.Increment = 0.050000000000000000
    BottomAxis.LabelsSeparation = 0
    BottomAxis.Maximum = 0.500100000000000000
    BottomAxis.Minimum = -0.000100000000000000
    BottomAxis.MinorTickCount = 4
    LeftAxis.Axis.Style = psClear
    LeftAxis.AxisValuesFormat = '#,##0.#######'
    LeftAxis.LabelsSeparation = 100
    LeftAxis.MinorTickCount = 5
    LeftAxis.MinorTickLength = 4
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.AutomaticMinimum = False
    RightAxis.AxisValuesFormat = '# ##0.#######'
    RightAxis.Grid.Style = psDashDot
    RightAxis.Increment = 30.000000000000000000
    RightAxis.LabelsSeparation = 200
    RightAxis.Maximum = 180.000000000000000000
    RightAxis.Minimum = -180.000000000000000000
    RightAxis.MinorTickCount = 4
    TopAxis.Visible = False
    View3D = False
    Zoom.Allow = False
    TabOrder = 0
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
  end
  object StaticText1: TStaticText
    Left = 16
    Top = 16
    Width = 120
    Height = 17
    Caption = 'Selected event channel:'
    TabOrder = 1
  end
  object StaticText2: TStaticText
    Left = 16
    Top = 224
    Width = 90
    Height = 17
    Caption = 'Selected channel:'
    TabOrder = 2
  end
  object eventChanName: TEdit
    Left = 88
    Top = 40
    Width = 201
    Height = 21
    TabStop = False
    Color = cl3DLight
    ReadOnly = True
    TabOrder = 3
  end
  object eventChanNum: TEdit
    Left = 16
    Top = 40
    Width = 49
    Height = 21
    TabOrder = 4
    Text = '1'
    OnChange = eventChanNumChange
  end
  object eventChanUpDown: TUpDown
    Left = 65
    Top = 40
    Width = 16
    Height = 21
    Associate = eventChanNum
    Min = 1
    Position = 1
    TabOrder = 5
  end
  object drawChanNum: TEdit
    Left = 16
    Top = 248
    Width = 49
    Height = 21
    TabOrder = 6
    Text = '1'
    OnChange = drawChanNumChange
  end
  object drawChanUpDown: TUpDown
    Left = 65
    Top = 248
    Width = 16
    Height = 21
    Associate = drawChanNum
    Min = 1
    Position = 1
    TabOrder = 7
  end
  object drawChanName: TEdit
    Left = 88
    Top = 248
    Width = 201
    Height = 21
    TabStop = False
    Color = cl3DLight
    ReadOnly = True
    TabOrder = 8
  end
  object startEvent: TEdit
    Left = 16
    Top = 96
    Width = 49
    Height = 21
    TabOrder = 9
    Text = '1'
    OnChange = startEventChange
  end
  object startEventUpDown: TUpDown
    Left = 65
    Top = 96
    Width = 16
    Height = 21
    Associate = startEvent
    Min = 1
    Position = 1
    TabOrder = 10
  end
  object endEvent: TEdit
    Left = 120
    Top = 96
    Width = 49
    Height = 21
    TabOrder = 11
    Text = '1'
    OnChange = endEventChange
  end
  object endEventUpDown: TUpDown
    Left = 169
    Top = 96
    Width = 16
    Height = 21
    Associate = endEvent
    Min = 1
    Position = 1
    TabOrder = 12
  end
  object StaticText3: TStaticText
    Left = 16
    Top = 72
    Width = 106
    Height = 17
    Caption = 'Selected event range'
    TabOrder = 13
  end
  object drawRange1: TEdit
    Left = 16
    Top = 304
    Width = 49
    Height = 21
    TabOrder = 14
    Text = '4000'
    OnChange = drawRange1Change
  end
  object drawRange1UpDown: TUpDown
    Left = 65
    Top = 304
    Width = 16
    Height = 21
    Associate = drawRange1
    Min = 1
    Max = 30000
    Increment = 10
    Position = 4000
    TabOrder = 15
  end
  object StaticText4: TStaticText
    Left = 16
    Top = 280
    Width = 99
    Height = 17
    Caption = 'Display range +-[ms]'
    TabOrder = 16
  end
  object Panel1: TPanel
    Left = 128
    Top = 216
    Width = 161
    Height = 25
    TabOrder = 17
    object radioButtonMeasurement: TRadioButton
      Left = 8
      Top = 5
      Width = 73
      Height = 17
      Caption = 'measured'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = radioButtonClick
    end
    object radioButtonEvent: TRadioButton
      Left = 72
      Top = 5
      Width = 81
      Height = 17
      Caption = 'event'
      TabOrder = 1
      OnClick = radioButtonClick
    end
  end
  object averageEnabled: TCheckBox
    Left = 16
    Top = 336
    Width = 137
    Height = 17
    Caption = 'Average value'
    Checked = True
    State = cbChecked
    TabOrder = 18
    OnClick = averageEnabledClick
  end
  object averageSamplingTrackBar: TTrackBar
    Left = 152
    Top = 336
    Width = 137
    Height = 25
    Max = 5
    Min = 1
    Position = 3
    TabOrder = 19
    ThumbLength = 15
    OnChange = averageWindowingTrackBarChange
  end
  object lineDrawCheckBox: TCheckBox
    Left = 128
    Top = 312
    Width = 97
    Height = 17
    Caption = 'Draw combined'
    Checked = True
    State = cbChecked
    TabOrder = 20
    OnClick = lineDrawCheckBoxClick
  end
  object startEventTime: TEdit
    Left = 16
    Top = 120
    Width = 89
    Height = 21
    TabOrder = 21
    OnChange = startEventTimeChange
  end
  object endEventTime: TEdit
    Left = 120
    Top = 120
    Width = 89
    Height = 21
    TabOrder = 22
    OnChange = endEventTimeChange
  end
  object IzvozPodatkov: TGroupBox
    Left = 12
    Top = 571
    Width = 285
    Height = 49
    Caption = 'Data export'
    TabOrder = 23
    object saveTextButton: TButton
      Left = 8
      Top = 16
      Width = 81
      Height = 25
      Caption = 'as text'
      TabOrder = 0
      OnClick = saveTextButtonClick
    end
    object SaveGraphButton: TButton
      Left = 96
      Top = 16
      Width = 81
      Height = 25
      Caption = 'graphics'
      TabOrder = 1
      OnClick = SaveGraphButtonClick
    end
    object clipboardButton: TButton
      Left = 192
      Top = 16
      Width = 81
      Height = 25
      Hint = 'clipboard'
      Caption = 'clipboard'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = clipboardButtonClick
    end
  end
  object autoYScaleCheck: TCheckBox
    Left = 16
    Top = 168
    Width = 129
    Height = 17
    Caption = 'Automatic Y axis'
    Checked = True
    State = cbChecked
    TabOrder = 24
    OnClick = autoYScaleCheckClick
  end
  object yScaleEdit: TEdit
    Left = 224
    Top = 168
    Width = 49
    Height = 21
    TabOrder = 25
    Text = '100'
    OnChange = autoYScaleCheckClick
  end
  object yScaleUpDown: TUpDown
    Left = 273
    Top = 168
    Width = 16
    Height = 21
    Associate = yScaleEdit
    Min = 1
    Max = 1000
    Position = 100
    TabOrder = 26
  end
  object infoGrid: TStringGrid
    Left = 8
    Top = 376
    Width = 289
    Height = 193
    Color = clBtnFace
    DefaultColWidth = 47
    DefaultRowHeight = 20
    RowCount = 9
    ScrollBars = ssNone
    TabOrder = 27
    OnDrawCell = infoGridDrawCell
    ColWidths = (
      47
      47
      47
      47
      47)
    RowHeights = (
      20
      20
      20
      20
      20
      20
      20
      20
      20)
  end
  object checkBoxAutoResample: TCheckBox
    Left = 176
    Top = 72
    Width = 113
    Height = 17
    Caption = 'Auto resampling'
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 28
    OnClick = checkBoxAutoResampleClick
  end
  object saveDialogGraph: TSaveDialog
    DefaultExt = 'wmf'
    Filter = 'Graphics (*.wmf)|*.wmf|All files|*.*'
    Left = 160
    Top = 280
  end
  object saveDialogText: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Text (*.txt)|*.txt|All files|*.*'
    Left = 192
    Top = 280
  end
end
