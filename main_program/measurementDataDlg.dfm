object dlgMeasurementData: TdlgMeasurementData
  Left = 397
  Top = 1
  BorderStyle = bsDialog
  Caption = 'Podatki o meritvi'
  ClientHeight = 705
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Microsoft Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 8
    Top = 8
    Width = 124
    Height = 13
    Caption = 'Name of the measurement'
  end
  object Label7: TLabel
    Left = 8
    Top = 64
    Width = 157
    Height = 13
    Caption = 'Measurement start date and time:'
  end
  object labelSamplingRate: TLabel
    Left = 8
    Top = 168
    Width = 93
    Height = 13
    Caption = 'Sampling frequency'
  end
  object Label9: TLabel
    Left = 8
    Top = 88
    Width = 44
    Height = 13
    Caption = 'Comment'
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 384
    Width = 529
    Height = 281
    Caption = 'Patient data'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 32
      Width = 28
      Height = 13
      Caption = 'Name'
    end
    object Label2: TLabel
      Left = 8
      Top = 64
      Width = 3
      Height = 13
    end
    object Label3: TLabel
      Left = 272
      Top = 72
      Width = 45
      Height = 13
      Caption = 'Birth date'
    end
    object Label4: TLabel
      Left = 8
      Top = 104
      Width = 46
      Height = 13
      Caption = 'Diagnosis'
    end
    object Label5: TLabel
      Left = 8
      Top = 184
      Width = 46
      Height = 13
      Caption = 'COmment'
    end
    object editName: TEdit
      Left = 80
      Top = 28
      Width = 281
      Height = 21
      MaxLength = 50
      TabOrder = 0
    end
    object RadioGroup1: TRadioGroup
      Left = 8
      Top = 56
      Width = 241
      Height = 41
      Caption = 'Sex'
      TabOrder = 1
    end
    object radioFemale: TRadioButton
      Left = 24
      Top = 72
      Width = 65
      Height = 17
      Caption = 'female'
      TabOrder = 2
    end
    object radioMale: TRadioButton
      Left = 96
      Top = 72
      Width = 65
      Height = 17
      Caption = 'male'
      TabOrder = 3
    end
    object radioUnspecified: TRadioButton
      Left = 152
      Top = 72
      Width = 89
      Height = 17
      Caption = 'undefined'
      Checked = True
      TabOrder = 4
      TabStop = True
    end
    object memoDiagnosis: TMemo
      Left = 8
      Top = 120
      Width = 513
      Height = 57
      ScrollBars = ssVertical
      TabOrder = 5
    end
    object memoComment: TMemo
      Left = 8
      Top = 200
      Width = 513
      Height = 73
      ScrollBars = ssVertical
      TabOrder = 6
    end
    object dateOfBirthPicker: TDateTimePicker
      Left = 352
      Top = 68
      Width = 105
      Height = 21
      Date = 18264.000000000000000000
      Time = 18264.000000000000000000
      MaxDate = 73050.999988425930000000
      MinDate = -7303.000000000000000000
      TabOrder = 7
    end
  end
  object btnOK: TBitBtn
    Left = 464
    Top = 672
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object btnCancel: TBitBtn
    Left = 384
    Top = 672
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object editMeasurementName: TEdit
    Left = 8
    Top = 24
    Width = 529
    Height = 21
    TabOrder = 3
  end
  object datePicker: TDateTimePicker
    Left = 176
    Top = 60
    Width = 113
    Height = 21
    Date = 42768.469982650500000000
    Time = 42768.469982650500000000
    Enabled = False
    TabOrder = 4
  end
  object timePicker: TDateTimePicker
    Left = 296
    Top = 60
    Width = 98
    Height = 21
    Date = 38026.470977291700000000
    Time = 38026.470977291700000000
    Enabled = False
    Kind = dtkTime
    TabOrder = 5
  end
  object memoMeasurementComment: TMemo
    Left = 8
    Top = 104
    Width = 529
    Height = 57
    ScrollBars = ssVertical
    TabOrder = 6
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 192
    Width = 529
    Height = 185
    Caption = 'Channels (event no., name (unit))'
    TabOrder = 7
    object listChannels: TListBox
      Left = 8
      Top = 16
      Width = 513
      Height = 161
      ItemHeight = 13
      TabOrder = 0
    end
  end
end
