
#include <stdio.h>
#include <string>

#include <System.hpp> //borland effin UnicodeString

namespace nevroreport{

std::string create_node(std::string const &node_name, std::string const  &data, bool const toBase64=true);

//double
std::string create_node_d(std::string const &node_name, double const data, bool const toBase64=false);

std::string create_node_borland(std::string const &node_name, UnicodeString const &text, bool const toBase64=true);

class ReportPrinter{
	std::string report_text;
    public:
	void print_to_file(FILE* file, UnicodeString const  &author);
	void add_root_node(std::string const &node_name, std::string const  &data, bool toBase64=false);
	void add_root_node_borland(std::string const &node_name, UnicodeString const  &data, bool toBase64=true);
};

}
