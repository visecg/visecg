�
 TDOCUMENTFORM 0_1  TPF0TDocumentFormDocumentFormLeft� Top!CaptionDocumentFormClientHeight�ClientWidthColor	clBtnFaceFont.CharsetEASTEUROPE_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 	FormStyle
fsMDIChild	Icon.Data
�             �     (       @          �                                                                                                                                                                                                                ��                                                                                                                          ��  ��  ��                                                                                                                      ��  ��  ��                                                                                                                  ��  ��  ��  ��  ��                                                                                                              ��  ��          ��  ��                                                                                                      ��  ��  ��          ��  ��                                                                                                  ��  ��  ��                  ��  ��                                                                                              ��  ��  ��                  ��  ��  ��                                                                                      ��  ��  ��                          ��  ��                                                                                  ��  ��  ��  ��                          ��  ��  ��                                                                              ��  ��  ��                                  ��  ��  ��                                                                      ��  ��  ��  ��                                  ��  ��  ��                                                                      ��  ��  ��                                          ��  ��  ��                                                              ��  ��  ��  ��                                          ��  ��  ��  ��                                                      ��  ��  ��  ��                                                  ��  ��  ��                                                      ��  ��  ��                                                          ��  ��  ��                                              ��  ��  ��  ��                                                          ��  ��  ��  ��                                          ��  ��  ��                                                                  ��  ��  ��                                      ��  ��  ��                                                                          ��  ��  ��                                  ��  ��  ��                                                                          ��  ��  ��                                  ��  ��  ��                          ��  ��  ��  ��  ��  ��                          ��  ��  ��                                      ��  ��  ��                      ��  ��          ��  ��                      ��  ��  ��                                          ��  ��  ��  ��          ��  ��  ��  ��          ��  ��  ��  ��          ��  ��  ��  ��                                              ��  ��  ��  ��  ��  ��  ��  ��                  ��  ��  ��  ��  ��  ��  ��  ��                                                              ��  ��                                          ��  ��                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ����������������?�������������������������������������?���?��������������������a���?����������������������������
KeyPreview	OldCreateOrderPosition	poDefaultScaledVisible	WindowStatewsMaximizedOnClose	FormCloseOnCreate
FormCreate	OnKeyDownFormKeyDownOnMouseWheelDownFormMouseWheelDownOnMouseWheelUpFormMouseWheelUpOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TLabellblStartDateTimeLeftTopWidth4HeightCaption
Start time  TLabellblStartDateTimeVarLefthTopWidthmHeightCaptionlblStartDateTimeVar  TChartChartLeft� Top(Width[HeightyCursorcrCrossAllowPanning
pmVerticalBackWall.Brush.StylebsClearLegend.Font.CharsetEASTEUROPE_CHARSETLegend.LegendStylelsSeriesLegend.Visible	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.LabelStyletalValueBottomAxis.MinorGrid.ColorclGrayBottomAxis.MinorGrid.StylepsDotBottomAxis.Title.CaptionTime from measurement start [s]LeftAxis.MinorGrid.ColorclGrayLeftAxis.MinorGrid.StylepsDotRightAxis.AutomaticRightAxis.AutomaticMaximumRightAxis.AutomaticMinimumRightAxis.Grid.VisibleRightAxis.LabelStyletalValueRightAxis.MinorTicks.VisibleRightAxis.Ticks.VisibleRightAxis.TicksInner.VisibleTopAxis.AutomaticTopAxis.AutomaticMaximumTopAxis.AutomaticMinimumTopAxis.Axis.VisibleTopAxis.Grid.VisibleTopAxis.MinorTicks.VisibleTopAxis.TickInnerLength
TopAxis.TickLengthTopAxis.Ticks.WidthTopAxis.TicksInner.SmallDots	View3D
Zoom.AllowOnAfterDrawChartAfterDrawTabOrder OnMouseMoveChartMouseMove	OnMouseUpChartMouseUpOnMouseWheelChartMouseWheelDefaultCanvasTGDIPlusCanvasPrintMargins ColorPaletteIndex  
TScrollBar
horzScrollLeft� Top�WidthSHeightLargeChange�Max�� PageSize SmallChange
TabOrderTabStopOnChangehorzScrollChange  TStringGridgridChannelViewLeft Top(Width� Height� DefaultColWidthZDefaultRowHeightRowCountFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoColSizing	goEditinggoTabsgoAlwaysShowEditor 
ParentFontParentShowHint
ScrollBars
ssVerticalShowHintTabOrderOnClickgridChannelViewClick
OnDblClickgridChannelViewDblClick
OnDrawCellgridChannelViewDrawCellOnMouseMovegridChannelViewMouseMoveOnMouseWheelDowngridChannelViewMouseWheelDownOnMouseWheelUpgridChannelViewMouseWheelUp	ColWidthsZZZZZ 
RowHeights   TToolBarToolBarLeft Top WidthHeight<AutoSize	BorderWidthButtonWidth9CaptionToolBarEdgeBordersebTopebBottom Font.CharsetEASTEUROPE_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontShowCaptions	TabOrder TToolButton	btnUnzoomLeft Top HintDecrease twice the x axisCaption-(x2)
ImageIndexOnClickbtnUnzoomClick  TToolButtonbtnZoomLeft9Top HintIncrease twice the x axisCaption+ (x2)
ImageIndex OnClickbtnZoomClick  TToolButtonbtnZoomCursorLeftrTop Hint=Show from first to last cursor or around the firstif only oneCaption
+ (cursor)
ImageIndexOnClickbtnZoomCursorClick  TToolButtonToolButton5Left� Top WidthCaptionToolButton5
ImageIndexStyletbsSeparator  TButtonshow10secBtnLeft� Top Width2HeightCaption10 sTabOrderOnClickshow10secBtnClick  TButtonshow30secBtnLeft� Top Width2HeightCaption30 sTabOrderOnClickshow30secBtnClick  TButtonshow60secBtnLeftTop Width2HeightCaption60 sTabOrderOnClickshow60secBtnClick  TButtonshow10minbuttonLeftITop Width2HeightCaption10 minTabOrderOnClickshow10minbuttonClick  TButtonshow30minbuttonLeft{Top Width2HeightCaption30 minTabOrderOnClickshow30minbuttonClick  TButtonButton3Left�Top Width2HeightCaption1 hTabOrderOnClickButton3Click  TButtonshow4hbuttonLeft�Top Width2HeightCaption4 h TabOrderOnClickshow4hbuttonClick  TToolButton
btnViewAllLeftTop HintShow whole measurementCaptionall
ImageIndexOnClickbtnViewAllClick  TToolButtonToolButton2Left Top WidthCaptionToolButton2
ImageIndexWrap	StyletbsSeparator  TButtonButton1Left TopWidthKHeightHintJump left in measurementCaption<TabOrderOnClickButton1Click  TButtonButton2LeftKTopWidthKHeightHintJump right in measurementCaption>TabOrder	OnClickButton2Click  TButtonButton4Left� TopWidthKHeightHint&Return to previous view of measurementCaptionRestore viewTabOrder
OnClickButton4Click  TToolButtonToolButton6Left� TopWidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonbtnTrueYscaleLeft� TopHint0Show the window for setting properties of y-axisCaptionEdit y-axis
ImageIndexOnClickbtnTrueYscaleClick  TToolButtonToolButton4Left"TopWidthCaptionToolButton4
ImageIndexStyletbsSeparator  TToolButtonbtnShowChannelTableLeft*TopHintShow/hide table of channelsCaptionTable
ImageIndexStyletbsCheckOnClickbtnShowChannelTableClick  TToolButtonbtnShowLegendLeftcTopHintShow/hide legend in the grafCaptionLegend
ImageIndexStyletbsCheckOnClickbtnShowLegendClick  TToolButtonToolButton1Left�TopWidthCaptionToolButton1
ImageIndexStyletbsSeparator  TToolButtonbtnShowAllChannelsLeft�TopHintVie all channelsCaptionView all
ImageIndexOnClickbtnShowAllChannelsClick  TToolButtonbtnHideAllChannelsLeft�TopHint%Hide all channels except the selectedCaptionHide all
ImageIndexOnClickbtnHideAllChannelsClick  TToolButtonToolButton3LeftTopWidthCaptionToolButton3
ImageIndexStyletbsSeparator  	TComboBoxcomboCursorColorLeftTopWidth;HeightHint:Select the cursor color to be set by the left mouse click.StylecsDropDownListDropDownCount	ItemIndex TabOrder TextblueItems.Stringsbluegreenred   TToolButtonbtnRemoveAllCursorsLeftYTopHintRemove all three cursorsCaptionRemove
ImageIndexOnClickbtnRemoveAllCursorsClick   	TGroupBoxgroupCursorsLeft Top(Width� Height� CaptionCursorsTabOrder TLabellblBlueCursorLeftTopWidthLHeightCaptionlblBlueCursorFont.CharsetEASTEUROPE_CHARSET
Font.ColorclBlueFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabellblGreenCursorLeftTop WidthUHeightCaptionlblGreenCursorFont.CharsetEASTEUROPE_CHARSET
Font.ColorclGreenFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabellblRedCursorLeftTop0WidthJHeightCaptionlblRedCursorFont.CharsetEASTEUROPE_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel1LeftTopHWidthHeightCaptionblue-Font.CharsetEASTEUROPE_CHARSET
Font.ColorclBlueFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel2Left0TopHWidth#HeightCaptiongreen:Font.CharsetEASTEUROPE_CHARSET
Font.ColorclGreenFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabellblBlueGreenDiffLeftPTopHWidthZHeightCaptionlblBlueGreenDiff  TLabelLabel3LeftTopXWidthHeightCaptionblue-Font.CharsetEASTEUROPE_CHARSET
Font.ColorclBlueFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel4Left0TopXWidthHeightCaptionred:Font.CharsetEASTEUROPE_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabellblBlueRedDiffLeftPTopXWidthOHeightCaptionlblBlueRedDiff  TLabelLabel6LeftTophWidth$HeightCaptiongreen-Font.CharsetEASTEUROPE_CHARSET
Font.ColorclGreenFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel7Left0TophWidthHeightCaptionred:Font.CharsetEASTEUROPE_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabellblGreenRedDiffLeftPTophWidthXHeightCaptionlblGreenRedDiff    