object CoherenceParamsForm: TCoherenceParamsForm
  Left = 393
  Top = 183
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Coherence parameters'
  ClientHeight = 201
  ClientWidth = 313
  Color = clBtnFace
  Constraints.MinHeight = 31
  Constraints.MinWidth = 160
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 71
    Width = 216
    Height = 13
    Caption = 'Enter the desired number of windows (1 to 15)'
  end
  object Label2: TLabel
    Left = 5
    Top = 103
    Width = 216
    Height = 13
    Caption = 'Enter the desired overlap (between 0 and 0.8)'
  end
  object Label3: TLabel
    Left = 5
    Top = 7
    Width = 204
    Height = 13
    Caption = 'Enter the number of the first event channel '
  end
  object Label4: TLabel
    Left = 5
    Top = 39
    Width = 223
    Height = 13
    Caption = 'Enter the number of the second  event channel'
  end
  object ErrorLabel: TLabel
    Left = 29
    Top = 177
    Width = 93
    Height = 13
    Caption = 'Input data error!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Dummy: TLabel
    Left = 0
    Top = 0
    Width = 3
    Height = 13
    AutoSize = False
  end
  object Label13: TLabel
    Left = 240
    Top = 160
    Width = 73
    Height = 41
    AutoSize = False
  end
  object Label5: TLabel
    Left = 5
    Top = 135
    Width = 185
    Height = 13
    Caption = 'Show frequency between 0 and ... [Hz]'
  end
  object OverlapEdit: TEdit
    Left = 237
    Top = 99
    Width = 65
    Height = 21
    TabOrder = 2
    Text = '0.5'
    OnChange = OverlapEditChange
  end
  object OKBitBtn: TBitBtn
    Left = 149
    Top = 171
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 3
  end
  object CancelBitBtn: TBitBtn
    Left = 229
    Top = 171
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    NumGlyphs = 2
    TabOrder = 4
  end
  object Channel1Edit: TEdit
    Left = 237
    Top = 3
    Width = 49
    Height = 21
    TabOrder = 6
    Text = '0'
    OnChange = Channel1EditChange
  end
  object Channel1UpDown: TUpDown
    Left = 286
    Top = 3
    Width = 16
    Height = 21
    Associate = Channel1Edit
    TabOrder = 5
  end
  object Channel2Edit: TEdit
    Left = 237
    Top = 35
    Width = 49
    Height = 21
    TabOrder = 0
    Text = '0'
    OnChange = Channel2EditChange
  end
  object NumOfWindowsEdit: TEdit
    Left = 237
    Top = 67
    Width = 49
    Height = 21
    TabOrder = 1
    Text = '0'
    OnChange = NumOfWindowsEditChange
  end
  object NumOfWindowsUpDown: TUpDown
    Left = 286
    Top = 67
    Width = 16
    Height = 21
    Associate = NumOfWindowsEdit
    TabOrder = 7
  end
  object Channel2UpDown: TUpDown
    Left = 286
    Top = 35
    Width = 16
    Height = 21
    Associate = Channel2Edit
    TabOrder = 8
  end
  object EditMaxFreq: TEdit
    Left = 237
    Top = 131
    Width = 65
    Height = 21
    TabOrder = 9
    Text = '0.5'
    OnChange = EditMaxFreqChange
  end
end
