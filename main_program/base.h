// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//comment-out the next line to make driver-independent version without recording functionality
//also comment-out the library in nevroEKG.cpp
//#define INCLUDE_USB_DRIVERS

#ifndef _INCLUDE_LIBCORE_BASE_H_
#define _INCLUDE_LIBCORE_BASE_H_

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#define NDEBUG
#include <assert.h>

#include <math.h>
#include <system.hpp>
#include <sysutils.hpp>

#include <vector>

#include "gnugettext.hpp"

#ifndef SAVING
	#define SAVING false
#endif

namespace LibCore
{

class EKGException : public Sysutils::Exception
//usage: throw new EKGException(__LINE__, "Class::Method: A bloody unbelievable error occurred.");
{
public:
	int errorNum;
//	AnsiString errorText;

	EKGException(const int _errorNum, const AnsiString _errorText) : Sysutils::Exception(_errorText)
	{
		errorNum = _errorNum;
//		errorText = _errorText;
	}
};
}; //end namespace LibCore

#ifdef INCLUDE_USB_DRIVERS
	#include "ftd2xx.h"
	void ftCheck(FT_STATUS ftStatus, char *funcName);
#else
	#define ftCheck(A,B) throw new LibCore::EKGException(__LINE__, "Measurement recording is disabled in this version.")
	#define FT_HANDLE int
#endif

namespace LibCore
{

void appendCommentWithTimeStamp(AnsiString& comment, const AnsiString& addedText);
void setCommentWithTimeStamp(AnsiString& comment, const AnsiString& addedText);

//replaces , with ., then uses atof
double myAtof(char *str);
inline double myAtof(AnsiString str) { return myAtof(str.c_str()); }

AnsiString makeStringASCII(const AnsiString &str);
//replace all characters with A..Z, a..z, ' ', _ , -

void fprintfMultilineString(FILE *file, AnsiString string, AnsiString numLinesString);
//example: fprintfMultilineString(file, "line1\nline2", "commentLines") outputs
//2 commentLines
//line1
//line2

void fgetAnsiString(FILE *file, AnsiString &string);
void fgetUnicodeString(FILE *file, UnicodeString &string);
//reads file until first '\n'; '\n' is removed from both the file and the string

int myfscanf(FILE *file, char *formatStr, ...);
//myscanf: reads a line from file and parses it with sscanf
//this is to avoid unwanted behaviour of fscanf which removes from the stream any newlines following the read string
//this function only works if formatStr is single-line, i.e. no \n except possibly at the end

void fscanfMultilineString(FILE *file, AnsiString &string, AnsiString numLinesString);
//inverse op of fprintfMultilineString

AnsiString extensionOfFilename(const AnsiString &filename);
AnsiString pathOfFilename(const AnsiString &filename);

AnsiString dateToString(const TDateTime date);
AnsiString dateToStringAccurate(const TDateTime date);
TDateTime stringToDate(const AnsiString &string);
//string format for both functions: 17.03.2003 04:12:05

FT_HANDLE openAndSetupDevice(int port, int baudRate, int timeOutMs, HANDLE hFtdMutex);
//opens device, sets RTS to stop any data, purge buffers & manually flush them because purging seems not to work reliably


class CDeviceStatus
{
public:
	static const int numPorts = 4;
	static double EKGbatteryVoltageCritical, EKGbatteryVoltageLow, EKGbatteryVoltageUnknown;
    static double EKGbatteryVoltageFactor;
		//critical: measurements not allowed, low: warning shown, unknown >> 12V

	bool serialPortConnected[numPorts]; //true if FTDI device is detected by computer
	bool dataAvailable[numPorts];		//true if any data are coming (using RTS signal where needed)
	double EKGbatteryVoltage;			//measured if dataAvailable[2], voltageUnknown otherwise

	void getDeviceStatus(bool testForColinDigital); //communicates with device to fill all the fields
    static void initStatic(); // call this function once Application is initialized
};

template <class T, int reallocSpace>
class CTable
//template class of a 1-D auto-allocate table
//only for simple types T that need not be explicitly constructed
//key method: operator[] returns reference to an element
//reallocSpace = number of elements allocated when out of space
{
protected:
	static const int maxSpace = 200*1024*1024/sizeof(T); //upper size limit in num of elements
	int allocedSpace, numElems, numElemsFromFile; //space currently allocated, elements actually contained
	T* data; //element store
	std::vector<T> dataVector; // hacked the old C code by using vector in the background (simplified resizing)
	T& access(const int index)
	//NOTE: accesses elements; if next non-present is demanded, it is added to table
	//(and realloc is done if necessary)
	{
		assert(index >= 0);
		assert(index <= numElems);
		if (index == numElems)
		{
			numElems++;
			if (numElems > allocedSpace)
				reallocTable(allocedSpace + reallocSpace);
		}
		return data[index];
	}
	const T& accessConst(const int index) const //not for left-side of assignment; is a const function
	{
		assert(index >= 0 && index < numElems);
		return data[index];
	}

public:
	CTable()
	{
		data = NULL;
		allocedSpace = 0;
		numElems = 0;
		reallocTable(0);
	}
	CTable(const CTable &other)
	{
		data = NULL;
		allocedSpace = 0;
		numElems = 0;
		*this = other;
	}
	CTable(const int spaceToAlloc)
	{
		data = NULL;
		allocedSpace = 0;
		numElems = 0;
		assert(spaceToAlloc >= 0 && spaceToAlloc <= maxSpace);
		if (spaceToAlloc > 0)
			reallocTable(spaceToAlloc);
	}
	virtual ~CTable()
	{
		if (data)
		{
			// VECTOR HACK 
            //free(data);
			data = NULL;
		}
	}

	/*
	* de-allocated space that is taken with swap
	*/
	void emptyTable(int spaceToAlloc)
	{
		assert(spaceToAlloc >= 0 && spaceToAlloc <= maxSpace);
		numElems = 0;
		std::vector<T> dataVector1(spaceToAlloc);
		dataVector1.swap(dataVector);
		allocedSpace = 0;
	}
	void reallocTable(const int newSpace)
	//NOTE: new space can be larger or smaller (or the same)
	{
		assert(newSpace > 0 && newSpace < maxSpace && newSpace >= numElems);
        // VECTOR HACK 
        //		data = (T*)realloc(data, newSpace*sizeof(T));
		try{
			dataVector.resize(newSpace);
			data = &dataVector[0];
		}catch(...){
			throw new EKGException(__LINE__, AnsiString("CTable::reallocTable: out of memory: ")+
				AnsiString((newSpace*sizeof(data[0]))/(1000000))+" MB could not be allocated!");
		}

		allocedSpace = newSpace;
	}

	int getNumElems() const
	{
		return numElems;
	}
	T& operator[](const int index)
	{
		return access(index);
	}
	const T& operator[](const int index) const
	{
        assert(index < numElems);
		return accessConst(index);
	}
	void deleteElems(int firstToDelete, int lastToDelete)
	{
		assert(firstToDelete <= lastToDelete);
		firstToDelete = std::max(firstToDelete, 0);
		lastToDelete = std::min(lastToDelete, numElems-1);
		for (int i = lastToDelete+1; i < numElems; i++)
			access(i-1-lastToDelete+firstToDelete) = access(i);
		numElems -= lastToDelete-firstToDelete+1;
		reallocTable(numElems + reallocSpace);
	}
	void insertElem(int insertBefore, const T &newElem)
	{
		assert(insertBefore >= 0 && insertBefore <= numElems);
		access(numElems);
		for (int i = numElems-1; i > insertBefore; i--)
			access(i) = access(i-1);
		access(insertBefore) = newElem;
	}
	void writeToFile(FILE *file) const
	{
		assert(file);
		if (fwrite(data, sizeof(T), numElems, file) != (size_t)numElems)
			throw new EKGException(__LINE__, "CTable::writeToFile: writing data failed");
		if (!fprintf(file, "\n"))
			throw new EKGException(__LINE__, "CTable::writeToFile: writing terminating newline failed");
	}
	void readFromFile(FILE *file)
	//NOTE: numElems should be already set because that number of items will be read
	{
		assert(file);
		reallocTable(std::max(numElems, 1));
		if ((int)fread(data, sizeof(T), numElems, file) != numElems)
			throw new EKGException(__LINE__, "CTable::readFromFile: reading data failed");
		if (fgetc(file) != '\n')
			throw new EKGException(__LINE__, "CTable::readFromFile: terminating newline not found");
	}
	CTable<T, reallocSpace> &operator=(const CTable<T, reallocSpace> &other)
	{
		numElems = 0;
		reallocTable(other.allocedSpace);
		for (int i = 0; i < other.numElems; i++)
			access(i) = other.accessConst(i);
		return *this;
	}
}; //end class CTable

struct ChannelViewProps
{
	bool visible;
	double offset; //offset == 1 -> channel is offset up from previous visible channel by 1 height of selected channel
	double zoomY; //zoom == selectedChannel.zoom -> channel has same total height on screen as selected channel
	ChannelViewProps()
	{
		visible = true;
		offset = 1;
		zoomY = 1;
	}
	void writeToFile(FILE *file, int fileVersion) const
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "ChannelViewProps::writeToFile: unsupported file version");
		if (fprintf(file, "viewProps visible %d offset %lf zoomY %lf\n",
				(visible ? 1 : 0), offset, zoomY) == EOF)
			throw new EKGException(__LINE__, "ChannelViewProps::writeToFile: error writing properties to file");
	}
	void readFromFile(FILE *file, int fileVersion)
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "ChannelViewProps::readFromFile: unsupported file version");
		int intVisible;
		if (myfscanf(file, "viewProps visible %d offset %lf zoomY %lf\n",
				&intVisible, &offset, &zoomY) != 3)
			throw new EKGException(__LINE__, "ChannelViewProps::readFromFile: error reading properties to file");
		if (zoomY < 0 || (intVisible != 1 && intVisible != 0))
			throw new EKGException(__LINE__, "ChannelViewProps::readFromFile: invalid property value");
		visible = (intVisible == 1);
	}
};

template <class T, int reallocSpace>
class IChannel: public CTable<T, reallocSpace>
//expanded functionality of CTable
//adds: -channel name & measurement unit
//      -min&max values for drawing purposes
//		-file format contains number of elements so that file I/O is self-contained
{
protected:
	AnsiString channelName, measurementUnit;
	AnsiString comment;
	double minValue, maxValue; //for drawing purposes;
					//NOTE consistence with data is not automatic (call refreshMinMaxValues() to refresh
					//NOTE: if T is non-numerical then min&max don't mean anything

	virtual void writeHeaderToFile(FILE *file, int fileVersion) const = 0;
	//should write everything except: measurement unit, number of elems, and actual data
	virtual void readHeaderFromFile(FILE *file, int fileVersion) = 0;
	//should read everything except: measurement unit, number of elems, and actual data

	virtual double &itemNumericalValue(T &item) = 0;

public:
	ChannelViewProps viewProps;
    double channelMultiplier;
    double channelOffset;

	IChannel()
		: CTable<T, reallocSpace>()
	{
		channelName = "untitled";
		measurementUnit = "1";
		comment = "";
		minValue = maxValue = 0;
	}
	IChannel(const IChannel &other)
		: CTable<T, reallocSpace>()
	{
		*this = other;
	}
	IChannel(const AnsiString _channelName)
		: CTable<T, reallocSpace>()
	{
		channelName = _channelName;
		measurementUnit = "1";
		comment = "";
		minValue = maxValue = 0;
	}
	IChannel(const AnsiString _channelName, int spaceToAlloc)
		: CTable<T, reallocSpace>(spaceToAlloc)
	{
		channelName = _channelName;
		measurementUnit = "1";
		comment = "";
		minValue = maxValue = 0;
	}
	AnsiString getChannelName() const
	{
		return channelName;
	}
	void setChannelName(const AnsiString &_channelName)
	{
		channelName = _channelName;
	}
	AnsiString getComment() const
	{
		return comment;
	}
	void setComment(const AnsiString &_comment, bool timeStamp = true)
	{
      if (timeStamp)
         setCommentWithTimeStamp(comment, _comment);
      else
         comment = _comment;
	}
	void appendComment(const AnsiString &textToAdd, bool timeStamp = true)
	{
      if (timeStamp)
         appendCommentWithTimeStamp(comment, textToAdd);
      else
         comment.Insert(textToAdd, comment.Length()+1);
	}
	AnsiString getMeasurementUnit() const
	{
		return measurementUnit;
	}
	void setMeasurementUnit(const AnsiString &_measurementUnit)
	{
		measurementUnit = _measurementUnit;
	}
	double getMinValue()
	{
		return minValue;
	}
	double getMaxValue()
	{
		return maxValue;
	}
	void append(const T &newElem)
	{
		assert(numElems < maxSpace);
		access(numElems) = newElem;
	}
	void refreshMinMaxValue()
	{
		if(numElems < 1){
			minValue = 0;
			maxValue = 0;
            return;
		}

		minValue = 1e+100;
		maxValue = -1e+100;
		for (int i = 0; i < numElems; i++)
		{
			minValue = std::min(minValue, itemNumericalValue(access(i)));
			maxValue = std::max(maxValue, itemNumericalValue(access(i)));
		}
	}
	double getAvgValue()
	{
		assert(numElems > 0);
		double sum = 0;
		for (int i = 0; i < numElems; i++)
			sum += itemNumericalValue(access(i));
		return sum/numElems;
	}
	void invertChannel(double invertionCentre)
	{
		assert(numElems > 0);
		for (int i = 0; i < numElems; i++)
			itemNumericalValue(access(i)) = 2*invertionCentre - itemNumericalValue(access(i));
        appendComment("\r\Inverted around "+AnsiString(invertionCentre));
	}
	void channelGain(double gain)
	{
		assert(numElems > 0);
		for (int i = 0; i < numElems; i++)
			itemNumericalValue(access(i)) = gain * itemNumericalValue(access(i));
        channelMultiplier *=gain;
        channelOffset *= gain;
        appendComment("\r\nMultiplied by "+AnsiString(gain));
	}
    void channelSetOffset(double offset)
	{
		assert(numElems > 0);
		for (int i = 0; i < numElems; i++)
			itemNumericalValue(access(i)) = offset + itemNumericalValue(access(i));
        channelOffset += offset;
        appendComment("\r\nShifted by "+AnsiString(offset));
	}
    void channelAbsolute()
	{
		assert(numElems > 0);
		for (int i = 0; i < numElems; i++)
			itemNumericalValue(access(i)) = fabs(itemNumericalValue(access(i)));
	}

	void writeToFile(FILE *file, int fileVersion) const
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "IChannel::writeToFile: unsupported file version");
		writeHeaderToFile(file, fileVersion);
		if (fprintf(file, "unit %s\n%d samples\n", measurementUnit, numElems) == EOF)
			throw new EKGException(__LINE__, "IChannel::writeToFile: error writing channel data");
		CTable<T, reallocSpace>::writeToFile(file);
	}
    void setNumElemsFile(int num){
        numElemsFromFile = num;
    }
	int getNumElemsFile(){
        return numElemsFromFile;
    }
	void readFromFile(FILE *file, int fileVersion)
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "IChannel::readFromFile: unsupported file version");
		readHeaderFromFile(file, fileVersion);
		char c;
		do {
			c = (char)fgetc(file);
			if (c == EOF)
				throw new EKGException(__LINE__, "IChannel::readFromFile: unexpected EOF");
		} while (c != ' ');
		fgetAnsiString(file, measurementUnit);
		if (myfscanf(file, "%d samples\n", &numElems) != 1)
			throw new EKGException(__LINE__, "IChannel::readFromFile: error reading numElems");
        numElemsFromFile = numElems; 
		CTable<T, reallocSpace>::readFromFile(file);
	}

    void readFromFileSkipData(FILE *file, int fileVersion)
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "IChannel::readFromFile: unsupported file version");
		readHeaderFromFile(file, fileVersion);
		char c;
		do {
			c = (char)fgetc(file);
			if (c == EOF)
				throw new EKGException(__LINE__, "IChannel::readFromFile: unexpected EOF");
		} while (c != ' ');
		fgetAnsiString(file, measurementUnit);
		if (myfscanf(file, "%d samples\n", &numElems) != 1)
			throw new EKGException(__LINE__, "IChannel::readFromFile: error reading numElems");
        numElemsFromFile = numElems;
        fseek(file, sizeof(T)*numElems, SEEK_CUR);
	}

	IChannel<T, reallocSpace> &operator=(const IChannel<T, reallocSpace> &other)
	{
		CTable<T, reallocSpace>::operator=(other);
		channelName = other.channelName;
		measurementUnit = other.measurementUnit;
		comment = other.comment;
		viewProps = other.viewProps;
		maxValue = other.maxValue;
		minValue = other.minValue;
		return *this;
	}
}; //end class IChannel

inline int doublecmp( const void *pa, const void *pb)
{
	double a = *(double*)pa, b = *(double*)pb;
	if (a < b) return -1;
	if (a > b) return 1;
	return 0;
}

#define CChannelReallocSpace 10000
class CChannel: public IChannel<double, CChannelReallocSpace>
//specialization for doubles
{
protected:
	bool frequencySpace; //default = false

	virtual void writeHeaderToFile(FILE *file, int fileVersion) const
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "CChannel::writeHeaderToFile: unsupported file version");
		if (fprintf(file, "Channel %s\n", channelName) == EOF)
			throw new EKGException(__LINE__, "CChannel::writeHeaderToFile: error writing channel header");
		fprintfMultilineString(file, comment, "numChannelCommentLines");
		viewProps.writeToFile(file, fileVersion);
        if(fileVersion == 3){
            if(tenBitSaving)
                fprintf(file,"%d bitValue\n", 10);
            else
                fprintf(file,"%d bitValue\n", 64);
			fprintf(file,"%.*lf valueMultiplier\n", 15, channelMultiplier);
            fprintf(file,"%.*lf valueOffset\n", 15, channelOffset);
        }
	}
	virtual void readHeaderFromFile(FILE *file, int fileVersion)
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "CChannel::readHeaderFromFile: unsupported file version");
		char c;
		do {
			c = (char)fgetc(file);
			if (c == EOF)
				throw new EKGException(__LINE__, "CChannel::readHeaderFromFile: unexpected EOF");
		} while (c != ' ');
		fgetAnsiString(file, channelName);
		fscanfMultilineString(file, comment, "numChannelCommentLines");
		viewProps.readFromFile(file, fileVersion);
        if(fileVersion == 3){
            int bitValue = 0;
            myfscanf(file,"%d bitValue\n", &bitValue);
            if(bitValue == 10)
                tenBitSaving = true;
            myfscanf(file,"%lf valueMultiplier\n", &channelMultiplier);
            myfscanf(file,"%lf valueOffset\n", &channelOffset);
        }

	}
	virtual double &itemNumericalValue(double &item)
	{
		return item;
	}

public:
    bool    tenBitSaving;
	CChannel()
		: IChannel<double, CChannelReallocSpace>()
	{
        tenBitSaving = false;
        channelOffset = 0;
        channelMultiplier = 1;
    }
	CChannel(const CChannel &other)
		: IChannel<double, CChannelReallocSpace>()
	{
		*this = other;
	}
	void sort()
	{
		assert(numElems > 0);
		std::qsort(data, numElems, sizeof(double), doublecmp);
	}
	CChannel &operator=(const CChannel &other)
	{
		assert(&other != this);
		IChannel<double, CChannelReallocSpace>::operator=(other);
		return *this;
	}

    bool statistics(double& min, double& max, double& avg, double& stdDev) {
        int n = getNumElems();
        if (n > 0) {
            max = getMaxValue();
	        min = getMinValue();
        	avg = 0;
	        for (int i = 0; i < n; ++i)
        		avg += access(i);
    	    avg /= n;
    	    for (int i = 0; i < n; ++i)
	    	    stdDev += (avg - access(i)) * (avg - access(i));
    	    stdDev = sqrt(stdDev / n);
        }
        return (n > 0);
    }

    void packStream(double *values, unsigned short *pack, short numEl) const{
        //values ->doubles holding 10 bit unsigned values
        //pack -> always 5 shorts  (16bit)
        //numEl -> num of elements to pack (max 8, min 1)
        double tmpD;
        for(int i=0;i<5;i++)
            pack[i] = 0;
        for(int i=0;i<numEl;i++){
            tmpD = values[i];
            tmpD = (tmpD < 0)?0:tmpD; //negative numbers to 0
            unsigned short  tmp = tmpD;
            tmp &= 0x3FF; //ensure just 10 bits are used
            switch (i){
            case 0:
                pack[0] |= (tmp << 6);
                break;
            case 1:
                pack[0] |= (tmp >> 4);
                pack[1] |= (tmp << 12);
                break;
            case 2:
                pack[1] |= (tmp << 2);
                break;
            case 3:
                pack[1] |= (tmp >> 8);
                pack[2] |= (tmp << 8);
                break;
            case 4:
                pack[2] |= (tmp >> 2);
                pack[3] |= (tmp << 14);
                break;
            case 5:
                pack[3] |= (tmp << 4);
                break;
            case 6:
                pack[3] |= (tmp >> 6);
                pack[4] |= (tmp << 10);
                break;
            default: //8
                pack[4] |= tmp;
                break;
            }

        }

    }
    void unpackStream(double *values, unsigned short *pack, short numEl) const{
        //values -> doubles holding 10 bit unsigned values
        //pack -> always 5 shorts (5x16 bit)
        //numEl -> num of elements to unpack (max 8, min 1)
        for(int i=0;i<numEl;i++)
            values[i] = 0;
        for(int i=0;i<numEl;i++){
            unsigned short  tmp = 0;
            switch (i){
            case 0:
                tmp = (pack[0] & 0xffc0) >> 6;
                break;
            case 1:
                tmp = (pack[0] & 0x003f) << 4;
                tmp |=(pack[1] & 0xf000) >> 12;
                break;
            case 2:
                tmp =  (pack[1] & 0x0ffc) >>2;
                break;
            case 3:
                tmp = (pack[1] & 0x0003) << 8;
                tmp |=(pack[2] & 0xff00) >> 8;
                break;
            case 4:
                tmp = (pack[2] & 0x00ff) << 2;
                tmp |=(pack[3] & 0xc000) >> 14;
                break;
            case 5:
                tmp = (pack[3] & 0x3ff0) >> 4;
                break;
            case 6:
                tmp = (pack[3] & 0x000f) << 6;
                tmp |=(pack[4] & 0xfc00) >> 10;
                break;
            default: //8
                tmp = pack[4] & 0x03ff;
                break;
            }
            values[i] = tmp;    //write to data
        }

    }

    void writeToFile(FILE *file, int fileVersion){
        if(fileVersion == 1 || fileVersion == 2){
            IChannel<double, CChannelReallocSpace>::writeToFile(file, fileVersion);
        }else if(fileVersion == 3){
            assert(file);
    		writeHeaderToFile(file, fileVersion);
    		if (fprintf(file, "unit %s\n%d samples\n", measurementUnit, numElems) == EOF)
    			throw new EKGException(__LINE__, "CChannel::writeToFile: error writing channel data");
            if(tenBitSaving){ //write 10 bit packed
                fixOffsetWritting();
                unsigned short temp[5];
                for(int i = 0; i< numElems; i+=8){
                    int elemsLeft = numElems - i;
                    elemsLeft = std::min(elemsLeft,8);
                    packStream(data+i,temp,elemsLeft);
            		if (fwrite(temp, sizeof(unsigned short), 5, file) != (size_t)5)
                        throw new EKGException(__LINE__, "CChannel::writeToFile: writing data failed");
                }
                fixOffsetReading();
            }else{ //write doubles
                channelOffset = 0;
                channelMultiplier = 1;
           		if (fwrite(data, sizeof(double), numElems, file) != (size_t)numElems)
	        		throw new EKGException(__LINE__, "CChannel::writeToFile: writing data failed");
            }
       		if (!fprintf(file, "\n"))
       			throw new EKGException(__LINE__, "CChannel::writeToFile: writing terminating newline failed");
        }else{
    		throw new EKGException(__LINE__, "CChannel::writeToFile: unsupported file version");
        }
    }

	void readFromFile(FILE *file, int fileVersion){
        if(fileVersion == 1 || fileVersion == 2){
            IChannel<double, CChannelReallocSpace>::readFromFile(file, fileVersion);
        }else if(fileVersion == 3){
            assert(file);
    		readHeaderFromFile(file, fileVersion);
    		char c;
    		do {
    			c = (char)fgetc(file);
    			if (c == EOF)
    				throw new EKGException(__LINE__, "IChannel::readFromFile: unexpected EOF");
    		} while (c != ' ');
    		fgetAnsiString(file, measurementUnit);
    		if (myfscanf(file, "%d samples\n", &numElems) != 1)
    			throw new EKGException(__LINE__, "IChannel::readFromFile: error reading numElems");
            numElemsFromFile = numElems; 
    		reallocTable(std::max(numElems, 1));
            if(tenBitSaving){
                unsigned short temp[5];
                int elemsLeft = numElems;
                for(int i = 0;i<numElems;i+=8){
                    if(fread(temp,sizeof(unsigned short),5 ,file) <1)
            			throw new EKGException(__LINE__, "CChannel::readFromFile: reading data failed");
                    elemsLeft = numElems - i;
                    elemsLeft = std::min(elemsLeft,8);
                    unpackStream(data+i,temp,elemsLeft);
                }
                fixOffsetReading();
                if (fgetc(file) != '\n')
        		    throw new EKGException(__LINE__, "CChannel::readFromFile: terminating newline not found; # 10 bit values read:"+IntToStr(numElems-elemsLeft));
            }else{//read doubles
                channelOffset = 0;
                channelMultiplier = 1;
                int read = (int)fread(data, sizeof(double), numElems, file);
           		if (read != numElems)
        			throw new EKGException(__LINE__, "CTable::readFromFile: reading data failed");
                if (fgetc(file) != '\n')
        			throw new EKGException(__LINE__, "CChannel::readFromFile: terminating newline not found; # doubles read:"+IntToStr(read));
            }

        }else{
            throw new EKGException(__LINE__, "CChannel::readFromFile: unsupported file version");
        }
    }

    void fixOffsetReading() const{
        for(int i= 0; i<numElems;i++)
            data[i] = (data[i] * channelMultiplier) + channelOffset ;
    }

    void fixOffsetWritting() const{
        for(int i= 0; i<numElems;i++)
            data[i] = (data[i] - channelOffset)  / channelMultiplier ;
    }


    void readFromFileSkipData(FILE *file, int fileVersion){
        if(fileVersion == 1 || fileVersion == 2){
			IChannel<double, CChannelReallocSpace>::readFromFile(file, fileVersion);
        }else if(fileVersion == 3){
            assert(file);
    		readHeaderFromFile(file, fileVersion);
    		char c;
    		do {
    			c = (char)fgetc(file);
    			if (c == EOF)
    				throw new EKGException(__LINE__, "IChannel::readFromFile: unexpected EOF");
    		} while (c != ' ');
    		fgetAnsiString(file, measurementUnit);
    		if (myfscanf(file, "%d samples\n", &numElems) != 1)
    			throw new EKGException(__LINE__, "IChannel::readFromFile: error reading numElems");
			numElemsFromFile = numElems;

            if(tenBitSaving){
                long int bytes_to_skip = sizeof(unsigned short) * 5 * (int)((numElems+7)/8);
                fseek(file, ftell(file) + bytes_to_skip, SEEK_SET);
            }else{//read doubles
                fseek(file, sizeof(double) * numElems, SEEK_CUR);
            }
            if (fgetc(file) != '\n')
                throw new EKGException(__LINE__, "CChannel::readFromFile: terminating newline not found!");
			reallocTable(0);
        }else{
            throw new EKGException(__LINE__, "CChannel::readFromFile: unsupported file version");
        }
    }

}; //end class CChannel

struct Event
//structure to memorize TIME when event happpened and its VALUE
{
	double time, value;
	Event() {}
	Event(const double _time, const double _value)
	{
		time = _time; value = _value;
	}
}; //end struct Event

inline int compareEvents(const void *ev1, const void *ev2)
{
	if (((Event*)ev1)->time < ((Event*)ev2)->time)
		return -1;
	if (((Event*)ev1)->time > ((Event*)ev2)->time)
		return 1;
	return 0;
}

#define CEventChannelReallocSpace 100
class CEventChannel: public IChannel<Event, CEventChannelReallocSpace>
//specialization for Event
//adds searching in time (getIndexPrecedingTime())
//event channels can be contiguous or not
/*contiguous looks like this:

   --------- ----
---                 -----
			-    ---

non-cont. consists of dirac deltas only:
   |         |
---|--------||---|-------
			|    |
*/
//NOTE: events are meant to occur much less frequently than sampling frequency of CChannels
//	(thus much smaller reallocSpace)
{
public:
	struct Statistics //the functions calculating event channel statistics fill in this structure
	{					//all times are in seconds
		double timeFrom, timeTo;
		int numOfEvents;
		double minValue, avgValue, maxValue, minTime, avgTime, maxTime;
		double stDevValue, stDevTime;
		double RMSSD; //RMS razlik med vrednostmi sosednjih dogodkov
	};

protected:
	bool contiguous; //default == true
    bool small_dots; //default == false
	virtual void writeHeaderToFile(FILE *file, int fileVersion) const
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "CEventChannel::writeHeaderToFile: unsupported file version");
		if (fprintf(file, "Channel %s\n%s\n",
				channelName, contiguous ? "contiguous" : "non-contiguous") == EOF)
			throw new EKGException(__LINE__, "CEventChannel::writeHeaderToFile: error writing channel header");
		fprintfMultilineString(file, comment, "numChannelCommentLines");
		viewProps.writeToFile(file, fileVersion);
	}
	virtual void readHeaderFromFile(FILE *file, int fileVersion)
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "CChannel::readHeaderFromFile: unsupported file version");
		char c;
		do {
			c = (char)fgetc(file);
			if (c == EOF)
				throw new EKGException(__LINE__, "CEventChannel::readHeaderFromFile: unexpected EOF");
		} while (c != ' ');
		fgetAnsiString(file, channelName);
		AnsiString contString;
		fgetAnsiString(file, contString);
		if (contString == "contiguous")
			contiguous = true;
		else if (contString == "non-contiguous")
			contiguous = false;
		else
			throw new EKGException(__LINE__, "CEventChannel::readHeaderFromFile: 'contiguous' or 'non-contiguous' expected");
		fscanfMultilineString(file, comment, "numChannelCommentLines");
		viewProps.readFromFile(file, fileVersion);
	}
	virtual double &itemNumericalValue(Event &item)
	{
		return item.value;
	}

public:
	CEventChannel()
		: IChannel<Event, CEventChannelReallocSpace>()
	{
		contiguous = true;
        small_dots = false;
	}
	CEventChannel(const CEventChannel &other)
		: IChannel<Event, CEventChannelReallocSpace>()
	{
		*this = other;
	}
	CEventChannel(const AnsiString _channelName)
		: IChannel<Event, CEventChannelReallocSpace>(_channelName)
	{
		contiguous = true;
        small_dots = false;
	}
	CEventChannel(const AnsiString _channelName, int spaceToAlloc)
		: IChannel<Event, CEventChannelReallocSpace>(spaceToAlloc)
	{
		contiguous = true;
        small_dots = false;
	}
	bool getContiguous() const
	{
		return contiguous;
	}
	void setContiguous(bool _contiguous)
	{
		contiguous = _contiguous;
	}
    bool getSmallDots() const
    {
        return small_dots;
    }
    void setSmallDots(bool dot){
        small_dots = dot;
    }

	double findEventByValue(double value, double tolerance = 1e-10) //returns time of first event with given value or -1 if not found
	{
		for (int i = 0; i < numElems; i++)
			if (fabs(access(i).value - value) < tolerance)
				return access(i).time;
		return -1;
	}
	int getIndexClosestToTime(double time)
	//uses bisection
	{
		assert(numElems > 0);
		int lower = 0, upper = numElems-1, middle;
		if (time < access(lower).time)
			return lower;
		if (time > access(upper).time)
			return upper;
		while (upper > lower+1)
		{
			middle = (lower+upper)/2;
			if (time > access(middle).time)
				lower = middle;
			else
				upper = middle;
		}
		return (time - access(lower).time < access(upper).time - time) ? lower : upper;
	}
	void appendEventIfChanged(double time, double value, double eps)
	{
		if (numElems == 0 || (fabs(value - access(numElems-1).value) > eps))
			append(Event(time, value));
	}
	CEventChannel &operator=(const CEventChannel &other)
	{
		assert(&other != this);
		IChannel<Event, CEventChannelReallocSpace>::operator=(other);
		contiguous = other.contiguous;
        small_dots = other.small_dots;
		return *this;
	}

	enum EventType {
		eventTypeUnknown,
		eventTypeInterval,
		eventTypeFreq,
		eventTypeOther
	};
	EventType getEventType(int decisiveElementIndex = -1)
	//tries to find out from event values; if decisiveElementIndex == -1, mid-measurement is used
	{
		if (numElems < 2)
			return eventTypeUnknown;

		int event2 = (decisiveElementIndex == -1 ? numElems/2 : std::max(1, decisiveElementIndex));
		int event1 = event2 - 1;
		while(access(event2).time - access(event1).time == 0 && event2 > 1){
			event1--;
            event2--;
		}
		if(event2 < 1 || event2 >= numElems)
			return eventTypeUnknown;
		assert(event2 > 0 && event2 < numElems);

		double intervalRelError = fabs((access(event2).value - (access(event2).time - access(event1).time))
										/ (access(event2).time - access(event1).time));
		double freqRelError = fabs((access(event2).value - 60/(access(event2).time - access(event1).time))
										/ (60/(access(event2).time - access(event1).time)));

		if (intervalRelError > 0.02 && freqRelError > 0.02)
			return eventTypeOther;
		if (intervalRelError <= 0.02 && freqRelError > 0.02)
			return eventTypeInterval;
		if (intervalRelError > 0.02 && freqRelError <= 0.02)
			return eventTypeFreq;
		return eventTypeUnknown;
	}
	void correctEventValue(int eventIndex, EventType eventType)
	{
		assert(eventIndex >= 0 && eventIndex < numElems);
		switch (eventType)
		{
		case eventTypeInterval:
			if (eventIndex == 0 && numElems > 1)
				access(0).value = access(1).value;
			else if (eventIndex > 0)
				access(eventIndex).value = access(eventIndex).time - access(eventIndex-1).time;
			break;
		case eventTypeFreq:
			if (eventIndex == 0 && numElems > 1)
				access(0).value = access(1).value;
			else if (eventIndex > 0)
				access(eventIndex).value = 60/(access(eventIndex).time - access(eventIndex-1).time);
			break;
		case eventTypeOther:
		case eventTypeUnknown:
			break;
		}
	}

    void correctAllEventValues(){
    	EventType type = getEventType();

	}

	void correctAllEventValuesByType(EventType type){
		for(int i=1; i< numElems; i++){
			correctEventValue(i, type);
		}
        correctEventValue(0, type);
	}
    
	void removeExtremeEventsSlow(double newMinimum, double newMaximum)
	{
		bool clipped = false;
		for (int i = 0; i < numElems; i++)
			while (i < numElems && (access(i).value < newMinimum || access(i).value > newMaximum))
			{
				deleteElems(i, i);
				clipped = true;
			}
		if (clipped){
			refreshMinMaxValue();
        }
	}
    
	void removeExtremeEvents(double newMinimum, double newMaximum) {
        CTable<Event, CEventChannelReallocSpace> newEventChannel;
        newEventChannel.reallocTable(numElems);

        int destI = 0;
        for (int i = 0; i < numElems; i++) {
            if ((access(i).value >= newMinimum) && (access(i).value <= newMaximum)) {
                newEventChannel[destI] = accessConst(i);
                ++destI;
			}
		}
		if (destI < numElems){
            CTable<Event, CEventChannelReallocSpace>::operator=(newEventChannel);
			refreshMinMaxValue();
        }
	}

    //removes extreme events, but keeps higher one inside
    void removeExtremeRRIEvents(double newMaximumBPM){
        CTable<Event, CEventChannelReallocSpace> newEventChannel;
        newEventChannel.reallocTable(numElems);

        double dx = 1 / (newMaximumBPM / 60.0);

        newEventChannel[0] = accessConst(0);
        int destI = 0;
        for (int i = 1; i < numElems; i++) {
            if(accessConst(i).time-accessConst(i-1).time < dx){
                if(accessConst(i).value > accessConst(i-1).value){
                    newEventChannel[destI].time = accessConst(i).time;
                    newEventChannel[destI].value = accessConst(i).value;
                }
                continue;
            }
            destI++;
            newEventChannel[destI] = accessConst(i);
		}
		if (destI < numElems){
            CTable<Event, CEventChannelReallocSpace>::operator=(newEventChannel);
            if (numElems < 1)
                throw new EKGException(__LINE__, "removeExtremeEvents: All events removed in the channel!");
			refreshMinMaxValue();
        }
    }

    //not used
	void ceilExtremeEvents(double newMaximum)
	{
        //after automatic beat detection ceil timestamps
		bool clipped = false;
		for (int i = 0; i < numElems-1; i++)
			while (access(i).value > newMaximum)
			{
                access(i).value = std::min(newMaximum, access(i+1).value);
				clipped = true;
			}
		if (clipped){
			refreshMinMaxValue();
        }
	}


	void sortEventsByAscendingTime()
	{
		std::qsort((void*)(data), numElems, sizeof(Event), compareEvents);
	}
	void shiftChannel(double time, double upperTimeLimit)
	{
		int numDeleted = 0;
		for (int i = 0; i < getNumElems(); i++)
			access(i).time += time;
		if (time < 0)
		{
			int firstValid = getIndexClosestToTime(0);
			if (access(firstValid).time < 0)
				firstValid++;
			if (firstValid > 0) {
				numDeleted = firstValid;
				deleteElems(0, firstValid-1);
			}
		} else {
			int lastValid = getIndexClosestToTime(upperTimeLimit);
			if (access(lastValid).time > upperTimeLimit)
				lastValid--;
			if (lastValid < getNumElems()-1) {
				numDeleted = getNumElems() - lastValid - 1;
				deleteElems(lastValid+1, getNumElems()-1);
			}
		}
		char buf[1000];
		if (numDeleted > 0)
			sprintf(buf, "\r\nChannel shifted for %.4f s; finished %d events.\r\n", time, numDeleted);
		else
			sprintf(buf, "\r\nChannel shifted for %.4f s.\r\n", time, numDeleted);
		appendComment(buf);
	}

	Statistics calcStatistics(double timeFrom, double timeTo);
	Statistics calcStatistics()
	{
		return calcStatistics(0, access(getNumElems()-1).time);
	}
	Statistics calcStatisticsBeatDetection(double filtMin, double filtMax);

}; //end class CEventChannel


struct Annotation
//structure to memorize TIME when annotation happened, additional value and annotation itself
{
	double time, length, value;
    int annotationNum;
	UnicodeString annotation;
	Annotation() {
		annotation = AnsiString("");
	}
	Annotation(const double _time, const UnicodeString _annotation, const double _length = 1.0, const double _value = 0.0)
	{
		time = _time;
		annotation = _annotation;
		value = _value;
		length = _length;
	}
	const Annotation &operator=(const Annotation &other){
		time = other.time;
		length = other.length;
		value = other.value;
		annotation = UnicodeString(other.annotation);
        return *this;
	}

    char* utf8Annotation(){
		RawByteString utf8String = System::UTF8Encode(annotation);
		return utf8String.c_str();
	}


}; //end struct Annotation

inline int compareAnnotations(const void *ev1, const void *ev2)
{
    assert(ev1 != null);
    assert(ev2 != null);
	if (((Annotation*)ev1)->time < ((Annotation*)ev2)->time)
		return -1;
	if (((Annotation*)ev1)->time > ((Annotation*)ev2)->time)
		return 1;
	return 0;
}

#define CAnnotationChannelReallocSpace 100
class CAnnotationChannel: public IChannel<Annotation, CEventChannelReallocSpace>
//similar to event, add annotation field
{
public:

	struct Statistics //the functions calculating event channel statistics fill in this structure
	{					//all times are in seconds
		double timeFrom, timeTo;
		int numOfEvents;
		double minValue, maxValue, avgValue, minTime, maxTime, minDuration, maxDuration, avgDuration;
	};

	Statistics calcStatistics(double timeFrom, double timeTo);
	Statistics calcStatistics()
	{
		return calcStatistics(0, access(getNumElems()-1).time);
	}               


protected:
	virtual void writeHeaderToFile(FILE *file, int fileVersion) const
	{
		assert(file);
		if (fileVersion != 3)
			throw new EKGException(__LINE__, "CAnnotationChannel::writeHeaderToFile: unsupported file version");
		if (fprintf(file, "Channel %s\n", channelName) == EOF)
			throw new EKGException(__LINE__, "CAnnotationChannel::writeHeaderToFile: error writing channel header");
		fprintfMultilineString(file, comment, "numChannelCommentLines");
		viewProps.writeToFile(file, fileVersion);
	}
	virtual void readHeaderFromFile(FILE *file, int fileVersion)
	{
		assert(file);
		if (fileVersion != 3)
			throw new EKGException(__LINE__, "CAnnotation::readHeaderFromFile: unsupported file version");
		char c;
		do {
			c = (char)fgetc(file);
			if (c == EOF)
				throw new EKGException(__LINE__, "CAnnotation::readHeaderFromFile: unexpected EOF");
		} while (c != ' ');
		fgetAnsiString(file, channelName);
		fscanfMultilineString(file, comment, "numChannelCommentLines");
		viewProps.readFromFile(file, fileVersion);
	}
	virtual double &itemNumericalValue(Annotation &item)
	{
		return item.value;
	}

public:
	CAnnotationChannel()
		: IChannel<Annotation, CEventChannelReallocSpace>()
	{
	}
	CAnnotationChannel(const CAnnotationChannel &other)
		: IChannel<Annotation, CAnnotationChannelReallocSpace>()
	{
		*this = other;
	}
	CAnnotationChannel(const AnsiString _channelName)
		: IChannel<Annotation, CEventChannelReallocSpace>(_channelName)
	{
	}
	CAnnotationChannel(const AnsiString _channelName, int spaceToAlloc)
		: IChannel<Annotation, CAnnotationChannelReallocSpace>(spaceToAlloc)
	{
	}

	double findEventByValue(double value, double tolerance = 1e-10) //returns time of first event with given value or -1 if not found
	{
		for (int i = 0; i < numElems; i++)
			if (fabs(access(i).value - value) < tolerance)
				return access(i).time;
		return -1;
	}
	int getIndexClosestToTime(double time)
	{
		assert(numElems > 0);
        double currerror = -1;
        int currindex = -1;
        for(int i=0;i<numElems;i++){
            if(currerror == -1 || abs(time - access(i).time) < currerror){
                currerror = abs(time - access(i).time);
                currindex = i;
            }
        }
        return currindex;
	}
	void appendAnnotationIfChanged(double time, double value, double eps)
	{
		if (numElems == 0 || (fabs(value - access(numElems-1).value) > eps))
			append(Annotation(time, value));
	}
	CAnnotationChannel &operator=(const CAnnotationChannel &other)
	{
		assert(&other != this);
		IChannel<Annotation, CAnnotationChannelReallocSpace>::operator=(other);
		return *this;
	}

	enum EventType {
		eventTypeOther
	};
	EventType getEventType(int decisiveElementIndex = -1)
	//tries to find out from event values; if decisiveElementIndex == -1, mid-measurement is used
	{
		return eventTypeOther;
	}
	void correctEventValue(int eventIndex, EventType eventType)
	{
        //in case we need it similar to event channel
	}
	void removeExtremeEvents(double newMinimum, double newMaximum)
	{
		bool clipped = false;
		for (int i = 0; i < numElems; i++)
			while (i < numElems && (access(i).value < newMinimum || access(i).value > newMaximum))
			{
				deleteElems(i, i);
				clipped = true;
			}
		if (clipped){
			refreshMinMaxValue();
        }
	}
    void deleteElems(double eventIndex1, double eventIndex2, bool fix_time = false){
        assert(eventIndex1 <= eventIndex2);

        //delete events that have same timestamp
        if(eventIndex1 == eventIndex2){
            for(int i=0;i<numElems;i++){
            if (access(i).time == eventIndex1){
                for(int j=i;j<numElems-1;j++)
                    access(j) = access(j+1);
                numElems--;
                }
            }
            return;
        }

        //delete events that cover selected period
        double delta = eventIndex2 - eventIndex1;
		bool clipped = false;
        int i = 0;
		while (i < numElems){
            //cut before
            if (access(i).time >= eventIndex2){
                if(fix_time) data[i].time = access(i).time - delta;
                i++;
                continue;
            }
            //cut after
            if( (access(i).time + access(i).length)  <= eventIndex1 ){
                i++;
                continue;
            }
            //cut whole
            if (access(i).time >= eventIndex1 && (access(i).time + access(i).length) <= eventIndex2){
                for(int j=i;j<numElems-1;j++)
                    access(j) = access(j+1);
                numElems--;
                continue;
            }
            //cut tail
            if (access(i).time < eventIndex1 && (access(i).time + access(i).length) < eventIndex2){
                access(i).length = eventIndex1 - access(i).time;
                i++;
                continue;
            }
            //cut head
            if (access(i).time > eventIndex1 && (access(i).time + access(i).length) > eventIndex2){
                if(fix_time) access(i).time = eventIndex1;
                access(i).length = (access(i).time + access(i).length) - eventIndex2;
                i++;
                continue;
            }
            //cut middle
            if (access(i).time  < eventIndex1 && (access(i).time + access(i).length) > eventIndex2){
                access(i).length -= delta;
                i++;
                continue;
            }
        }
        if(numElems > 0)
            refreshMinMaxValue();

    }

	void sortEventsByAscendingTime()
	{
		std::qsort((void*)(data), numElems, sizeof(Annotation), compareAnnotations);
	}
	void shiftChannel(double time, double upperTimeLimit)
	{
		int numDeleted = 0;
		for (int i = 0; i < getNumElems(); i++)
			access(i).time += time;
		if (time < 0)
		{
			int firstValid = getIndexClosestToTime(0);
			if (access(firstValid).time < 0)
				firstValid++;
			if (firstValid > 0) {
				numDeleted = firstValid;
				deleteElems(0, firstValid-1);
			}
		} else {
			int lastValid = getIndexClosestToTime(upperTimeLimit);
			if (access(lastValid).time > upperTimeLimit)
				lastValid--;
			if (lastValid < getNumElems()-1) {
				numDeleted = getNumElems() - lastValid - 1;
				deleteElems(lastValid+1, getNumElems()-1);
			}
		}
		char buf[1000];
		if (numDeleted > 0)
			sprintf(buf, "\r\nChannel shifted for %.4f s; finished %d events.\r\n", time, numDeleted);
		else
			sprintf(buf, "\r\nChannel shifted for  %.4f s.\r\n", time, numDeleted);
		appendComment(buf);
	}

/*	Statistics calcStatistics(double timeFrom, double timeTo);
	Statistics calcStatistics()
	{
		return calcStatistics(0, access(getNumElems()-1).time);
	}
	Statistics calcStatisticsBeatDetection(double filtMin, double filtMax);
*/
#define MAX_ANNOTATION_LENGTH 511
    void writeToFile(FILE *file, int fileVersion)
	{
		assert(file);

        sortEventsByAscendingTime();
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "IChannel::writeToFile: unsupported file version");
		writeHeaderToFile(file, fileVersion);
		if (fprintf(file, "unit %s\n%d samples\n", measurementUnit, numElems) == EOF)
			throw new EKGException(__LINE__, "IChannel::writeToFile: error writing channel data");

		char utf8_annotation[MAX_ANNOTATION_LENGTH+1];
		utf8_annotation[MAX_ANNOTATION_LENGTH] = 0;

        for(int i = 0;i<numElems;i++){
            fprintf(file, "time %lf\n", data[i].time);
            fprintf(file, "length %lf\n", data[i].length);
            fprintf(file, "value %lf\n", data[i].value);

			System::UnicodeToUtf8(utf8_annotation, data[i].annotation.c_str(), MAX_ANNOTATION_LENGTH);
			fprintf(file, "annotation %s\n", utf8_annotation);
        }

		if (!fprintf(file, "\n"))
			throw new EKGException(__LINE__, "CTable::writeToFile: writing terminating newline failed");
	}

    void readFromFile(FILE *file, int fileVersion)
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "IChannel::readFromFile: unsupported file version");
		readHeaderFromFile(file, fileVersion);
		char c;
		do {
			c = (char)fgetc(file);
			if (c == EOF)
				throw new EKGException(__LINE__, "IChannel::readFromFile: unexpected EOF");
		} while (c != ' ');
		fgetAnsiString(file, measurementUnit);
		if (myfscanf(file, "%d samples\n", &numElems) != 1)
			throw new EKGException(__LINE__, "IChannel::readFromFile: error reading numElems");
        numElemsFromFile = numElems; 
		reallocTable(std::max(numElems, 1));

        char readAnnotation[3000] = {0};
		char annotationChar = 'A'-1;
		int annotationNum = 0;
		wchar_t utf8_annotation[MAX_ANNOTATION_LENGTH+1];
		utf8_annotation[MAX_ANNOTATION_LENGTH] = 0;
        for(int i = 0;i<numElems;i++){
            readAnnotation[0] = 0;

            myfscanf(file, "time %lf\n", &data[i].time);
            myfscanf(file, "length %lf\n", &data[i].length);
			myfscanf(file, "value %lf\n", &data[i].value);
            if( fgets(readAnnotation, 1000, file) == NULL){
                throw new EKGException(__LINE__, "fgetAnsiString: error reading from file");
			}

			System::Utf8ToUnicode(utf8_annotation, readAnnotation, MAX_ANNOTATION_LENGTH);
			if(wcslen(utf8_annotation) > 12)
				data[i].annotation = UnicodeString(utf8_annotation+11);   //offset by 11 to handle "annotation"
			else
				data[i].annotation = "";
			data[i].annotation = data[i].annotation.Trim();

			//naming connvention
			if(data[i].value == 1){
				annotationNum++;
				data[i].annotationNum = annotationNum;
			}else if(data[i].value > 1){
				annotationChar++;
				if(annotationChar > 'Z')
					annotationChar = 'A'-1;
				data[i].annotationNum = annotationChar;
			}
		}

		if (fgetc(file) != '\n')
			throw new EKGException(__LINE__, "CTable::readFromFile: (annotation) terminating newline not found");
	}

	char* getUtf8Annotation(int i){
		RawByteString utf8String = System::UTF8Encode(access(i).annotation);
		return utf8String.c_str();
	}
}; //end class AnnotationChannel





class CPatientData
//struct-like class to memorize patient data
{
public:
	enum Sex
	{
		female,
		male,
		unspecifiedSex
	};
	AnsiString name;
	TDateTime dateOfBirth;
	Sex sex;
	AnsiString diagnosis;
    AnsiString comment;

private:
	static AnsiString SexToStr(const Sex sex)
	{
		switch (sex) {
			case female:return "female";
			case male:	return "male  ";
			case unspecifiedSex:
						return "unspecSex";
			default:
				throw new EKGException(__LINE__, "CPatientData::SexToStr: invalid sex");
		}
	}
	static Sex StrToSex(const AnsiString sexStr)
	{
		if (sexStr == "female") return female;
		if (sexStr == "male  ") return male;
		if (sexStr == "unspecSex") return unspecifiedSex;
		throw EKGException(__LINE__, "CPatientData::StrToSex: invalid sex string");
    }
public:
	CPatientData()
	{
		reset();
	}
	void reset()
	{
		sex = unspecifiedSex;
		name = "";
		dateOfBirth = TDateTime(1900, 1, 1);
		diagnosis = "";
		comment = "";
	}
	static char SexToChar(const Sex sex)
	{
		switch (sex) {
			case female:return 'Z';
			case male:	return 'M';
			case unspecifiedSex:
						return 'N';
			default:
				throw new EKGException(__LINE__, "CPatientData::SexToStr: invalid sex");
		}
	}
	void writeToFile(FILE *file, int fileVersion) const
	{
		assert(file);
		if (fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "CPatientData::writeToFile: unsupported file version");
        if(name.Length() > 0){
    		if (fprintf(file, "PatientData\n%s\n%s\n%s\n", name, dateToString(dateOfBirth), SexToStr(sex)) == EOF)
    			throw new EKGException(__LINE__, "CPatientData::writeToFile: error writing PatientData header");
        }else{
    		if (fprintf(file, "PatientData\n%s\n%s\n%s\n", "", dateToString(dateOfBirth), SexToStr(sex)) == EOF)
    			throw new EKGException(__LINE__, "CPatientData::writeToFile: error writing PatientData header");
        }
		fprintfMultilineString(file, diagnosis, "numDiagnosisLines");
		fprintfMultilineString(file, comment, "numCommentLines");
	}
	void readFromFile(FILE *file, int fileVersion)
	{
		assert(file);
		if (fileVersion != 1 && fileVersion != 2 && fileVersion != 3)
			throw new EKGException(__LINE__, "CPatientData::readFromFile: unsupported file version");
		AnsiString string;
		fgetAnsiString(file, string);
		if (string != "PatientData")
			throw new EKGException(__LINE__, "CPatientData::readFromFile: 'PatientData' expected");
		fgetAnsiString(file, name);
		if (fileVersion == 2 || fileVersion == 3)
		{
			AnsiString dateString;
			fgetAnsiString(file, dateString);
			dateOfBirth = stringToDate(dateString);
		} else {
			int yearOfBirth;
			if (myfscanf(file, "%d yearOfBirth\n", &yearOfBirth) != 1)
				throw new EKGException(__LINE__, "CPatientData::readFromFile: error reading year of birth");
			if (yearOfBirth == -1)
				yearOfBirth = 1900;
			dateOfBirth = TDateTime((unsigned short)yearOfBirth, 1, 1);
		}
		fgetAnsiString(file, string);
		sex = StrToSex(string);
		fscanfMultilineString(file, diagnosis, "numDiagnosisLines");
		fscanfMultilineString(file, comment, "numCommentLines");


	}
}; //end class CPatientData

class CursorPositions
{
public:
	static const int numCursors = 3;
	double c[numCursors]; //-1 == not set

	CursorPositions()
	{
		for (int i = 0; i < numCursors; i++)
			c[i] = -1;
	}
	CursorPositions(const CursorPositions &other)
	{
		for (int i = 0; i < numCursors; i++)
			c[i] = other.c[i];
	}
}; //end class CursorPositions

struct ColinDigitalData
//struct to memorize everything that COLIN sends in a single line
{
	//for every numerical value, -1 signals error (i.e. COLIN did not send the value)
	int avgSystPressure, avgMapPressure, avgDiastPressure; //averaged pressures in mmHg; MAP = medium arterial pressure
	int avgPulseRate; //averaged beat rate in bpm
	int avgPulseCount; //no of beats used for averaging beat rate and pressures
	int systPressure, mapPressure, diastPressure, pulseRate; //non-averaged values
	int activeElement; //active element number used (0-29)
	int tonometryStatus1, tonometryStatus2; //status bytes
		//status1: bit  7=motion
		//				6=hold-down pressure error
		//				5=always 1
		//				4=bad pulse
		//				3=low pulse
		//				2=time-out error (2 sec)
		//				1=cuff cal error
		//				0=sensor disconnected
		//status2: bit  7=not used
		//				6=not used
		//				5=always 1
		//				4=sensor zero calibrated
		//				3=sensor on patient
		//				2=HD phase disconnected
		//			1&0=sensor position: 00=LL (far left), 01=L (left center), 10=R (right center), 11=RR (far right)
	int PPtime, RRtime; //TBP peak-to-peak interval in msec; ECG RR interval
	int ECGstatus; //status
		//bit		    7=always 0
		//				6=not used
		//				5=always 1
		//				4=always 0
		//				3=always 0
		//				2=RR int. <= 170 msec
		//				1=RR int.(n)/RR int.(n-1), 80% ?!
		//				0=RR int. > 3 sec

	void readFromString(char *buf)
	//decodes data sent from COLIN device; throws exception if format is invalid
	{
		assert(buf);
		readItemFromString(buf, avgSystPressure, 3);
		readItemFromString(buf+4, avgMapPressure, 3);
		readItemFromString(buf+8, avgDiastPressure, 3);
		readItemFromString(buf+12, avgPulseRate, 3);
		readItemFromString(buf+16, avgPulseCount, 3);
		readItemFromString(buf+20, systPressure, 3);
		readItemFromString(buf+24, mapPressure, 3);
		readItemFromString(buf+28, diastPressure, 3);
		readItemFromString(buf+32, pulseRate, 3);
		readItemFromString(buf+36, activeElement, 2);
		tonometryStatus1 = buf[39];
		tonometryStatus2 = buf[40];
		readItemFromString(buf+42, PPtime, 4);
		readItemFromString(buf+47, RRtime, 4);
		ECGstatus = buf[52];
		if (buf[71] != 0x0D || buf[72] != 0x0A)
			throw new EKGException(__LINE__, "ColinDigitalData::readFromString: invalid string format (terminating newline incorrect)");
	}
private:
	void readItemFromString(char *buf, int &item, int numChars)
	//reads integer from first numChars places of buf
	//if unsuccessful (e.g. only spaces there), assign -1
	{
		char tempBuf[10];
		for (int i = 0; i < numChars; i++)
			tempBuf[i] = buf[i];
		buf[numChars] = 0;
		if (sscanf(tempBuf, "%d", &item) != 1)
			item = -1;
	}
};

}; //end namespace LibCore

#endif
