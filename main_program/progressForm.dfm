object ProgressBarForm: TProgressBarForm
  Left = 528
  Top = 233
  BorderIcons = []
  BorderWidth = 5
  Caption = 'Progress'
  ClientHeight = 43
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label: TLabel
    Left = 16
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Label'
  end
end
