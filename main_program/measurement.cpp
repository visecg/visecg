// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


#include "measurement.h"
#include "instantDialog.h"
#include <cmath>
#include <string>
#include <fstream>
#include <sstream>
#include "Ini.h"
#include "StringSupport.h"
#include <windows.h>
#include <VclTee.GanttCh.hpp>
#include "getline.h"
#include "gnugettext.hpp"

#include <vector>

namespace LibCore
{

const double CMeasurement::maxMeasurementLength = 2*3600;


std::vector<std::string> CMeasurement::findConvertedS2Files(UnicodeString path){
	std::vector<std::string> convertedFiles;
	if(FileExists(path+".nekg"))
		convertedFiles.push_back(AnsiString(path+".nekg").c_str());
	if(FileExists(path+".nekge"))
		convertedFiles.push_back(AnsiString(path+".nekge").c_str());
	int i=1;
	while(FileExists(path+"_part_"+i+".nekg")){
		convertedFiles.push_back(AnsiString(path+"_part_"+i+".nekg").c_str());
		i++;
	}

	i=1;
	while(FileExists(path+"_part_"+i+".nekge")){
		convertedFiles.push_back(AnsiString(path+"_part_"+i+".nekge").c_str());
        i++;
	}

	return convertedFiles;
}

/**
 ** CALLER SHOULD NOT EXPECT ONLY PATHS!
 ** Will return std::string-s of paths to existing files
 ** STRINGS CAN ALSO CONTAINED NORMAL STRINGS USED AS "FLAGS" (e.g. give isntcutions not to convert files....)
 **/
std::vector<std::string> CMeasurement::convertS2File(UnicodeString s2FilePath, long maxLengthPerFile, bool force, double missingValue){
	std::vector<std::string> convertedFiles;

	UnicodeString partialPathToFile = s2FilePath.SubString(1,s2FilePath.LastDelimiter("\\"));
	convertedFiles = findConvertedS2Files(s2FilePath);

	if(!force && convertedFiles.size() > 0){
        //convertedFiles.push_back("EXISTING");
		return convertedFiles;
	}

	if(maxLengthPerFile < 0 ){
		maxLengthPerFile = 43200;       //12h

		UnicodeString settingsFname(L"nastavitve.ini");
		for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
			if (Application->ExeName[i] == '\\') {
				settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
				break;
			}
		}
		Ini::File ini(settingsFname.c_str());
		ini.loadVar(maxLengthPerFile, L"nekgeConversionLength");
		if(maxLengthPerFile <= 0)
            maxLengthPerFile = 43200;
	}


	//use system call to convert to nekg
	wchar_t convFilename[2000], convParams[2000];
	wcscpy(convFilename, Application->ExeName.c_str());
	int i;
	for (i = wcslen(convFilename)-1; i >= 0; i--)
		if (convFilename[i] == '\\')
			break;
	wcscpy(convFilename + i + 1, L"\\resources\\convertToNEKG.exe");

	SHELLEXECUTEINFO info = {0};
	info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = NULL;
	info.lpVerb = NULL;
	info.lpFile = convFilename; //program

	UnicodeString params = "\""+s2FilePath+"\"" + " -t "+ maxLengthPerFile + " -m " + missingValue ; //43200 , -1 //TODO output
	info.lpParameters = params.c_str(); //file to convert

	info.lpDirectory = partialPathToFile.c_str(); //folder
	info.nShow = SW_HIDE;
	info.hInstApp = NULL;
	ShellExecuteEx(&info);
	WaitForSingleObject(info.hProcess,INFINITE);

	unsigned long exitCode = 0;
	GetExitCodeProcess(info.hProcess, &exitCode);
	TerminateProcess(info.hProcess,0);
	CloseHandle(info.hProcess);

	if (exitCode != 0){ //unsuccessful
		//What if file is normal .txt file..
		//throw new EKGException(__LINE__, "CMeasurement::loadFromFile: cannot convert txt file");
	}
	else{
        //now only converting file, doing nothing more at the moment
	}

	return findConvertedS2Files(s2FilePath);

}

std::vector<std::string> CMeasurement::loadFromFile(const UnicodeString fileName, LibCore::CMeasurement::FileType fileType)
{
	std::vector<std::string> returnFilenames;
	returnFilenames.clear();

	//set aux data to default (in case it is not part of format)
	editable = false;
    fileLocation = fileName; 
	patientData.reset();
	measurementName = fileName;
	comment = "";
	numEventChannels = 0;
	if (!FileExists(fileName) || FileAge(fileName) == -1)
		throw new EKGException(__LINE__, "CMeasurement::loadFromFile: file does not exist: \"" + fileName+"\"");
	date = TDateTime::FileDateToDateTime(FileAge(fileName));

	//read file
	FILE *file;
	switch (fileType) {

	case ppi_ascii: {
		// ppi (peak-peak interval) ASCII -  file type *.ppi  (first a fictive signal channel is created,
		//then event channel.time is filled  with RRtime data and channel.value
		//with RRtime(i+1)-RRtime(i) from file.
		int i, countRR;
        double startTime, endTime;
		char line[2000], string[2000];
		file = _wfopen(fileName.c_str(), L"r");
		if (file == NULL)
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: cannot open file");

		int numHeadlines = InstantDialog::query( gettext("VisECG"),	gettext("Number of initial lines:"), "4").ToInt();
		i= numHeadlines;
		while  (i > 0)
		{
			fgets (line, sizeof(line), file);
			i--;
		}
		countRR = 0;
		while (fgets (line, sizeof(line), file) != NULL)
        {
          if (countRR==0) startTime = atof(line);  //save start time
          endTime = atof(line);  //save end time
          countRR++;        // count data
		}

        channel[0].setChannelName("FICTIVE");    // generate fictive signal channel
		double sampRate = InstantDialog::query(gettext("VisECG"), gettext("Sampling frequency:"), "1000").ToInt();
		samplingRate = sampRate;
        const int   channel0gain = 1;
        for (i = int(startTime); i < int((endTime-startTime)*sampRate); i++)
            channel[0][i] = channel0gain;     // fill signal channel with value 1
		fclose(file);

		// scan the file for second time to read data
		file = _wfopen(fileName.c_str(), L"r");
		if (numHeadlines > 0)
		{
			fgets (string, sizeof(string), file); 	 // save heading text  in the first line
			comment=string;
		}
		for (i = 1; i < numHeadlines; i++)
        	 fgets (line, sizeof(line), file);     // skip other text lines

		//create new event channel for detrended events, set its name, unit and comment
		int RRINum = addEventChannel ("PPI uvo�en", "s");
		CEventChannel &RRI = eventChannel[RRINum];
		RRI.setContiguous(false);
		RRI.setComment("Peak-Peak Interval imported from " + fileName + "\r\n");

        double number; i=0;
		while ( fgets (line, sizeof(line), file) != NULL)
        {
			number = atof(line);
            RRI[i].time = number;
            if (i>0) RRI[i].value = RRI[i].time - RRI[i-1].time;
            i++;
		 }
		if (RRI.getNumElems() > 1)
			RRI[0].value = RRI[1].value;  // fill first data
        else  if (RRI.getNumElems() == 1)
			Application->MessageBox(gettext("This file contains only one interval.").data(), gettext("Import Peak-Peak Interval - Warning!").data() , MB_OK);
		else
			Application->MessageBox(gettext("This file does not contain any interval.").data(), gettext("Import Peak-Peak Interval - Warning!").data() , MB_OK);

		fclose(file);
		break;
	}

	// DEKG ASCII
	case dekg_ascii: {
		int i, count, count_tmp, chan_index;
		double number;
		char line[2000], string[2000];
        int channelsPerLine = 0;
        
		file = _wfopen(fileName.c_str(), L"r");
		if (file == NULL)
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: cannot open file");

		UnicodeString settingsFname("nastavitve.ini");
    	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
		    if (Application->ExeName[i] == '\\') {
                settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
	    		break;
            }
    	}
        Ini::File ini(settingsFname.c_str());
		std::string defaultParam("2");
		static const wchar_t paramStr1[] = L"DEKG parameter number of lines";
		ini.loadVar(defaultParam, paramStr1);
        
		int numHeadlines = InstantDialog::query(gettext("VisECG").data(), gettext("Number of initial lines:").data(), defaultParam.c_str()).ToInt();
		i= numHeadlines;
		while  (i > 0)
		{
			fgets (line, sizeof(line), file);
			i--;
		}
		count = 0; count_tmp=0; chan_index=0;
		while (fgets (line, sizeof(line), file) != NULL)   {
			if (line[0]!='N') count_tmp++;   // search for Lines starting with N (Next channel)
			else  {
				chan_index++; 			// count channels
				count_tmp=0;
			}
            if (count == 0) {
                float dummy;
                channelsPerLine = sscanf(line, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f &f &f &f", &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy);
            }
            ++count;
		}
        count = 0;
		if (chan_index == 0)
			chan_index=1;
		numChannels = chan_index;
        if (channelsPerLine > numChannels)
            numChannels = channelsPerLine;
        else 
            channelsPerLine = 1;
		if (numChannels == 1) channel[0].setChannelName("EKG");
		if (numChannels == 2) {
			channel[0].setChannelName("EKG");
			channel[1].setChannelName("respiration");
		} else {
			for (i = 0; i < numChannels; i++) //set default channel names
			{
				char buf[1000];
				sprintf(buf, "kanal %d", i+1);
				channel[i].setChannelName(AnsiString(buf));
			}
		}

		defaultParam = "450";
		static const wchar_t paramStr2[] = L"DEKG parameter frequency";
		ini.loadVar(defaultParam, paramStr2);

		double sampRate = myAtof(InstantDialog::query(gettext("VisECG").data(), gettext("Sampling frequency:").data(), defaultParam.c_str()));

		if (!ini.storeVar(numHeadlines, paramStr1) || !ini.storeVar(sampRate, paramStr2) || !ini.save())
			Application->MessageBox(gettext("Error storing settings. The selected settings will not be saved!").data(), gettext("Warning").data(), MB_OK);

		samplingRate = sampRate;
		const int   channel0gain = 1;
		fclose(file);

		// scan the file for second time to read data
		file = _wfopen(fileName.c_str(), L"r");
		if (numHeadlines > 0)
		{
			fgets (string, sizeof(string), file); 	 // save heading text
			comment=string;
		}
		for (i = 1; i < numHeadlines; i++)
			 fgets (line, sizeof(line), file);

		count = 0; chan_index=0;
		while ( fgets (line, sizeof(line), file) != NULL) {
			if (line[0]!='N'){
				number = atof(line);
                channel[chan_index][count] = number/channel0gain;
                char* lineN = line;
                for (int c = 1; c < channelsPerLine; ++c) {
                    while (lineN[0] != ' ') {
                        if (lineN[0] == 0) {
                            Application->MessageBox(gettext("Reading error! The measurement is not valid").data(), gettext("Error").data(), MB_OK);
                        }
                        ++lineN;             
                    }                      
                    number = atof(lineN);
	    			channel[chan_index + c][count] = number/channel0gain;
                }
				count++;
			} else {
				chan_index++;
				count=0;
		 }  }
		fclose(file);
		break;
	}

	// DEKG BINARY
	case dekg_binary: {
		throw new EKGException(__LINE__, "CMeasurement::loadFromFile: format dekg_binary not implemented");
		//file = fopen(fileName.c_str(), "rb");
		//fclose(file);
		//break;
	}

	// MECG ASCII
	case mecg_ascii: {
		char string[200], string1[200], header[400];
		file = _wfopen(fileName.c_str(), L"r");
		if (file == NULL)
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: cannot open file");

		fgets (string, sizeof(string), file); 	 // save heading text 1. line
		fgets (string1, sizeof(string1), file);  // save heading text 2. line
		sprintf(header, "%s\r\n%s", string, string1);
		comment=header;
		int i,j, K, N, Tmin, Tmax, graph_flag, points, skip;
		double sample;

		fscanf (file, "%d", &K);
		fscanf (file, "%d", &N);
		fscanf (file, "%d", &Tmin);
		fscanf (file, "%d", &Tmax);
		fscanf (file, "%d", &graph_flag);
		fscanf (file, "%d", &points);
		fscanf (file, "%d", &skip);
// every skip sample is shown, test scale of x axis!!
		samplingRate = N/skip;
		numChannels = K;
        
		for (i = 0; i < numChannels; i++) //set default channel names
		{
			char buf[1000];
			sprintf(buf, "channel %d", i+1);

            if ((K == 38) && (i==36))
                channel[i].setChannelName(AnsiString("pressure"));
            else
    			channel[i].setChannelName(AnsiString(buf));
		}
		const double  oneLevel = 0.00018311;
        const double  oneLevelmmHg = 0.007629627369;

		for (j=0; j<points; j++) {
			for (i=0; i<K; i++) {
				fscanf (file, "%lf", &sample);

                if ((K == 38) && (i == 36))
                    channel[i][j] = sample * oneLevelmmHg;
                else
	    			channel[i][j] = sample * oneLevel;
		}   }
		fclose(file);
		break;
	}

	// MECG BINARY
	case mecg_binary: {
		file = _wfopen(fileName.c_str(), L"rb");
		if (file == NULL)
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: cannot open file");

		int i, j, K, K66, N, T, Off, graph_flag_K66;
		short int sample;
		char ID_string[80];
		const double  oneLevel = 0.00018311;
        const double  oneLevelmmHg = 0.007629627369;
        const int init_jump = 0xF080;

		fgets (ID_string, sizeof(ID_string)+1, file); 	 // skip heading text 1. line
		comment=ID_string; //header;

		fread(&K, sizeof(int), 1, file);
		fread(&N, sizeof(int), 1, file);
		fread(&T, sizeof(int), 1, file);
		fread(&Off, sizeof(int), 1, file);
		fread(&graph_flag_K66, sizeof(int), 1, file);
		K66 = graph_flag_K66;
		K66 = K66 >> 8; 				/* restore K66 */
		if (K66 == 0) K66 = 66;			/* to support old files */
        if (K66 == 0x80) K66 = init_jump; /* to support files after version 1.7.7.*/
		samplingRate = N;
		numChannels = K;
		for (i = 0; i < numChannels; i++) //set default channel names
		{
			char buf[1000];
			sprintf(buf, "channel %d", i+1);
            
            if ((K == 38) && (i==36))
                channel[i].setChannelName(AnsiString("pressure"));
            else
    			channel[i].setChannelName(AnsiString(buf));
		}

        // skip some data
		short int discardBuf[init_jump+500];
		fread(discardBuf, sizeof(short int), Off, file);

        // read data
		for (j = 0; j < N*T; j++) {
			for (i = 0; i < 64; i++) {
				fread(&sample, sizeof(short int), 1, file);
				if (i < numChannels) {
                    if ((K == 38) && (i == 36))
                        channel[i][j] = sample * oneLevelmmHg;
                    else
			    		channel[i][j] = sample * oneLevel;
                }
		}    }
		fclose(file);
		break;
	}

	// FINAPRESS BINARY
	case finapress: {
		file = _wfopen(fileName.c_str(), L"rb");
		if (file == NULL)
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: cannot open file");

		samplingRate = 500;
		comment = "imported from finapress file (EKG + pressure)";
		numChannels = 2;
		channel[0].setChannelName("EKG");
        channel[0].setMeasurementUnit("mV");
		channel[1].setChannelName("pressure");
        channel[1].setMeasurementUnit("mmHg");
		const double channel0gain = 1.0/6000,
					channel1gain = 1.0/166.5;
		unsigned char byte[4];
		int i = 0, bytesRead;
		short sample;
		do {
			//read 4 bytes & shuffle to get 2 samples
			bytesRead = fread(byte, sizeof(char), 4, file);
			if (bytesRead == 4)
			{
				sample = short((byte[1] << 8) | byte[0]);
				channel[0][i] = sample*channel0gain;
				sample = short((byte[3] << 8) | byte[2]);
				channel[1][i] = sample*channel1gain;
			}
			else if (bytesRead != 0)
			{
				fclose(file);
				throw new EKGException(__LINE__, "CMeasurement::loadFromFile: error in finapress file");
			}
			i++;
		} while (bytesRead != 0); //if bytesRead==0 then file end was reached
		fclose(file);
		break;
	}

    // HL/
    case hl7x: {
        std::ifstream input(fileName.c_str(), std::ios::binary);
        if (!input.is_open())
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: cannot open file");
                     
        // one line at a time, first entry in line determines line type
        std::string line;
        std::string lineType;
        int dataLineCount = 0;
        char lineDelimiter = 0; // set it to default value, and the first call to getline will check line delimiter type
        
        while(input) {
            lineDelimiter = getline(input, line, lineDelimiter);
            /*line.clear();
            while (input) {
                char ch;
                input.get(ch);
                if ((ch == '\r') || (ch == '\n')) {
                    char next = (char)input.peek();
                    if ((next == '\r') || (next == '\n'))
                        input.ignore(1);
                    break;
                }
                line.push_back(ch);
            } */
            std::stringstream lineStream(line);
			getline(lineStream, lineType, '|');
			// only parse OBX lines
			if (lineStream && (lineType == "OBX")) {
				// skip next entry (unknown)
				lineStream.ignore(1000, '|');
				// next entry determines weather the line is parsed or not
				getline(lineStream, lineType, '|');
                if (lineType == "CD") {
                    // CD == Column Description?
                    // ignore two entries (unknown + lineNum)
                    lineStream.ignore(1000, '|');
                    lineStream.ignore(1000, '|');   
                    AnsiString unit;

                    // now read the names of the channels + measurement units and frequency
                    for (int chanCnt = 0; lineStream; ++chanCnt) {
                        std::string name;
                        // names are separadew with ~ 
						getline(lineStream, name, '~');
                        if (name == "")
                            break;
                        if (lineStream.eof())
                            name.erase(name.find('|'));
                        std::stringstream nameStream(name);
                        // ignore first entry (channel numerical id, starts with 1)
                        // entries within the name are separated with ^
						nameStream.ignore(1000, '^');
                        // the actual channel name
                        std::string entry;
						getline(nameStream, entry, '^');
                        channel[chanCnt].setChannelName(entry.c_str());

                        numChannels = chanCnt+1;
                        // there might be more entries (maybe only possible for the first name)
						getline(nameStream, entry, '^');
                        if (nameStream) {
                            // this entry should be the unit
                            unit = entry.c_str();
                        }
                        channel[chanCnt].setMeasurementUnit(unit);
                        if (nameStream) {
                            nameStream.ignore(1000, '^');
                            if (nameStream)
                                nameStream >> samplingRate;
                        }
                    }
                } else if (lineType == "MA") {
                    // MA = Measurement?
                    // ignore two entries (unknown + lineNum)
                    lineStream.ignore(1000, '|');
                    lineStream.ignore(1000, '|');

                    // read channels
                    for (int chanCnt = 0; chanCnt < numChannels; ++chanCnt) {
                        double temp;
                        lineStream >> temp;
                        lineStream.ignore(1, '^');
                        channel[chanCnt][dataLineCount] = temp;
                    }
                    ++dataLineCount;
                }
            }
//            if (dataLineCount > 100000)                break;
        }

        for (int i = numChannels-1; i >= 0; --i)
            if (channel[i].getNumElems() < 1)
                deleteChannel(i);

		break;
    }
	// s2 native mobecg file (Should not come to here -> handled in docForm)
	case s2:
    // debug file (Should not come to here -> handled in docForm)
	case txt:
        break;
   	// native
	case nevroEKGeditable:
    	editable = true;
	//loading is the same as for nevroEKG
	case nevroEKG: {
   		file = _wfopen(fileName.c_str(), L"rb");
		if (file == NULL)
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: cannot open file");

		int fileVersion;
		if (myfscanf(file, "NEKG measurement file version %d\n", &fileVersion) != 1)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: file version not specified correctly in file");
		}
        nekgFileVersion = fileVersion;
		if (fileVersion != 1 && fileVersion != 2  && fileVersion != 3)
		{
            fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: unsupported version of nevroEKG format");
		}
		fgetUnicodeString(file, measurementName);
		fscanfMultilineString(file, comment, "numCommentLines");
		if (myfscanf(file, "%d numChannels\n", &numChannels) != 1)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: invalid file format");
		}
		if (myfscanf(file, "%lf samplingRate\n", &samplingRate) != 1)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: invalid file format");
		}
        if(samplingRate <=0){
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: sampling rate is negative or zero!");
        }
		AnsiString dateString;
		fgetAnsiString(file, dateString);
		date = stringToDate(dateString);

        if(fileVersion == 3){
            myfscanf(file,"%d numStatData\n", &numStatData);

            statDataString = new TStringList();
            statDataData = (double*)malloc(numStatData * sizeof(double)); //instead of new
            AnsiString statString;
            AnsiString recordingDevicesParams;
            int specialCasesNum = 0;
            for(int i=0;i<numStatData;i++){
                fgetAnsiString(file, statString);
                //special cases for   recorder MAC, recording hardware, recording software
                if(statString.AnsiCompare("recorder MAC") == 0){
                    fgetAnsiString(file, recordingDevicesParams);
                    setGadgetmac(recordingDevicesParams);
                    specialCasesNum++;
                    continue; 
                }else if(statString.AnsiCompare("recording hardware") == 0){
                    fgetAnsiString(file, recordingDevicesParams);
                    setGadgetVersion(recordingDevicesParams);
                    specialCasesNum++;
                    continue;
                }else if(statString.AnsiCompare("recording software") == 0){
                    fgetAnsiString(file, recordingDevicesParams);
                    setMobEcgVersion(recordingDevicesParams);
                    specialCasesNum++;
                    continue;
                }

                statDataString->Add(statString);
                myfscanf(file,"%lf\n", &statDataData[i]);
            }
            numStatData -= specialCasesNum; 
        }
		patientData.readFromFile(file, fileVersion);
		for (int channelToRead = 0; channelToRead < numChannels; channelToRead++){
			channel[channelToRead].readFromFile(file, fileVersion);
        }
		if (myfscanf(file, "%d numEventChannels\n", &numEventChannels) == 1)
		{
			for (int channelToRead = 0; channelToRead < numEventChannels; channelToRead++)
				eventChannel[channelToRead].readFromFile(file, fileVersion);
		} else
			numEventChannels = 0;
		try{
            if (myfscanf(file, "%d numAnnotationChannels\n", &numAnnotationChannels) == 1)
    		{
    			for (int channelToRead = 0; channelToRead < numAnnotationChannels; channelToRead++)
    				annotationChannel[channelToRead].readFromFile(file, fileVersion);
    		} else
    			numAnnotationChannels = 0;
        }catch(LibCore::EKGException *e){
            //there is no annotation channel found
            numAnnotationChannels = 0;
        }

		fclose(file);
		break;
	}
	default:
		throw new EKGException(__LINE__, "CMeasurement::loadFromFile: unsupported format");
		//break;
	}
	refreshAllMinMaxValues();
	return returnFilenames;
}

void CMeasurement::saveToFile(const UnicodeString fileName, const FileType fileType)
{
	FILE *file = _wfopen(fileName.c_str(), L"wb");
	if (file == NULL)
		throw new EKGException(__LINE__, "CMeasurement::saveToFile: cannot open file");

	switch (fileType) {

	// DEKG ASCII
	case dekg_ascii:
		fclose(file);
		throw new EKGException(__LINE__, "CMeasurement::saveToFile: unsuppored format");
		//break;

	// DEKG BINARY
	case dekg_binary:
		fclose(file);
		throw new EKGException(__LINE__, "CMeasurement::saveToFile: unsuppored format");
		//break;

	// MECG ASCII
	case mecg_ascii:
		fclose(file);
		throw new EKGException(__LINE__, "CMeasurement::saveToFile: unsuppored format");
		//break;

	// MECG BINARY
	case mecg_binary:
		fclose(file);
		throw new EKGException(__LINE__, "CMeasurement::saveToFile: unsuppored format");
		//break;

	// FINAPRESS BINARY
	case finapress:
		fclose(file);
		throw new EKGException(__LINE__, "CMeasurement::saveToFile: unsuppored format");
		//break;

	//native
	case nevroEKGeditable:
		editable = true;
		//saving is the same as for nevroEKG
	case nevroEKG: {
		int fileVersion = nekgFileVersion;
		RawByteString measurementNameUtf8 = System::UTF8Encode(measurementName);

		if(fileVersion < 3){

			UnicodeString text = UnicodeString(gettext("File you are trying to save is of older nekge format.")+"\n"+
				gettext("Do you wish tp update it to a newer file format?"));
			UnicodeString title = gettext("Saving file");
			auto result = Application->MessageBox(
			text.c_str()
			, title.c_str() ,
			MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON1);

			if(result == ID_YES){
				fileVersion = 3;
				for(int i=0; i<numChannels; i++){
					double missingValue =  channel[i].getMinValue();
					AnsiString userInput = AnsiString(missingValue);
					if (InstantDialog::queryOKCancel(
						gettext("Input missing data value."),
						gettext("New missing data value for channel ") + UnicodeString(i+1) +": ",
						userInput,
						userInput))
					{
						missingValue = LibCore::myAtof(userInput.c_str());
					}else{
                        return;
                    }
					channel[i].channelOffset = missingValue;
					channel[i].channelMultiplier = 1.0;
					channel[i].tenBitSaving = false;
				}
			}
		}


		if (fprintf(file, "NEKG measurement file version %d\n%s\n", fileVersion, measurementNameUtf8.c_str()) == EOF)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::saveToFile: error writing to file");
		}
		fprintfMultilineString(file, comment, "numCommentLines");
        AnsiString dateStr(dateToStringAccurate(date));
		if (fprintf(file, "%d numChannels\n%lf samplingRate\n%s\n",
					numChannels, samplingRate, dateStr.c_str()) == EOF)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::saveToFile: error writing to file");
		}

        if(fileVersion == 3){
            int newNumStatData = 0;
            TStringList * newstatDataString = new TStringList();
            //count number of non-standard parameters
            double measurementShownLength = getDuration();
            double measurementMissingDataLength = 0, measurementMissingDataLengthFix = 0;
            bool fixMissingDataLength = false;
            if(statDataString!=NULL){
                for(int i=0;i<statDataString->Count;i++)
                {
					if(statDataString->Strings[i].Compare("Event channel used") == 0 ||
					   statDataString->Strings[i].Compare("BPM AVG") == 0 ||
					   statDataString->Strings[i].Compare("BPM STD") == 0 ||
					   statDataString->Strings[i].Compare("Number of beats") == 0 ||
					   statDataString->Strings[i].Compare("mark cut") == 0 ||
                       statDataString->Strings[i].Compare("Beats coverage percentage") == 0)
                    {
                        continue;
                    }
					else if(statDataString->Strings[i].Compare("part length") == 0)
                    {
                        if(measurementShownLength < statDataData[i]){
                            fixMissingDataLength = true;
                            measurementMissingDataLengthFix = statDataData[i] - measurementShownLength;
                        }
                        measurementShownLength = statDataData[i];
                        continue;
					}
					else if(statDataString->Strings[i].Compare("part missing data length") == 0)
                    {
                        measurementMissingDataLength = statDataData[i];
                        continue;
                    }
                    newNumStatData++;
                }
                if(fixMissingDataLength){
                    measurementMissingDataLength = measurementMissingDataLengthFix;
                }
            }else{
            	statDataString = new TStringList();
            }

            int numDataAlwaysWritten = 4;//number of statistical data written when RRI channel exists
			double * newStatDataData = (double*)malloc((newNumStatData+numDataAlwaysWritten+10) * sizeof(double)); //10 as reserve
            int pos = 0;
            for(int i=0;i<statDataString->Count;i++){
				if(statDataString->Strings[i].Compare(L"Event channel used") == 0 ||
				   statDataString->Strings[i].Compare(L"BPM AVG") == 0 ||
				   statDataString->Strings[i].Compare(L"BPM STD") == 0 ||
				   statDataString->Strings[i].Compare(L"Number of beats") == 0 ||
				   statDataString->Strings[i].Compare(L"Beats coverage percentage") == 0 ||
				   statDataString->Strings[i].Compare(L"part length") == 0 ||
				   statDataString->Strings[i].Compare(L"mark cut") == 0 ||
				   statDataString->Strings[i].Compare(L"part missing data length") == 0)
                   {
                   continue;
                   }
                newstatDataString->Add(statDataString->Strings[i]);
    			newStatDataData[pos] = statDataData[i];
                pos++;
            }

            numStatData = newNumStatData+2; // +2 for part length and missing data
            statDataData = newStatDataData;

            for(int i = getNumEventChannels()-1;i>=0;i--){
    			if(1 == (getEventChannel(i).getChannelName()).AnsiPos("Frequency from ")){
                    getEventChannel(i).appendComment("Channel used for calculating statistics(BPM)");

    				numStatData += numDataAlwaysWritten;
    				statDataString = newstatDataString;
                    LibCore::CEventChannel::Statistics stat = getEventChannel(i).calcStatistics();

       				//statDataString->Add("Event channel used");
    				//statDataData[newNumStatData+0] = i+1;
    				statDataString->Add("BPM AVG");
    				statDataData[newNumStatData+0] = stat.avgValue;
					statDataString->Add("BPM STD");
					if(stat.avgValue>0)
						statDataData[newNumStatData+1] = fabs(stat.stDevValue/stat.avgValue);
					else
						statDataData[newNumStatData+1] = 0;
       				statDataString->Add("Number of beats");
    				statDataData[newNumStatData+2] = stat.numOfEvents;
    			}
                if(1 == (getEventChannel(i).getChannelName()).AnsiPos("Frequency from ")){
                    LibCore::CEventChannel::Statistics stat = getEventChannel(i).calcStatistics();
                    getEventChannel(i).appendComment("Channel used for calculating statistics (coverage)");
					statDataString->Add("Beats coverage percentage");
					if(stat.avgValue>0 && (getDuration() > 0))
						statDataData[newNumStatData+3] = std::min(100.0,100.0 * (((stat.numOfEvents / (stat.avgValue/60.0))) / getDuration())); //TODO check
					else
						statDataData[newNumStatData+3] = 0;
                }
            }

            fprintf(file,"%d numStatData\n", numStatData+4);
			for(int i=0;i<numStatData-2;i++){
				AnsiString temporaryName =  AnsiString(statDataString->Strings[i]);
				fprintf(file,"%s\n", temporaryName.c_str());
				fprintf(file,"%lf\n", statDataData[i]);
            }
            fprintf(file,"part length\n"); //part length is always written
            fprintf(file, "%lf\n", measurementShownLength);
            fprintf(file,"part missing data length\n"); //part missing length is always written
            fprintf(file, "%lf\n", measurementMissingDataLength);
            fprintf(file,"mark cut\n");
            fprintf(file, "%lf\n", (markCut)?1.0:0.0);

            fprintf(file,"recording software\n");
            fprintf(file, "%s\n", getMobEcgVersion().c_str());

            fprintf(file,"recording hardware\n");
            fprintf(file, "%s\n", getGadgetVersion().c_str());
            
            fprintf(file,"recorder MAC\n");
            fprintf(file, "%s\n", getGadgetMac().c_str());

        }

		patientData.writeToFile(file, fileVersion);
		for (int channelToWrite = 0; channelToWrite < numChannels; channelToWrite++){
			channel[channelToWrite].writeToFile(file, fileVersion);
        }
		if (fprintf(file, "%d numEventChannels\n", numEventChannels) == EOF)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::saveToFile: error writing to file");
		}
		for (int channelToWrite = 0; channelToWrite < numEventChannels; channelToWrite++)
			eventChannel[channelToWrite].writeToFile(file, fileVersion);
        if (fprintf(file, "%d numAnnotationChannels\n", numAnnotationChannels) == EOF)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::saveToFile: error writing to file");
		}
  		for (int channelToWrite = 0; channelToWrite < numAnnotationChannels; channelToWrite++)
			annotationChannel[channelToWrite].writeToFile(file, fileVersion);
		fclose(file);
		break;
	}
	default:
		throw new EKGException(__LINE__, "CMeasurement::saveToFile: unsuppored format");
		//break;
	}
}

bool CMeasurement::importEventIntervals(const AnsiString fileName)
{
// ppi (peak-peak interval) ASCII -  file type *.ppi  (read as many events as possible for the
//curent open measurement file.
//New event channel.time is filled  with RRtime data and channel.value with PPtime(i+1)-PPtime(i) from file.
	FILE *file = fopen(fileName.c_str(), "r");
	if (!file)
		throw new EKGException(__LINE__, "CMeasurement::importEventIntervals: cannot open file");
    int i, countRR;
    double startTime, endTime;
    char line[2000], string[2000];

	int numHeadlines = InstantDialog::query( gettext("VisECG"),	gettext("Number of start lines:"), "4").ToInt();
	i= numHeadlines;
    while  (i > 0)
    {
        fgets (line, sizeof(line), file);
        i--;
    }
    countRR = 0;
    while (fgets (line, sizeof(line), file) != NULL)
    {
        if (countRR==0) startTime = atof(line);  //save start time
        endTime = atof(line);  //save end time
        countRR++;        // count data
    }
	fclose(file);

    // scan the file for second time to read data
    file = fopen(fileName.c_str(), "r");
    if (numHeadlines > 0)
    {
        fgets (string, sizeof(string), file); 	 // save heading text  in the first line
        comment=string;
    }
    for (i = 1; i < numHeadlines; i++)
        fgets (line, sizeof(line), file);     // skip other text lines

    //create new event channel, set its name, unit and comment
    int RRINum = addEventChannel ("PPI imported", "s");
    CEventChannel &RRI = eventChannel[RRINum];
    RRI.setContiguous(false);
    RRI.setComment("Peak-Peak Interval imported from " + fileName + "\r\n");

    double number; i=0;
    while ( fgets (line, sizeof(line), file) != NULL)
//�as number moramo primerjati z za�etkom in koncem odprtega okna!
//�e sedaj je mo�no v�itati vse dogodke, �eprav je meritev kraj�a???
        if (((number = atof(line)) >= startTime) && ((number = atof(line)) <= endTime))
        {
            RRI[i].time = number;
            if (i>0) RRI[i].value = RRI[i].time - RRI[i-1].time;
            i++;
         }
    if (RRI.getNumElems() > 1)
        RRI[0].value = RRI[1].value;  // fill first data
    else  if (RRI.getNumElems() == 1)
		Application->MessageBox(gettext("Datoteka vsebuje le en interval.").data(), gettext("Import Peak-Peak Interval - Warning!").data() , MB_OK);
	else
		Application->MessageBox(gettext("Datoteka ne vsebuje nobenega intervala.").data(), gettext("Import Peak-Peak Interval - Warning!").data() , MB_OK);

    char buff[1000];
	sprintf(buff, "�tevilo uvo�enih �asov %d, �tevilo zavr�enih �asov %d.\r\n", i, countRR-i);
	RRI.appendComment(buff);

    fclose(file);
	//do sth here; return true on success/false on failure
	return true;
}

void CMeasurement::exportAllSignalChannels(const AnsiString fileName)
{
	FILE *file = fopen(fileName.c_str(), "w");
	if (!file)
		throw new EKGException(__LINE__, "CMeasurement::exportAllSignalChannels: cannot open file");
	fprintf(file, "%lf\n", getSamplingRate());
	for (int i = 0; i < channel[0].getNumElems(); i++)
	{
		for (int channelNum = 0; channelNum < numChannels; channelNum++)
			fprintf(file, "%lf\t", channel[channelNum][i]);
		fprintf(file, "\n");
	}
	fclose(file);
}

void CMeasurement::exportSignalChannel(const AnsiString fileName, int channelNum)
{
	FILE *file = fopen(fileName.c_str(), "w");
	if (!file)
		throw new EKGException(__LINE__, "CMeasurement::exportSignalChannel: cannot open file");
	fprintf(file, "%lf\n", getSamplingRate());
	for (int i = 0; i < channel[channelNum].getNumElems(); i++)
		fprintf(file, "%lf\n", channel[channelNum][i]);
	fclose(file);
}

void CMeasurement::exportEventChannel(const AnsiString fileName, int eventChannelNum)
{
	FILE *file = fopen(fileName.c_str(), "w");
	if (!file)
		throw new EKGException(__LINE__, "CMeasurement::exportEventChannel: cannot open file");
	for (int i = 0; i < eventChannel[eventChannelNum].getNumElems(); i++)
		fprintf(file, "%lf\t%lf\n", eventChannel[eventChannelNum][i].time, eventChannel[eventChannelNum][i].value);
	fclose(file);
}

static const int NUM_SAMPLES_TO_PLOT = 1600;

TColor CMeasurement::getChannelColor(int channelNum)
{
	static const TColor channelColors[] = {
		clRed,
		clBlue,
		clGreen,
		clMaroon,
		clNavy,
		clOlive,
		clBlack,
		clPurple,
		clTeal,
		clFuchsia,
		clDkGray
    };
	static const int numChannelColors = sizeof(channelColors)/sizeof(TColor);
	return channelColors[channelNum % numChannelColors];
}

double yValue2graph(bool trueYscale, double y, int _channelNum, bool defaults = true,
					double _thisChannelMin = 0, double _thisChannelMax = 0,
					double _selectedChannelMin = 0, double _selectedChannelMax = 0,
					double _zoom = 0, double _offset = 0)
//maps y into selected channel's y-space
{
	static int channelNum = -1;
	static double thisChannelMin, thisChannelMax, selectedChannelMin, selectedChannelMax, zoom, offset;

	if (defaults)
	{
		//defaults can only be used if they were previously stored for the same channel number
		//(channel number is used only for this safety check!)
		assert(channelNum == _channelNum);
	} else {
		//else, defaults are stored for future use for the same channel
		channelNum = _channelNum;
		thisChannelMin = _thisChannelMin;
		thisChannelMax = _thisChannelMax;
		if (thisChannelMax == thisChannelMin)
			thisChannelMax++;
		selectedChannelMin = _selectedChannelMin;
		selectedChannelMax = _selectedChannelMax;
		if (selectedChannelMax == selectedChannelMin)
			selectedChannelMax++;
		zoom = _zoom;
		offset = _offset;
	}
	if (trueYscale)
		return y;
	else {
		if ( ((thisChannelMax-thisChannelMin) > 0) && (zoom > 0))
			return ((y-thisChannelMin)/(thisChannelMax-thisChannelMin)*zoom - offset) //here the offset is negated so that first channel is drawn on top
				*(selectedChannelMax-selectedChannelMin) + selectedChannelMin;
		else
            return -1;
	}
}

//deletes everything from the chart and puts in current data from time tLeft to tRight
void CMeasurement::refreshGraphValues(TChart *chart, double tLeft, double tRight, YscaleMode yScaleMode,
						bool trueYscale, bool eventChannelSelected, bool annotationChannelSelected, int numSelectedChannel, bool genReport)
{
	bool setAutoRepaint = chart->AutoRepaint;
	chart->AutoRepaint = false;
	UnicodeString leftAxisLabel = gettext("selected channel unit:")+L" ", rightAxisLabel = "";
	assert(chart);
	assert(tLeft >= 0);
	assert(numChannels > 0 || numEventChannels > 0);
	assert(tRight > tLeft && (numChannels == 0 || time2SampleIndex(tRight) <= channel[0].getNumElems()));
	assert(numSelectedChannel >= 0);
	assert((!eventChannelSelected && !annotationChannelSelected && numSelectedChannel < numChannels)
			|| (eventChannelSelected  && !annotationChannelSelected &&  numSelectedChannel < numEventChannels)
            || (annotationChannelSelected && numSelectedChannel < numAnnotationChannels));

	TeeDefaultCapacity = NUM_SAMPLES_TO_PLOT+100;

	for (int i = chart->SeriesList->Count-1; i >= 0; i--)
	{
		delete chart->Series[i];
	}
	chart->RemoveAllSeries();

	double minAllChannels = 1e+100, maxAllChannels = -1e+100;
			//total y-range of all shown channels in graph coord. system
			//this range must be shown on left axis if fixYscale (i.e. x-scrolling does not change y-axis)
	double minValueDrawn = 1e+100, maxValueDrawn = -1e+100;
			//total y-range of parts of shown channels from tLeft-tRight; in graph coord. system
			//this range must be shown on left axis if !fixYscale (i.e. chart shoulb be y-zoomed to what is actually shown on it)

	double graphValue; //temp variable

	double selectedMin, selectedMax, selectedZoom; //properties of selected channel
	double chOffset[maxNumChannels], eventChOffset[maxNumChannels], annotationChOffset[maxNumChannels];
		//absolute offsets of all shown channels (viewProps.offset is relative (additive) to previous channels)

	//calc selected channel props & absolute offsets
	if(annotationChannelSelected)
	{
        selectedMin = annotationChannel[numSelectedChannel].getMinValue();
		selectedMax = annotationChannel[numSelectedChannel].getMaxValue();
		selectedZoom = annotationChannel[numSelectedChannel].viewProps.zoomY;
		annotationChOffset[numSelectedChannel] = 0;

		//from selected up, offsets are added
		for (int channelNum = numSelectedChannel+1; channelNum < numAnnotationChannels; channelNum++)
			annotationChOffset[channelNum] = annotationChOffset[channelNum-1]
				+ annotationChannel[channelNum].viewProps.offset*annotationChannel[channelNum].viewProps.zoomY/selectedZoom;
		//from selected down, offsets are subtracted
   		for (int channelNum = numSelectedChannel-1; channelNum >= 0; channelNum--)
			annotationChOffset[channelNum] = annotationChOffset[channelNum+1]
				- annotationChannel[channelNum+1].viewProps.offset*annotationChannel[channelNum+1].viewProps.zoomY/selectedZoom;
        rightAxisLabel = gettext("selected decription channel unit:")+L" " + annotationChannel[numSelectedChannel].getMeasurementUnit();

        if(numEventChannels>0)
            eventChOffset[numEventChannels-1] = annotationChOffset[0]
			    - annotationChannel[0].viewProps.offset*annotationChannel[0].viewProps.zoomY/selectedZoom;
    		for (int channelNum = numEventChannels-2; channelNum >= 0; channelNum--)
    			eventChOffset[channelNum] = eventChOffset[channelNum+1]
    				- eventChannel[channelNum].viewProps.offset*eventChannel[channelNum].viewProps.zoomY/selectedZoom;

		chOffset[numChannels-1] = ((numEventChannels>0)?eventChOffset[0]:annotationChOffset[0])
			- eventChannel[0].viewProps.offset*eventChannel[0].viewProps.zoomY/selectedZoom;
		for (int channelNum = numChannels-2; channelNum >= 0; channelNum--)
			chOffset[channelNum] = chOffset[channelNum+1]
				- channel[channelNum+1].viewProps.offset*channel[channelNum+1].viewProps.zoomY/selectedZoom;
    }
    else if (eventChannelSelected && !annotationChannelSelected)
	{
		selectedMin = eventChannel[numSelectedChannel].getMinValue();
		selectedMax = eventChannel[numSelectedChannel].getMaxValue();
		selectedZoom = eventChannel[numSelectedChannel].viewProps.zoomY;
		eventChOffset[numSelectedChannel] = 0;
		rightAxisLabel = gettext("selected event channel unit:")+L" " + eventChannel[numSelectedChannel].getMeasurementUnit();

		//from selected up, offsets are added
		for (int channelNum = numSelectedChannel+1; channelNum < numEventChannels; channelNum++)
			eventChOffset[channelNum] = eventChOffset[channelNum-1]
				+ eventChannel[channelNum].viewProps.offset*eventChannel[channelNum].viewProps.zoomY/selectedZoom;
		if(numAnnotationChannels > 0){
            annotationChOffset[0] = eventChOffset[numEventChannels-1]
    			+ annotationChannel[0].viewProps.offset*annotationChannel[0].viewProps.zoomY/selectedZoom;
    		for (int channelNum = 1; channelNum < numAnnotationChannels; channelNum++)
    			annotationChOffset[channelNum] = annotationChOffset[channelNum-1]
    				+ annotationChannel[channelNum].viewProps.offset*annotationChannel[channelNum].viewProps.zoomY/selectedZoom;
        }
		//from selected down, offsets are subtracted
		for (int channelNum = numSelectedChannel-1; channelNum >= 0; channelNum--)
			eventChOffset[channelNum] = eventChOffset[channelNum+1]
				- eventChannel[channelNum+1].viewProps.offset*eventChannel[channelNum+1].viewProps.zoomY/selectedZoom;
		chOffset[numChannels-1] = eventChOffset[0]
			- eventChannel[0].viewProps.offset*eventChannel[0].viewProps.zoomY/selectedZoom;
		for (int channelNum = numChannels-2; channelNum >= 0; channelNum--)
			chOffset[channelNum] = chOffset[channelNum+1]
				- channel[channelNum+1].viewProps.offset*channel[channelNum+1].viewProps.zoomY/selectedZoom;
	} else {

		selectedMin = channel[numSelectedChannel].getMinValue();
		selectedMax = channel[numSelectedChannel].getMaxValue();
		selectedZoom = channel[numSelectedChannel].viewProps.zoomY;
		chOffset[numSelectedChannel] = 0;
		for (int channelNum = numSelectedChannel+1; channelNum < numChannels; channelNum++)
			chOffset[channelNum] = chOffset[channelNum-1]
				+ channel[channelNum].viewProps.offset*channel[channelNum].viewProps.zoomY/selectedZoom;
		for (int channelNum = numSelectedChannel-1; channelNum >= 0; channelNum--)
			chOffset[channelNum] = chOffset[channelNum+1]
				- channel[channelNum+1].viewProps.offset*channel[channelNum+1].viewProps.zoomY/selectedZoom;

		if(numEventChannels > 0)
			rightAxisLabel = gettext("First event channel unit:")+L" " + eventChannel[numSelectedChannel].getMeasurementUnit();
		if (numEventChannels > 0)
		{
			eventChOffset[0] = chOffset[numChannels-1]
				+ eventChannel[0].viewProps.offset*eventChannel[0].viewProps.zoomY/selectedZoom;
			for (int channelNum = 1; channelNum < numEventChannels; channelNum++)
				eventChOffset[channelNum] = eventChOffset[channelNum-1]
					+ eventChannel[channelNum].viewProps.offset*eventChannel[channelNum].viewProps.zoomY/selectedZoom;
		}

        if(numAnnotationChannels > 0){
    		if (numEventChannels > 0)
                annotationChOffset[0] = eventChOffset[numEventChannels-1];
            else
			    annotationChOffset[0] = chOffset[numChannels-1];
            annotationChOffset[0] += annotationChannel[0].viewProps.offset*annotationChannel[0].viewProps.zoomY/selectedZoom;
		    for (int channelNum = 1; channelNum < numAnnotationChannels; channelNum++)
			    annotationChOffset[channelNum] = annotationChOffset[channelNum-1]
				    + annotationChannel[channelNum].viewProps.offset*annotationChannel[channelNum].viewProps.zoomY/selectedZoom;
        }
	}
	//draw signal channels
	int numChannelsShown = 0;
	for (int channelNum = 0; channelNum < numChannels; channelNum++)
	{
		if(numChannelsShown==0)
			leftAxisLabel += channel[0].getMeasurementUnit();
		if (channel[channelNum].viewProps.visible){
			//set default values for yValue2graph()
			if (!eventChannelSelected && numSelectedChannel == channelNum)
				//selected channel has zoom 1 and offset 0, so that true values are shown on left axis
				yValue2graph(trueYscale, 0, channelNum, false, channel[channelNum].getMinValue(), channel[channelNum].getMaxValue(),
							selectedMin, selectedMax, 1, 0);
			else
				yValue2graph(trueYscale, 0, channelNum, false, channel[channelNum].getMinValue(), channel[channelNum].getMaxValue(),
							selectedMin, selectedMax, channel[channelNum].viewProps.zoomY/selectedZoom, chOffset[channelNum]);

			minAllChannels = std::min(minAllChannels, yValue2graph(trueYscale, channel[channelNum].getMinValue(), channelNum, true));
			maxAllChannels = std::max(maxAllChannels, yValue2graph(trueYscale, channel[channelNum].getMaxValue(), channelNum, true));
		}
		TFastLineSeries *series = new TFastLineSeries(chart);
		series->Title = "M" + AnsiString(channelNum+1) + ": " + channel[channelNum].getChannelName();
		series->ParentChart = chart;
		series->SeriesColor = getChannelColor(channelNum);
		if (!channel[channelNum].viewProps.visible)
			continue;
		series->AddXY(tLeft, yValue2graph(trueYscale, accessByTime(channelNum, tLeft), channelNum, true), "", clTeeColor);

		//go through samples; draw if
		//a) a pack of samples that corresponds to 1 sample on screen was gone through
		//b) derivative changes sign
		//this ensures that high frequencies are shown as solid black instead of becoming invisible
		bool goingUp = true;
		int lastDrawnSample = time2SampleIndex(tLeft);
		int rightmostShownSample = std::min(time2SampleIndex(tRight)+1, channel[channelNum].getNumElems()-1);

		for (int sample = std::max(1,time2SampleIndex(tLeft)); sample <= rightmostShownSample; sample++)
		{

			if( channel[channelNum][sample] == channel[channelNum].channelOffset)
				continue;
			if( (channel[channelNum][sample-1] == channel[channelNum].channelOffset
				&& channel[channelNum][sample] != channel[channelNum].channelOffset)
				||sample >= rightmostShownSample){

				if(rightmostShownSample == channel[channelNum].getNumElems()-1 && sample == rightmostShownSample){
					graphValue = yValue2graph(trueYscale, channel[channelNum][rightmostShownSample], channelNum, true);
					series->AddXY(sampleIndex2Time(rightmostShownSample), graphValue, "", clTeeColor);
				}
				series = new TFastLineSeries(chart);
				series->ParentChart = chart;
				series->SeriesColor = getChannelColor(channelNum);
				series->ShowInLegend = false;
			}

			if ((sampleIndex2Time(sample)-sampleIndex2Time(lastDrawnSample) > (tRight-tLeft)/NUM_SAMPLES_TO_PLOT)
				|| (lastDrawnSample < sample &&
				((goingUp && channel[channelNum][sample] < channel[channelNum][sample-1]) ||
				 (!goingUp && channel[channelNum][sample] > channel[channelNum][sample-1]))))
			{
				lastDrawnSample = sample;
				graphValue = yValue2graph(trueYscale, channel[channelNum][sample], channelNum, true);
				minValueDrawn = std::min(minValueDrawn, graphValue);
				maxValueDrawn = std::max(maxValueDrawn, graphValue);
				series->AddXY(sampleIndex2Time(sample), graphValue, "", clTeeColor);
				goingUp = channel[channelNum][sample] > channel[channelNum][sample-1];
			}
		}
		//series->AddXY(sampleIndex2Time(rightmostShownSample), yValue2graph(trueYscale, channel[channelNum][rightmostShownSample], channelNum, true), "", clTeeColor);
		numChannelsShown++;
	}

	if(numChannelsShown == 0){
		if(annotationChannelSelected)
			leftAxisLabel += annotationChannel[numSelectedChannel].getMeasurementUnit();
		else
			leftAxisLabel += eventChannel[numSelectedChannel].getMeasurementUnit();
	}


	double maxYeventChannel=-1e+100, minYeventChannel=1e+100;
    double maxYeventChannelAll=-1e+100, minYeventChannelAll=1e+100;
	//draw event channels
	for (int channelNum = 0; channelNum < numEventChannels; channelNum++)
	{
		if (eventChannel[channelNum].viewProps.visible){
			if (eventChannelSelected && !annotationChannelSelected && numSelectedChannel == channelNum)
				yValue2graph(trueYscale, 0, channelNum, false, eventChannel[channelNum].getMinValue(), eventChannel[channelNum].getMaxValue(),
							selectedMin, selectedMax, 1, 0);
			else
				yValue2graph(trueYscale, 0, channelNum, false, eventChannel[channelNum].getMinValue(), eventChannel[channelNum].getMaxValue(),
							selectedMin, selectedMax, eventChannel[channelNum].viewProps.zoomY/selectedZoom, eventChOffset[channelNum]);

			if(!trueYscale){
				minAllChannels = std::min(minAllChannels, yValue2graph(trueYscale, eventChannel[channelNum].getMinValue(), channelNum, true));
				maxAllChannels = std::max(maxAllChannels, yValue2graph(trueYscale, eventChannel[channelNum].getMaxValue(), channelNum, true));
			}
			else{
				minYeventChannelAll = std::min(minYeventChannelAll, yValue2graph(trueYscale, eventChannel[channelNum].getMinValue(), channelNum, true));
				maxYeventChannelAll = std::max(maxYeventChannelAll, yValue2graph(trueYscale, eventChannel[channelNum].getMaxValue(), channelNum, true));
			}
		}
		TChartSeries *series;
		TColor seriesColor =  getChannelColor(numChannels+channelNum);
		if (eventChannel[channelNum].getContiguous())
		{
			TFastLineSeries *newSeries = new TFastLineSeries(chart);
			series = newSeries;
		} else {
			TPointSeries *newSeries = new TPointSeries(chart);
			if(eventChannel[channelNum].getSmallDots()){
				newSeries->Pointer->Style = psSmallDot;
			}else{
				newSeries->Pointer->Style = psDiagCross;
				newSeries->Pointer->Pen->Width = 2;
				newSeries->Pointer->Pen->Color = seriesColor;
				newSeries->Pointer->Color = seriesColor;
				newSeries->Pointer->HorizSize = 2;
				newSeries->Pointer->VertSize = 2;
			}
			series = newSeries;
		}
		series->VertAxis = aRightAxis;
//		series->ColorEachPoint = false;

		series->SeriesColor = seriesColor;
		series->Color = seriesColor;
		series->Title = "D" + AnsiString(channelNum+1) +": " + eventChannel[channelNum].getChannelName();
		series->ParentChart = chart;
		if (!eventChannel[channelNum].viewProps.visible)
			continue;
		//every event from event channels is drawn because there are supposed to be relatively few events
		int sample = std::max(0, eventChannel[channelNum].getIndexClosestToTime(tLeft)-1);
		graphValue = yValue2graph(trueYscale, eventChannel[channelNum][sample].value, channelNum, true);
		if(!trueYscale){
			minValueDrawn = std::min(minValueDrawn, graphValue);
			maxValueDrawn = std::max(maxValueDrawn, graphValue);
		}
		minYeventChannel = std::min(minYeventChannel, graphValue);
		maxYeventChannel = std::max(maxYeventChannel, graphValue);
		series->AddXY(eventChannel[channelNum][sample].time, graphValue, "", series->Color);
		sample++;
		for (; sample < eventChannel[channelNum].getNumElems() && ((sample-1)<0 || eventChannel[channelNum][sample-1].time <= tRight); sample++)
		{
			graphValue = yValue2graph(trueYscale, eventChannel[channelNum][sample].value, channelNum, true);

            if(!trueYscale){
				minValueDrawn = std::min(minValueDrawn, graphValue);
    			maxValueDrawn = std::max(maxValueDrawn, graphValue);
            }
            else{
                minYeventChannelAll = std::min(minYeventChannelAll, yValue2graph(trueYscale, eventChannel[channelNum].getMinValue(), channelNum, true));
                maxYeventChannelAll = std::max(maxYeventChannelAll, yValue2graph(trueYscale, eventChannel[channelNum].getMaxValue(), channelNum, true));
            }
            minYeventChannel = std::min(minYeventChannel, graphValue);
            maxYeventChannel = std::max(maxYeventChannel, graphValue);

            // when doing reports ommit long pauses
            if(genReport && eventChannel[channelNum].getContiguous() && sample > 0 &&
                eventChannel[channelNum][sample].time - eventChannel[channelNum][sample-1].time > 60){
    			TFastLineSeries *newSeries = new TFastLineSeries(chart);
	    		series = newSeries;
                series->VertAxis = aRightAxis;
        		series->SeriesColor = seriesColor;
        		series->ParentChart = chart;
                series->ShowInLegend = false; 
            }
			series->AddXY(eventChannel[channelNum][sample].time, graphValue, "", series->Color);

            // this is a hack to draw last correct sample when zoomed in
			if(!eventChannel[channelNum].getContiguous()
				&& sample < (eventChannel[channelNum].getNumElems()-1)
				&& eventChannel[channelNum][sample+1].time > tRight
				&& minValueDrawn != 1e+100)
				series->AddXY(tRight, minValueDrawn, "", clNone);
		}
		numChannelsShown++;
	}

    //draw Annotation channels
    int verticalBarSize = 12;
    double annotationChannelDrawingOffset = 1.0;
	for (int channelNum = 0; channelNum < numAnnotationChannels; channelNum++)
	{
		if (annotationChannel[channelNum].viewProps.visible){
			if  (annotationChannelSelected && numSelectedChannel == channelNum)
				yValue2graph(trueYscale, 0, channelNum, false, annotationChannel[channelNum].getMinValue(), annotationChannel[channelNum].getMaxValue() + annotationChannelDrawingOffset,
							selectedMin, selectedMax, 1, 0);
			else
				yValue2graph(trueYscale, 0, channelNum, false, annotationChannel[channelNum].getMinValue(), annotationChannel[channelNum].getMaxValue() + annotationChannelDrawingOffset,
						selectedMin, selectedMax, annotationChannel[channelNum].viewProps.zoomY/selectedZoom, annotationChOffset[channelNum]);

			if(!trueYscale){
				minAllChannels = std::min(minAllChannels, yValue2graph(trueYscale, annotationChannel[channelNum].getMinValue() , channelNum, true));
				maxAllChannels = std::max(maxAllChannels, yValue2graph(trueYscale, annotationChannel[channelNum].getMaxValue() + annotationChannelDrawingOffset, channelNum, true));
			}
			else{
				minYeventChannelAll = std::min(minYeventChannelAll, yValue2graph(trueYscale, eventChannel[channelNum].getMinValue(), channelNum, true));
				maxYeventChannelAll = std::max(maxYeventChannelAll, yValue2graph(trueYscale, eventChannel[channelNum].getMaxValue(), channelNum, true));
			}
		}
		TChartSeries *series;
		TGanttSeries *newSeries = new TGanttSeries(chart);
		//if(channelNum == 0)
		//    verticalBarSize = std::max(1.0, 0.1 * (maxAllChannels - minAllChannels));
		newSeries->Pointer->VertSize = verticalBarSize;
		newSeries->VertAxis = aRightAxis;
		newSeries->Marks->Visible = true;
//        newSeries->Marks->Transparent = true;
		newSeries->Marks->Clip = true;
		newSeries->ColorEachPoint = false;
		newSeries->SeriesColor = clGray;
		if(annotationChannel[channelNum].getChannelName().AnsiCompare(markedChName) == 0)
			newSeries->SeriesColor = clRed;
		newSeries->Marks->Clip = false;

		series = newSeries;
		series->Title = "A" + AnsiString(channelNum+1) + ": " +annotationChannel[channelNum].getChannelName();
		series->ParentChart = chart;
		series->XValues->DateTime = false;

		if (!annotationChannel[channelNum].viewProps.visible)
            continue;
		for (int sample = 0; sample < annotationChannel[channelNum].getNumElems(); sample++)
		{
			if(annotationChannel[channelNum][sample].time > tRight ||
				annotationChannel[channelNum][sample].time + annotationChannel[channelNum][sample].length < tLeft)
				continue;
            graphValue = yValue2graph(trueYscale, annotationChannel[channelNum][sample].value, channelNum, true);
            if(!trueYscale){
                minValueDrawn = std::min(minValueDrawn, graphValue);
    			maxValueDrawn = std::max(maxValueDrawn, graphValue);
            }
            minYeventChannel = std::min(minYeventChannel, graphValue);
            maxYeventChannel = std::max(maxYeventChannel, graphValue);
//			series->AddXY(annotationChannel[channelNum][sample].time, graphValue, "", clTeeColor);
            newSeries->AddGantt(annotationChannel[channelNum][sample].time,annotationChannel[channelNum][sample].time + annotationChannel[channelNum][sample].length, graphValue, annotationChannel[channelNum][sample].annotation);
//            newSeries->AddGanttColor(annotationChannel[channelNum][sample].time,annotationChannel[channelNum][sample].time + annotationChannel[channelNum][sample].length, graphValue, annotationChannel[channelNum][sample].annotation,clTeeColor);
		}
		numChannelsShown++;
	}


	//check left axis values
	for(int i=0; i< getNumChannels(); i++){
		if(channel[i].viewProps.visible)
			break;
		if(i==getNumChannels()-1){
			minAllChannels = minYeventChannelAll;
			maxAllChannels = maxYeventChannelAll;
		}
	}

	//set axis ranges
	if (tLeft < chart->BottomAxis->Minimum)
	//because axis minimum must always be lower than maximum, order of setting axis limits
	//depends on old & new limits
	{
		chart->BottomAxis->Minimum = tLeft;
		chart->BottomAxis->Maximum = tRight;
		chart->TopAxis->Minimum = tLeft/86400 + getDate().Val;
		chart->TopAxis->Maximum = tRight/86400 + getDate().Val;
	} else {
		chart->BottomAxis->Maximum = tRight;
		chart->BottomAxis->Minimum = tLeft;
		chart->TopAxis->Maximum = tRight/86400 + getDate().Val;
        chart->TopAxis->Minimum = tLeft/86400 + getDate().Val;
	}
	if (numChannelsShown == 0)
		return;
	switch (yScaleMode) {
	case yScaleModeAll:
		minValueDrawn = minAllChannels - 0.02*(maxAllChannels - minAllChannels);
		maxValueDrawn = maxAllChannels + 0.01*(maxAllChannels - minAllChannels);
		minYeventChannel = minYeventChannelAll - (maxYeventChannelAll - minYeventChannelAll) * 0.02;
		maxYeventChannel = maxYeventChannelAll + (maxYeventChannelAll - minYeventChannelAll) * 0.01;
		break;
	case yScaleModeManual:
		minValueDrawn = chart->LeftAxis->Minimum;
		maxValueDrawn = chart->LeftAxis->Maximum;
        minYeventChannel = chart->RightAxis->Minimum;
        maxYeventChannel = chart->RightAxis->Maximum;
		break;
    default : //auto
        minValueDrawn -= (maxValueDrawn - minValueDrawn)*0.02;
        maxValueDrawn += (maxValueDrawn - minValueDrawn)*0.01;
        minYeventChannel -= (maxYeventChannel - minYeventChannel) * 0.02;
        maxYeventChannel += (maxYeventChannel - minYeventChannel) * 0.01;
        break;
	}

    chart->LeftAxis->Title->Caption = leftAxisLabel;
    if(minValueDrawn > maxValueDrawn){
        //if this happened there is a possibility no measurement signal is shown
		minValueDrawn = minYeventChannel;// 0;
        maxValueDrawn = maxYeventChannel; // 1;
        chart->LeftAxis->Title->Caption = rightAxisLabel;
    }

    if(minValueDrawn > maxValueDrawn){ // no signal channel shown, only event channels
		minValueDrawn = minYeventChannel;
        maxValueDrawn = maxYeventChannel;
    }

	chart->LeftAxis->SetMinMax(minValueDrawn, maxValueDrawn);

    chart->RightAxis->Title->Caption = rightAxisLabel;
    if(minYeventChannel > maxYeventChannel){
        //if this happened there is a possibility no measurement signal is shown
        minYeventChannel = minValueDrawn;
        maxYeventChannel = maxValueDrawn;
        chart->RightAxis->Title->Caption = rightAxisLabel;
    }
	if(trueYscale){
		chart->RightAxis->Visible = true;
		chart->RightAxis->SetMinMax(minYeventChannel, maxYeventChannel);
	}else{
		chart->RightAxis->Visible = false;
		chart->RightAxis->SetMinMax(minValueDrawn, maxValueDrawn);
    }


    //always display top and left axis
	TChartSeries *series;
	TPointSeries *newSeries = new TPointSeries(chart);
	series = newSeries;
    series->ShowInLegend = false;
	series->VertAxis = aLeftAxis;
    series->HorizAxis = aTopAxis;
    series->AddXY(chart->TopAxis->Minimum, chart->LeftAxis->Minimum, "", clNone);
    series->AddXY(chart->TopAxis->Maximum, chart->LeftAxis->Maximum, "", clNone);
    series->ParentChart = chart;
    series->XValues->DateTime = true;

    chart->TopAxis->Visible = true;
    chart->TopAxis->DateTimeFormat = "dddddd hh:mm:ss";
    chart->BottomAxis->Visible = true;
    chart->LeftAxis->Visible = true;
    chart->RightAxis->Visible = true;
	chart->LeftAxis->MinorTickCount = 4;
	chart->BottomAxis->MinorTickCount = 4;

	chart->AutoRepaint = setAutoRepaint;
	if(setAutoRepaint) //refresh is refresh is expected, otherwise leave to caller
		chart->Repaint();
}


// deprecated method (moved to FFTForm class):
//draws only the last event channel into the chart; used to draw DFT into the separate window
void CMeasurement::redrawFourierTransform(TChart *chart, int smoothingWidth, bool coherence)
{
	assert(chart);
	assert(numEventChannels > 0);

	chart->RemoveAllSeries();
	CEventChannel DFTCh = eventChannel[getNumEventChannels()-1];

	//draw the channel
	TBarSeries *series = new TBarSeries(chart);
	series->Title = coherence ? gettext("DFT") : gettext("coherence");
	series->Marks->Visible = false;
	series->ParentChart = chart;
	series->SeriesColor = clRed;
	series->BarPen->Color = clRed;

    double yValueNormalization;
    if (!coherence)
    {
    	double average = 0, standardDev = 0;
    	for (int i = 0; i < DFTCh.getNumElems(); i++)
        average += DFTCh[i].value;

		average /= DFTCh.getNumElems();
    	for (int i = 0; i < DFTCh.getNumElems(); i++)
	    	standardDev += (average - DFTCh[i].value) * (average - DFTCh[i].value);

    	standardDev = sqrt(standardDev/DFTCh.getNumElems());
        yValueNormalization = standardDev;
    } else
        yValueNormalization = 1;

	for (int bar = 0; (bar < DFTCh.getNumElems()) && (DFTCh[bar].time <= 0.5); bar++)
		//series->AddXY((double)(bar+1)*2.0/DFTCh.getNumElems(), DFTCh[bar].value/yValueNormalization, "", clTeeColor);
		series->AddXY(DFTCh[bar].time, DFTCh[bar].value/yValueNormalization, "", clTeeColor);

    //series->CustomBarWidth = chart->ChartWidth * 3 / ((1+DFTCh.getNumElems()) * 4);

	if (smoothingWidth >= 1)   // for smoothingWidth = 2, halfWindow=2, -> sum over 5 bars
								// 1/3*b[i-2]+2/3*b[i-1]+b[i]+2/3b[i+1]+1/3b[i+2].
	{
		TFastLineSeries *series = new TFastLineSeries(chart);
		series->Title = coherence ? gettext("DFT by averaging") : gettext("Coherence by averaging");
		series->ParentChart = chart;
		series->LinePen->Color = clBlue;
		series->LinePen->Width = 3;

//        for (bar = 0; bar<smoothingWidth; bar++)   //fill half of Window width elements by unfiltered data
//            series->AddXY((double)bar/(DFTCh.getNumElems()-1)*2.0, DFTCh[bar].value/yValueNormalization, "", clTeeColor);
		double sum;
		for ( int bar = smoothingWidth; (bar < DFTCh.getNumElems()-smoothingWidth)  && (DFTCh[bar].time <= 0.5); bar++)
		{
			sum = DFTCh[bar].value/yValueNormalization;
			for (int avgI = 1; avgI <= smoothingWidth; avgI++)  {
				double w = 1.0 - (double)avgI/(smoothingWidth+1);
				sum += w*DFTCh[bar+avgI].value/yValueNormalization;
				sum += w*DFTCh[bar-avgI].value/yValueNormalization;
			}
			series->AddXY(DFTCh[bar].time, sum/(smoothingWidth+1), "", clTeeColor);
		}
	}

	if (coherence) {
		TFastLineSeries *phiSeries = new TFastLineSeries(chart);
		phiSeries->Title = coherence ? gettext("DFT by averaging") : gettext("Coherence by averaging");
		phiSeries->ParentChart = chart;
		phiSeries->LinePen->Color = clGreen;
		phiSeries->LinePen->Width = 3;
		phiSeries->VertAxis = aRightAxis;

		CEventChannel& PCh = eventChannel[getNumEventChannels()-2];
		for (int bar = 0; (bar < PCh.getNumElems()) && (PCh[bar].time <= 0.5); bar++)
			phiSeries->AddXY(PCh[bar].time, PCh[bar].value*180/M_PI, "", clTeeColor);
	}

	/*
	chart->BottomAxis->Minimum = - 0.99/(DFTCh.getNumElems()-1);
	chart->BottomAxis->Maximum = 0.5 + 0.49/(DFTCh.getNumElems()-1);
	*/
	chart->BottomAxis->Minimum = 0;
	chart->BottomAxis->Maximum = 0.5 + 0.49/(DFTCh.getNumElems()-1);

	chart->LeftAxis->Maximum = coherence ? 1 : 4;
	if (coherence) {
		chart->LeftAxis->Increment = 0.5;
		chart->RightAxis->Visible = true;
	} else {
		chart->RightAxis->Visible = false;
	}
}


CMeasurement::BeatTimesReturn CMeasurement::beatTimesAmForEventChan(int sourceChannelNum, double AMparam, bool automatic, double AMmaxValue) {
	CEventChannel &signal = eventChannel[sourceChannelNum]; //open measurement channel
	int beatChannelNum = addEventChannel("", "s");
	CEventChannel &beat = eventChannel[beatChannelNum]; //create event channel
	beat.setContiguous(false);
	int TminInSamples = 1;                         // for all samples
	int TmaxInSamples = signal.getNumElems();

    {
        beat.setChannelName("RRI from D" + AnsiString(sourceChannelNum+1)+ " with AM");
		double avgValue = 0, maxValue=0;
		for (int i = TminInSamples-1; i < TmaxInSamples; i++) {
			avgValue += signal[i].value;
            if (signal[i].value > maxValue)
                maxValue = signal[i].value;
        }
		avgValue /= signal.getNumElems();

        double odmik;
        if(AMparam < 0){
            UnicodeString settingsFname("nastavitve.ini");
        	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
    		    if (Application->ExeName[i] == '\\') {
                    settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
    	    		break;
                }
        	}
            Ini::File ini(settingsFname.c_str());
            static const wchar_t paramStr1[] = L"AM positive shift";
            ini.loadVar<double>(odmik, paramStr1);
			odmik = myAtof(InstantDialog::query(gettext("Algorithm AM."), gettext("Enter positive shift of the average.")+L" "+gettext("Average:")+L" " + AnsiString(avgValue) + ". "+gettext("Maximum:")+" "+ AnsiString(maxValue) + ".", odmik));
			if (!ini.storeVar(odmik, paramStr1) || !ini.save())
                Application->MessageBox(gettext("Saving error! The selected settings will not be saved!").data(), gettext("Warning").data(), MB_OK);
        }else{
            odmik = AMparam;
        }

   		avgValue += odmik;
   		Event peak;
   		bool waveFound = false;
   		for (int i = TminInSamples-1; i < TmaxInSamples; i++)
    	{
    		if (signal[i].value > avgValue)
            {
                if (!waveFound || (signal[i].value > peak.value))
   				    peak = Event(signal[i].time, signal[i].value);
   				waveFound = true;
   			}
   			else if (waveFound)
			{
                if(AMmaxValue != -1 && peak.value > AMmaxValue)
                {
                    peak.value = 0;
                }else{
    				peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
	    					: (peak.time - beat[beat.getNumElems()-1].time));
                    beat.append(peak);
                }
				waveFound = false;
			}
		}
		beat.setComment("Time of R-waves from " + signal.getChannelName() + " with algorithm AM.\r\n"
            "Average: " + AnsiString(avgValue-odmik) + ".\r\n"
            "Maximum: "+ AnsiString(maxValue)+ ".\r\n"
            "Used shift: " + AnsiString(odmik)+".\r\n");
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
        else  if (beat.getNumElems() == 1 && !automatic)
			Application->MessageBox(gettext("Only one beat found.").data(), gettext("Algorithm AM - Warning!").data() , MB_OK);
        else if(!automatic)
			Application->MessageBox(gettext("No beat found.").data(), gettext("Algorithm AM - Warning!").data() , MB_OK);
    }

    if (beat.getNumElems() == 0)
		deleteEventChannel(beatChannelNum);
    return BeatTimesReturn();
}

CMeasurement::BeatTimesReturn CMeasurement::beatTimesAmForEventChanBeatDetection(int sourceChannelNum, double AMparam, double AMmaxValue) {
	CEventChannel &signal = eventChannel[sourceChannelNum]; //open measurement channel
	int beatChannelNum = addEventChannel("", "s");
	CEventChannel &beat = eventChannel[beatChannelNum]; //create event channel
	beat.setContiguous(true);
	int TminInSamples = 1;                         // for all samples
	int TmaxInSamples = signal.getNumElems();

    {
        // get max and avg values of the input signal
		beat.setChannelName("RRI from D" + AnsiString(sourceChannelNum+1)+ " with AM");
		double avgValue = 0, maxValue=0;
		for (int i = TminInSamples-1; i < TmaxInSamples; i++) {
			avgValue += signal[i].value;
            if (signal[i].value > maxValue)  
                maxValue = signal[i].value;
        }
		avgValue /= signal.getNumElems();

        // get the threshold parameter
        double odmik;
        if(AMparam < 0){
            UnicodeString settingsFname("nastavitve.ini");
        	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
    		    if (Application->ExeName[i] == '\\') {
                    settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
    	    		break;
                }
        	}
            Ini::File ini(settingsFname.c_str());
			static const wchar_t paramStr1[] = L"AM positive shift";
            ini.loadVar<double>(odmik, paramStr1);
            odmik = myAtof(InstantDialog::query("Algorithm AM.", "Enter a positive deviation from the average. \r\nAverage: " + AnsiString(avgValue) + ".\r\nMaximum: "+ AnsiString(maxValue) + ".", odmik));
            if (!ini.storeVar(odmik, paramStr1) || !ini.save())
				Application->MessageBox(gettext("Saving error!The selected settings will not be saved!").data(), gettext("Warning").data(), MB_OK);
        }else{
            odmik = AMparam;
        }

        // search for peaks that extend beyond the threshold (form a continuous set of samples that extend beyond the threshold, only the highest will be registered as peak)
   		avgValue += odmik;
   		Event peak;
   		bool waveFound = false;
   		for (int i = TminInSamples-1; i < TmaxInSamples; i++) {
    		if (signal[i].value > avgValue) {
                if (!waveFound || signal[i].value > peak.value)
                    peak = Event(signal[i].time, signal[i].value);
   				waveFound = true;
   			} else if (waveFound) {
				peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
						: (peak.time - beat[beat.getNumElems()-1].time));
				if(AMmaxValue == -1 || AMmaxValue > peak.value)
                    beat.append(peak);
				waveFound = false;
			}
		}
		beat.setComment("Time of R-waves from " + signal.getChannelName() + " algorithm AM.\r\n"
            "Average: " + AnsiString(avgValue-odmik) + ".\r\n"
            "Maximum: "+ AnsiString(maxValue)+ ".\r\n"
            "Used offset: " + AnsiString(odmik)+".\r\n");
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
    }
    return BeatTimesReturn();
}


CMeasurement::BeatTimesReturn CMeasurement::calculateBeatTimesBeatDetection(const BeatTimesParams& params, double AMparam, double AMmaxValue)
{
	int sourceChannelNum = params.sourceChannelNum;
	CChannel &signal = channel[sourceChannelNum]; //open measurement channel
	int beatChannelNum = addEventChannel("", "s");
	CEventChannel &beat = eventChannel[beatChannelNum]; //create event channel
	beat.setContiguous(false);
	int TminInSamples = 1;                         // for all samples
	int TmaxInSamples = signal.getNumElems();

//**************** BEAT RATE DETECTORS ************

	switch (params.algorithm) {

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	case beatAlgorithmAM:
	{  // AM - simple positive R peak detector based on treshold value and maximal amplitude
		beat.setChannelName("RRI from M" + AnsiString(sourceChannelNum+1)+ " and AM");
		double avgValue = 0, maxValue=0;
		for (int i = TminInSamples-1; i < TmaxInSamples; i++) {
			avgValue += signal[i];
            if (signal[i]>maxValue)  maxValue = signal[i];
        }
		avgValue /= TmaxInSamples;
        double odmik;
        if(AMparam < 0){
			UnicodeString settingsFname("nastavitve.ini");
        	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
    		    if (Application->ExeName[i] == '\\') {
                    settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
    	    		break;
                }
        	}
            Ini::File ini(settingsFname.c_str());
			static wchar_t* paramStr1 = gettext("AM positive shift").data();
            ini.loadVar<double>(odmik, paramStr1);
			odmik = myAtof(InstantDialog::query(gettext("Algorithm AM.").data(),
				gettext("Enter positive offset from average.")+" \r\n"+
				gettext("Average")+": " + AnsiString(avgValue)+".\r\n"+
				gettext("Maximum")+": " + AnsiString(maxValue) + ".", AnsiString(odmik)));
				if (!ini.storeVar(odmik, paramStr1) || !ini.save())
					Application->MessageBox(gettext("Error storing settings. The selected settings will not be saved!").data(), gettext("Warning").data(), MB_OK);
		}
        else{
            odmik = AMparam;
        }
		avgValue += odmik;
		Event peak;
		bool waveFound = false;
   		for (int i = TminInSamples-1; i < TmaxInSamples; i++)
		{
			if (signal[i] > avgValue)
			{
				if (!waveFound || signal[i] > peak.value)
					peak = Event(sampleIndex2Time(i), signal[i]);
				waveFound = true;
			}
			else if (waveFound)
			{
				peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
						: (peak.time - beat[beat.getNumElems()-1].time));
                if(AMmaxValue == -1 || AMmaxValue > peak.value)
                    beat.append(peak);
				waveFound = false;
			}
		}
		beat.setComment("Time of R-waves from " + signal.getChannelName() + " with algorithm AM.\r\nAverage: " + AnsiString(avgValue-odmik) + ".\r\nMaximum: "+ AnsiString(maxValue)+ ".\r\nUsed offset: " + AnsiString(odmik)+".\r\n");
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
		break; }
	default:
		throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: algorithm not supported");
	}
	return BeatTimesReturn();
}

// warning! only FCOREL supports the new syntax, other algorithms are unchanged (return void,
// take params through dialogs not params variable, and return errors through message boxes)
CMeasurement::BeatTimesReturn CMeasurement::calculateBeatTimes(const BeatTimesParams& params, double AMparam, bool automatic, double AMmaxValue)
{
	int sourceChannelNum = params.sourceChannelNum;
	CChannel &signal = channel[sourceChannelNum]; //open measurement channel
	int beatChannelNum = addEventChannel("", "s");
	CEventChannel &beat = eventChannel[beatChannelNum]; //create event channel
	beat.setContiguous(false);
	int TminInSamples = 1;                         // for all samples
	int TmaxInSamples = signal.getNumElems();

//**************** BEAT RATE DETECTORS ************

	switch (params.algorithm) {

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	case beatAlgorithmAM:
	{  // AM - simple positive R peak detector based on treshold value and maximal amplitude
		beat.setChannelName("RRI from M" + AnsiString(sourceChannelNum+1)+ " with AM");
		double avgValue = 0, maxValue=0;
		for (int i = TminInSamples-1; i < TmaxInSamples; i++) {
			avgValue += signal[i];
            if (signal[i]>maxValue)  maxValue = signal[i];
        }
		avgValue /= TmaxInSamples;
        double odmik;
        if(AMparam < 0){
			UnicodeString settingsFname("nastavitve.ini");
        	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
    		    if (Application->ExeName[i] == '\\') {
                    settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
    	    		break;
                }
        	}
            Ini::File ini(settingsFname.c_str());
				static const wchar_t paramStr1[] = L"AM positive shift";
            ini.loadVar<double>(odmik, paramStr1);
			odmik = myAtof(InstantDialog::query(gettext("Algorithm AM."), gettext("Enter positive offset from average.")+" \r\n"+gettext("Average:")+" " + AnsiString(avgValue) + ".\r\nMaximum: "+ AnsiString(maxValue) + ".", odmik));
                if (!ini.storeVar(odmik, paramStr1) || !ini.save())
					Application->MessageBox(gettext("Error storing settings. The selected settings will not be saved!").data(), gettext("Warning").data(), MB_OK);
        }
        else{
            odmik = AMparam;
        }
		avgValue += odmik;
		Event peak;
		bool waveFound = false;
   		for (int i = TminInSamples-1; i < TmaxInSamples; i++)
		{
			if (signal[i] > avgValue)
			{
				if (!waveFound || signal[i] > peak.value)
					peak = Event(sampleIndex2Time(i), signal[i]);
				waveFound = true;
			}
			else if (waveFound)
			{
				peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
						: (peak.time - beat[beat.getNumElems()-1].time));
				if(AMmaxValue == -1 || AMmaxValue > peak.value)
                    beat.append(peak);
				waveFound = false;
			}
		}
		beat.setComment("Time of R-waves from " + signal.getChannelName() + " with algorithm AM.\r\nAverage: " + AnsiString(avgValue-odmik) + ".\r\nMaximum: "+ AnsiString(maxValue)+ ".\r\nUsed offset: " + AnsiString(odmik)+".\r\n");
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
        else  if (beat.getNumElems() == 1 && !automatic)
			Application->MessageBox(gettext("Found only one beat.").data(), gettext("Algorithm AM - Warning!").data() , MB_OK);
        else if(!automatic)
			Application->MessageBox(gettext("No beats found.").data(), gettext("Algorithm AM - Warning!").data() , MB_OK);
		break; }

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	case beatAlgorithmAF: {           // AF - positive R peak detector based on derivative and amplitude
		beat.setChannelName("RRI from M" + AnsiString(sourceChannelNum+1) + " with AF");
		Event peak;
		#define	dt_af1		0.3			// treshold of maximal and minimal derivative values
		#define	at_af1		0.3			// treshold of maximal amplitude value
		#define	time_af1	0.05 		// search interval in seconds

		int interval = int(time_af1 * getSamplingRate());  // number of samples in search interval
		if (interval < 6)
		{
			Application->MessageBox(gettext("Min 6 samples for AF algorithm in R wave.").data(), gettext("Algorithm AF - Warning!").data(), MB_OK); //izpis sporocila na ekran
			break;
		}

		// temporary channel for derivative
		int derivativeChNum = addChannel("First derivate " +  signal.getChannelName(), signal.getMeasurementUnit() + " s^-1");
		CChannel &derived = channel[derivativeChNum];

		// first derivative is invalid and set to 0
		derived[0]=0;

		for (int i = TminInSamples; i < TmaxInSamples; i++)
			derived[i]= signal[i]-signal[i-1];
		derived.refreshMinMaxValue();
		double amplitueTresh = at_af1 * signal.getMaxValue();
		double posDerivativeTresh = dt_af1 * derived.getMaxValue();
		double negDerivativeTresh = dt_af1 * derived.getMinValue();

		// AF algorithm  (3 derivatives > posDerivativeTresh) & (2 derivatives < negDerivativeTresh) & (amplitude > amplitueTresh)
		//                 in the search interval.

		for (int i = TminInSamples; i < TmaxInSamples; i++)
		{   // search for 3 consecutive derivatives > posDerivativeTresh
			if ((derived[i] > posDerivativeTresh) && (derived[i+1] > posDerivativeTresh) && (derived[i+2] > posDerivativeTresh))
			{    //search for 2 consecutive derivatives < negDerivativeTresh and amplitude > amplitueTresh within search interval
				double MaxAmplitude = signal[i];
				int RelIndex = 0;
				for (int s = 0; s <= interval; s++)
				{
					if (signal[i+s] > amplitueTresh)
					{
						if (signal[i+s] > MaxAmplitude)  { // save in the same time the maximal amplitude on the interval
							RelIndex = i+s;
							MaxAmplitude = signal[RelIndex];
						}
						if ((derived[i+s] < negDerivativeTresh) && (derived[i+s+1] < negDerivativeTresh))
						{
							peak = Event(sampleIndex2Time(RelIndex), MaxAmplitude);
							peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
								: (peak.time - beat[beat.getNumElems()-1].time));
                            beat.append(peak);
 				            i = time2SampleIndex(sampleIndex2Time(i)+ time_af1); // jump over analized period
                            s=interval;  // because this search was successful - finish for
  				        }
					}else {
						i=i+s;    // ??? ali je tako OK? because the amplitude is too small at s
                        s=interval;  // because this search was not successful - finish for
                    }
                }
			}
		}
		beat.setComment("Time for R-waves from " + signal.getChannelName() + " with algorithm AF.\r\nLimit of positive derivate: " + AnsiString(posDerivativeTresh) + ".\r\nLimit of negative derivate: " + AnsiString(negDerivativeTresh)+".\r\nAmplitude limit: " + AnsiString(amplitueTresh)+".\r\n");
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
		else  if (beat.getNumElems() == 1)
			Application->MessageBox(gettext("Found only one beat.").data(), gettext("Algorithm AF - Warning!").data() , MB_OK);
		else
			Application->MessageBox(gettext("No beat found.").data(), gettext("Algorithm AF - Warning!").data(), MB_OK);

		deleteChannel(derivativeChNum);
		break; }

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	case beatAlgorithmFZ: { // FZ - positive R peak detector based on derivative
    	beat.setChannelName("RRI from M" + AnsiString(sourceChannelNum+1)+ " with FZ");
		Event peak;
        #define	dt_fz		0.3			// treshold of maximal and minimal derivative values
 	    #define	time_af1	0.05 		// search interval in seconds

		int interval = int(time_af1 * getSamplingRate());  // number of samples in search interval
        if (interval < 7)
        {
			Application->MessageBox(gettext("Min 7 samples for FZ algorithm in R wave.").data(), gettext("Algorithm FZ - Warning!").data(), MB_OK); //izpis sporocila na ekran
			break;
        }

        // temporary channel for derivative
		int derivativeChNum = addChannel("First derivate " +  signal.getChannelName(), signal.getMeasurementUnit() + " s^-1");
		CChannel &derived = channel[derivativeChNum];

		// first derivative is invalid and set to 0
        derived[0]=0;

        for (int i = TminInSamples; i < TmaxInSamples; i++)
            derived[i]= signal[i]-signal[i-1];

        CChannel sortedderived = derived; //make a copy
        sortedderived.sort();   // sort
        double negDerivativeTresh = dt_fz * sortedderived[(int)(sortedderived.getNumElems()/100)]; // take lower values within 1%
 		double posDerivativeTresh = dt_fz * sortedderived[(int)(sortedderived.getNumElems()-(sortedderived.getNumElems()/100))]; // take upper values within 1%

        // FZ algorithm  (4 derivatives > posDerivativeTresh) & (4 derivatives < negDerivativeTresh)
        //                 in the search interval.
		int s;
		for (int i = TminInSamples; i < TmaxInSamples; i++)
        {   // search for 4 consecutive derivatives > posDerivativeTresh
            if ((derived[i] > posDerivativeTresh) && (derived[i+1] > posDerivativeTresh) &&
                                (derived[i+2] > posDerivativeTresh) && (derived[i+3] > posDerivativeTresh))
            {    //search for 4 consecutive derivatives < negDerivativeTresh
                double MaxAmplitude = signal[i];
                int RelIndex = 0;
                for (s = 0; s <= interval; s++)
                {
                    if (signal[i+s] > MaxAmplitude)  { // save in the same time the maximal amplitude on the interval
                       RelIndex = i+s;
                       MaxAmplitude = signal[RelIndex];
                   }
                   if ((derived[i+s] < negDerivativeTresh) && (derived[i+s+1] < negDerivativeTresh) &&
                                        (derived[i+s+2] < negDerivativeTresh) && (derived[i+s+3] < negDerivativeTresh))
                   {
						peak = Event(sampleIndex2Time(RelIndex), MaxAmplitude);
                        peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
								: (peak.time - beat[beat.getNumElems()-1].time));
						beat.append(peak);
						i = time2SampleIndex(sampleIndex2Time(i)+ time_af1); // jump over analized period
						s=interval;  // because this search was successful - finish for
				   }
				}
			}
		}
		beat.setComment("Time for R-waves from " + signal.getChannelName() + " with algorithm FZ.\r\nLimit of positive derivate: " + AnsiString(posDerivativeTresh) + ".\r\nLimit of negative derivate: " + AnsiString(negDerivativeTresh)+".\r\n");
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
		else  if (beat.getNumElems() == 1)
			Application->MessageBox(gettext("Found only one beat.").data(), gettext("Algorithm FZ - Warning!").data() , MB_OK);
		else
			Application->MessageBox(gettext("No beat found.").data(), gettext("Algorithm FZ - Warning!").data() , MB_OK);

		deleteChannel(derivativeChNum);
		break; }
    
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	case beatAlgorithmFX: { // FX - R peak detector based on comparision of prespecified and average derivative
		if (params.cursorPos->c[0] < 0 || params.cursorPos->c[1] < 0 || params.cursorPos->c[2] < 0)
		{
			Application->MessageBox(gettext("All three cursors need to be set for FX algorithm.").data(), gettext("Algorithm FX - Error!").data(), MB_OK); //izpis sporocila na ekran
			break;
		}

		beat.setChannelName("RRI from M" + AnsiString(sourceChannelNum+1)+ " with FX");
		Event peak;
		#define dt_fx       0.8     // treshold of maximal and minimal derivative values
		#define sam_fx      0.95     // number of averaged samples treshold

        // temporary channel for derivative
		int derivativeChNum = addChannel("First derivate " +  signal.getChannelName(), signal.getMeasurementUnit() + " s^-1");
		CChannel &derived = channel[derivativeChNum];

        // first derivative is invalid and set to 0
        derived[0]=0;

        for (int i = TminInSamples; i < TmaxInSamples; i++)
            derived[i]= signal[i]-signal[i-1];

		// FX algorithm  ("inter" derivatives > posDerivativeTresh) & ("inter" derivatives < negDerivativeTresh)
        //                 in the search interval between left and right cursor.

      	int  c1x, c2x, c3x; // cursor positions in sampels
		double  slope1, slope2, sum;

		c1x = time2SampleIndex(params.cursorPos->c[0]);
		c2x = time2SampleIndex(params.cursorPos->c[1]);
		c3x = time2SampleIndex(params.cursorPos->c[2]);

		int width1 = c2x-c1x;        // in samples
		int width2 = c3x-c2x;        // in samples
		slope1 = (signal[c2x] - signal[c1x])/width1;	  // leading slope
		slope2 = (signal[c3x] - signal[c2x])/width2;	  // trailing slope
		if (width1+width2 < 6)
        {
			Application->MessageBox(gettext("Min 6 samples for FX algorithm in R wave.").data(), gettext("Algorithm FX - Warning!").data(), MB_OK); //izpis sporocila na ekran
			break;
		}
		if ((width1 || width2 || slope1 || slope2) == 0)
			Application->MessageBox(gettext("Cursors not set correctly!").data() , gettext("Algorithm FX - Warning!").data(), MB_OK); //izpis sporocila na ekran

        double leadDerivativeTresh = slope1 * dt_fx;
		double trailDerivativeTresh = slope2 * dt_fx;
        int inter1 = int(width1 * sam_fx);
        int inter2 = int(width2 * sam_fx);

		for (int i = TminInSamples; i < TmaxInSamples; i++)
        {
            double MaxAmplitude = signal[i];
            int RelIndex = 0;
		    if (leadDerivativeTresh > 0)  // if leading derivative is positive
            {
                sum=0;
                for (int s=0; s < inter1; s++)
					sum += derived[i+s];
                sum = sum / inter1;     // appropriate average leading derivative
                if (sum > leadDerivativeTresh) // found
                {
                    sum=0;
                    for (int s = inter1; s < inter1 + inter2 ; s++)
                        sum += derived[i+s];
                    sum = sum / inter2;     // appropriate average trailin derivative
                    if (sum < trailDerivativeTresh) // found
                    {
                        for (int s = 0; s < inter1 + inter2 ; s++)
                        {
                            if (signal[i+s] > MaxAmplitude)  { // test and save in the same time the maximal amplitude on the interval
                            RelIndex = i+ s;
                            MaxAmplitude = signal[RelIndex];
                            }
                        }
                        peak = Event(sampleIndex2Time(RelIndex), MaxAmplitude);
                        peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
								: (peak.time - beat[beat.getNumElems()-1].time));
						beat.append(peak);
 				        i = time2SampleIndex(sampleIndex2Time(i))+ width1 + width2; // jump over analized period
					}
                }
            }
            else if (leadDerivativeTresh < 0)  // if leading derivative is negative
                {
                    sum=0;
                    for (int s=0; s < inter1; s++)
						sum += derived[i+s];
                    sum = sum / inter1;     // appropriate average leading derivative
                    if (sum < leadDerivativeTresh) // found
                    {
                        sum=0;
                        for (int s = inter1; s < inter1+inter2 ; s++)
                            sum += derived[i+s];
                        sum = sum / inter2;     // appropriate average trailin derivative
                        if (sum > trailDerivativeTresh) // found
						{
                            for (int s = 0; s < inter1 + inter2 ; s++)
                            {
                                if (signal[i+s] < MaxAmplitude)  { // test and save in the same time the maximal amplitude on the interval
								RelIndex = i+ s;
                                MaxAmplitude = signal[RelIndex];   // in fact minimal amplitude
                                }
                            }
                            peak = Event(sampleIndex2Time(RelIndex), MaxAmplitude);
                            peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
									: (peak.time - beat[beat.getNumElems()-1].time));
                            beat.append(peak);
 				            i = time2SampleIndex(sampleIndex2Time(i))+ width1 + width2; // jump over analized period
                        }
					}
            }
			else {
				Application->MessageBox(gettext("Warning").data(),gettext("Cursors not set correctly!").data() , MB_OK); //izpis sporocila na ekran
                break;
			}
        }
		beat.setComment("Time for R-waves from " + signal.getChannelName() + " with algorithm FX.\r\nTime of left cursor: " + AnsiString(sampleIndex2Time(c1x)) + ".\r\nTime of middle cursor: " + AnsiString(sampleIndex2Time(c2x))+".\r\nTime of right cursor: " + AnsiString(sampleIndex2Time(c3x))+".\r\n");
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
        else  if (beat.getNumElems() == 1)
			Application->MessageBox(gettext("Only one beat found.").data(), gettext("Algorithm FX - Warning!").data() , MB_OK);
		else
			Application->MessageBox(gettext("No beat found.").data(), gettext("Algorithm FX - Warning!").data() , MB_OK);

        deleteChannel(derivativeChNum);
		break; }
    
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	case beatAlgorithmFCOREL: {           // FCOREL - Wave peak detector based on correlation
		assert(params.cursorPos);
		assert(params.cursorPos->c[0] >= 0 && params.cursorPos->c[1] >= 0);

		Event peak;

		// signal channel for corelation
		int correlationChNum = addChannel("Correlation M" + AnsiString(sourceChannelNum+1) +" with samples", "");
		CChannel &correlation = channel[correlationChNum];

		int  c1x, c2x; // cursor positions in samples
		double *pattern = NULL;

		c1x = time2SampleIndex(params.cursorPos->c[0]);
		c2x = time2SampleIndex(params.cursorPos->c[1]);

		// FCOREL algorithm  (maximal correlation between template and signal).
		int width = c2x-c1x;

		// Copy selected template into a separate buffer
		if (pattern != NULL) delete pattern;
		pattern = new double[width];
		for (int j=0; j < width; j++)
			pattern[j] = signal[c1x+j];

		// Calculate correlation
		for (int i=0; i < TmaxInSamples - width; i++)  {
			double sumX = 0, sumY = 0, sumXY = 0, sumXX = 0, sumYY = 0;
			for (int j = 0; j < width; j++)
			{
				sumX += signal[i+j];
				sumY += pattern[j];
				sumXY += signal[i+j] * pattern[j];
				sumXX += signal[i+j]*signal[i+j];
				sumYY += pattern[j]*pattern[j];
			}
			correlation[i] = (sumXY - sumX*sumY/width)
				/ sqrt( fabs((sumXX - sumX*sumX/width) * (sumYY - sumY*sumY/width)));
		}
		for (int j=0; j < width; j++) 		   // fill last width points with the last correlation
			correlation[TmaxInSamples - width + j] = correlation[TmaxInSamples - width - 1];

		correlation.setComment("Correlation of signala " + signal.getChannelName() + " with samples.\r\nTime of the left cusrsor of sampled signal: " + AnsiString(sampleIndex2Time(c1x))+".\r\nTime f right cursor of smapled signal: " + AnsiString(sampleIndex2Time(c2x))+".\r\n");

		/*
		char buff[1000];
		sprintf(buff, "Vnesi �tevilko dogodkovnega kanala, ki ga bo� uporabil za dolo�itev utripov po FCOREL (%d-%d)\r\nVnesi 0, �e dolo�a� brez predhodno dolo�enih utripov:", 0, getNumEventChannels()-1);
		int existBeatNum = InstantDialog::query("Postopek FCOREL.", buff, "0").ToInt()-1;
		if (existBeatNum > getNumEventChannels()-2)   {
			Application->MessageBox("Ta kanal ne obstaja.", "Algorithm FCOREL - Warning!" , MB_OK);
			deleteChannel(correlationChNum);
			break;
		}
		*/

		if (params.existBeatNum < 0)
		{    // find number of peaks with the correlation greater than specified value - around 0.9
			beat.setChannelName("PPI from M" + AnsiString(sourceChannelNum+1)+ " with FCOREL");
			double avgValue = 0;
			// some more help should be given to the user here , avg is not a good solution ????
			for (int i = TminInSamples-1; i < TmaxInSamples; i++)
				avgValue += correlation[i];
			avgValue /= TmaxInSamples;
			double odmik = params.minCorel;
			Event peak;
			bool waveFound = false;
			for (int i = TminInSamples-1; i < TmaxInSamples; i++)
			{
				if (correlation[i] > odmik)
				{
					if (!waveFound || correlation[i] > peak.value)
						peak = Event(sampleIndex2Time(i), correlation[i]);
					waveFound = true;
				}
				else if (waveFound)
				{
					peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
							: (peak.time - beat[beat.getNumElems()-1].time));
					beat.append(peak);
					waveFound = false;
				}
			}
			beat.setComment("Peak times from " + signal.getChannelName() + " with algorithm FCOREL.\r\nAverage: " + AnsiString(avgValue) + ".\r\nUsed offset: " + AnsiString(odmik)+".\r\nTime of left cursor in sampled signal: " + AnsiString(sampleIndex2Time(c1x))+".\r\nTime of right cursor in sampled signal: " + AnsiString(sampleIndex2Time(c2x))+".\r\n");

		} else {    // find the maximal correlation in the already defined RR interval
			CEventChannel &existBeat = eventChannel[params.existBeatNum];

			beat.setChannelName("Max.cor.M" + AnsiString(sourceChannelNum+1)+" v int.D"+ AnsiString(params.existBeatNum+1)+ " with FCOREL");

			//for each beat, find time & value of maximal correlation
			//between this and next beat; add event to beat channel
        	for (int beatNo = 0; beatNo < existBeat.getNumElems()-1; beatNo++)
        	{
        		Event  maxCorr(-1, -1e+100);
        		int startSample = time2SampleIndex(existBeat[beatNo].time);
				if (sampleIndex2Time(startSample) < existBeat[beatNo].time)
					startSample++;
				for (int sample = startSample; sampleIndex2Time(sample) < existBeat[beatNo+1].time; sample++)
				{
					if (correlation[sample] > maxCorr.value)
						maxCorr = Event(sampleIndex2Time(sample), correlation[sample]);
				}
				beat.append(maxCorr);
			}

			beat.setComment("Max correlation of the channel " + signal.getChannelName() + " with algorithm FCOREL \r\nat intervals specified by the event channel "+existBeat.getChannelName()+".\r\nTime of the left curosr of the sampled signal: " + AnsiString(sampleIndex2Time(c1x))+".\r\nTime of the right cursor of the sampled signal: " + AnsiString(sampleIndex2Time(c2x))+".\r\n");
		}
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
		else  if (beat.getNumElems() == 1)
			Application->MessageBox(gettext("Only one beat found.").data(), gettext("Algorithm FCOREL - Warning!").data() , MB_OK);
		else
			Application->MessageBox(gettext("No beat found.").data(), gettext("Algorithm FCOREL - Warning!").data() , MB_OK);

		// deleteChannel(correlationChNum);
		break; }
    
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	case beatAlgorithmFRMS: { // FRMS - - Wave peak detector based on template minimal RMS error
		if (params.cursorPos->c[0] < 0 || params.cursorPos->c[1] < 0)
		{
			Application->MessageBox(gettext("The blue and green cursors to be set for FRMS algorithm.").data(), gettext("Algorithm FRMS - Error!").data(), MB_OK); //izpis sporocila na ekran
			break;
		}

		Event peak;

		// temporary channel for RMS error
		int RMSerrorChNum = addChannel("RMSe M" +  AnsiString(sourceChannelNum+1) +" and sample", "");
		CChannel &RMSerror = channel[RMSerrorChNum];

      	int  c1x, c2x; // cursor positions in sampels
		double  *pattern = NULL;

		c1x = time2SampleIndex(params.cursorPos->c[0]);
		c2x = time2SampleIndex(params.cursorPos->c[1]);

		// FRMS algorithm  (minimal RMS error between template and signal).
		int width = c2x-c1x;
		double time_int = (2* width) / getSamplingRate();  // search interval in time

	    // Copy selected template into a separate buffer
		if (pattern != NULL) delete pattern;
		pattern = new double[width];
		for (int j=0; j < width; j++)
			pattern[j] = signal[c1x+j];

		// Calculate RMS error
		for (int i=0; i < TmaxInSamples - width; i++){
			for (int j=0; j < width; j++){
				if(signal[i+j] == signal.getMinValue()){
					RMSerror[i] = 1;
					break;
				}
				RMSerror[i] += (signal[i+j]-pattern[j]) * (signal[i+j]-pattern[j]);
			}
		}
		for (int j=0; j < width; j++) 		   // fill last width points with the last RMS error
			RMSerror[TmaxInSamples - width + j] = RMSerror[TmaxInSamples - width - 1];
        RMSerror.setComment("RMS error in sample channel " + signal.getChannelName() + ". Left cursor position : " + AnsiString(sampleIndex2Time(c1x))+". Right cursor position: " + AnsiString(sampleIndex2Time(c2x))+".\n");

		wchar_t buff[1000];
		swprintf(buff, gettext("Enter the event channel number to determine the beats with FRMS (%d-%d). Enter 0, if you want no prior beats:").data(), 0, getNumEventChannels()-1);
		int existBeatNum = InstantDialog::query(gettext("Postopek FRMS."), buff, "0").ToInt()-1;
        if (existBeatNum > getNumEventChannels()-2)   {
			Application->MessageBox(gettext("This channel does not exist.").data(), gettext("Algorithm FRMS - Warning!").data() , MB_OK);
            deleteChannel(RMSerrorChNum);
            break;
        }

        if (existBeatNum < 0)
		{   // find number of peaks with the algorithm similar to FZ, without use of RR intervals
        	beat.setChannelName("PPI from M" + AnsiString(sourceChannelNum+1) + " with FRMS");
            #define	dt_RMS   0.3	   	// derivative treshold

            // temporary channel for derivative
			int derivativeRMSChNum = addChannel("First derivate " +  RMSerror.getChannelName(), RMSerror.getMeasurementUnit() + " s^-1");
			CChannel &derivedRMS = channel[derivativeRMSChNum];

            derivedRMS [0]=0;   // first derivative is invalid and set to 0

		   for (int i = TminInSamples; i < TmaxInSamples; i++)
					derivedRMS[i]= RMSerror[i]-RMSerror[i-1];

			CChannel sortedderivedRMS = derivedRMS; //make a copy
			sortedderivedRMS.sort();   // sort
			double negDerivativeTresh = dt_RMS * sortedderivedRMS[(int)(sortedderivedRMS.getNumElems()/100)]; // take lower values within 1%
			double posDerivativeTresh = dt_RMS * sortedderivedRMS[(int)(sortedderivedRMS.getNumElems()-(sortedderivedRMS.getNumElems()/100))]; // take upper values within 1%

            int s;
			for (int i = TminInSamples; i < TmaxInSamples-width; i++)
            {   // search for 4 consecutive derivatives < negDerivativeTresh
				if ((derivedRMS[i] < negDerivativeTresh) && (derivedRMS[i+1] < negDerivativeTresh) &&
                                   (derivedRMS[i+2] < negDerivativeTresh) && (derivedRMS[i+3] < negDerivativeTresh))
                {    //search for 4 consecutive derivatives  > posDerivativeTresh
                    double MinAmplitude = RMSerror[i];
                    int RelIndex = 0;
                    for (s = 0; s <= width; s++)
                    {
                      if (RMSerror[i+s] < MinAmplitude)  { // save in the same time the minimal error on the interval
                         RelIndex = i+s;
                         MinAmplitude = RMSerror[RelIndex];
                      }
					  if ((derivedRMS[i+s] > posDerivativeTresh) && (derivedRMS[i+s+1] > posDerivativeTresh) &&
                                        (derivedRMS[i+s+2] > posDerivativeTresh) && (derivedRMS[i+s+3] > posDerivativeTresh))
                      {
						peak = Event(sampleIndex2Time(RelIndex), MinAmplitude);
                        peak.value = (beat.getNumElems() == 0 ? 0 // first beat rate invalid
								: (peak.time - beat[beat.getNumElems()-1].time));
                        beat.append(peak);
 				        i = time2SampleIndex(sampleIndex2Time(i)+ time_int); // jump over analized period
                        s=width;  // because this search was successful - finish for
                      }
                    }
                }
            beat.setComment("Peak times from signal channel " + signal.getChannelName() + " with algorithm FRMS. Limit of positive derivate: " + AnsiString(posDerivativeTresh) + ". Limit of negative derivate: " + AnsiString(negDerivativeTresh)+". Position of the left cursor: " + AnsiString(sampleIndex2Time(c1x))+". Position of the right cursor: " + AnsiString(sampleIndex2Time(c2x))+".\n");
            }
            deleteChannel(derivativeRMSChNum);

	    } else {    // find the minimal RMS error in the first half of beats, use of RR intervals

		    CEventChannel &existBeat = eventChannel[existBeatNum];

        	beat.setChannelName("Min.RMSe M" + AnsiString(sourceChannelNum+1)+" in int.D"+ AnsiString(existBeatNum+1)+ " with FRMS");

        	//for each beat, find time & value of maximal correlation
        	//between this and next beat; add event to beat channel
        	for (int beatNo = 0; beatNo < existBeat.getNumElems()-1; beatNo++)
        	{
        		Event  minRMS(-1, 1e+100);
        		int startSample = time2SampleIndex(existBeat[beatNo].time);
        		if (sampleIndex2Time(startSample) < existBeat[beatNo].time)
        			startSample++;
        		for (int sample = startSample; sampleIndex2Time(sample) < existBeat[beatNo+1].time; sample++)
        		{
        			if (RMSerror[sample] < minRMS.value)
        				minRMS = Event(sampleIndex2Time(sample), RMSerror[sample]);
        		}
        		beat.append(minRMS);
        	}

            beat.setComment("Min RMS error in signal channel " + signal.getChannelName() + " with algorithm FRMS at intervals, which determine the event channel "+existBeat.getChannelName()+". Position of the left cursor: " + AnsiString(sampleIndex2Time(c1x))+". Position of the right cursor: " + AnsiString(sampleIndex2Time(c2x))+". \n");

		}
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;  // fill first data
        else  if (beat.getNumElems() == 1)
			Application->MessageBox(gettext("Only one beat found.").data(), gettext("Algorithm FRMS - Warning!").data() , MB_OK);
		else
			Application->MessageBox(gettext("No beat found.").data(), gettext("Algorithm FRMS - Warning!").data() , MB_OK);

		break; }
		
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	default:
		throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: algorithm not supported");
	}
	// deleteChannel(RMSerrorChNum);
	if (beat.getNumElems() == 0)
		deleteEventChannel(beatChannelNum);

	return BeatTimesReturn();
}

void CMeasurement::lowPassFilter(DSPWindow windowType, int sourceChannelNum, double cutoffFreq, double errorValue)
{
	CChannel &signal = channel[sourceChannelNum]; //open measurement channel
	int filteredChNum = addChannel("LPF", signal.getMeasurementUnit());
	CChannel &filtered = channel[filteredChNum];
    filtered.tenBitSaving = false;
	int TminInSamples = 1;                         // for all samples
	int TmaxInSamples = signal.getNumElems();

	switch (windowType) {
	case windowRectangular: { // Low pass filter using rectangular window
		int filtHalfWindow = (int)(getSamplingRate()/ cutoffFreq/2); // the half width of filter window
		if (TmaxInSamples < (filtHalfWindow*2+1))
			throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Window wider than signal - use higher frequency.");
		if (filtHalfWindow == 0)
			throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Window width 0 - use lower frequency.");

		filtered.setChannelName("LPFr"+AnsiString(cutoffFreq)+" from M" +AnsiString(sourceChannelNum+1));  // zgradi ime novega kanala
		filtered.setComment("Lowpass filter " + AnsiString(cutoffFreq)+"Hz" +" channel "+signal.getChannelName()+". Used window width "+AnsiString(filtHalfWindow*2) +".\n" );  // zgradi ime novega kanala

		int i;
		for (i = 0; i<filtHalfWindow; i++)
		filtered[TminInSamples-1+i] = signal[TminInSamples-1+i];   //fill first filtHalfWindow elements by unfiltered data

		for (  ; i < TmaxInSamples-filtHalfWindow; i++)
		{
            filtered[i] = signal[i];
            for (int j = 1; j<=filtHalfWindow; j++)
            {
				filtered[i] += signal[i-j];
                filtered[i] += signal[i+j];
            }
			filtered[i] = filtered[i] / (2*filtHalfWindow +1);
		}

        for ( ; i < TmaxInSamples; i++)
		    filtered[i] = signal[i];   //fill last filtHalfWindow elements by unfiltered data
		break; }
	case windowTriangular: {      // Low pass filter using triangular window
		int filtHalfWindow = (int)(getSamplingRate()/ cutoffFreq/2); // the half width of filter window
        if (TmaxInSamples < (filtHalfWindow*2+1))
            throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Window wider than signal - use higher frequency.");
        if (filtHalfWindow == 0)
            throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Window width 0 - use lower frequency.");

		filtered.setChannelName("LPFt"+AnsiString(cutoffFreq)+" from M" +AnsiString(sourceChannelNum+1));  // zgradi ime novega kanala
		filtered.setComment("Low pass filter " + AnsiString(cutoffFreq)+"Hz" +" channel "+signal.getChannelName()+". Used triangular window width "+AnsiString(filtHalfWindow*2) +".\n" );  // zgradi ime novega kanala

        int i;
        for (i = 0; i<filtHalfWindow; i++)
        filtered[TminInSamples-1+i] = signal[TminInSamples-1+i];   //fill first filtHalfWindow elements by unfiltered data

		for (  ; i < TmaxInSamples-filtHalfWindow; i++)
		{
            filtered[i] = signal[i];
            for (int j = 1; j<=filtHalfWindow; j++)
            {
                filtered[i] += (signal[i-j] * (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1));
                filtered[i] += (signal[i+j] * (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1));
            }
            filtered[i] = filtered[i] / (filtHalfWindow +1);
		}

		for ( ; i < TmaxInSamples; i++)
            filtered[i] = signal[i];   //fill last filtHalfWindow elements by unfiltered data
		break; }
	default:
		throw new EKGException(__LINE__, "CMeasurement::lowPassFilter: window type not supported");
	}
}

void CMeasurement::lowPassFilterBeatDetection(DSPWindow windowType, int sourceChannelNum, double cutoffFreq, double errorValue)
{
	CChannel &signal = channel[sourceChannelNum]; //open measurement channel
	int filteredChNum = addChannel("LPF", signal.getMeasurementUnit());
	CChannel &filtered = channel[filteredChNum];
    filtered.tenBitSaving = false;
	int TminInSamples = 1;                         // for all samples
	int TmaxInSamples = signal.getNumElems();

	switch (windowType) {
	case windowRectangular: { // Low pass filter using rectangular window
		int filtHalfWindow = (int)(getSamplingRate()/ cutoffFreq/2); // the half width of filter window
		if (TmaxInSamples < (filtHalfWindow*2+1))
			throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Window wider than signal - use higher frequency.");
		if (filtHalfWindow == 0)
			throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Window width 0 - use lower frequency.");

		filtered.setChannelName("LPFr"+AnsiString(cutoffFreq)+" from M" +AnsiString(sourceChannelNum+1));  // zgradi ime novega kanala
		filtered.setComment("Low pass filter " + AnsiString(cutoffFreq)+"Hz" +" channel "+signal.getChannelName()+". Used window width "+AnsiString(filtHalfWindow*2) +".\n" );  // zgradi ime novega kanala
		filtered.channelOffset = signal.channelOffset;
        filtered.channelMultiplier = signal.channelMultiplier;

		int i;
		for (i = 0; i<filtHalfWindow; i++)
		    filtered[TminInSamples-1+i] = signal[TminInSamples-1+i];   //fill first filtHalfWindow elements by unfiltered data

		for (  ; i < TmaxInSamples-filtHalfWindow; i++)
		{
            int numSkippedSamples = 0;
            filtered[i] = signal[i];
            if(filtered[i] == errorValue)
                continue;
            for (int j = 1; j<=filtHalfWindow; j++)
            {
                if(signal[i-j] == errorValue)
                    numSkippedSamples++;
                else
                    filtered[i] += signal[i-j];
                if(signal[i-j] == errorValue)
                    numSkippedSamples++;
                else
                    filtered[i] += signal[i+j];
            }
			filtered[i] = filtered[i] / (2*(filtHalfWindow) - numSkippedSamples +1);
		}

        for ( ; i < TmaxInSamples; i++)
		    filtered[i] = signal[i];   //fill last filtHalfWindow elements by unfiltered data
		break; }
	case windowTriangular: {      // Low pass filter using triangular window
		int filtHalfWindow = (int)(getSamplingRate()/ cutoffFreq/2); // the half width of filter window
        if (TmaxInSamples < (filtHalfWindow*2+1))
            throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Window wider than signal - use higher frequency.");
        if (filtHalfWindow == 0)
            throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Window width 0 - use lower frequency.");

		filtered.setChannelName("LPFt"+AnsiString(cutoffFreq)+" from M" +AnsiString(sourceChannelNum+1));  // zgradi ime novega kanala
		filtered.setComment("Low pass filter " + AnsiString(cutoffFreq)+"Hz" +" channel "+signal.getChannelName()+". Used triangular window width "+AnsiString(filtHalfWindow*2) +".\n" );  // zgradi ime novega kanala
   		filtered.channelOffset = signal.channelOffset;
        filtered.channelMultiplier = signal.channelMultiplier;

        int i;
        for (i = 0; i<filtHalfWindow; i++)
            filtered[TminInSamples-1+i] = signal[TminInSamples-1+i];   //fill first filtHalfWindow elements by unfiltered data

		for (  ; i < TmaxInSamples-filtHalfWindow; i++)
		{
            double numSkippedSamples = 0;
            filtered[i] = signal[i];
            if(filtered[i] == errorValue)
                continue;
            for (int j = 1; j<=filtHalfWindow; j++)
            {
                if(signal[i-j] == errorValue)
                    numSkippedSamples+= (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1);
                else
                    filtered[i] += (signal[i-j] * (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1));
                if(signal[i+j] == errorValue)
                    numSkippedSamples+= (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1);
                else
                    filtered[i] += (signal[i+j] * (double)((filtHalfWindow+1)-j)/(filtHalfWindow+1));
            }
            filtered[i] = filtered[i] / (filtHalfWindow + 1 - numSkippedSamples);
		}

		for ( ; i < TmaxInSamples; i++)
            filtered[i] = signal[i];   //fill last filtHalfWindow elements by unfiltered data
		break; }
	default:
		throw new EKGException(__LINE__, "CMeasurement::lowPassFilter: window type not supported");
	}
}

void CMeasurement::correctBaseline(int signalChannelNum, int existBeatChannelNum, CursorPositions *cursorPos)
{
	CChannel &signal = channel[signalChannelNum]; //open measurement channel
	CEventChannel &existBeat = eventChannel[existBeatChannelNum];

	int TminInSamples = 1;                         // for all samples
	int TmaxInSamples = signal.getNumElems();
	
	int correctedChNum = addChannel("Izr.BL na M" + AnsiString(signalChannelNum+1), signal.getMeasurementUnit());
	CChannel &corrected = channel[correctedChNum];

	#define     avgB1_time  0.01    // half of the time interval for averaging B1 point
	int  avgB1_sam = int(avgB1_time * getSamplingRate());  // number of samples in search interval
	if (avgB1_sam < 1)
	{
		Application->MessageBox(gettext("Low number of samples - average not possible!").data(), gettext("Baseline drift elimination - Warning!").data(), MB_OK); //izpis sporocila na ekran
		return;
	}

	// Ta kurzor doloca kateri del signala tocka B1 (koliko dalec od R peaka) naj v vsakem beatu pripne na nic.
	double cursorB1time = cursorPos->c[0];
	int cursorB1sam = time2SampleIndex(cursorB1time);
	double newBaseline = signal[cursorB1sam];  // the value of new base-line

	int odmikB1odR;
	for (int i = 0; i < existBeat.getNumElems(); i++)  // find the selected beat by cursor
		if (existBeat[i].time - cursorB1time > 0)     // relative time to R
		{
			odmikB1odR = time2SampleIndex(existBeat[i].time - cursorB1time);
			break;
		}

	double *Offset = NULL;
	if (Offset != NULL) delete Offset;  // calculate average values for Offsets
	Offset = new double[existBeat.getNumElems()];
	ZeroMemory(Offset,existBeat.getNumElems()*sizeof(double));
	int i = 0, min_flag=0;
	if (time2SampleIndex(existBeat[i].time) - odmikB1odR - avgB1_sam > 0)
	{
		for (; i < existBeat.getNumElems(); i++)
		{
		   for (int j = 0; j <= 2*avgB1_sam; j++)
			 Offset[i] += signal[time2SampleIndex(existBeat[i].time) - odmikB1odR - avgB1_sam + j];
		   Offset[i] = Offset[i]/((2*avgB1_sam)+1);     // average values around B1
		}
	} else {
		Application->MessageBox(gettext("First beat not compensated since there is not enough signal prior to it.").data(), gettext("Baseline drift - Warning!").data() , MB_OK); //izpis sporocila na ekran
		i++;
		if (i == existBeat.getNumElems()-1)
			throw new EKGException(__LINE__, "CMeasurement::calculateBeatTimes: Insufficient number of samples.");
		for (; i < existBeat.getNumElems(); i++)
		{
		   for (int j = 0; j <= 2*avgB1_sam; j++)
			 Offset[i] += signal[time2SampleIndex(existBeat[i].time) - odmikB1odR - avgB1_sam + j];
		   Offset[i] = Offset[i]/((2*avgB1_sam)+1);     // average values around B1
		}
		min_flag = 1;
	}
	if (min_flag==1) i=1; else i=0;    // if first beat can not be corrected set i=1
	int k, delta1, delta2=0;      // correct base line by linear correction
	double slope;

	// kompensate base-line shift before the first B1
	delta1 = time2SampleIndex(existBeat[i].time) - odmikB1odR;
	for (k = TminInSamples-1; k < delta1; k++)  {
		slope = (Offset[i+1]-Offset[i])*(double)(delta1-k) / (double)(delta1);
		corrected[k] = signal[k] + (newBaseline - Offset[i]) + slope;
	}
	int s=0;
	for ( ; i < existBeat.getNumElems()-1; i++)    // compensate all other beats
	{
		delta1 = time2SampleIndex(existBeat[i].time) - odmikB1odR;
		delta2 = time2SampleIndex(existBeat[i+1].time) - odmikB1odR;
		for ( s = 0; k <  delta2; k ++, s++) {
			slope = (Offset[i+1]-Offset[i])* (double)(s)/(double)(delta2-delta1);
			corrected[k] = signal[k] + (newBaseline - Offset[i]) - slope;
		}
	}
	for ( ; k < TmaxInSamples; k ++,s++) { // and for the last beat
		slope = (Offset[i]-Offset[i-1])* (double)(s)/(double)(delta2-delta1);
		corrected[k]=signal[k] + (newBaseline - Offset[i]) - slope;
	}
	corrected.setComment("Baseline drift eliminated on channel " + signal.getChannelName()+". B1 in "+AnsiString(cursorB1time)+" sec. New baseline "+AnsiString(newBaseline)+" level.\n");
    corrected.tenBitSaving = false;
}

void CMeasurement::assignInterEventTimeAsEventValues(int sourceEventChannelNum)
{
	CEventChannel &existBeat = eventChannel[sourceEventChannelNum];

	int beatChannelNum = addEventChannel("Time from D" + AnsiString(sourceEventChannelNum+1), "s");
	CEventChannel &beat = eventChannel[beatChannelNum]; //open event channel
	beat.setContiguous(false);
	beat.setComment("Time between beats (RR), obtained by event channel D " + AnsiString(sourceEventChannelNum+1)+".\n");

	assert(existBeat.getNumElems() > 0);
	beat[0].time = existBeat[0].time;
	if (existBeat.getNumElems() >= 2)
		beat[0].value = existBeat[1].time  - existBeat[0].time;
	for (int i = 1; i < existBeat.getNumElems(); i++)
	{
		beat[i].time = existBeat[i].time;
		beat[i].value = existBeat[i].time  - existBeat[i-1].time;
	}
}

void CMeasurement::assignInterEventTimeAsEventValuesSameChannel(int sourceEventChannelNum)
{
	CEventChannel &existBeat = eventChannel[sourceEventChannelNum];
	assert(existBeat.getNumElems() > 0);
	for (int i = 1; i < existBeat.getNumElems(); i++)
		existBeat[i].value = existBeat[i].time  - existBeat[i-1].time;
}


void CMeasurement::assignEventFreqAsEventValues(int sourceEventChannelNum)
{
	CEventChannel &existBeat = eventChannel[sourceEventChannelNum];

	int beatChannelNum = addEventChannel("Frequency from D" + AnsiString(sourceEventChannelNum+1), "/min");
	CEventChannel &beat = eventChannel[beatChannelNum]; //open event channel
	beat.setContiguous(false);
	beat.setComment("Frequency of beats, obtained from D " + AnsiString(sourceEventChannelNum+1)+".\n");

	assert(existBeat.getNumElems() > 0);
	beat[0].time = existBeat[0].time;
	for (int i = 1; i < existBeat.getNumElems(); i++)
	{
		beat[i].time = existBeat[i].time;
		beat[i].value = 60/(existBeat[i].time  - existBeat[i-1].time);
	}
	if (beat.getNumElems() > 1)
		beat[0].value = beat[1].value;
}

void CMeasurement::assignEventFreqAsEventValuesFromRRI(int sourceEventChannelNum)
{
	CEventChannel &existBeat = eventChannel[sourceEventChannelNum];

	int beatChannelNum = addEventChannel("Frequency from D" + AnsiString(sourceEventChannelNum+1), "/min");
	CEventChannel &beat = eventChannel[beatChannelNum]; //open event channel
	beat.setContiguous(false);
	beat.setComment("Frequency of beats, obtained from D " + AnsiString(sourceEventChannelNum+1)+".\n");

	assert(existBeat.getNumElems() > 0);
    beat.reallocTable(existBeat.getNumElems()-1);
	for (int i = 0; i < existBeat.getNumElems(); i++)
	{
		beat[i].time = existBeat[i].time;
		beat[i].value = (existBeat[i].value > 0)?(60/existBeat[i].value):0.0;
	}
}


void CMeasurement::refineBeatRateWithInterpolation(int signalChannelNum, int existBeatChannelNum)
{
	CChannel &signal = channel[signalChannelNum]; //open measurement channel
	CEventChannel &existBeat = eventChannel[existBeatChannelNum];

	int beatChannelNum = addEventChannel("Interp. D" + AnsiString(existBeatChannelNum+1)+" with M"+AnsiString(signalChannelNum+1), existBeat.getMeasurementUnit());
	CEventChannel &beat = eventChannel[beatChannelNum]; //open event channel
	beat.setContiguous(false);
	beat.setComment("Interpolated beats from event channel " + existBeat.getChannelName()+" with samples of measured channel "+ signal.getChannelName()+".\n");

	// take 3 neighboring points, x2 is already found. Let x2=0, so c=y2. Note that 1/(x2-x1) is sample rate.
	for (int i = 0; i < existBeat.getNumElems(); i++)
	{
		double b = (signal[time2SampleIndex(existBeat[i].time)+1] -
					signal[time2SampleIndex(existBeat[i].time)-1]) * getSamplingRate() /2;
		double a = (signal[time2SampleIndex(existBeat[i].time)+1] -
					2*signal[time2SampleIndex(existBeat[i].time)] +
					signal[time2SampleIndex(existBeat[i].time)-1])*getSamplingRate()*getSamplingRate()/2;
		if (a==0)       // for the case of three coliner points, take central point
			beat[i].time =  existBeat[i].time;
		else {
			double toExtrema = - b / (2*a);
			beat[i].time =  existBeat[i].time + toExtrema;
		}
		beat[i].value = (i == 0 ? 0 // first beat rate invalid
					: (beat[i].time - beat[i-1].time));
		if (beat.getNumElems() > 1)
			beat[0].value = beat[1].value;
	}
}

void CMeasurement::calcStatisticsSignal(int signalChannelNum)
{
	CChannel &measuChann = channel[signalChannelNum];
	int numElements = measuChann.getNumElems();

	double max = measuChann.getMaxValue();
	double min = measuChann.getMinValue();
	double average = 0, standardDev = 0;
	for (int i = 0; i < numElements; i++)
		average +=  measuChann[i];
	average = average / numElements;
	for (int i = 0; i < numElements; i++)
		standardDev += (average - measuChann[i]) * (average - measuChann[i]);
	standardDev = sqrt(standardDev / numElements);

//	wchar_t buff[1000];
	UnicodeString unit = UnicodeString(measuChann.getMeasurementUnit());
//	swprintf(buff, gettext("Number of found samples: %i\r\nAverage sample value: %.4f [%ls]\r\nMaximum: %.4f [%ls]\r\nMinimum: %.4f [%ls]\r\nStandard deviation: %.4f [%ls]\r\n").data(), numElements, average, unit.c_str(),  max, unit.c_str(), min, unit.c_str(), standardDev, unit.c_str());
	UnicodeString abuff;
	abuff.sprintf((gettext("Number of samples found")+": %i\n"+
	gettext("Average sample value")+": %.4f [%ls]\n"+
	gettext("Maximum")+": %.4f [%ls]\n"+
	gettext("Minimum")+": %.4f [%ls]\n"+
	gettext("Standard deviation")+": %.4f [%ls]\n").data(),
	numElements, average, unit.c_str(),  max, unit.c_str(), min, unit.c_str(), standardDev, unit.c_str());

	Application->MessageBox(abuff.c_str(), gettext("Basic statistics of measured channel.").data() , MB_OK);
	measuChann.appendComment(abuff);
}

void CMeasurement::calcStatisticsSignalBetweenCursors(int signalChannelNum, double startTime, double stopTime)
{
	CChannel &measuChann = channel[signalChannelNum];


    int start = std::max(0, (int) (startTime * samplingRate));
    int stop = std::min(measuChann.getNumElems(), (int) (stopTime * samplingRate));

	double maxi = measuChann[start];
	double mini = measuChann[start];
	double average = 0, standardDev = 0;

	for (int i = start; i < stop; i++){
        mini = std::min(measuChann[i], mini);
        maxi = std::max(measuChann[i], maxi);
		average +=  measuChann[i];
    }
	average = average / (stop-start);

	for (int i = start; i < stop; i++)
		standardDev += (average - measuChann[i]) * (average - measuChann[i]);
	standardDev = sqrt(standardDev / (stop-start));

//	wchar_t buff[1000];
	AnsiString unit = measuChann.getMeasurementUnit();
//	swprintf(buff, gettext("Number of found samples: %i\r\nAverage sample value: %.4f [%s]\r\nMaximum: %.4f [%s]\r\nMinimum: %.4f [%s]\r\nStandard deviation: %.4f [%s]\r\n").data(), stop-start, average, unit,  maxi, unit, mini, unit, standardDev, unit);
	UnicodeString abuff;
	abuff.sprintf((gettext("Number of samples found")+": %i\r\n"+
		gettext("Average sample value")+": %.4f [%s]\r\n"+
		gettext("Maximum")+": %.4f [%s]\r\n"+
		gettext("Minimum")+": %.4f [%s]\r\n"+
		gettext("Standard deviation")+": %.4f [%s]\r\n").data(),
		stop-start, average, unit,  maxi, unit, mini, unit, standardDev, unit);
	Application->MessageBox(abuff.c_str(), gettext("Basic statistics of measured channel.").data() , MB_OK);
	measuChann.appendComment(abuff);
}

void CMeasurement::MaxMin(int eventChannelNum, CursorPositions *cursorPos)
{
	CEventChannel &origCh = eventChannel[eventChannelNum];

   // get positions of blue and green cursor
    double c1b, c2g;
    int c1blue, c2green;
    c1b= cursorPos->c[0];
    c2g= cursorPos->c[1];
    // determine index
    c1blue = origCh.getIndexClosestToTime(c1b);
    c2green = origCh.getIndexClosestToTime(c2g);
    if (c1blue >= c2green)
    {
		Application->MessageBox(gettext("Green cursor in front of blue.").data(), gettext("Max/Min - Warning!").data() , MB_OK);
		return;
    }
    double min = 10e+20, max=-10e+20;
	for (int k = c1blue; k < c2green; k++)
    {
      if (origCh[k].value < min)  min= origCh[k].value;
	  if (origCh[k].value > max)  max= origCh[k].value;
    }
//	wchar_t buff[1000];
	AnsiString unit = origCh.getMeasurementUnit();
//	swprintf(buff, gettext("\r\nThe maximum and minimum an their ratio\r\nin time of the blue cursor %.4f till the green cursor %.4f [s]\r\nMaximum: %.4f [%s]\r\nMinimum: %.4f [%s]\r\nRatio max/min: %.4f\r\n").data(), c1b, c2g, max, unit, min, unit, max/min);
	UnicodeString abuff;
	abuff.sprintf((UnicodeString("\r\n")+gettext("The maximum and minimum an their ratio")+
		"\r\n"+gettext("in time between the blue cursor")+
		" %.4f "+gettext("and the green cursor")+" %.4f [s]\r\n"+
		gettext("Maximum")+": %.4f [%s]\r\n"+gettext("Minimum")+
		": %.4f [%s]\r\n"+gettext("Ratio max/min")+": %.4f\r\n").c_str(), c1b, c2g, max, unit, min, unit, max/min);
	Application->MessageBox(abuff.c_str(), gettext("Extremes and ratio.").data() , MB_OK);
	origCh.appendComment(abuff);
}

CMeasurement::BRSReturn CMeasurement::sequentialBRS(int beatChannelNum, int systBPChannelNum, double dRRThreshold, double dBPThreshold)
{
	BRSReturn returnValue(BRSReturn::method_sBRS, beatChannelNum, systBPChannelNum);
	returnValue.dRRThreshold = dRRThreshold;
	returnValue.dBPThreshold = dBPThreshold;

	CEventChannel &beatCh = eventChannel[beatChannelNum];
	CEventChannel &systBPCh = eventChannel[systBPChannelNum];

	//create new event channel for sBRS-Up; set its name, unit and comment
	int BRSChNum = addEventChannel ("sBRS-Up from D"+ AnsiString(beatChannelNum+1)+" and D"+AnsiString(systBPChannelNum+1), "ms/mmHg");
	CEventChannel &BRSCh = eventChannel[BRSChNum];
	BRSCh.setContiguous(false);
	BRSCh.setComment("sBRS-Up from channels: " + beatCh.getChannelName()+ " and "+ systBPCh.getChannelName()+".\r\n");

	double dRR = dRRThreshold;
	// = 0.001*myAtof(InstantDialog::query("sBRS analiza.", "Vnesi druga�no vrednost za dRR[ms]: ", "5"));
	// default > 5ms == 0.005
	double dBP = dBPThreshold;
	// = myAtof(InstantDialog::query("sBRS analiza.", "Vnesi druga�no vrednost za dBP[mmHg]: ", "1"));
	// default  > 1mmHg
	double nseq[2][11]={{0},{0}};   //sequences counter and legth


	int j;      //counter of events in a sequence
    int k;    // BP index
    int i;    // RR index
	// search for the best shift
	for(int p=0; p<11; p++)    // BP shift factor
	{ k=0;i=1;
		while (k < systBPCh.getNumElems()-5-p)
		{ j=0;
			if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time) &&
				(beatCh[i+1+p].time>systBPCh[k+1+p].time) && (systBPCh[k+1+p].time>beatCh[i+p].time) &&
				(beatCh[i+2+p].time>systBPCh[k+2+p].time) && (systBPCh[k+2+p].time>beatCh[i+1+p].time) &&
				(beatCh[i+3+p].time>systBPCh[k+3+p].time) && (systBPCh[k+3+p].time>beatCh[i+2+p].time))
			{   //check if 3-sequence  exists
				if ((beatCh[i+1+p].value - beatCh[i+p].value > dRR &&  systBPCh[k+1].value - systBPCh[k].value > dBP) &&
					(beatCh[i+2+p].value - beatCh[i+1+p].value > dRR &&  systBPCh[k+2].value - systBPCh[k+1].value > dBP) &&
					(beatCh[i+3+p].value - beatCh[i+2+p].value > dRR &&  systBPCh[k+3].value - systBPCh[k+2].value > dBP))
				{   // 3-sequence found
					j=3; i=i+3; k=k+3; nseq[0][p]+=1;
				}
				//forward and try to find 4-sequence
				if (j!=0)
				{
					if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time))
					{
						if (beatCh[i+1+p].value - beatCh[i+p].value > dRR &&  systBPCh[k+1].value - systBPCh[k].value > dBP)
						{   // 4-sequence found
							j=j+1; i=i+1; k=k+1;
						}
					}
				}
				//forward and try to find 5-sequence
				if (j!=0)
				{
					if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time))
					{
						if (beatCh[i+1+p].value - beatCh[i+p].value > dRR &&  systBPCh[k+1].value - systBPCh[k].value > dBP)
						{   // 5-sequence found
							j=j+1; i=i+1; k=k+1;
						}
					}
				}
			}
			//if no sequence forward RR and BP
			if ((beatCh[i+p].time>systBPCh[k+p].time) && (j==0))
			{k++; i++;}
			//if gap, forward RR index
			if ((beatCh[i+p].time<systBPCh[k+p].time) && (j==0))
				while (beatCh[i+p].time<systBPCh[k+p].time)
					i++;
			//if sequence found
			if (j!=0)
			{nseq[1][p]+=double(j);}
		}
	}
	// find the shift with maximal number of seuences
	int maxseqind = 0, maxseq=0, p;
	for(p=0; p<11; p++) if (nseq[0][p]>maxseq) { maxseq=nseq[0][p]; maxseqind = p;}
	// run once more with the best shift and write to channel

	double deltaRR=0, deltaBP=0;
	p= maxseqind;
	k=0; i=1;
	while (k < systBPCh.getNumElems()-5-p)
	{ j=0;
		if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time) &&
             (beatCh[i+1+p].time>systBPCh[k+1+p].time) && (systBPCh[k+1+p].time>beatCh[i+p].time) &&
             (beatCh[i+2+p].time>systBPCh[k+2+p].time) && (systBPCh[k+2+p].time>beatCh[i+1+p].time) &&
             (beatCh[i+3+p].time>systBPCh[k+3+p].time) && (systBPCh[k+3+p].time>beatCh[i+2+p].time))
		{   //check if 3-sequence  exists
			if ((beatCh[i+1+p].value - beatCh[i+p].value > dRR &&  systBPCh[k+1].value - systBPCh[k].value > dBP) &&
				(beatCh[i+2+p].value - beatCh[i+1+p].value > dRR &&  systBPCh[k+2].value - systBPCh[k+1].value > dBP) &&
				(beatCh[i+3+p].value - beatCh[i+2+p].value > dRR &&  systBPCh[k+3].value - systBPCh[k+2].value > dBP))
			{   // 3-sequence found, write to event channel
				BRSCh.append(Event(systBPCh[k+1].time, systBPCh[k+1].value));
				BRSCh.append(Event(systBPCh[k+2].time, systBPCh[k+2].value));
				BRSCh.append(Event(systBPCh[k+3].time, systBPCh[k+3].value));
				BRSCh.append(Event(beatCh[i+1+p].time, beatCh[i+1+p].value*100));   // factor 100 for graph (unit is 10ms)
				BRSCh.append(Event(beatCh[i+2+p].time, beatCh[i+2+p].value*100));
				BRSCh.append(Event(beatCh[i+3+p].time, beatCh[i+3+p].value*100));
				deltaRR += (beatCh[i+1+p].value - beatCh[i+p].value) + (beatCh[i+2+p].value - beatCh[i+1+p].value) + (beatCh[i+3+p].value - beatCh[i+2+p].value);
				deltaBP += (systBPCh[k+1].value - systBPCh[k].value) + (systBPCh[k+2].value - systBPCh[k+1].value) + (systBPCh[k+3].value - systBPCh[k+2].value);
				j=3; i=i+3; k=k+3;
			}
			//forward and try to find 4-sequence
			if (j!=0)
			{
				if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time))
				{
					if (beatCh[i+1+p].value - beatCh[i+p].value > dRR &&  systBPCh[k+1].value - systBPCh[k].value > dBP)
					{   // 4-sequence found
						BRSCh.append(Event(systBPCh[k].time, systBPCh[k].value));
						BRSCh.append(Event(beatCh[i+p].time, beatCh[i+p].value*100));
						deltaRR += (beatCh[i+1+p].value - beatCh[i+p].value);
						deltaBP += (systBPCh[k+1].value - systBPCh[k].value);
						j=j+1; i=i+1; k=k+1;
					}
				}
			}
			//forward and try to find 5-sequence
			if (j!=0)
			{
				if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time))
				{
					if (beatCh[i+1+p].value - beatCh[i+p].value > dRR &&  systBPCh[k+1].value - systBPCh[k].value > dBP)
					{   // 5-sequence found
						BRSCh.append(Event(systBPCh[k].time, systBPCh[k].value));
						BRSCh.append(Event(beatCh[i+p].time, beatCh[i+p].value*100));
						deltaRR += (beatCh[i+1+p].value - beatCh[i+p].value);
						deltaBP += (systBPCh[k+1].value - systBPCh[k].value);
						j=j+1; i=i+1; k=k+1;
					}
				}
			}
		}
		//if no sequence forward RR and BP
		if (beatCh[i+p].time>systBPCh[k+p].time && j==0)
		{k++; i++;}
		//if gap, forward RR index
		if (beatCh[i+p].time<systBPCh[k+p].time && j==0)
			while (beatCh[i+p].time<systBPCh[k+p].time)
				i++;
	}
	if (BRSCh.getNumElems() != 0)
	{
		// calculate average length
		for(p=0; p<11; p++) if (nseq[0][p]!=0) {nseq[1][p]/=nseq[0][p] ;}
		double SBRS = (deltaRR/deltaBP)*1000; //average slope in ms/mmHg
		char buff[1000];
		sprintf(buff,"Measurement time = %.4f - %.4fs.  Max offset = %d. dRR=%4.3fs, dBP=%2.1fmmHg.\r\nPressure in [mmHg], RR times in [s/100].\r\n \r\nNumber of found sBRS-Up sekvences = %2.0f.  Average positive tilt = %4.3f ms/mmHg.\r\nDelay RR-BP:           0     1     2     3     4     5     6     7     8     9    10 \r\n#sekve.sBRS-Up:   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f \r\nAvg length:          %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f \r\n\r\n",
			  beatCh[0].time, beatCh[beatCh.getNumElems()-1].time, maxseqind, dRR, dBP, nseq[0][maxseqind], SBRS, nseq[0][0],nseq[0][1],nseq[0][2],nseq[0][3],nseq[0][4],nseq[0][5],nseq[0][6],nseq[0][7],nseq[0][8],nseq[0][9],nseq[0][10], nseq[1][0],nseq[1][1],nseq[1][2],nseq[1][3],nseq[1][4],nseq[1][5],nseq[1][6],nseq[1][7],nseq[1][8],nseq[1][9],nseq[1][10]);
		//Application->MessageBox(buff, "Resuls sBRS-Up!" , MB_OK);
		returnValue.comment = buff;
		returnValue.sBRSUp = SBRS;
		returnValue.sBRSUpCount = nseq[0][maxseqind];
		BRSCh.appendComment(buff);
	} else {
		deleteEventChannel(BRSChNum);
		//Application->MessageBox("Najdeno ni nobeno Up zaporedje.", "Osnovni sBRS rezultati." , MB_OK);
		returnValue.comment = "No Up sequence found.\r\n";
    }

//create new event channel for sBRS-Down; set its name, unit and comment
 	int BRSChNumD = addEventChannel ("sBRS-Down from D"+ AnsiString(beatChannelNum+1)+" and D"+AnsiString(systBPChannelNum+1), "ms/mmHg");
	CEventChannel &BRSChD = eventChannel[BRSChNumD];
	BRSChD.setContiguous(false);
	BRSChD.setComment("sBRS-Down from channels: " + beatCh.getChannelName()+ " and "+ systBPCh.getChannelName()+".\r\n");

    double nseqD[2][11]={{0},{0}};   //sequences counter and legth for sBRS-Down

    //int j;      //counter of events in a sequence
    //int k;    // BP index
    //int i;    // RR index
  // search for the best shift
 for(int p=0; p<11; p++)    // BP shift factor
 { k=0;i=1;
  while (k < systBPCh.getNumElems()-5-p)
  { j=0;
    if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time) &&
             (beatCh[i+1+p].time>systBPCh[k+1+p].time) && (systBPCh[k+1+p].time>beatCh[i+p].time) &&
             (beatCh[i+2+p].time>systBPCh[k+2+p].time) && (systBPCh[k+2+p].time>beatCh[i+1+p].time) &&
             (beatCh[i+3+p].time>systBPCh[k+3+p].time) && (systBPCh[k+3+p].time>beatCh[i+2+p].time))
    {   //check if 3-sequence  exists
        if ((beatCh[i+1+p].value - beatCh[i+p].value < -dRR &&  systBPCh[k+1].value - systBPCh[k].value < -dBP) &&
            (beatCh[i+2+p].value - beatCh[i+1+p].value < -dRR &&  systBPCh[k+2].value - systBPCh[k+1].value < -dBP) &&
            (beatCh[i+3+p].value - beatCh[i+2+p].value < -dRR &&  systBPCh[k+3].value - systBPCh[k+2].value < -dBP))
        {   // 3-sequence found
            j=3; i=i+3; k=k+3; nseqD[0][p]+=1;
        }
        //forward and try to find 4-sequence
        if (j!=0)
        {
            if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time))
            {
                if (beatCh[i+1+p].value - beatCh[i+p].value < -dRR &&  systBPCh[k+1].value - systBPCh[k].value < -dBP)
                {   // 4-sequence found
                    j=j+1; i=i+1; k=k+1;
                }
            }
         }
        //forward and try to find 5-sequence
        if (j!=0)
        {
            if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time))
            {
                if (beatCh[i+1+p].value - beatCh[i+p].value < -dRR &&  systBPCh[k+1].value - systBPCh[k].value < -dBP)
                {   // 5-sequence found
                    j=j+1; i=i+1; k=k+1;
                }
            }
         }
    }
    //if no sequence forward RR and BP
    if ((beatCh[i+p].time>systBPCh[k+p].time) && (j==0))
        {k++; i++;}
    //if gap, forward RR index
    if ((beatCh[i+p].time<systBPCh[k+p].time) && (j==0))
        while (beatCh[i+p].time<systBPCh[k+p].time)
            i++;
    //if sequence found
    if (j!=0)
        {nseqD[1][p]+=double(j);}
  }
 }
 // find the shift with maximal number of seuences
 //int maxseqind = 0, maxseq=0, p;
 maxseqind = 0; maxseq=0;
 for(p=0; p<11; p++) if (nseqD[0][p]>maxseq) { maxseq=nseqD[0][p]; maxseqind = p;}
 // run once more with the best shift and write to channel

  deltaRR=0, deltaBP=0;
  p= maxseqind;
  k=0; i=1;
  while (k < systBPCh.getNumElems()-5-p)
  { j=0;
    if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time) &&
             (beatCh[i+1+p].time>systBPCh[k+1+p].time) && (systBPCh[k+1+p].time>beatCh[i+p].time) &&
             (beatCh[i+2+p].time>systBPCh[k+2+p].time) && (systBPCh[k+2+p].time>beatCh[i+1+p].time) &&
             (beatCh[i+3+p].time>systBPCh[k+3+p].time) && (systBPCh[k+3+p].time>beatCh[i+2+p].time))
    {   //check if 3-sequence  exists
        if ((beatCh[i+1+p].value - beatCh[i+p].value < -dRR &&  systBPCh[k+1].value - systBPCh[k].value < -dBP) &&
            (beatCh[i+2+p].value - beatCh[i+1+p].value < -dRR &&  systBPCh[k+2].value - systBPCh[k+1].value < -dBP) &&
            (beatCh[i+3+p].value - beatCh[i+2+p].value < -dRR &&  systBPCh[k+3].value - systBPCh[k+2].value < -dBP))
        {   // 3-sequence found, write to event channel
            BRSChD.append(Event(systBPCh[k+1].time, systBPCh[k+1].value));
            BRSChD.append(Event(systBPCh[k+2].time, systBPCh[k+2].value));
            BRSChD.append(Event(systBPCh[k+3].time, systBPCh[k+3].value));
            BRSChD.append(Event(beatCh[i+1+p].time, beatCh[i+1+p].value*100));   // factor 100 for graph (unit is 10ms)
            BRSChD.append(Event(beatCh[i+2+p].time, beatCh[i+2+p].value*100));
            BRSChD.append(Event(beatCh[i+3+p].time, beatCh[i+3+p].value*100));
            deltaRR += (beatCh[i+1+p].value - beatCh[i+p].value) + (beatCh[i+2+p].value - beatCh[i+1+p].value) + (beatCh[i+3+p].value - beatCh[i+2+p].value);
            deltaBP += (systBPCh[k+1].value - systBPCh[k].value) + (systBPCh[k+2].value - systBPCh[k+1].value) + (systBPCh[k+3].value - systBPCh[k+2].value);
            j=3; i=i+3; k=k+3;
        }
        //forward and try to find 4-sequence
        if (j!=0)
        {
            if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time))
            {
                if (beatCh[i+1+p].value - beatCh[i+p].value < -dRR &&  systBPCh[k+1].value - systBPCh[k].value < -dBP)
                {   // 4-sequence found
                    BRSChD.append(Event(systBPCh[k+1].time, systBPCh[k+1].value));
                    BRSChD.append(Event(beatCh[i+1+p].time, beatCh[i+1+p].value*100));
                    deltaRR += (beatCh[i+1+p].value - beatCh[i+p].value);
                    deltaBP += (systBPCh[k+1].value - systBPCh[k].value);
                    j=j+1; i=i+1; k=k+1;
                }
            }
         }
        //forward and try to find 5-sequence
        if (j!=0)
        {
            if ((beatCh[i+p].time>systBPCh[k+p].time) && (systBPCh[k+p].time>beatCh[i-1+p].time))
            {
                if (beatCh[i+1+p].value - beatCh[i+p].value < -dRR &&  systBPCh[k+1].value - systBPCh[k].value < -dBP)
                {   // 5-sequence found
                    BRSChD.append(Event(systBPCh[k+1].time, systBPCh[k+1].value));
                    BRSChD.append(Event(beatCh[i+1+p].time, beatCh[i+1+p].value*100));
                    deltaRR += (beatCh[i+1+p].value - beatCh[i+p].value);
                    deltaBP += (systBPCh[k+1].value - systBPCh[k].value);
                    j=j+1; i=i+1; k=k+1;
                }
            }
         }
    }
    //if no sequence forward RR and BP
    if (beatCh[i+p].time>systBPCh[k+p].time && j==0)
        {k++; i++;}
    //if gap, forward RR index
	if (beatCh[i+p].time<systBPCh[k+p].time && j==0)
		while (beatCh[i+p].time<systBPCh[k+p].time)
			i++;
	}
	if (BRSChD.getNumElems() != 0)
	{
		// calculate average length
		for(p=0; p<11; p++) if (nseqD[0][p]!=0) {nseqD[1][p]/=nseqD[0][p] ;}
		double SBRS = (deltaRR/deltaBP)*1000; //average slope in ms/mmHg
		char buff[1000];
		sprintf(buff,"Measurement time = %.4f - %.4fs.  Max offset = %d. dRR=%4.3fs, dBP=%2.1fmmHg.\r\nPressure in [mmHg], RR timing in [s/100].\r\n \r\nNumber of found sBRS-Down sequences = %2.0f.  Average negative tilt = %4.3f ms/mmHg.\r\Offset RR-BP:           0     1     2     3     4     5     6     7     8     9    10 \r\n#sekve.sBRS-Down:%3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f   %3.0f \r\nAvg length:           %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f  %3.1f \r\n",
			  beatCh[0].time, beatCh[beatCh.getNumElems()-1].time, maxseqind, -dRR, -dBP, nseqD[0][maxseqind], SBRS, nseqD[0][0],nseqD[0][1],nseqD[0][2],nseqD[0][3],nseqD[0][4],nseqD[0][5],nseqD[0][6],nseqD[0][7],nseqD[0][8],nseqD[0][9],nseqD[0][10], nseqD[1][0],nseqD[1][1],nseqD[1][2],nseqD[1][3],nseqD[1][4],nseqD[1][5],nseqD[1][6],nseqD[1][7],nseqD[1][8],nseqD[1][9],nseqD[1][10]);
		//Application->MessageBox(buff, "Results sBRS-Down!" , MB_OK);
		returnValue.comment += buff;
		returnValue.sBRSDown = SBRS;
		returnValue.sBRSDownCount = nseqD[0][maxseqind];
		BRSChD.appendComment(buff);
    } else {
		deleteEventChannel(BRSChNumD);
//		Application->MessageBox("Najdeno ni nobeno Down zaporedje.", "Osnovni sBRS rezultati." , MB_OK);
		returnValue.comment += "No Down sequence found.\r\n";
	}



	return returnValue;
}

CMeasurement::BRSReturn CMeasurement::correlationxBRS(int beatChannelNum, int systBPChannelNum, double ccThresh, double xBrsByCcThresh, int samplesCount) {
	BRSReturn returnValue(BRSReturn::method_xBRS, beatChannelNum, systBPChannelNum);

//It calculates correlation coeficients between RRI and SBP with different RRI delay (from NEG_DELAY through 0 to POZ_DELAY).
//Than it calculates xBRS at delay, where correlation coeficient is at its max.
//Than it calculates xBRS/cc, and it averages it only where correlation coeficient > CC_LIMIT and xBRS/cc > XBRS_CC_LIMIT.
//Than it graphs xBRS/cc where correlation coeficient > CC_LIMIT.

    //number of samples in each sequence of RRI and SBP
    const int NUM_SAMPLE = samplesCount;           
    //number of samples for which RRI delays(towards SBP) into negative direction
    const int NEG_DELAY = 2;
    //number of all possible delays; pozitive, negative and without delay
    const int NUM_DELAY = NEG_DELAY + (samplesCount+1) / 2;
    //number of samples for which RRI delays(towards SBP) into pozitive direction
    const int POZ_DELAY = NUM_DELAY - NEG_DELAY - 1;
    //RRI
	CEventChannel &beatCh = eventChannel[beatChannelNum];
    //SBP
	CEventChannel &systBPCh = eventChannel[systBPChannelNum];

	//create new event channel for xBRS/cc; set its name, unit and comment
 	int BRSChNum = addEventChannel ("xBRS/cc from D"+AnsiString(beatChannelNum+1)+" and D"+AnsiString(systBPChannelNum+1),"ms/mmHg");
 	CEventChannel &BRSCh = eventChannel[BRSChNum];
 	BRSCh.setContiguous(false);
	BRSCh.setComment("xBRS/cc from event channels: "+beatCh.getChannelName()+" and "+systBPCh.getChannelName()+".\r\n");
    //number of samples
    int numS = std::min(beatCh.getNumElems(),systBPCh.getNumElems());
    //averages and sums
    double xa = 0; double ya = 0; double sumxy = 0; double sumx = 0; double sumy = 0;
    //average xBRS/cc
    double xbrs_cca = 0; double num = 0;
    //correlation coeficents
    double* cc = new double[NUM_DELAY];
    //maximum correlation coeficient delay

    if (numS - (POZ_DELAY + NEG_DELAY + NUM_SAMPLE - 1) < 1) {
        deleteEventChannel(BRSChNum);
        returnValue.error = true;
        returnValue.comment = "Wrong channel selection!";
        return returnValue;
    }

    //unsigned int xBrsTestCount = numS-(POZ_DELAY+NEG_DELAY+NUM_SAMPLE-1);
    double avgShift = 0;
    
    double *indM = NULL; indM = (double *)calloc (numS-(POZ_DELAY+NEG_DELAY+NUM_SAMPLE-1), sizeof(double));
    //maximum correlation coeficient
    double *ccM = NULL; ccM = (double *)calloc (numS-(POZ_DELAY+NEG_DELAY+NUM_SAMPLE-1), sizeof(double));
    //xBRS with delay where correlation coeficient is max
    double *xbrsM = NULL; xbrsM = (double *)calloc (numS-(POZ_DELAY+NEG_DELAY+NUM_SAMPLE-1), sizeof(double));
    //xBRS/cc
    double *xbrs_ccM = NULL; xbrs_ccM = (double *)calloc (numS-(POZ_DELAY+NEG_DELAY+NUM_SAMPLE-1), sizeof(double));
    //through all samples
    for (int l = 0; l < numS-(POZ_DELAY+NEG_DELAY+NUM_SAMPLE-1); l++){
        //j = delay of RRI from -NEG_DELAY to +POZ_DELAY
        for (int j = -(NEG_DELAY); j <= POZ_DELAY; j++) {
            //i = first sample of SBP
            //i+j = first sample of RRI
            for (int i = NEG_DELAY+l; i < NEG_DELAY+NUM_SAMPLE+l; i++){
                //averages
                xa += beatCh[i+j].value/NUM_SAMPLE;
                ya += systBPCh[i].value/NUM_SAMPLE;
            }
            for (int i = NEG_DELAY+l; i < NEG_DELAY+NUM_SAMPLE+l; i++){
                //sums
                sumxy += (beatCh[i+j].value - xa) * (systBPCh[i].value - ya);
                sumx += pow(beatCh[i+j].value - xa,2);
                sumy += pow(systBPCh[i].value - ya,2);
            }
            //temp correlation coeficient (formula is diffrent but result is the same as in FCOREL)
            cc[j+NEG_DELAY] = sumxy/(sqrt(sumx*sumy));
            xa = 0; ya = 0; sumxy = 0; sumx = 0; sumy = 0;
        }
        //max correlation coeficient and its delay
        ccM[l] = cc[0]; indM[l] = -(NEG_DELAY);
        for (int k = 1; k < NUM_DELAY; k++) {
            if (cc[k] > ccM[l]) {
                ccM[l] = cc[k];
                indM[l] = k-NEG_DELAY;
            }
        }
        
        //averages where correlation coeficient is max
        for (int i = NEG_DELAY+l; i < NEG_DELAY+NUM_SAMPLE+l; i++){
            xa += beatCh[i+indM[l]].value/NUM_SAMPLE;
            ya += systBPCh[i].value/NUM_SAMPLE;
        }
        //sums where correlation coeficient is max
        for (int i = NEG_DELAY+l; i < NEG_DELAY+NUM_SAMPLE+l; i++){
            sumxy += (beatCh[i+indM[l]].value - xa) * (systBPCh[i].value - ya);
            sumx += pow(systBPCh[i].value - ya,2);
        }
        //xBRS(slope) where correlation coeficient is max, multiplied by 1000(from ms to s)
        xbrsM[l] = 1000*sumxy/sumx;
        //xBRS/cc where correlation coeficient is max
        xbrs_ccM[l] = xbrsM[l]/ccM[l];
        xa = 0; ya = 0; sumxy = 0; sumx = 0;
        //average xBRS/cc where correlation coeficient is greater than CC_LIMIT and xBRS/cc greater than XBRS_CC_LIMIT
        if (ccM[l] > ccThresh && xbrs_ccM[l] > xBrsByCcThresh){
            xbrs_cca += xbrs_ccM[l];
            avgShift += indM[l];
            num++;
        }
    }
        
    //xBRS/cc graph where correlation coeficient is greater than CC_LIMIT
    double avgCC = 0;
    double maxCC = 0;
	for (int n = 0, nGoodCCs = 0; n < numS-(POZ_DELAY+NEG_DELAY+NUM_SAMPLE-1); n++) {
		if (ccM[n] > ccThresh) {
			BRSCh[nGoodCCs].value = xbrs_ccM[n];
            if (maxCC < ccM[n])
                maxCC = ccM[n];
            avgCC += ccM[n];
            //BRSCh[nChan].value = xbrsM[n];
            BRSCh[nGoodCCs].time = beatCh[n].time;
            ++nGoodCCs;
        }
	}
        
	//average xBRS/cc
	if (num > 0) {
        avgCC /= num;
		xbrs_cca /= num;
        avgShift /= num;
    }        
        
	char buff[1000];
	sprintf(buff, "xBRS/CC = %.2f [ms/mmHg] in time %.5f s do %.5f s.\r\n \
        Correlation CC treshold = %.2f, Template legth = %d, Shift: -%d +%d\r\n \
        max CC = %.2f, avg CC = %.2f, number of xBRS sequences = %d, \r\n \
        avg shift: %.2f", 
        xbrs_cca, beatCh[0].time, beatCh[beatCh.getNumElems()-1].time, ccThresh, 
        NUM_SAMPLE, NEG_DELAY, POZ_DELAY, maxCC, avgCC, (int)num, avgShift);
	returnValue.comment = buff;
	returnValue.xBRS = xbrs_cca;
	returnValue.xBRSCount = num;
    returnValue.xBRSAvgSeqElValue = avgCC;
    returnValue.xBRSMaxSeqElValue = maxCC;    
	//Application->MessageBox(buff, "Results xBRS!" , MB_OK);
	BRSCh.appendComment(buff);
	
	//frees used memory
	free(indM);
	free(ccM);
	free(xbrsM);
	free(xbrs_ccM);
    delete[] cc;

	return returnValue;
}

CMeasurement::BRSReturn CMeasurement::frequencyfBRS(int beatChannelNum, int systBPChannelNum)
{
	BRSReturn returnValue(BRSReturn::method_fBRS, beatChannelNum, systBPChannelNum);
	returnValue.error = false;

	//calculate both fourier transforms and create channels
	LibCore::CMeasurement::DFTParams params1 = fourierTransform(beatChannelNum);
	LibCore::CEventChannel *pDFTChannel1 = &(getEventChannel(getNumEventChannels()-1));

	LibCore::CMeasurement::DFTParams params2 = fourierTransform(systBPChannelNum);
	LibCore::CEventChannel *pDFTChannel2 = &(getEventChannel(getNumEventChannels()-1));

	double LF1 = 0, HF1 = 0;
	double LF2 = 0, HF2 = 0;
	double LFLow = 0.04, LFHigh = 0.15, HFLow = 0.15, HFHigh = 0.4;
	double resol1 = 1 / (params1.end - params1.start);
	double resol2 = 1 / (params2.end - params2.start);

	for (int i = 0; i < pDFTChannel1->getNumElems(); i++)
	{
		double freq = (i+1)*resol1;

		if (freq > LFLow && freq <= LFHigh)
			LF1 += (*pDFTChannel1)[i].value;
		if (freq > HFLow && freq <= HFHigh)
			HF1 += (*pDFTChannel1)[i].value;
	}

	for (int i = 0; i < pDFTChannel2->getNumElems(); i++)
	{
		double freq = (i+1)*resol2;

		if (freq > LFLow && freq <= LFHigh)
			LF2 += (*pDFTChannel2)[i].value;
		if (freq > HFLow && freq <= HFHigh)
			HF2 += (*pDFTChannel2)[i].value;
	}

	double alfaLF=0, alfaHF=0;
	alfaLF = sqrt(LF1/LF2);
	alfaHF = sqrt(HF1/HF2);

	// delete both fourier transforms
	deleteEventChannel(getNumEventChannels()-1);
	deleteEventChannel(getNumEventChannels()-1);

	char buff[1000];
	sprintf(buff, "Frequency f-BRS analysis.\r\nalfaLF[0,04-0,15Hz]: %0.2f[ms/mmHg]\r\nalfaHF[0,15-0,4Hz]: %0.2f[ms/mmHg]",
		alfaLF, alfaHF);

	returnValue.comment = buff;
	returnValue.alphaLF = alfaLF;
	returnValue.alphaHF = alfaHF;

	return returnValue;
}

CMeasurement::BRSReturn CMeasurement::sinhronizedrBRS(int beatChannelNum, int systBPChannelNum, int respChannelNum, CursorPositions *cursorPos)
{
	BRSReturn returnValue(BRSReturn::method_rBRS, beatChannelNum, systBPChannelNum);
	double resamplingRate = 10;
	//Resample both channels
	resampleEventChannel(beatChannelNum, resamplingRate);
	int resamNumCh1 = getNumEventChannels()-1;
	CEventChannel &beatCh = eventChannel[resamNumCh1];

	resampleEventChannel(systBPChannelNum, resamplingRate);
	int resamNumCh2 = getNumEventChannels()-1;
	CEventChannel &systBPCh = eventChannel[resamNumCh2];

	CEventChannel &respCh = eventChannel[respChannelNum];

	// create a event channel for average RR segment
	int aRRChNum = addEventChannel ("avgRR from D"+ AnsiString(beatChannelNum+1)+" and D"+AnsiString(respChannelNum), "ms");
	CEventChannel &aRRCh = eventChannel[aRRChNum];
	aRRCh.setContiguous(true);
	aRRCh.setComment("Average RR segment from channel: " + beatCh.getChannelName()+ " sinhronized with inspiration of "+ respCh.getChannelName()+".\r\n");

	// create a new event channel for average SBP segment
	int aSBPChNum = addEventChannel ("avgSBP from D"+ AnsiString(systBPChannelNum+1)+" and D"+AnsiString(respChannelNum), "ms");
	CEventChannel &aSBPCh = eventChannel[aSBPChNum];
	aSBPCh.setContiguous(true);
	aSBPCh.setComment("Average SBP segment from channel: " + systBPCh.getChannelName()+ " sinhronized with inspiration of "+ respCh.getChannelName()+".\r\n");

   // get positions of blue and green cursor
	double c1t, c2t;
	c1t = cursorPos->c[0];
	c2t = cursorPos->c[1];

	// determine the number of respiratory periods
	int  sindex = respCh.getIndexClosestToTime(c1t);
	int  eindex = respCh.getIndexClosestToTime(c2t);

	int  refperiod, sRRindex, eRRindex;
	// determine the length of the first period and use it for all other periods
	sRRindex = beatCh.getIndexClosestToTime(respCh[sindex].time);
	eRRindex = beatCh.getIndexClosestToTime(respCh[sindex+1].time);
	refperiod = eRRindex - sRRindex +1;
	//assume that the number of RR and SBP events is the same
	//check if enough samples at the beginning and end
	if  ((sRRindex-refperiod)<0)
	{
		sindex++;
		sRRindex = beatCh.getIndexClosestToTime(respCh[sindex].time);
	}
	if  ((beatCh.getIndexClosestToTime(respCh[eindex].time)+refperiod)<beatCh.getNumElems())
		eindex--;
	int num_respperiods = eindex-sindex +1;

	//initialize both average channels to 0, fill initial zeros
	// restore original event time
	//analyze double length - one period before and one after
	for (int i=0; i < 2*refperiod; i++)
	{
		aRRCh[i].value = 0;
		aRRCh[i].time = beatCh[sRRindex-refperiod+i].time;
		aSBPCh[i].value = 0;
		aSBPCh[i].time = systBPCh[sRRindex-refperiod+i].time;
	}

	//calculate synchronized and detrended average values
	for  (int k=0; k < num_respperiods; k++)    //count respiration periods
	{
		sRRindex = beatCh.getIndexClosestToTime(respCh[sindex+k].time);
		for (int i=0; i < 2*refperiod; i++)
		{
			aRRCh[i].value += beatCh[sRRindex-refperiod+i].value/num_respperiods;
			detrendEventChannel(aRRChNum);
			aSBPCh[i].value += systBPCh[sRRindex-refperiod+i].value/num_respperiods;
			detrendEventChannel(aSBPChNum);
		}
  }

  //calculate rBRS and delay
  double RRmin, RRmax, SBPmin, SBPmax, tRRmin, tSBPmin;
  RRmin=100; RRmax=-100;  SBPmin=100; SBPmax=-100;
	for (int i=0; i < 2*refperiod; i++)
	{
	  if (aRRCh[i].value < RRmin)
	  { RRmin = aRRCh[i].value; tRRmin=  aRRCh[i].time; }
	  if (aRRCh[i].value > RRmax) RRmax = aRRCh[i].value;
	  if (aSBPCh[i].value < SBPmin)
	  { SBPmin = aSBPCh[i].value; tSBPmin=  aSBPCh[i].time; }
	  if (aSBPCh[i].value > SBPmax) SBPmax = aSBPCh[i].value;
	}
	double deltaRR = RRmax-RRmin;
	double deltaSBP = SBPmax-SBPmin;
	double rBRS = (deltaSBP != 0.0 ? deltaRR*1000 / deltaSBP : 0.0);
	double RRdelay = tRRmin-tSBPmin;

	char buff[1000];
	if (deltaSBP == 0.0) {
		returnValue.error = true;
		sprintf(buff, "rBRS = %.2f [ms/mmHg], delay RR = %.2f [s] in time %.5f s until %.5f s.\r\nDelta RR = %.2f ms in delta SBP = %.2f mmHg.\r\n�tevilo analiziranih izdihov = %d.\r\n", rBRS, RRdelay, respCh[sindex].time,respCh[eindex].time,deltaRR*1000, deltaSBP,num_respperiods);
		returnValue.comment = AnsiString("Calculation error!\r\n\r\n") + buff;
	} else {
		sprintf(buff, "rBRS = %.2f [ms/mmHg], delay RR = %.2f [s] in time %.5f s until %.5f s.\r\nDelta RR = %.2f ms in delta SBP = %.2f mmHg.\r\n�tevilo analiziranih izdihov = %d.\r\n", rBRS, RRdelay, respCh[sindex].time,respCh[eindex].time,deltaRR*1000, deltaSBP,num_respperiods);
		returnValue.comment = buff;
	}
	returnValue.rBRS = rBRS;
	returnValue.rBRSDelay = RRdelay;
	returnValue.rBRSCount = num_respperiods;
	aRRCh.appendComment(buff);
    aSBPCh.appendComment(buff);
    
	//delete both resempled channels
  	deleteEventChannel(resamNumCh1);
	deleteEventChannel(resamNumCh1);

	return returnValue;
}

void CMeasurement::signalDifferential(int signalChannelNum, double errorValue) {
    CChannel &origCh = channel[signalChannelNum];
	//create new resampled event channel; set its name, unit and comment
	int newChNum = addEventChannel("Derivate "+ origCh.getChannelName(),origCh.getMeasurementUnit()+"/ms");
	CEventChannel &differential = eventChannel[newChNum];
	differential.setComment("Derivate channel M" + origCh.getChannelName()+".");
    differential.setContiguous(true);

	for (int i = 1; i < origCh.getNumElems()-1; i++)
	{
        differential[i-1].time = sampleIndex2Time(i);
        if(origCh[i-1] == 0 || origCh[i+1] == 0)
            differential[i-1].value = 0;
        else
            differential[i-1].value = (origCh[i+1] - origCh[i-1]) / (sampleIndex2Time(i+1) - sampleIndex2Time(i-1));
    }
}

void CMeasurement::eventDifferential(int eventChannelNum) {
    CEventChannel &origCh = eventChannel[eventChannelNum];
	//create new resampled event channel; set its name, unit and comment
	int newChNum = addEventChannel("Derivate "+ origCh.getChannelName(), origCh.getMeasurementUnit()+"/ms");
	CEventChannel &differential = eventChannel[newChNum];
	differential.setComment("Derivate channel D" + origCh.getChannelName()+".");
    differential.setContiguous(true);

	for (int i = 1; i < origCh.getNumElems(); i++)
	{
        differential[i-1].time = (origCh[i].time + origCh[i-1].time) * 0.5;
        differential[i-1].value = (origCh[i].value - origCh[i-1].value) / (origCh[i].time - origCh[i-1].time);
    }
}

void CMeasurement::signalDifferentialAbsolute(int signalChannelNum, double errorValue) {
    CChannel &origCh = channel[signalChannelNum];
	//create new resampled event channel; set its name, unit and comment
	int newChNum = addEventChannel("Derivate "+ origCh.getChannelName(),origCh.getMeasurementUnit()+"/ms");
	CEventChannel &differential = eventChannel[newChNum];
	differential.setComment("Derivate channel M" + origCh.getChannelName()+".");
    differential.setContiguous(true);

	for (int i = 1; i < origCh.getNumElems()-1; i++)
	{
        differential[i-1].time = sampleIndex2Time(i);
        if(origCh[i-1] == errorValue || origCh[i+1] == errorValue)
            differential[i-1].value = 0;
        else
            differential[i-1].value = fabs((origCh[i+1] - origCh[i-1]) / (sampleIndex2Time(i+1) - sampleIndex2Time(i-1)));
    }
}

void CMeasurement::eventDifferentialAbsolute(int eventChannelNum) {
    CEventChannel &origCh = eventChannel[eventChannelNum];
	//create new resampled event channel; set its name, unit and comment
	int newChNum = addEventChannel("Derivate "+ origCh.getChannelName(), origCh.getMeasurementUnit()+"/ms");
	CEventChannel &differential = eventChannel[newChNum];
	differential.setComment("Derivate channel D" + origCh.getChannelName()+".");
    differential.setContiguous(true);

	for (int i = 1; i < origCh.getNumElems(); i++)
	{
        differential[i-1].time = (origCh[i].time + origCh[i-1].time) * 0.5;
        differential[i-1].value = fabs((origCh[i].value - origCh[i-1].value) / (origCh[i].time - origCh[i-1].time));

    }
}

void CMeasurement::analyzeTWave() {
    
}

void CMeasurement::resampleEventChannel(int eventChannelNum, double newFrequency)
{
	CEventChannel &origCh = eventChannel[eventChannelNum];
	//create new resampled event channel; set its name, unit and comment
	int newChNum = addEventChannel("Resampled "+ origCh.getChannelName(),origCh.getMeasurementUnit());
	CEventChannel &resampledBeat = eventChannel[newChNum];
	resampledBeat.setComment("Resampled channel: " + origCh.getChannelName()+" with frequency: "+AnsiString(newFrequency)+" Hz"+".\r\n");

    int i=0;
	double restime = origCh[i].time;
    while (origCh[origCh.getNumElems()-1].time > restime)
    {
        resampledBeat[i].time = restime;
        restime += 1/newFrequency;         // set resemple time
        i++;
    }
    i=0;
	for (int k = 0; k < origCh.getNumElems()-1; k++)
    {
        while ((i < resampledBeat.getNumElems()) && (resampledBeat[i].time < origCh[k+1].time))  // linear interpolation
        {
            resampledBeat[i].value = origCh[k].value + (origCh[k+1].value - origCh[k].value)/(origCh[k+1].time - origCh[k].time)*(resampledBeat[i].time - origCh[k].time);
            i++;
        }
    }
}

void CMeasurement::resampleSignalChannel(int signalChannelNum, double newFrequency)
{
	CChannel &origCh = channel[signalChannelNum];
	//create new resampled event channel; set its name, unit and comment
	int newChNum = addEventChannel("Resampled "+ origCh.getChannelName(),origCh.getMeasurementUnit());
	CEventChannel &resampledBeat = eventChannel[newChNum];
	resampledBeat.setComment("Resampled channel: " + origCh.getChannelName()+" with frequency: "+AnsiString(newFrequency)+" Hz"+".");

	int i=0;
	double restime = 0;
	int newSize = (origCh.getNumElems()-1) * ((newFrequency/this->samplingRate)+1);
	resampledBeat.reallocTable((int)(newSize));
	for (int k = 0; k < origCh.getNumElems()-1; k++)
	{
		double timeK = sampleIndex2Time(k), timeK1 = sampleIndex2Time(k+1);
		while (restime < timeK1)  // linear interpolation
		{
			resampledBeat[i].time = restime;
			restime += 1/newFrequency;
			resampledBeat[i].value =
				origCh[k] + (origCh[k+1] - origCh[k])/(timeK1 - timeK) * (resampledBeat[i].time - timeK1);
			i++;
		}
	}
	resampledBeat.reallocTable(i);
}

CMeasurement::DFTParams CMeasurement::fourierTransform(int existBeatChannelNum, double maxFrequency)
{
	//resample to obtain events with constant frequency
//	double resamplingRate = 4;
    double resamplingRate = 8.0 * maxFrequency;
    if (resamplingRate > 20.0)
        resamplingRate = 20.0;
	CEventChannel &origCh = eventChannel[existBeatChannelNum];

/*
    //test signal sin 0.1Hz
    int num1 = eventChannel[existBeatChannelNum].getNumElems();
    for (int i = 0; i < num1; i++)
    {
        eventChannel[existBeatChannelNum][i].value = sin(i*2*M_PI*0.1);    //0.1Hz
     //   eventChannel[existBeatChannelNum][i].value = sin(i*2*M_PI*0.1)+sin(i*2*M_PI*0.3);  //0.1Hz+0.3Hz
     //   eventChannel[existBeatChannelNum][i].value = sin(i*2*M_PI*0.05)+sin(i*2*M_PI*0.1)+sin(i*2*M_PI*0.3);  //0.05Hz+0.1Hz+0.3Hz
     //   eventChannel[existBeatChannelNum][i].value = 1*sin(i*2*M_PI*0.1)+1*sin(i*2*M_PI*0.42);  //.1Hz*1+.42Hz*1
     //   eventChannel[existBeatChannelNum][i].value = 1*sin(i*2*M_PI*0.1)+1*sin(i*2*M_PI*0.42)+0.2*(1-2*rand()/RAND_MAX);  //.1Hz*1+.42Hz*1+.1noise
        eventChannel[existBeatChannelNum][i].time = i;
    }
*/
	resampleEventChannel(existBeatChannelNum, resamplingRate);
	int resampledBeatChNum = getNumEventChannels()-1;
	CEventChannel &resampledBeat = eventChannel[resampledBeatChNum];

    AnsiString DFTunit;
    double unitFactor;
    if (origCh.getMeasurementUnit() == "s")
	{
        DFTunit = "ms^2";
        unitFactor = 1000000;
    } else {
		DFTunit = origCh.getMeasurementUnit() + "^2";
        unitFactor = 1;
    }

	//create new event channel for the transform; set its name, unit and comment
	int dftChNum = addEventChannel("DFT od D"+ AnsiString(existBeatChannelNum+1), DFTunit);
	CEventChannel &dftCh = eventChannel[dftChNum];
    dftCh.setContiguous(false);
    dftCh.setComment("DFT channel: " + origCh.getChannelName()+".\r\n");

	double FijR, FijI, FTR=0, FTI=0;
	double resol = 1 / (resampledBeat[resampledBeat.getNumElems()-1].time - resampledBeat[0].time);
	int numP = resampledBeat.getNumElems();
	for (int i = 0; i < floor((double)numP/2)+1; i++)
    {
		for (int j = 0; j < numP; j++)
		{
			FijR = cos (-2*M_PI*i*j/numP);
            FijI = sin (-2*M_PI*i*j/numP);
            FTR =  FTR + FijR* resampledBeat[j].value /numP;
            FTI =  FTI + FijI* resampledBeat[j].value /numP;
        }
        if (i>0)
        {     //power 
            dftCh[i-1].value =  unitFactor*( FTR*FTR + FTI*FTI )*2 ;
			dftCh[i-1].time = i*resol;
        }
        FTR=0; FTI=0;
	}
	if (numP % 2 == 0)    //if n is even
		dftCh[numP/2-1].value =  dftCh[numP/2-1].value /2;   //substract the half of the value of the central line

	DFTParams params;
	params.start = resampledBeat[0].time;
	params.end = resampledBeat[numP-1].time;
	params.samplingRate = resamplingRate;
    params.maxFrequency = maxFrequency;

	/*
		double VLF1=0.001, VLF2 = 0.04, LF1=0.04, LF2=0.15, HF1=0.15, HF2=0.4;
	double TTPS = 0, TPS = 0, VLF = 0, LF = 0, HF = 0;
	double LFn, HFn;
	double resol = 1 / (resampledBeat[numP-1].time - resampledBeat[0].time);
	for (int i=0; i<dftCh.getNumElems(); i++)
		TTPS += dftCh[i].value;
	for (int i=0; i<=floor(0.4/resol); i++)
		TPS += dftCh[i].value;
	for (int i=floor(VLF1/resol); i<floor(VLF2/resol); i++)
		VLF += dftCh[i].value;
	for (int i=floor(LF1/resol); i<floor(LF2/resol); i++)
		LF += dftCh[i].value;
	for (int i=floor(HF1/resol); i<=floor(HF2/resol)-1; i++)
		HF += dftCh[i].value;
	LFn = 100*LF/(TTPS-VLF);
	HFn = 100*HF/(TTPS-VLF);

	char buff[1000];
	sprintf(buff, "\r\n\r\nOsnovni DFT rezultati.\r\n�as meritve od %.4f - %.4fs \r\n�tevilo to�k = %i, Resempliranje = %.2fHz, Lo�ljivost: %e[Hz/�rto]\r\nCelotna mo� TTPS= %.2e[ms^2], TPS[0-0.4Hz]= %.2e[ms^2]\r\nVLF[%.3f-%.3f]= %.2e[ms^2], LF[%.3f-%.3f]= %.2e[ms^2], LFnorm= %.2f[n.u.]\r\nHF[%.3f-%.3f]= %.2e[ms^2], HFnorm= %.2f[n.u.] \r\nLF/HF= %.2f\r\n",
		resampledBeat[0].time, resampledBeat[numP-1].time, numP, resamplingRate, resol, TTPS, TPS, VLF1, VLF2, VLF, LF1, LF2, LF, LFn, HF1, HF2, HF, HFn, LF/HF);
//	Application->MessageBox(buff, "Osnovni DFT results." , MB_OK);
//	dftCh.appendComment(buff);
		origCh.appendComment(buff);
		*/
		
		// delete resampled channel
	deleteEventChannel(resampledBeatChNum);
	return params;
}

void CMeasurement::detrendEventChannel(int channelNum)
{
	CEventChannel &eventChann = eventChannel[channelNum];
	int numElements =   eventChann.getNumElems();
	AnsiString unit = eventChann.getMeasurementUnit();

	double a11=0, a1221=0, a22=0, y1=0, y2=0, x1=0, x2;
	for (int i = 0; i < numElements; i++)
	{
		a11 += 1;
		a1221 += eventChann[i].time;   //symetric normal matrix
		a22 += eventChann[i].time * eventChann[i].time;
		y1 +=  eventChann[i].value;
		y2 +=  eventChann[i].time * eventChann[i].value;
	}
	x2 = (a1221*y1-a11*y2) / (a1221*a1221 - a11*a22);
	x1 =  (y2 - x2*a22) / a1221;
	for (int i = 0; i < numElements; i++)
		eventChann[i].value = eventChann[i].value - (x1 + x2*eventChann[i].time);

    char buff[1000];
	sprintf(buff, "\r\nLinear detrend channel with tilt a= %.6f in y(0)= %.6f, new average= 0.\r\n", x2, x1);
	eventChann.appendComment(buff);
}

void CMeasurement::offsetEventChannel(int channelNum, double offset)
{
	CEventChannel &eventChann = eventChannel[channelNum];
	AnsiString unit = eventChann.getMeasurementUnit();
	int numElements =   eventChann.getNumElems();

	double avgnew = myAtof(InstantDialog::query(gettext("New average."), gettext("Enter new channel average:")+" ", "0"));

	for (int i = 0; i < numElements; i++)
		eventChann[i].value = eventChann[i].value + (avgnew+offset);

	char buff[1000];
	sprintf(buff, "\r\nAverage %.6f [%s] moved to: %.6f [%s]. New average: %.6f [%s].\r\n", -offset, unit, avgnew+offset, unit, avgnew, unit);
	eventChann.appendComment(buff);
}

void CMeasurement::createBPEventChannel(int BPSignalChNum, int existBeatChNum)
{
	CEventChannel &beatCh = eventChannel[existBeatChNum];
	CChannel &BPSignalCh = channel[BPSignalChNum];

	//create new event channels for the two pressures; set their names, units and comments
	int systChNum = addEventChannel("SBP from M"+ AnsiString(BPSignalChNum+1)+" and D"+AnsiString(existBeatChNum+1), "mmHg");
	int diastChNum = addEventChannel("DBP from M"+ AnsiString(BPSignalChNum+1)+" and D"+AnsiString(existBeatChNum+1), "mmHg");
	CEventChannel &diastCh = eventChannel[diastChNum];
	CEventChannel &systCh = eventChannel[systChNum];
 	diastCh.setContiguous(true);
 	systCh.setContiguous(true);
	diastCh.setComment("Diastolic pressure from: " + BPSignalCh.getChannelName()+ " and: " + beatCh.getChannelName()+".\r\n");
	systCh.setComment("Systolipressure from: " + BPSignalCh.getChannelName()+ " and: " + beatCh.getChannelName()+".\r\n");

	//for each beat, find time & value of minimum (diast) and maximum (sist) pressure
	//between this and next beat; add the two events to corresponding new channels
	for (int beat = 0; beat < beatCh.getNumElems()-1; beat++)
	{                                                             
		int startSample = time2SampleIndex(beatCh[beat].time);
		Event minBP(sampleIndex2Time(startSample), BPSignalCh[startSample]);
		Event maxBP(sampleIndex2Time(startSample), BPSignalCh[startSample]);

		if (sampleIndex2Time(startSample) < beatCh[beat].time)
			startSample++;
        // find systolic value first
		for (int sample = startSample; sampleIndex2Time(sample) < beatCh[beat+1].time; sample++)
		{
			if (BPSignalCh[sample] > maxBP.value)
				maxBP = Event(sampleIndex2Time(sample), BPSignalCh[sample]);
		}
        // and then diastolic value in the interval from R to sistolic.time
		for (int sample = startSample; sampleIndex2Time(sample) < maxBP.time; sample++)
		{
			if (BPSignalCh[sample] < minBP.value)
				minBP = Event(sampleIndex2Time(sample), BPSignalCh[sample]);
		}
		diastCh.append(minBP);
		systCh.append(maxBP);
	}
}

void CMeasurement::createRTEventChannel(int RRchannel, int TTchannel)
{
	CEventChannel &chRR = eventChannel[RRchannel];
	CEventChannel &chTT = eventChannel[TTchannel];
	int numElements1 =   chRR.getNumElems();
	int numElements2 =   chTT.getNumElems();
	int numElements;
	if ( numElements1 <= numElements2) numElements = numElements1; else numElements = numElements2;
//	wchar_t buff[1000];
//	swprintf(buff, gettext("The distance between adjacent pairs of events in the two channels(ex. RT).\r\nNumber of elements in channel 1 =  %d. Number of elements in channel 2 =  %d.\r\nDistances calculated for %d events.\r\n").data(),numElements1,numElements2,numElements);
	UnicodeString abuff;
	abuff.sprintf((gettext("The distance between adjacent pairs of events in the two channels (e.g. for calculating RT interval).")+
		"\r\n"+gettext("Number of elements in channel")+" 1 =  %d. "+
		gettext("Number of elements in channel")+" 2 =  %d.\r\n"+
		gettext("Distances calculated for %d events")+".\r\n").data(),numElements1,numElements2,numElements);
	Application->MessageBox(abuff.c_str(), gettext("Razdalje med dogodki.").data() , MB_OK);

	//create new event channel for detrended events, set its name, unit and comment
	AnsiString unit = chRR.getMeasurementUnit();
	int DiffNum = addEventChannel ("Distance D"+ AnsiString(RRchannel+1)+"-D"+AnsiString(TTchannel+1), unit);
	CEventChannel &Diff = eventChannel[DiffNum];
	Diff.setContiguous(false);
	Diff.setComment(abuff);
	Diff.appendComment("Distance between adjacent pairs of events in channels: " + chRR.getChannelName()+" - "+ chTT.getChannelName()+ ".\r\n");

	for (int i = 0; i < numElements; i++)
	{
		Diff[i].value = chTT[i].time - chRR[i].time;
		Diff[i].time = chTT[i].time;
	}
}

void CMeasurement::createMBPEventChannel(int SBPchannel, int DBPchannel) {
    CEventChannel &chSBP = eventChannel[SBPchannel];
	CEventChannel &chDBP = eventChannel[DBPchannel];
	int numElements1 = chSBP.getNumElems();
	int numElements2 = chDBP.getNumElems();
	int numElements;
	if ( numElements1 <= numElements2) numElements = numElements1; else numElements = numElements2;

	//create new event channel for events, set its name, unit and comment
	AnsiString unit = chDBP.getMeasurementUnit();
	int MBPchannel = addEventChannel("MBP from D"+ AnsiString(SBPchannel+1)+" and D"+AnsiString(DBPchannel+1), unit);
	CEventChannel &chMBP = eventChannel[MBPchannel];
	chMBP.setContiguous(chSBP.getContiguous());
	chMBP.setComment("Average pressure from: " + chSBP.getChannelName()+" and "+chDBP.getChannelName()+"\r\n");
	chMBP.appendComment("MBP = (SBP + 2*DBP)/3");

	for (int i = 0; i < numElements; i++) {
		chMBP[i].value = (chSBP[i].value + chDBP[i].value * 2) / 3.0f;
        chMBP[i].time = (chSBP[i].time + chDBP[i].time * 2) / 3.0f;
	}
}


CMeasurement::DFTParams CMeasurement::spectrumCoherence(int eventCh1, int eventCh2, int nwin, double owin, double maxFreq, bool messages)
{
	double resamplingRate = maxFreq * 2;

	/*
		//test signal 1
		int num1 = eventChannel[eventCh1].getNumElems();
		for (int i = 0; i < num1; i++)
		{
			 eventChannel[eventCh1][i].value = sin(i*2*M_PI*0.1) + 0.1*(float)rand()/RAND_MAX;  //0.1Hz+random sum
		//   eventChannel[eventCh1][i].value = sin(i*2*M_PI*0.3);   //0.3Hz
		//    eventChannel[eventCh1][i].value = sin(i*2*M_PI*0.05)+sin(i*2*M_PI*0.1)+sin(i*2*M_PI*0.2);  //0.05Hz+0.1Hz+0.2Hz
		//    eventChannel[eventCh1][i].value = 1*sin(i*2*M_PI*0.1)+1*sin(i*2*M_PI*0.42);  //0.1Hz+0.42Hz
				eventChannel[eventCh1][i].time = i;
		}
		// test signal 2
		int num2 = eventChannel[eventCh2].getNumElems();
		for (int i = 0; i < num2; i++)
		{
		//    eventChannel[eventCh2][i].value = sin(i*2*M_PI*0.1)*sin(i*2*M_PI*0.1); //0.1Hz phase=0
				eventChannel[eventCh2][i].value = sin(i*2*M_PI*0.05)+sin(i*2*M_PI*0.1+M_PI/4);  //0.05Hz+0.1Hz => coh =0 by 0.05Hz
	 //     eventChannel[eventCh2][i].value = 1*sin(i*2*M_PI*0.1)+1*sin(i*2*M_PI*0.42)+0.2*sin(i*2*M_PI*0.05)+0.2*sin(i*2*M_PI*0.22);  //.1Hz*1+.42Hz*1+.05Hz*.2+.22Hz*.2
	 //     eventChannel[eventCh2][i].value = 1*sin(i*2*M_PI*0.1)+1*sin(i*2*M_PI*0.42)+0.2*sin(i*2*M_PI*0.05)+0.2*sin(i*2*M_PI*0.22+0.2*(1-2*rand()/RAND_MAX);  //.1Hz*1+.42Hz*1+.05Hz*.2+.22Hz*.2+.2noise
	 //     eventChannel[eventCh2][i].value = sin(M_PI/10+i*2*M_PI*0.1); //0.1Hz phase=Pi/10 => coh<1
	 //     eventChannel[eventCh2][i].value = sin(M_PI/4+i*2*M_PI*0.1); //0.1Hz phase=Pi/4 => coh<<1
	 //     eventChannel[eventCh2][i].value = sin(M_PI/2+i*2*M_PI*0.1); //0.1Hz phase=Pi/2 => coh<1
	 //     eventChannel[eventCh2][i].value = sin(M_PI+i*2*M_PI*0.1); //0.1Hz phase=Pi => coh=1
	//        eventChannel[eventCh2][i].value = sin(i*2*M_PI*0.1)*sin(i*2*M_PI*0.1); //0.1Hz)^2 nonlinear system
				eventChannel[eventCh2][i].time = i;
		}
    */
    
    //calculate both fourier transforms
    fourierTransform(eventCh1);
    CEventChannel &fftCh1 = eventChannel[getNumEventChannels()-1];
    fourierTransform(eventCh2);
    CEventChannel &fftCh2 = eventChannel[getNumEventChannels()-1];

	//Resample Ch1
	resampleEventChannel(eventCh1, resamplingRate);
	int resamNumCh1 = getNumEventChannels()-1;
	CEventChannel &resampledCh1 = eventChannel[resamNumCh1];

	//Resample Ch2
	resampleEventChannel(eventCh2, resamplingRate);
	int resamNumCh2 = getNumEventChannels()-1;
	CEventChannel &resampledCh2 = eventChannel[resamNumCh2];

	//create channels for coherence and angle
	DFTParams params;

	// kot
	int phiChNum = addEventChannel("Fi between D"+ AnsiString(eventCh1+1)+" and D"+
		AnsiString(eventCh2+1)+".", "");
	CEventChannel &phiCh = eventChannel[phiChNum];
	phiCh.setContiguous(false);
	phiCh.setComment("Phase between channels: " + eventChannel[eventCh1].getChannelName()+" in " +
		eventChannel[eventCh2].getChannelName()+".\r\n");
    
    // alfa spekter
	int alphaChNum = addEventChannel("Alfa between D"+ AnsiString(eventCh1+1)+" and D"+
		AnsiString(eventCh2+1)+".", "");
	CEventChannel &alphaCh = eventChannel[alphaChNum];
	alphaCh.setContiguous(false);
	alphaCh.setComment("Alfa between channels: " + eventChannel[eventCh1].getChannelName()+
		" and " + eventChannel[eventCh2].getChannelName()+".\r\n");
        
    // amplitudni spekter
	int coherenceChNum = addEventChannel("Coher. between D"+ AnsiString(eventCh1+1)+" and D"+
		AnsiString(eventCh2+1)+".", "");
	CEventChannel &coherenceCh = eventChannel[coherenceChNum];
	coherenceCh.setContiguous(false);
	coherenceCh.setComment("Coherence between channels: " + eventChannel[eventCh1].getChannelName()+
		" and " + eventChannel[eventCh2].getChannelName()+".\r\n");

	double FijR, FijI, FTR1 = 0, FTI1 = 0, FTR2 = 0, FTI2 = 0;
	int numP1 = resampledCh1.getNumElems();
	int numP2 = resampledCh2.getNumElems();
	int numP = 0;

	// make starttime and endtime fit both records
	double  starttime, endtime;
	if (resampledCh1[0].time >= resampledCh2[0].time)
		starttime = resampledCh1[0].time;
	else
		starttime = resampledCh2[0].time;

	if (resampledCh1[numP1-1].time <= resampledCh2[numP2-1].time)
		endtime = resampledCh1[numP1-1].time;
	else
		endtime = resampledCh2[numP2-1].time;

	double strT1 = resampledCh1[0].time;
	double strT2 = resampledCh2[0].time;
	double endT1 = resampledCh1[numP1-1].time;
	double endT2 = resampledCh2[numP2-1].time;

	if (starttime < endtime)
	{
		int i = 0;
		while (resampledCh1[i].time < starttime)
			i++;
		int k = 0;
		// rewrite Ch1 time and data
		while (resampledCh1[i].time <= endtime && i <= numP1)
		{
            resampledCh1[k].time = resampledCh1[i].time;
            resampledCh1[k].value = resampledCh1[i].value;
            i++; k++;
		}
		numP1 = k - 1; // save new number of elements

		i = 0;
		while (resampledCh2[i].time < starttime)
			i++;
		k = 0;
		// rewrite Ch2 time and data
		while (resampledCh2[i].time <= endtime && i <= numP2)
		{
			resampledCh2[k].time = resampledCh2[i].time;
			resampledCh2[k].value = resampledCh2[i].value;
			i++; k++;
		}
		numP2 = k - 1; // save new number of elements

		numP = std::min(numP1,numP2);

//		wchar_t buff[1000];
//		swprintf(buff,gettext("\r\nNumber of points:\r\nChannel1 = %d, Channel2 = %d. Used number = %d.\r\nStart time:\r\nChannel1 = %.3f, Channel2 = %.3f. Used = %.3f.\r\nFinish time:\r\nChannel1 = %.3f, Channel2 = %.3f. Used = %.3f.\r\n").data(),numP1,numP2,numP,strT1,strT2,starttime,endT1,endT2,endtime);
		UnicodeString abuff;

		abuff.sprintf((UnicodeString("\r\n")+gettext("Number of points")+":\r\n"+
			gettext("Channel")+"1 = %d, "+gettext("Channel")+"2 = %d."+
			gettext("Used number")+" = %d.\r\n"+gettext("Start time")+":\r\n"+
			gettext("Channel")+"1 = %.3f, "+gettext("Channel")+"2 = %.3f. "+
			gettext("Used")+" = %.3f.\r\n"+gettext("Finish time")+":\r\n"+
			gettext("Channel")+"1 = %.3f,"+gettext("Channel")+"2 = %.3f. "+
			gettext("Used")+" = %.3f.\r\n").c_str(),numP1,numP2,numP,strT1,strT2,starttime,endT1,endT2,endtime);
		if (messages)
			Application->MessageBox(abuff.c_str(), gettext("Warning").data() , MB_OK);
		coherenceCh.appendComment(abuff);
	} else {
		numP = 0;

//		wchar_t buff[1000];
		UnicodeString abuff;
		abuff.sprintf((gettext("Signals do not match!")+"\r\n"+
			gettext("Number of points")+"\r\n"+
			gettext("Channel")+"1 = %d, "+gettext("Channel")+"2 = %d. "+
			gettext("Used number")+" = 0.\r\n"+gettext("Start time")+":\r\n"+
			gettext("Channel")+"1 = %.3f, "+gettext("Channel")+"2 = %.3f. "+
			gettext("Used")+" = x.\r\n"+gettext("Finish time")+":\r\n"+
			gettext("Channel")+"1 = %.3f,"+gettext("Channel")+"2 = %.3f. "+
			gettext("Used")+" = x.\r\n").data(),numP1,numP2,strT1,strT2,endT1,endT2);
		if (messages)
			Application->MessageBox(abuff.c_str(), gettext("Warning").data() , MB_OK);

		deleteEventChannel(coherenceChNum);
		// delete both resempled channels
		deleteEventChannel(resamNumCh1);
		deleteEventChannel(resamNumCh1);
		params.end = -1;
		return params;
	}             

	int wwin;                   // window width
	wwin = numP / (owin + (nwin*(1.0-owin)));

	double *Pxx= NULL;
	Pxx = (double *)calloc (wwin/2, sizeof(double));
	double *Pyy= NULL;
	Pyy = (double *)calloc (wwin/2, sizeof(double));
	double *PxyR= NULL;
	PxyR = (double *)calloc (wwin/2, sizeof(double));
	double *PxyI= NULL;
	PxyI = (double *)calloc (wwin/2, sizeof(double));

	// work with consecutive windows
	for(int k=0; k<nwin; k++)
	{
		// popravljeno (Matjaz) iz "; i < wwin / 2;" v "; i <= numP/2;"
		for (int i = 0; i <= wwin/2; i++)
		{
			for (int j = 0; j < wwin; j++)
			{
				FijR = cos (-2*M_PI*i*j/wwin);
				FijI = sin (-2*M_PI*i*j/wwin);
				FTR1 =  FTR1 + FijR* resampledCh1[j + k*(int)(wwin*(1.0-owin))].value /wwin;
				FTI1 =  FTI1 + FijI* resampledCh1[j + k*(int)(wwin*(1.0-owin))].value /wwin;
				FTR2 =  FTR2 + FijR* resampledCh2[j + k*(int)(wwin*(1.0-owin))].value /wwin;
				FTI2 =  FTI2 + FijI* resampledCh2[j + k*(int)(wwin*(1.0-owin))].value /wwin;
			}
			if (i>0)
			{     //calculate coherence, exlude DC component
				Pxx[i-1] +=  FTR1*FTR1+FTI1*FTI1;
				Pyy[i-1] +=  FTR2*FTR2+FTI2*FTI2;
				PxyR[i-1] +=  FTR2*FTR1 + FTI2*FTI1;
				PxyI[i-1] += FTI1*FTR2 - FTR1*FTI2;
			}
			FTR1=0; FTI1=0; FTR2=0; FTI2=0;
		}
	}

	double resol = (wwin > 0 ? 1 / (resampledCh1[wwin-1].time - resampledCh1[0].time) : 0);
	for (int i = 0; i < wwin/2-1; i++) {
		coherenceCh[i].value = (PxyR[i]*PxyR[i]+PxyI[i]*PxyI[i])/(Pxx[i]*Pyy[i]);
		coherenceCh[i].time = (i+1)*resol;
		phiCh[i].time = (i+1)*resol;
		phiCh[i].value = std::atan2(-PxyI[i], PxyR[i]);
	}

	free(Pxx);
	free(Pyy);
	free(PxyR);
	free(PxyI);
        
    /*
    */      
            
    double freq = resol;
    for (int i = 0, j = 0; i < coherenceCh.getNumElems(); ++i) {
        double sum1 = 0, sum2 = 0;
        int count = 0;
        for (; fftCh1[j].time < freq; ++j) {
            sum1 += fftCh1[j].value;
            sum2 += fftCh2[j].value;
            ++count;
        }
        if (sum2 > 0) {
            alphaCh[i].value = sqrt(sum1 / sum2);
            alphaCh[i].time = coherenceCh[i].time;
        } else {
        }
        freq += resol;
    }
    /*
    */


	//delete both resempled channels
	deleteEventChannel(resamNumCh1);
	deleteEventChannel(resamNumCh1);

//	DFTParams params;
	params.start = starttime;
	params.end = endtime;
	params.samplingRate = resamplingRate;
	params.numberOfWindows = nwin;
	params.overlay = owin;
	params.windowWidth = wwin;
    params.maxFrequency = maxFreq;

	return params;
}


void CMeasurement::addEventChannels(int chan1, int chan2) {
	CEventChannel &ch1 = eventChannel[chan1];
	CEventChannel &ch2 = eventChannel[chan2];

	int newChNum = addEventChannel("= D"+AnsiString(chan1+1)+" + D"+AnsiString(chan2+1), "");
	CEventChannel &newCh = eventChannel[newChNum];
	newCh.setContiguous(ch1.getContiguous());
	newCh.setComment("Sum channel (" + eventChannel[chan1].getChannelName()+
		") in (" + eventChannel[chan2].getChannelName()+").\r\n");

	int numElements1 = ch1.getNumElems();
	int numElements2 = ch2.getNumElems();
	int i1 = 1, i2 = 1;
	double maxTime = std::min(ch1[numElements1-1].time, ch2[numElements2-1].time);
	for (int i = 0; ;++i) {
		double time1 = ch1[i1].time;
		double time2 = ch2[i2].time;
		double time = time1 = time2 = std::max(time1, time2);

		double value1 = (time == time1 ? ch1[i1].value :
			(ch1[i1+1].value*(time - ch1[i1].time) + ch1[i1].value*(ch1[i1+1].time - time)) / (ch1[i1+1].time - ch1[i1].time));
		double value2 = (time == time2 ? ch2[i2].value :
			(ch2[i2+1].value*(time - ch2[i2].time) + ch2[i2].value*(ch2[i2+1].time - time)) / (ch2[i2+1].time - ch2[i2].time));

		newCh[i].time = time;
		newCh[i].value = value1 + value2;

		if (time == maxTime)
			break;

		if ((i1+1 == numElements1) || (i2+1 == numElements2)) {
			Beep(1000,30);
			break;
		}

		if (ch1[i1+1].time == ch2[i2+1].time) {
			++i1;
			++i2;
		} else if (ch1[i1+1].time < ch2[i2+1].time) {
			++i1;
		} else {
			++i2;
		}
	}

	for (int i=0; i<numElements1; ++i) {
		newCh[i].time = ch1[i].time;
		newCh[i].value = ch1[i].value;
	}
}

        //IvanT201109

        //calculate 12-lead ECG for a mesurement
        //calculation depends on the number of leads
        //not sure where are the "=" operators and what they do so go sample by sample copying
void CMeasurement::calculateECG12(CMeasurement* target) const {
    //    CMeasurement *ECG12 = new CMeasurement();
    CMeasurement& ECG12 = *target; // just rename target
    ECG12.setSamplingRate(samplingRate);
    ECG12.setMeasurementName("12-channel");

    ECG12.setNumChannels(12);
    ECG12[0].setChannelName("I");
    ECG12[1].setChannelName("II");
    ECG12[2].setChannelName("III");
    ECG12[3].setChannelName("aVR");
	ECG12[4].setChannelName("aVL");
    ECG12[5].setChannelName("aVF");
    ECG12[6].setChannelName("V1");
    ECG12[7].setChannelName("V2");
    ECG12[8].setChannelName("V3");
    ECG12[9].setChannelName("V4");
    ECG12[10].setChannelName("V5");
    ECG12[11].setChannelName("V6");
    
    int NumSamples;
    NumSamples = channel[0].getNumElems();

    if(numChannels == 31) {
        for(int iSample=0;iSample<NumSamples;iSample++) {
            //I = pL-pR
            ECG12[0][iSample] = channel[2][iSample] - channel[1][iSample];
            //II = pF-pR
            ECG12[1][iSample] = channel[3][iSample] - channel[1][iSample];
            //III = pF-pL = II - I
            ECG12[2][iSample] = channel[3][iSample] - channel[2][iSample];

            ECG12[3][iSample] = 1.5*channel[1][iSample];
            ECG12[4][iSample] = 1.5*channel[2][iSample];
            ECG12[5][iSample] = 1.5*channel[3][iSample];

            //Precordial leads (V1,...,V6)
            ECG12[6][iSample] = 0.44*channel[6][iSample]+0.66*channel[10][iSample];
            ECG12[7][iSample] = 0.66*channel[10][iSample]+0.33*channel[14][iSample];
            ECG12[8][iSample] = 0.33*(channel[14][iSample]+channel[15][iSample]+channel[11][iSample]);
            ECG12[9][iSample] = 0.66*channel[15][iSample]+0.33*channel[18][iSample];
            ECG12[10][iSample] = channel[18][iSample];
            ECG12[11][iSample] = channel[20][iSample];
        }
    } else if(numChannels == 35) {
        for(int iSample=0;iSample<NumSamples;iSample++) {
            //I = pL-pR
            ECG12[0][iSample] = channel[2][iSample] - channel[1][iSample];
            //II = pF-pR
            ECG12[1][iSample] = channel[3][iSample] - channel[1][iSample];
            //III = pF-pL = II - I
            ECG12[2][iSample] = channel[3][iSample] - channel[2][iSample];

            ECG12[3][iSample] = 1.5*channel[1][iSample];
            ECG12[4][iSample] = 1.5*channel[2][iSample];
            ECG12[5][iSample] = 1.5*channel[3][iSample];

            //Precordial leads (V1,...,V6)
            ECG12[6][iSample] = channel[14][iSample];
            ECG12[7][iSample] = channel[18][iSample];
            ECG12[8][iSample] = 0.25*(channel[18][iSample]+channel[22][iSample]+channel[19][iSample]+channel[23][iSample]);
            ECG12[9][iSample] = 0.5*(channel[23][iSample]+channel[26][iSample]);
            ECG12[10][iSample] = 0.5*(channel[26][iSample]+channel[30][iSample]);
            ECG12[11][iSample] = 0.5*(channel[30][iSample]+channel[34][iSample]);
        }
    } else if(numChannels == 3) {
        double Transformation [10][12] = { {0.00831291648361557,0.0164529978724518,0.00814008138883656,-0.0132931442351256,-0.000823769509702531,0.0113863525735525,0.00219035919469482,0.00333814911258786,8.70705059921898e-05,-0.00409440079348931,-0.00595296138265821,7.41547800068456e-05},
                                                 {0.351589708146991,-0.204936226985226,-0.556525935132216,-0.0905669725606169,0.436817589659870,-0.397971313038455,-0.327384664802939,-0.0728308289139158,0.144648071244921,0.116169414616303,-0.0105309820813329,-0.131279442355683},
                                                 {-1.05620201997119,-1.49511826846161,-0.438916248490413,1.25490356223723,-0.329399467719567,-0.987773840455186,0.348529342280740,-0.349244646914129,-1.53294223749344,-2.18409628103121,-2.02837066293121,-1.35731489125595},
                                                 {0.286286184989383,-0.303775138498000,-0.590061323487382,-0.00910743899647183,0.420321838487604,-0.464770146743471,0.607812492829302,0.956806821724933,0.647932249491155,0.319911682784639,0.0225041572671289,-0.205977873058049},
                                                 {-0.273484010360720,0.124219119917521,0.397703130278242,0.118272729994058,-0.291953285547022,0.304601409870341,0.0619445501656277,0.470336529748884,1.24969179030799,1.47054984104611,0.906019038450624,0.0745647063395311},
                                                 {-0.183118008304737,-0.204152677510105,-0.0210346692053698,0.0759450795006550,-0.198731932956448,-0.230283936764504,0.344591613151752,-0.315881087617787,-0.829795001950286,-0.864736857595191,-0.555206446952453,0.143860052686955},
                                                 {-0.0948166283710823,-0.000457992186653559,0.0943586361844288,0.0820774030268035,-0.0601475395298185,0.0813904147468249,0.0924840317230891,0.437030189800421,1.26207004074428,1.48264151701850,0.897158759652214,0.0640055499610969}, 
                                                 {-0.153659257905709,-0.187865093346823,-0.0342058354411144,0.0990447854064905,-0.131444101452071,-0.182752854613744,0.353008663991348,0.0473282539786482,-0.122994160218574,-0.344884447115153,-0.416680429283093,-0.0523748885794416}, 
                                                 {-0.196220296917049,-0.583310578098174,-0.387090281181125,0.362129229431194,0.0677987840556191,-0.512836637716068,-0.134038364464978,-0.541617297319243,-1.16461075882278,-1.04218340813413,-0.474471748510774,-0.134377532669766}, 
                                                 {-0.147681153036603,-0.129578474716452,0.0181026783201514,0.0875565586834985,-0.133965170871406,-0.106811153391179,0.145259229862428,-0.212878150878399,-0.622632432031290,-0.577358354984481,-0.262488591917613,0.0913755483408668}, 
                                                };
        typedef double* doublePtr;
        doublePtr Model[10];
        for (int m = 0; m < 10; ++m)
            Model[m] = new double[NumSamples];

        for (int i = 0; i < NumSamples; ++i) {
            Model[0][i] = 1;
            Model[1][i] = channel[0][i];
            Model[2][i] = channel[1][i];
            Model[3][i] = channel[2][i];
            Model[4][i] = channel[0][i] * channel[1][i];
            Model[5][i] = channel[0][i] * channel[2][i];
            Model[6][i] = channel[1][i] * channel[2][i];
            Model[7][i] = channel[0][i] * channel[0][i];
            Model[8][i] = channel[1][i] * channel[1][i];
            Model[9][i] = channel[2][i] * channel[2][i];
        }
                
        for(int iSample=0;iSample<NumSamples;iSample++) {
            for (int j = 0; j < 12; ++j) {
                ECG12[j][iSample] = 0;
                for (int k = 0; k < 10; ++k) {
                    ECG12[j][iSample] += Model[k][iSample] * Transformation[k][j];
                }
            }
        }

        for (int m = 0; m<10; ++m)
            delete[] Model[m];
    } else {
		UnicodeString msg(AnsiString("Not sorted 12-channel from ") + numChannels + AnsiString(" channels."));
		Application->MessageBox(msg.c_str(), gettext("Error").data(), MB_OK);
        for (int i = 0; i < 12; ++i)
            ECG12[i][0] = 0;
    }
}


// retuns number of samples on graphs that belong to the graph calibration signal
int CMeasurement::generate12leadGraph(const CMeasurement* twelveLeadMeasurement) {
    setSamplingRate(twelveLeadMeasurement->getSamplingRate());
    setMeasurementName("12-channel display");

    // parametri za prikaz
    const double secondsPerChan = 2.45;
    const double impulseHeight = 1; // mV
    const double impulseLength = 0.2; // s 
    int vMax = 1, vMin = 0;
    int calibrationSignalLength = 0;
                                                              
    setNumChannels(4); // display all channels in 4 rows (standard 12-lead ECG)
    double f = twelveLeadMeasurement->getSamplingRate();
    if (4 + f * secondsPerChan*4 < twelveLeadMeasurement->channel[0].getNumElems()) {
        // generate calibration signal
        int i = 0;
        // 10 pixels at 0mV 
        for (; i < 10; ++i)
            for (int c = 0; c < 4; ++c) channel[c][i] = 0; 
        // generate step signal of one "impulseLength" length and amplitude of "impulseHeight"
        int iiMax = (int)ceil(impulseLength * f - 0.5);
        for (int c = 0; c < 4; ++c) {
            for (int ii = 0; ii < iiMax; ++ii) {
                channel[c][i+ii] = impulseHeight;
            }
        }
        i += iiMax;
        calibrationSignalLength = i;
        
        // merge 4 signals per row
        iiMax = (int)floor(f * secondsPerChan + 0.5);

        for (int b = 0; b < 4; ++b) { // blocks of ECGs (4 blocks per row)
            for (int c = 0; c < 4; ++c) { // virtual channels (graph rows)
                for (int ii = 0; ii < iiMax; ++ii) { // samples
                    if (c < 3)
                        channel[c][i+ii] = twelveLeadMeasurement->channel[b*3+c][ii + b*iiMax];
                    else  // 4th row is special, it is just a copy of lead V2
                        channel[c][i+ii] = twelveLeadMeasurement->channel[7][ii + b*iiMax];

                    // get max and min values of all displayes values
                    if (channel[c][i+ii] > vMax) vMax = channel[c][i+ii];
                    if (channel[c][i+ii] < vMin) vMin = channel[c][i+ii];
                }
            }
            i += iiMax;
        }
        
        // fix max and min on graph
        for (int c = 0; c < 4; ++c) {
            channel[c][0] = vMax;
            channel[c][1] = vMin;
        }   
    } else {
		Application->MessageBox(gettext("Too short measurement").data(), gettext("Error").data(), MB_OK);
    }
    return calibrationSignalLength;
}


void CMeasurement::loadHeaderFromFile(const UnicodeString fileName, const FileType fileType)
{
	//set aux data to default (in case it is not part of format)
	editable = false;
    fileLocation = fileName;
    patientData.reset();
	measurementName = fileName;
	comment = "";
	numEventChannels = 0;
	if (!FileExists(fileName) || FileAge(fileName) == -1)
		throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: file does not exist: "+fileName);
	date = TDateTime::FileDateToDateTime(FileAge(fileName));

	//read file
	FILE *file;
	switch (fileType) {
   	// native
	case nevroEKGeditable:
    	editable = true;
	//loading is the same as for nevroEKG
	case nevroEKG: {
   		file = _wfopen(fileName.t_str(), L"rb");
		if (file == NULL)
			throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: cannot open file");

		int fileVersion;
		if (myfscanf(file, "NEKG measurement file version %d\n", &fileVersion) != 1)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: file version not specified correctly in file");
		}
		if (fileVersion != 1 && fileVersion != 2  && fileVersion != 3)
		{
            fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: unsupported version of nevroEKG format");
		}
		fgetUnicodeString(file, measurementName);
		fscanfMultilineString(file, comment, "numCommentLines");
		if (myfscanf(file, "%d numChannels\n", &numChannels) != 1)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: invalid file format");
		}
		if (myfscanf(file, "%lf samplingRate\n", &samplingRate) != 1)
		{
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: invalid file format");
		}
        if(samplingRate <=0){
			fclose(file);
			throw new EKGException(__LINE__, "CMeasurement::loadFromFile: sampling rate is negative or zero!");
        }
		AnsiString dateString;
		fgetAnsiString(file, dateString);
		date = stringToDate(dateString);
        if(fileVersion == 3){
            myfscanf(file,"%d numStatData\n", &numStatData);

            statDataString = new TStringList();
            statDataData = (double*)malloc(numStatData * sizeof(double)); //instead of new
            AnsiString statString;
            AnsiString recordingDevicesParams;

            int specialCasesNum = 0;
            for(int i=0;i<numStatData;i++){
                fgetAnsiString(file, statString);
                //special cases for   recorder MAC, recording hardware, recording software
                if(statString.AnsiCompare("recorder MAC") == 0){
                    fgetAnsiString(file, recordingDevicesParams);
                    setGadgetmac(recordingDevicesParams);
                    specialCasesNum++;
                    continue; 
                }else if(statString.AnsiCompare("recording hardware") == 0){
                    fgetAnsiString(file, recordingDevicesParams);
                    setGadgetVersion(recordingDevicesParams);
                    specialCasesNum++;
                    continue;
                }else if(statString.AnsiCompare("recording software") == 0){
                    fgetAnsiString(file, recordingDevicesParams);
                    setMobEcgVersion(recordingDevicesParams);
                    specialCasesNum++;
                    continue;
                }

                statDataString->Add(statString);
                myfscanf(file,"%lf\n", &statDataData[i-specialCasesNum]);
            }
            numStatData -= specialCasesNum; 
        }
   		patientData.readFromFile(file, fileVersion);

   		for (int channelToRead = 0; channelToRead < numChannels; channelToRead++){
			channel[channelToRead].readFromFileSkipData(file, fileVersion);
			channel[channelToRead].emptyTable(1); //should save some space
        }
   		if (myfscanf(file, "%d numEventChannels\n", &numEventChannels) == 1)
   		{
			for (int channelToRead = 0; channelToRead < numEventChannels; channelToRead++){
				eventChannel[channelToRead].readFromFile(file, fileVersion);
				// DO NOT do empty table here -> need events for statistic calculation
			}
		} else
   			numEventChannels = 0;
   		try{
            if (myfscanf(file, "%d numAnnotationChannels\n", &numAnnotationChannels) == 1)
       		{
       			for (int channelToRead = 0; channelToRead < numAnnotationChannels; channelToRead++){
       				annotationChannel[channelToRead].readFromFile(file, fileVersion);
                }
       		} else
       			numAnnotationChannels = 0;
        }catch(LibCore::EKGException *e){
           //there is no annotation channel found
           numAnnotationChannels = 0;
        }
   		fclose(file);
   		break;



/*
        try{
            AnsiString channelName, comment, measurementUnit;
            int numElems;
            char c;
    		do {
    			c = (char)fgetc(file);
    			if (c == EOF)
    				throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: unexpected EOF");
    		} while (c != ' ');
    		fgetAnsiString(file, channelName);
            channel[0].setChannelName(channelName);
    		fscanfMultilineString(file, comment, "numChannelCommentLines");
            channel[0].setComment(comment,false);
    		channel[0].viewProps.readFromFile(file, fileVersion);
            if(fileVersion == 3){
                int bitValueTmp = 0;
                double channelTmp = 0;
                myfscanf(file,"%d bitValue\n", &bitValueTmp);
                myfscanf(file,"%lf valueMultiplier\n", &channelTmp);
                myfscanf(file,"%lf valueOffset\n", &channelTmp);
            }
       		do {
       			c = (char)fgetc(file);
       			if (c == EOF)
       				throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: unexpected EOF");
       		} while (c != ' ');
       		fgetAnsiString(file, measurementUnit);
            channel[0].setMeasurementUnit(measurementUnit);
       		if (myfscanf(file, "%d samples\n", &numElems) != 1)
       			throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: error reading numElems");
            channel[0].setNumElemsFile(numElems);
        }catch(int e){
            throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: no channel to read");
        }


        for (int channelToRead = 0; channelToRead < numChannels; channelToRead++){
			channel[channelToRead].readFromFile(file, fileVersion);
        }
		if (myfscanf(file, "%d numEventChannels\n", &numEventChannels) == 1)
		{
			for (int channelToRead = 0; channelToRead < numEventChannels; channelToRead++)
				eventChannel[channelToRead].readFromFile(file, fileVersion);
		} else
			numEventChannels = 0;
		try{
            if (myfscanf(file, "%d numAnnotationChannels\n", &numAnnotationChannels) == 1)
    		{
    			for (int channelToRead = 0; channelToRead < numAnnotationChannels; channelToRead++)
    				annotationChannel[channelToRead].readFromFile(file, fileVersion);
    		} else
    			numAnnotationChannels = 0;
        }catch(LibCore::EKGException *e){
            //there is no annotation channel found
            numAnnotationChannels = 0;
        }

		fclose(file);
		break;
*/
	}
	default:
		throw new EKGException(__LINE__, "CMeasurement::loadHeaderFromFile: unsupported format");
	}
}

int CMeasurement::getNekgFileVersion()
{
    return nekgFileVersion;
}

double CMeasurement::getMissingSamplesValue(bool eventChannelSelected, int numSelectedChannel){
    if(eventChannelSelected){
        if(nekgFileVersion < 3)
            return -1.0;
        else
            return 0.0;
    }
    else
        return channel[numSelectedChannel].channelOffset;
}

double CMeasurement::getMulitplierValue(bool eventChannelSelected, int numSelectedChannel){
    if(eventChannelSelected)
        return eventChannel[numSelectedChannel].channelOffset;
    else
        return channel[numSelectedChannel].channelOffset;
}

void CMeasurement::setNekgFileVersion(int ver)
{
    if(ver>2 && nekgFileVersion<3){
        for(int ch=0; ch<numChannels;ch++){
			int numEl = channel[ch].getNumElems();
			for(int i = 0;i<numEl;i++)
				if(channel[ch][i]==-1)
					channel[ch][i]=0;
		}
	}
	nekgFileVersion = ver;
}
void CMeasurement::setBitSaving(int bits)
{
	for(int i=0; i<numChannels;i++)
		channel[i].tenBitSaving = (bits == 10)?true:false;
}

double CMeasurement::getMinimumHeartBeatEvents(int numSelectedChannel){
	double count = 0;
	//get valid sample number
	for(int i=0;i<channel[numSelectedChannel].getNumElems();i++){
		if(channel[numSelectedChannel][i] == channel[numSelectedChannel].channelOffset)
            continue;
        count++;
    }
    // get num minutes
    count = count / (samplingRate*60);

    return count * 4; // there must be at least 15BPM detected in a file

}

//new eventChannel should be empty, else return without changes
    void CMeasurement::calcWindowAverage(int evtCh, int newCh, double windowWidth, double step, double start, double stop, bool strictTime){
        if(step <= 0 || start >stop)
            return;

        eventChannel[newCh].setChannelName("Window average of channel " + eventChannel[evtCh].getChannelName());
        AnsiString temp = "Window average of width " + AnsiString(windowWidth) + " with step " + AnsiString(step);
        eventChannel[newCh].appendComment(temp);
        eventChannel[newCh].setContiguous(true);

        //do while to calculate at least one sample <-measurements with length < windowsWidth
        double currStart = start;
        do{
            double avg = 0;
            double numSamples = 0;
            int currIndexStart = std::max(0,eventChannel[evtCh].getIndexClosestToTime(currStart));

			if((currIndexStart < (eventChannel[evtCh].getNumElems() - 1)) && (eventChannel[evtCh][currIndexStart].time < currStart)) //ensure we are in 60seconds we sample
                currIndexStart++;


            int i;
            for(i = currIndexStart; (i < eventChannel[evtCh].getNumElems()); i++){
                if( i > (eventChannel[evtCh].getNumElems() - 1) || eventChannel[evtCh][i].time > (currStart + windowWidth)){
                    if(i>0)
                        i--;
                    break;
                }

                avg += eventChannel[evtCh][i].value;
				numSamples++;
			}
			if(i == eventChannel[evtCh].getNumElems())
				i--;
			if( numSamples < 1 ||
				(eventChannel[evtCh][i].time - eventChannel[evtCh][currIndexStart].time) > windowWidth ){
                currStart += step;
                continue;
            }
            avg = avg / numSamples;
            double time;
            if(strictTime){
                time = currStart + (windowWidth/2);
            } else {
                time = (eventChannel[evtCh][currIndexStart].time + eventChannel[evtCh][i].time)/2;
            }

            Event *tempEvt = new Event(time, avg);
            eventChannel[newCh].append(*tempEvt);

            currStart += step;
        }while((currStart + windowWidth) < stop);

        eventChannel[newCh].refreshMinMaxValue();

    }

/**

 brief: fit times of all events of existBeatChannelNum to quadratic interpolated peak
 ASSUMPTION: times of existing events are PEAKS on signal channel ( so time can change +-1/frequency) MAX
 errCount will count when this does not occur (three points are not colinear, but middle one is not a peak)!

*/
void CMeasurement::refineBeatRateWithInterpolationSameChannel(int signalChannelNum, int existBeatChannelNum)
{
	CChannel &signal = channel[signalChannelNum]; //open measurement channel
	CEventChannel &existBeat = eventChannel[existBeatChannelNum];

    int errCount = 0;
	// take 3 neighboring points, x2 is already found. Let x2=0, so c=y2. Note that 1/(x2-x1) is sample rate.
	for (int i = 0; i < existBeat.getNumElems(); i++)
	{
		int sample = time2SampleIndex(existBeat[i].time);

		double b = (signal[sample+1] - signal[sample-1]) * getSamplingRate() /2;
		double a = (signal[sample+1] - 2*signal[sample] +
					signal[sample-1])*getSamplingRate()*getSamplingRate()/2;

		if(a != 0) { // a == 0-> colinear points
			double toExtrema = - b / (2*a);
			if(std::abs(toExtrema) < (1.0/getSamplingRate())) // BUGFIX peak can be moved MAX 1 sample left or right!
				existBeat[i].time =  existBeat[i].time + toExtrema;
			else
                errCount++;
		}
		existBeat[i].value = (i == 0 ? existBeat[i].time // first beatrate stays the same
					: (existBeat[i].time - existBeat[i-1].time));
	}
}

void CMeasurement::addEventChannelChangedMarker(double value, AnsiString channelName, double startTime, double endTime, AnsiString comment){
	int channel = findAnnotationChannelByName(markedChName);
    if(channel == -1){
    	addAnnotationChannel(markedChName, "");
		channel = findAnnotationChannelByName(markedChName);
		annotationChannel[channel].viewProps.visible = false;
		if(channel == -1) return;
    }

    Annotation newAnn = Annotation(startTime, channelName + ": " + comment, endTime -startTime, value);

	annotationChannel[channel].append(newAnn);
    annotationChannel[channel].sortEventsByAscendingTime();
    annotationChannel[channel].viewProps.visible = true;
}

}; //end namespace libcore
