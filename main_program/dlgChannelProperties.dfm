object dlgChannelProps: TdlgChannelProps
  Left = 282
  Top = 98
  Caption = 'Channel features'
  ClientHeight = 392
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object labelChannelType: TLabel
    Left = 8
    Top = 24
    Width = 70
    Height = 13
    Caption = 'Signal channel'
  end
  object Label1: TLabel
    Left = 8
    Top = 48
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object Label2: TLabel
    Left = 8
    Top = 112
    Width = 44
    Height = 13
    Caption = 'Comment'
  end
  object Label3: TLabel
    Left = 8
    Top = 80
    Width = 22
    Height = 13
    Caption = 'Unit:'
  end
  object editChannelName: TEdit
    Left = 64
    Top = 48
    Width = 441
    Height = 21
    TabOrder = 0
  end
  object checkContiguous: TCheckBox
    Left = 120
    Top = 24
    Width = 97
    Height = 17
    Caption = 'Continous'
    TabOrder = 1
    Visible = False
  end
  object memoComment: TMemo
    Left = 64
    Top = 112
    Width = 449
    Height = 153
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object BitBtn1: TBitBtn
    Left = 440
    Top = 272
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 3
  end
  object BitBtn2: TBitBtn
    Left = 360
    Top = 272
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 4
  end
  object editUnit: TEdit
    Left = 64
    Top = 80
    Width = 161
    Height = 21
    TabOrder = 5
  end
  object checkSmallDots: TCheckBox
    Left = 240
    Top = 24
    Width = 97
    Height = 17
    Caption = 'Show small dots'
    TabOrder = 6
    Visible = False
  end
end
