//---------------------------------------------------------------------------

#ifndef simpleDocFormH
#define simpleDocFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ToolWin.hpp>

#include "measurement.h"

//---------------------------------------------------------------------------
class TsDocForm : public TForm
{
__published:	// IDE-managed Components
	TChart *Chart;
	TToolBar *ToolBar1;
	TScrollBar *ScrollBar1;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TToolButton *ToolButton2;
	TToolButton *ToolButton3;
	TToolButton *ToolButton1;
	TToolButton *ToolButton4;
	TToolButton *ToolButton5;
	TToolButton *ToolButton6;
	TToolButton *GridButton;
	TToolButton *ToolButton7;
	TToolButton *moveLeft;
	TToolButton *moveRight;
	void __fastcall FormResize(TObject *Sender);
	void __fastcall ScrollBar1Change(TObject *Sender);
	void __fastcall ChartMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta,
          TPoint &MousePos, bool &Handled);
	void __fastcall ChartClickLegend(TCustomChart *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall ChartMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall ToolButton2Click(TObject *Sender);
	void __fastcall ToolButton3Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall GridButtonClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall moveLeftClick(TObject *Sender);
	void __fastcall moveRightClick(TObject *Sender);
private:	// User declarations
	LibCore::CMeasurement measurement; //measurement-document that this form shows
	double xLeft, xRight;
    bool scrollEnabled;
	UnicodeString fileName;
	bool dirty;
	double cursor;

	//this is used for a workaround for behaviour of chart when clicking on legend
    bool legendClickOccured;
public:		// User declarations
	__fastcall TsDocForm(TComponent* Owner);
	virtual bool __fastcall CloseQuery();
	void LoadMeasurement(const UnicodeString _fileName, bool findHR = false);
	void SaveMeasurement(UnicodeString _fileName, bool automatic = false);
	void processOpenedFile();
	void refreshWholeWindow(bool automatic=false);
	void repaintGraph(bool genReport = false);
	void zoomX(double factor, double centre);
	void updateScrollbar();
	void moveGraphToCenter(double center, double width);
	bool SaveMeasurement();
	void sortAllEventChannels();
	UnicodeString rootFolder;
	void Generirajporoilo();
};
//---------------------------------------------------------------------------
extern PACKAGE TsDocForm *sDocForm;
//---------------------------------------------------------------------------
#endif
