// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


#include "base.h"
#include <vcl.h>
#include <vector>
#include <set>
#include <algorithm>
#include "ini.h"

#ifdef INCLUDE_USB_DRIVERS
	void ftCheck(FT_STATUS ftStatus, char *funcName)
	{
		if (ftStatus != FT_OK)
		{
			char buf[1000];
			sprintf(buf, "ftCheck: %s failed with error %d.\n", funcName, ftStatus);
			throw new LibCore::EKGException(__LINE__, buf);
		}
	}
#endif

namespace LibCore
{

double myAtof(char *str)
//replaces , with ., then uses atof
{
	for (unsigned int i = 0; i < strlen(str); i++)
		if (str[i] == ',')
			str[i] = '.';
	return atof(str);
}   /*
double myAtof(AnsiString str)
{
	return myAtof(str.c_str());
}  */


void appendCommentWithTimeStamp(AnsiString& comment, const AnsiString& addedText) {
   // append to comment string: <optional multiple new lines> <time stamp> <one new line> addedText <new line>

   // add newlines from addedText
   int newLines = 0;
   while ((addedText.Length() >= 2 * (newLines + 1)) && (addedText[2 * newLines + 1] == '\r') && (addedText[2 * newLines + 2] == '\n'))
   {
      ++newLines;
	  comment += "\r\n";
   }
   // add time stamp and a newline
   UnicodeString timeStamp;
   DateTimeToString(timeStamp, L"(d.m.yyyy ob h:nn:ss)\r\n", TDateTime::CurrentDateTime());
   comment += timeStamp;
   // add addedText without starting newlines
   if (newLines == 0)
      comment += addedText;
   else
      comment += addedText.SubString(newLines*2+1, addedText.Length() - newLines*2);
   // add newline if there is none at the end of addedText
   if ((addedText.Length() < 2) || (addedText[addedText.Length()-1] != '\r') || (addedText[addedText.Length()] != '\n'))
      comment += "\r\n";
}


void setCommentWithTimeStamp(AnsiString& comment, const AnsiString& addedText) {
   // create comment string: <optional new lines - should get rid of that> <time stamp> <one new line> addedText <new line>

   // reset comment
   comment = "";
   appendCommentWithTimeStamp(comment, addedText);
}

void fprintfMultilineString(FILE *file, AnsiString string, AnsiString numLinesString)
//example: fprintfMultilineString(file, "line1\nline2", "commentLines") outputs
//2 commentLines
//line1
//line2
{
	assert(file);
	if (string.Length() == 0)
	{
		if (fprintf(file, "0 %s\n", numLinesString) == EOF)
			throw new EKGException(__LINE__, "fprintfMultilineString: Error writing empty string to file");
		return;
	}

	while (string.Pos("\r")) //delete all occurences of '\r'
		string.Delete(string.Pos("\r"), 1);
	while (string[1] == '\n') //delete leading newlines
		string.Delete(1, 1);
	while (string[string.Length()] == '\n') //delete trailing newlines
		string.Delete(string.Length(), 1);

	//count lines
	int numLines = 1;
	for (int i = 1; i <= string.Length(); i++)
		if (string[i] == '\n')
			numLines++;

	if (fprintf(file, "%d %s\n%s\n", numLines, numLinesString, string) == EOF)
		throw new EKGException(__LINE__, "fprintfMultilineString: Error writing non-empty string to file");
}

#define GET_ANSI_BUFFER_LEN 100
void fgetAnsiString(FILE *file, AnsiString &string)
//reads file until first '\n'; '\n' is removed from both the file and the string
{
	assert(file);
	char buf[GET_ANSI_BUFFER_LEN+1];
	if (fgets(buf, GET_ANSI_BUFFER_LEN, file) == NULL)
		throw new EKGException(__LINE__, "fgetAnsiString: error reading from file");
	if (buf[strlen(buf)-1] != '\n'){
		buf[GET_ANSI_BUFFER_LEN] = '\0';
		AnsiString result;
		fgetAnsiString(file, result);
		string = AnsiString(buf) + result;
		return;
	}else{
		buf[strlen(buf)-1] = '\0';
	}
	string = AnsiString(buf);
}

void fgetUnicodeString(FILE *file, UnicodeString &string)
//reads file until first '\n'; '\n' is removed from both the file and the string
{
	assert(file);
	char buf[GET_ANSI_BUFFER_LEN+1];
	if (fgets(buf, GET_ANSI_BUFFER_LEN, file) == NULL)
		throw new EKGException(__LINE__, "fgetUnicodeString: error reading from file");
	if (buf[strlen(buf)-1] != '\n'){
		buf[GET_ANSI_BUFFER_LEN] = '\0';
		UnicodeString result;
		fgetUnicodeString(file, result);
		string = UnicodeString(buf) + result;
		return;
	}else{
		buf[strlen(buf)-1] = '\0';
	}
	string = UnicodeString(buf);
}

int myfscanf(FILE *file, char *formatStr, ...)
//myscanf: reads a line from file and parses it with sscanf
//this is to avoid unwanted behaviour of fscanf which removes from the stream any newlines following the read string
//this function only works if formatStr is single-line, i.e. no \n except possibly at the end
{
	va_list args;
	AnsiString line;
	int result;

	fgetAnsiString(file, line);
	assert(strstr(formatStr, "\n") == NULL || strstr(formatStr, "\n") == formatStr+strlen(formatStr)-1);
	va_start(args, formatStr);
	result = vsscanf(line.c_str(), formatStr, args);
	va_end(args);
	return result;
}

void fscanfMultilineString(FILE *file, AnsiString &string, AnsiString numLinesString)
//inverse op of fprintfMultilineString
{
	assert(file);

	//read number of lines
	char formatStr[1000];
	int numLines;
	sprintf(formatStr, "%%d %s\n", numLinesString);
	if (myfscanf(file, formatStr, &numLines) != 1)
		throw new EKGException(__LINE__, "fscanfMultilineString: error reading numOfLines");

	//read string
	string = AnsiString("");
	for (int line = 0; line < numLines; line++)
	{
		AnsiString lineBuf;
		fgetAnsiString(file, lineBuf);
		string += AnsiString(lineBuf) + "\r\n";
	}

	//trim trailing newlines
	if (numLines > 0 && string[string.Length()] == '\n')
		string = string.TrimRight();
}

AnsiString extensionOfFilename(const AnsiString &filename)
{
	int dotpos;
	for (dotpos = filename.Length(); dotpos > 0; dotpos--)
		if (filename[dotpos] == '.')
			return filename.SubString(dotpos+1, filename.Length());
	return "";
}

AnsiString pathOfFilename(const AnsiString &filename)
{
	int slashpos;
	for (slashpos = filename.Length(); slashpos > 0; slashpos--)
		if (filename[slashpos] == '\\')
			return filename.SubString(1, slashpos-1);
	return "";
}

AnsiString dateToString(const TDateTime date)
//string format: 17.03.2003 04:12:05
{
	char result[1000];
	unsigned short year, month, day, hour, min, sec, msec;
	date.DecodeDate(&year, &month, &day);
	date.DecodeTime(&hour, &min, &sec, &msec);
	sprintf(result, "%02d.%02d.%d %02d:%02d:%02d", day, month, year, hour, min, sec);
	return AnsiString(result);
}

AnsiString dateToStringAccurate(const TDateTime date)
//string format: 17.03.2003 04:12:05
{
	char result[1000];
	unsigned short year, month, day, hour, min, sec, msec;
	date.DecodeDate(&year, &month, &day);
	date.DecodeTime(&hour, &min, &sec, &msec);
	sprintf(result, "%02d.%02d.%d %02d:%02d:%02d.%03d", day, month, year, hour, min, sec, msec);
	return AnsiString(result);
}

TDateTime stringToDate(const AnsiString &string)
//string format: 17.03.2003 04:12:05
{
	unsigned short year, month, day, hour, min;
    double sec, msec;
    unsigned short timezoneh = 0, timezonem = 0; //if in future we decide to support timezones in any capacity
	if (sscanf(string.c_str(), "%hd.%hd.%hd %hd:%hd:%lf",
					&day, &month, &year, &hour, &min, &sec) == 6
  	    || sscanf(string.c_str(), "%hd-%hd-%hdT%hd:%hd:%lf+%2hd%2hd",
					&year, &month, &day, &hour, &min, &sec, &timezoneh, &timezonem) == 8
        || sscanf(string.c_str(), "%hd-%hd-%hdT%hd:%hd:%lf",
    		&year, &month, &day, &hour, &min, &sec) == 6
        || sscanf(string.c_str(), "%hd-%hd-%hd %hd:%hd:%lf",
    		&year, &month, &day, &hour, &min, &sec) == 6)
        {
                msec =  1000.0 * (sec - ((long)sec));
            	return TDateTime(year, month, day) + TDateTime(hour, min, (unsigned short) sec, (unsigned short) msec);
        }
    throw new EKGException(__LINE__, "stringToDate:: invalid date");
}

FT_HANDLE openAndSetupDevice(int port, int baudRate, int timeOutMs, HANDLE hFtdMutex)
//opens device, sets RTS to stop any data, purge buffers & manually flush them because purging seems not to work reliably
{
/* //Commmented out -> removal of ftd2xx library
	FT_HANDLE hDevice;
	char serialNumber[20];
	sprintf(serialNumber, "INTEKOM%d", port+1);

//    ftCheck(FT_OpenEx(serialNumber, FT_OPEN_BY_SERIAL_NUMBER, &hDevice), "FT_OpenEx");
    // FT_OpenEx lahko faila v multithreaded okolju, zato zgornja vrstica ni dobra, in jo razsirimo
    // v spodnjo kodo (zavarujemo z mutexi)
    if (hFtdMutex == NULL) {
        ftCheck(FT_OpenEx(serialNumber, FT_OPEN_BY_SERIAL_NUMBER, &hDevice), "FT_OpenEx");
    } else {
        DWORD dwWaitResult;
        dwWaitResult = WaitForSingleObject(hFtdMutex, 500);  // time-out in milliseconds
        switch (dwWaitResult) {
            case WAIT_OBJECT_0: { // The thread got ownership of the mutex
                DWORD ret = FT_OpenEx(serialNumber, FT_OPEN_BY_SERIAL_NUMBER, &hDevice);
                ReleaseMutex(hFtdMutex);
                if (ret != FT_OK) {
                    char buf[1000];
                    sprintf(buf, "Pri poskusu odpiranja vrata %d je pri�lo do napake �tevilka %d.", port+1, ret);
            	    Application->MessageBox(buf, "Sporo�ilo pred zaprtjem programa", MB_OK);
                    throw new EKGException(__LINE__, "openAndSetupDevice:: FT_openEx failed");
                }
                break;
            }
            default:
                break;
        }
    }

	ftCheck(FT_ResetDevice(hDevice), "FT_ResetDevice");
	ftCheck(FT_SetBaudRate(hDevice, baudRate), "FT_SetBaudRate");
	ftCheck(FT_SetDataCharacteristics(hDevice, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_NONE), "FT_SetDataCharacteristics");
	ftCheck(FT_SetFlowControl(hDevice, FT_FLOW_NONE, 0, 0), "FT_SetFlowControl");

	//set RTS to stop any data
	ftCheck(FT_SetRts(hDevice), "FT_SetRts");

	//set timeouts and purge input buffers
	ftCheck(FT_SetTimeouts(hDevice, timeOutMs, timeOutMs), "FT_SetTimeouts");
	ftCheck(FT_Purge(hDevice, FT_PURGE_RX | FT_PURGE_TX), "FT_Purge");

	//because purging doesn't always work (or I use it incorrectly), manually flush buffers
	DWORD bytesInQueue, numRead;
	ftCheck(FT_GetQueueStatus(hDevice, &bytesInQueue), "FT_GetQueueStatus");
	char *buf = new char[bytesInQueue+10];
	ftCheck(FT_Read(hDevice, buf, bytesInQueue, &numRead), "FT_Read");
	delete buf;
	return hDevice;
    */
    return NULL;
}


void CDeviceStatus::initStatic() {
    UnicodeString settingsFname(L"nastavitve.ini");
   	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
	    if (Application->ExeName[i] == '\\') {
            settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
    		break;
        }
  	}
    Ini::File ini(settingsFname.c_str());
    ini.loadVar(EKGbatteryVoltageLow, L"nizka napetost EKG baterije");
    ini.loadVar(EKGbatteryVoltageCritical, L"kriti�na napetost EKG baterije");
    ini.loadVar(EKGbatteryVoltageFactor, L"faktor EKG baterije");
}
        

double CDeviceStatus::EKGbatteryVoltageCritical = 7.5;
double CDeviceStatus::EKGbatteryVoltageLow = 7.7;
double CDeviceStatus::EKGbatteryVoltageUnknown = 1e+6;
double CDeviceStatus::EKGbatteryVoltageFactor = 2.8788/1000;

void CDeviceStatus::getDeviceStatus(bool testForColinDigital)
{
/* //Commmented out -> removal of ftd2xx library
	//check for presence of all 4 serial->USB converters
	DWORD numConnectedDevices;
	char serNum[100];
	ftCheck(FT_ListDevices(&numConnectedDevices, NULL, FT_LIST_NUMBER_ONLY), "FT_ListDevices");
	for (int devIndex = 0; devIndex < numPorts; devIndex++)
		serialPortConnected[devIndex] = false;
	for (DWORD devIndex = 0; devIndex < numConnectedDevices; devIndex++)
	{
		ftCheck(FT_ListDevices((PVOID)devIndex, serNum, FT_LIST_BY_INDEX | FT_OPEN_BY_SERIAL_NUMBER), "FT_ListDevices");
		if (strcmp(serNum, "INTEKOM1") == 0)
			serialPortConnected[0] = true;
		if (strcmp(serNum, "INTEKOM2") == 0)
			serialPortConnected[1] = true;
		if (strcmp(serNum, "INTEKOM3") == 0)
			serialPortConnected[2] = true;
		if (strcmp(serNum, "INTEKOM4") == 0)
			serialPortConnected[3] = true;
	}

	FT_HANDLE hDevice, hDevice1;
	DWORD bytesInQueue, numRead;

	//test port 0: COLIN digital
	if (serialPortConnected[0] && testForColinDigital)
	{
		//open device
		hDevice = openAndSetupDevice(0, 4800, 1000, NULL);
		//see if there is any data
		Sleep(3000);
		ftCheck(FT_GetQueueStatus(hDevice, &bytesInQueue), "FT_GetQueueStatus");
		dataAvailable[0] = (bytesInQueue > 0);
		//close device
		ftCheck(FT_Close(hDevice), "FT_Close");
	} else
		dataAvailable[0] = false;

	//test port 1: COLIN blood pressure + 3 x analog
	if (serialPortConnected[1])
	{
		//open device
		hDevice1 = hDevice = openAndSetupDevice(1, 115200, 100, NULL);
		//clear RTS to start data flow
		ftCheck(FT_ClrRts(hDevice), "FT_ClrRts");
		//see if there is any data
		Sleep(200);
		ftCheck(FT_GetQueueStatus(hDevice, &bytesInQueue), "FT_GetQueueStatus");
		dataAvailable[1] = (bytesInQueue > 0);
		//DO NOT CLOSE DEVICE!
	} else {
		hDevice1 = NULL;
		dataAvailable[1] = false;
	}

	//test port 2: EKG + breathing + battery
	if (hDevice1  && serialPortConnected[2])
	{
		//open device
		hDevice = openAndSetupDevice(2, 115200, 100, NULL);

		//see if there is any data
		Sleep(200);
		ftCheck(FT_GetQueueStatus(hDevice, &bytesInQueue), "FT_GetQueueStatus");
		if (bytesInQueue > 0)
		{
			dataAvailable[2] = true;
			//read one regular sample to find the battery voltage

			//ignore first 260 bytes because they're usually corrupted
			ftCheck(FT_GetQueueStatus(hDevice, &bytesInQueue), "FT_GetQueueStatus");
			char *buf = new char[bytesInQueue+10];
			ftCheck(FT_Read(hDevice, buf, std::min(int(bytesInQueue),260), &numRead), "FT_Read");

			int carryBytes;
			//first, read a byte 01xxxxxx, which marks 1st byte of a sample
			do {
				do
					ftCheck(FT_Read(hDevice, buf, 1, &numRead), "FT_Read");
				while (numRead == 0);
				assert(numRead == 1);
			} while ((buf[0] & 0xC0) != 0xC0);
			carryBytes = 1;

			//wait for full sample & read it
			do
				ftCheck(FT_GetQueueStatus(hDevice, &bytesInQueue), "FT_GetStatus");
			while (bytesInQueue < 7);
			ftCheck(FT_Read(hDevice, buf+carryBytes, 7, &numRead), "FT_Read");
			assert(numRead == 7);
			assert((buf[0] & 0xC0) == 0xC0);

			//decode battery voltage
			int lowByte, highByte, sampleValue;
			highByte  = int(buf[2*2] & 0x1F);
			lowByte = int(buf[2*2+1] & 0x7F);
			sampleValue = highByte*128 + lowByte;
			EKGbatteryVoltage = sampleValue*EKGbatteryVoltageFactor; // 2.8788/1000;

			delete []buf;
		} else {
			dataAvailable[2] = false;
			EKGbatteryVoltage = EKGbatteryVoltageUnknown;
		}

		//close device
		ftCheck(FT_Close(hDevice), "FT_Close");
	} else {
		dataAvailable[2] = false;
		EKGbatteryVoltage = -666;
	}

	//close device1
	if (hDevice1)
		ftCheck(FT_Close(hDevice1), "FT_Close");

	dataAvailable[3] = false; //last port not supported
*/
}

CAnnotationChannel::Statistics CAnnotationChannel::calcStatistics(double timeFrom, double timeTo)
{
	Statistics stat;
	CAnnotationChannel &eventChann = *this;

	stat.timeFrom = timeFrom;
	stat.timeTo = timeTo;
	stat.numOfEvents = 0;
	stat.minValue = 1e+100;
	stat.maxValue = -1e+100;
	stat.avgValue = 0;
	stat.minTime = 1e+100, stat.maxTime = -1e+100;
    stat.avgDuration = 0;
	double timeDelta;
	for (int i = 0; i < getNumElems(); i++)
	{
		if (eventChann[i].time < timeFrom || eventChann[i].time > timeTo)
			continue;

		stat.numOfEvents++;
		stat.avgValue += eventChann[i].value;
		stat.avgDuration += eventChann[i].length;
		stat.minValue = std::min(stat.minValue, eventChann[i].value);
		stat.maxValue = std::max(stat.maxValue, eventChann[i].value);
		stat.minDuration = std::min(stat.minDuration, eventChann[i].length);
		stat.maxDuration = std::min(stat.maxDuration, eventChann[i].length);
		stat.minTime = std::min(stat.minTime, eventChann[i].time);
		stat.maxTime = std::max(stat.maxTime, eventChann[i].time);

	}
	if (stat.numOfEvents > 0){
		stat.avgValue /= stat.numOfEvents;
		stat.avgDuration /= stat.numOfEvents;
	}else{
		stat.avgValue = 0;
		stat.avgDuration = 0;
		stat.maxValue = 0;
		stat.minValue = 0;
		stat.minTime = 0;
		stat.maxTime = 0;
		stat.minValue = 0;
		stat.maxValue = 0;
    }

	return stat;
}


CEventChannel::Statistics CEventChannel::calcStatistics(double timeFrom, double timeTo)
{
	Statistics stat;
	CEventChannel &eventChann = *this;

	stat.timeFrom = timeFrom;
	stat.timeTo = timeTo;
	stat.numOfEvents = 0;
	stat.minValue = 1e+100;
	stat.maxValue = -1e+100;
	stat.avgValue = 0;
	stat.minTime = 1e+100, stat.avgTime = 0, stat.maxTime = -1e+100;
	double timeDelta;
	for (int i = 0; i < getNumElems() && eventChann[i].time <= timeTo; i++)
	{
		if (eventChann[i].time < timeFrom)
			continue;

		stat.numOfEvents++;
		stat.avgValue += eventChann[i].value;
		stat.minValue = std::min(stat.minValue, eventChann[i].value);
		stat.maxValue = std::max(stat.maxValue, eventChann[i].value);

		if (i < getNumElems()-1)
		{
			timeDelta = eventChann[i+1].time-eventChann[i].time;
			stat.avgTime += timeDelta;
			stat.minTime = std::min(stat.minTime, timeDelta);
			stat.maxTime = std::max(stat.maxTime, timeDelta);
		}
	}
	if (stat.numOfEvents > 0)
		stat.avgValue /= stat.numOfEvents;
	if (stat.numOfEvents > 1)
		stat.avgTime /= (stat.numOfEvents-1);

	stat.stDevValue = 0, stat.stDevTime = 0, stat.RMSSD = 0;
	for (int i = 0; i < getNumElems() && eventChann[i].time <= timeTo; i++)
	{
		if (eventChann[i].time < timeFrom)
			continue;

		stat.stDevValue += (stat.avgValue - eventChann[i].value) * (stat.avgValue - eventChann[i].value);
		if (i < getNumElems()-1)
		{
			timeDelta = eventChann[i+1].time-eventChann[i].time;
            //TODO this is weird -> check why this values are high when calculation is finished
			stat.stDevTime += (stat.avgTime - timeDelta) * (stat.avgTime - timeDelta);
			stat.RMSSD += (eventChann[i+1].value-eventChann[i].value)*(eventChann[i+1].value-eventChann[i].value);
		}
	}
	if (stat.numOfEvents > 0)
		stat.stDevValue = sqrt(stat.stDevValue/stat.numOfEvents);
	if (stat.numOfEvents > 1)
	{
		stat.stDevTime = sqrt(stat.stDevTime/(stat.numOfEvents-1));
		stat.RMSSD= sqrt(stat.RMSSD/(stat.numOfEvents-1));
	}

	return stat;
}

CEventChannel::Statistics CEventChannel::calcStatisticsBeatDetection(double filtMin, double filtMax)
{
    double timeFrom = 0;
    double timeTo = access(getNumElems()-1).time;
	Statistics stat;
	CEventChannel &eventChann = *this;

	stat.timeFrom = timeFrom;
	stat.timeTo = timeTo;
	stat.numOfEvents = 0;
	stat.minValue = 1e+100;
	stat.maxValue = -1e+100;
	stat.avgValue = 0;
	stat.minTime = 1e+100, stat.avgTime = 0, stat.maxTime = -1e+100;
	double timeDelta;
	for (int i = 0; i < getNumElems() && eventChann[i].time <= timeTo; i++)
	{
		if (eventChann[i].time < timeFrom || eventChann[i].value < filtMin || eventChann[i].value > filtMax)
			continue;

		stat.numOfEvents++;
		stat.avgValue += eventChann[i].value;
		stat.minValue = std::min(stat.minValue, eventChann[i].value);
		stat.maxValue = std::max(stat.maxValue, eventChann[i].value);

		if (i < getNumElems()-1)
		{
			timeDelta = eventChann[i+1].time-eventChann[i].time;
			stat.avgTime += timeDelta;
			stat.minTime = std::min(stat.minTime, timeDelta);
			stat.maxTime = std::max(stat.maxTime, timeDelta);
		}
	}
	if (stat.numOfEvents > 0)
		stat.avgValue /= stat.numOfEvents;
	if (stat.numOfEvents > 1)
		stat.avgTime /= (stat.numOfEvents-1);

	stat.stDevValue = 0, stat.stDevTime = 0, stat.RMSSD = 0;
	for (int i = 0; i < getNumElems() && eventChann[i].time <= timeTo; i++)
	{
		if (eventChann[i].time < timeFrom || eventChann[i].value < filtMin || eventChann[i].value > filtMax)
			continue;

		stat.stDevValue += (stat.avgValue - eventChann[i].value) * (stat.avgValue - eventChann[i].value);
		if (i < getNumElems()-1)
		{
			timeDelta = eventChann[i+1].time-eventChann[i].time;
			stat.stDevTime += (stat.avgTime - timeDelta) * (stat.avgTime - timeDelta);
			stat.RMSSD += (eventChann[i+1].value-eventChann[i].value)*(eventChann[i+1].value-eventChann[i].value);
		}
	}
	if (stat.numOfEvents > 0)
		stat.stDevValue = sqrt(stat.stDevValue/stat.numOfEvents);
	if (stat.numOfEvents > 1)
	{
		stat.stDevTime = sqrt(stat.stDevTime/(stat.numOfEvents-1));
		stat.RMSSD= sqrt(stat.RMSSD/(stat.numOfEvents-1));
	}

	return stat;
}




}; //end namespace LibCore
