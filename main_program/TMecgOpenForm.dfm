object MecgOpenForm: TMecgOpenForm
  Left = 363
  Top = 328
  Caption = 'Openning MECG file'
  ClientHeight = 130
  ClientWidth = 304
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DescriptionLabel: TLabel
    Left = 8
    Top = 8
    Width = 270
    Height = 13
    Caption = 'Enter the number of channels to import (space-separated)'
  end
  object Label1: TLabel
    Left = 8
    Top = 32
    Width = 95
    Height = 13
    Caption = 'Number of channels'
  end
  object NumChannelsLabel: TLabel
    Left = 144
    Top = 32
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object SaveSettingsCheckBox: TCheckBox
    Left = 8
    Top = 80
    Width = 297
    Height = 17
    Caption = 'Set default'
    TabOrder = 1
  end
  object ChannelNumInput: TEdit
    Left = 8
    Top = 56
    Width = 297
    Height = 21
    TabOrder = 0
  end
  object OkBitBtn1: TBitBtn
    Left = 8
    Top = 104
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 2
  end
  object CancelBitBtn: TBitBtn
    Left = 232
    Top = 104
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 3
  end
  object SelectAllBitBtn: TBitBtn
    Left = 208
    Top = 24
    Width = 97
    Height = 25
    Caption = 'Select all channels'
    TabOrder = 4
    OnClick = SelectAllBitBtnClick
  end
end
