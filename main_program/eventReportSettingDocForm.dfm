object EventreportOnDocSettings: TEventreportOnDocSettings
  Left = 0
  Top = 0
  Caption = 'Report visible on screen'
  ClientHeight = 382
  ClientWidth = 657
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 18
    Top = 11
    Width = 134
    Height = 39
    Caption = 
      'Enter the length of the recorded measurements around event   (in' +
      ' minutes):'
    WordWrap = True
  end
  object Label6: TLabel
    Left = 358
    Top = 181
    Width = 40
    Height = 13
    Caption = 'Author: '
  end
  object Label7: TLabel
    Left = 370
    Top = 19
    Width = 26
    Height = 13
    Caption = 'User:'
  end
  object Label8: TLabel
    Left = 376
    Top = 43
    Width = 19
    Height = 13
    Caption = 'Age'
  end
  object Label9: TLabel
    Left = 364
    Top = 67
    Width = 34
    Height = 13
    Caption = 'Weight'
  end
  object Label5: TLabel
    Left = 377
    Top = 91
    Width = 18
    Height = 13
    Caption = 'Sex'
  end
  object Label10: TLabel
    Left = 348
    Top = 115
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Birth date'
  end
  object Label11: TLabel
    Left = 310
    Top = 139
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Electrode position'
  end
  object Label12: TLabel
    Left = 8
    Top = 67
    Width = 107
    Height = 13
    Caption = 'ECG length per line [s]'
  end
  object Label13: TLabel
    Left = 18
    Top = 142
    Width = 97
    Height = 13
    Caption = 'ECG mV height [mm]'
  end
  object Label14: TLabel
    Left = 22
    Top = 115
    Width = 93
    Height = 13
    Caption = 'mV around baseline'
  end
  object Label15: TLabel
    Left = 8
    Top = 91
    Width = 103
    Height = 13
    Caption = 'ECG time scale [cm/s]'
  end
  object Label16: TLabel
    Left = 8
    Top = 233
    Width = 169
    Height = 13
    Caption = 'Additional comments about report: '
  end
  object OkButton: TButton
    Left = 409
    Top = 348
    Width = 145
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object CancelButton: TButton
    Left = 73
    Top = 348
    Width = 145
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object timeMinutes: TEdit
    Left = 201
    Top = 16
    Width = 41
    Height = 21
    TabOrder = 2
    Text = '4'
    OnChange = timeMinutesChange
  end
  object AuthorInp: TEdit
    Left = 409
    Top = 177
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'AuthorInp'
  end
  object UserInp: TEdit
    Left = 408
    Top = 13
    Width = 241
    Height = 21
    TabOrder = 4
  end
  object AgeInp: TEdit
    Left = 409
    Top = 40
    Width = 89
    Height = 21
    MaxLength = 3
    NumbersOnly = True
    TabOrder = 5
    OnChange = timeMinutesChange
  end
  object WeightInp: TEdit
    Left = 409
    Top = 64
    Width = 89
    Height = 21
    MaxLength = 3
    NumbersOnly = True
    TabOrder = 6
    OnChange = timeMinutesChange
  end
  object GenderInp: TEdit
    Left = 409
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 7
  end
  object BirthInp: TEdit
    Left = 409
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 8
  end
  object PositionInp: TEdit
    Left = 409
    Top = 136
    Width = 121
    Height = 21
    TabOrder = 9
  end
  object Edit2: TEdit
    Left = 121
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 10
    Text = 'Edit2'
    OnChange = timeMinutesChange
  end
  object Edit3: TEdit
    Left = 121
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 11
    Text = 'Edit3'
    OnChange = timeMinutesChange
  end
  object Edit4: TEdit
    Left = 121
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 12
    Text = 'Edit4'
    OnChange = timeMinutesChange
  end
  object Edit5: TEdit
    Left = 121
    Top = 139
    Width = 121
    Height = 21
    TabOrder = 13
    Text = 'Edit5'
    OnChange = timeMinutesChange
  end
  object Memo1: TMemo
    Left = 8
    Top = 252
    Width = 642
    Height = 90
    Lines.Strings = (
      '')
    TabOrder = 14
  end
end
