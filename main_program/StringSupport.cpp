#include "StringSupport.h"


WideString string2WideString(const std::string& str) {
	return System::UTF8ToUnicodeString(RawByteString(str.c_str()));
}

UnicodeString string2UnicodeString(const std::string& str) {
	return System::UTF8ToUnicodeString(RawByteString(str.c_str()));
}

std::string wideString2String(const WideString& str) {
	RawByteString utf8String = System::UTF8Encode(str);
	return std::string(utf8String.c_str());
}
