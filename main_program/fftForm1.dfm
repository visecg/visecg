object FFTform: TFFTform
  Left = 200
  Top = 0
  AutoSize = True
  Caption = 'FFTform'
  ClientHeight = 743
  ClientWidth = 930
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCanResize = FormCanResize
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel
    Left = 4
    Top = 0
    Width = 73
    Height = 49
    AutoSize = False
  end
  object Label15: TLabel
    Left = 0
    Top = 0
    Width = 57
    Height = 57
    AutoSize = False
  end
  object ResizerLabel: TLabel
    Left = 854
    Top = 691
    Width = 76
    Height = 52
    AutoSize = False
  end
  object Panel3: TPanel
    Left = 10
    Top = 312
    Width = 237
    Height = 129
    TabOrder = 4
    object Label1: TLabel
      Left = 8
      Top = 77
      Width = 14
      Height = 13
      Caption = 'HF'
    end
    object Label2: TLabel
      Left = 8
      Top = 53
      Width = 12
      Height = 13
      Caption = 'LF'
    end
    object Label3: TLabel
      Left = 8
      Top = 29
      Width = 19
      Height = 13
      Caption = 'VLF'
    end
    object Label4: TLabel
      Left = 38
      Top = 76
      Width = 12
      Height = 13
      Caption = 'od'
    end
    object Label5: TLabel
      Left = 123
      Top = 76
      Width = 12
      Height = 13
      Caption = 'do'
    end
    object Label6: TLabel
      Left = 38
      Top = 52
      Width = 12
      Height = 13
      Caption = 'od'
    end
    object Label7: TLabel
      Left = 123
      Top = 52
      Width = 12
      Height = 13
      Caption = 'do'
    end
    object Label8: TLabel
      Left = 38
      Top = 28
      Width = 12
      Height = 13
      Caption = 'od'
    end
    object Label9: TLabel
      Left = 123
      Top = 28
      Width = 12
      Height = 13
      Caption = 'do'
    end
    object Label10: TLabel
      Left = 207
      Top = 76
      Width = 21
      Height = 13
      Caption = 'mHz'
    end
    object Label11: TLabel
      Left = 207
      Top = 52
      Width = 21
      Height = 13
      Caption = 'mHz'
    end
    object Label12: TLabel
      Left = 207
      Top = 28
      Width = 21
      Height = 13
      Caption = 'mHz'
    end
    object CSpinEdit1: TCSpinEdit
      Left = 59
      Top = 72
      Width = 57
      Height = 22
      TabOrder = 0
      OnChange = CSpinEdit1Change
    end
    object CSpinEdit3: TCSpinEdit
      Left = 59
      Top = 48
      Width = 57
      Height = 22
      TabOrder = 1
      OnChange = CSpinEdit1Change
    end
    object CSpinEdit5: TCSpinEdit
      Left = 59
      Top = 24
      Width = 57
      Height = 22
      TabOrder = 2
      OnChange = CSpinEdit1Change
    end
    object CSpinEdit2: TCSpinEdit
      Left = 146
      Top = 72
      Width = 57
      Height = 22
      TabOrder = 3
      OnChange = CSpinEdit1Change
    end
    object CSpinEdit4: TCSpinEdit
      Left = 146
      Top = 48
      Width = 57
      Height = 22
      TabOrder = 4
      OnChange = CSpinEdit1Change
    end
    object CSpinEdit6: TCSpinEdit
      Left = 146
      Top = 24
      Width = 57
      Height = 22
      TabOrder = 5
      OnChange = CSpinEdit1Change
    end
    object ButtonDefaults: TButton
      Left = 68
      Top = 99
      Width = 105
      Height = 25
      Caption = 'Set default'
      TabOrder = 6
      OnClick = ButtonDefaultsClick
    end
  end
  object Panel2: TPanel
    Left = 4
    Top = 448
    Width = 249
    Height = 57
    TabOrder = 3
    object lblAveragingColumns: TLabel
      Left = 8
      Top = 4
      Width = 160
      Height = 13
      Caption = 'Width of the triangular windows: 7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object trackAveragingColumns: TTrackBar
      Left = 8
      Top = 24
      Width = 233
      Height = 25
      Hint = 'Choose width of window'
      Max = 15
      Position = 3
      TabOrder = 0
      OnChange = trackAveragingColumnsChange
    end
  end
  object Panel1: TPanel
    Left = 4
    Top = 512
    Width = 249
    Height = 82
    TabOrder = 2
    object ScaleMaxLabel: TLabel
      Left = 7
      Top = 36
      Width = 92
      Height = 13
      Caption = 'Maksimum y skale: '
    end
    object RadioButtonAngle: TRadioButton
      Left = 8
      Top = 5
      Width = 113
      Height = 17
      Caption = 'Show phase'
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = RadioButtonAngleClick
    end
    object RadioButtonAlpha: TRadioButton
      Left = 8
      Top = 63
      Width = 113
      Height = 17
      Caption = 'Show alpha line'
      TabOrder = 3
      OnClick = RadioButtonAlphaClick
    end
    object ResetScale: TButton
      Left = 168
      Top = 32
      Width = 73
      Height = 25
      Caption = '= 4 std.dev.'
      TabOrder = 0
      OnClick = ResetScaleClick
    end
    object EditScale: TEdit
      Left = 105
      Top = 36
      Width = 57
      Height = 21
      TabOrder = 1
      Text = '0'
      OnChange = EditScaleChange
    end
  end
  object gridStats: TStringGrid
    Left = 8
    Top = 8
    Width = 241
    Height = 321
    ColCount = 2
    DefaultColWidth = 130
    DefaultRowHeight = 21
    RowCount = 20
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 0
    ColWidths = (
      130
      130)
    RowHeights = (
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21
      21)
  end
  object Chart: TChart
    Left = 259
    Top = 8
    Width = 666
    Height = 729
    Cursor = crCross
    AllowPanning = pmVertical
    BackWall.Brush.Style = bsClear
    LeftWall.Pen.Style = psClear
    Legend.Visible = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.Increment = 0.050000000000000000
    BottomAxis.LabelsSeparation = 0
    BottomAxis.Maximum = 0.500100000000000000
    BottomAxis.Minimum = -0.000100000000000000
    BottomAxis.MinorTickCount = 4
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.Axis.Style = psClear
    LeftAxis.AxisValuesFormat = '#,##0.#######'
    LeftAxis.LabelsSeparation = 100
    LeftAxis.Maximum = 1.000000000000000000
    LeftAxis.MinorTickCount = 5
    LeftAxis.MinorTickLength = 4
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.AutomaticMinimum = False
    RightAxis.AxisValuesFormat = '# ##0.#######'
    RightAxis.Grid.Style = psDashDot
    RightAxis.Increment = 30.000000000000000000
    RightAxis.LabelsSeparation = 200
    RightAxis.Maximum = 180.000000000000000000
    RightAxis.Minimum = -180.000000000000000000
    RightAxis.MinorTickCount = 4
    TopAxis.Visible = False
    View3D = False
    Zoom.Allow = False
    TabOrder = 1
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
  end
  object IzvozPodatkov: TGroupBox
    Left = 8
    Top = 600
    Width = 245
    Height = 49
    Caption = 'Data export'
    TabOrder = 5
    object Button1: TButton
      Left = 8
      Top = 16
      Width = 109
      Height = 25
      Caption = 'text'
      TabOrder = 0
      OnClick = Button1Click
    end
    object SaveGraphButton: TButton
      Left = 128
      Top = 16
      Width = 109
      Height = 25
      Caption = 'graphical'
      TabOrder = 1
      OnClick = SaveGraphButtonClick
    end
  end
  object SaveDialogText: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Text files (*.txt)|*.txt|All files|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Shrani DFT kot tekst'
    Left = 24
    Top = 688
  end
  object SaveDialogGraph: TSaveDialog
    DefaultExt = 'wmf'
    Filter = 'Grafics (*.wmf)|*.wmf|All files|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Shrani DFT kot graf'
    Left = 56
    Top = 688
  end
end
