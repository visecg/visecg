//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "simpleDocForm.h"
#include "base.h"
#include "measurement.h"
#include "docForm.h"
#include "Main.h"
#include "ini.h"
#include "eventReportSettingDocForm.h"
#include "StringSupport.h"
#include "base64.h"
#include "gitVersion.h"
#include "nevroreport.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TsDocForm *sDocForm;
//---------------------------------------------------------------------------
__fastcall TsDocForm::TsDocForm(TComponent* Owner)
	: TForm(Owner)
{
	scrollEnabled = true;
	dirty = false;
	cursor = -1;
	legendClickOccured = false;
}

void TsDocForm::LoadMeasurement(const UnicodeString _fileName, bool findHR)
{
	LibCore::CMeasurement::FileType fileType = LibCore::CMeasurement::other;
	AnsiString fileExtension = LibCore::extensionOfFilename(_fileName);
	//decode fileType from filename extension
	if (fileExtension != "")
	{
		if (fileExtension.AnsiCompareIC("nekg") == 0)
			fileType = LibCore::CMeasurement::nevroEKG;
		else if (fileExtension.AnsiCompareIC("nekge") == 0)
			fileType = LibCore::CMeasurement::nevroEKGeditable;
		else if (fileExtension.AnsiCompareIC("txt") == 0)
			fileType = LibCore::CMeasurement::txt;
		else if (fileExtension.AnsiCompareIC("afd") == 0)
			fileType = LibCore::CMeasurement::finapress;
		else if (fileExtension.AnsiCompareIC("dat") == 0)
			fileType = LibCore::CMeasurement::dekg_binary;
		else if (fileExtension.AnsiCompareIC("ecg") == 0)
			fileType = LibCore::CMeasurement::mecg_binary;
		else if (fileExtension.AnsiCompareIC("asc") == 0)
			fileType = LibCore::CMeasurement::mecg_ascii;
		else if (fileExtension.AnsiCompareIC("txt") == 0)
			fileType = LibCore::CMeasurement::dekg_ascii;
		else if (fileExtension.AnsiCompareIC("ppi") == 0)
			fileType = LibCore::CMeasurement::ppi_ascii;
		else if (fileExtension.AnsiCompareIC("rep") == 0)
			fileType = LibCore::CMeasurement::hl7x;
		else if (fileExtension.AnsiCompareIC("s2") == 0)
			fileType = LibCore::CMeasurement::s2;
	}

	if(fileType == LibCore::CMeasurement::s2 || fileType == LibCore::CMeasurement::txt){
		std::vector<std::string> files = LibCore::CMeasurement::convertS2File(_fileName);
		if(files.size() > 0){
			for(int i= 0; i<files.size(); i++){
				if(!FileExists(files[i].c_str())){
                    continue;
				}
				TDocumentForm *newForm;
				try {
					newForm = new TDocumentForm(this->GetParentComponent());
					newForm->LoadMeasurement(UnicodeString(files[i].c_str()));
					const auto ind = _fileName.Pos("_RTM.s2");
					if(findHR && (ind > 0) && files.back().compare("EXISTING")!= 0)
						newForm->processOpenedFile();
					TDocumentForm::copyFileTimestamps(_fileName, UnicodeString(files[i].c_str()));
					newForm->Close();
                    delete newForm;
				} catch (LibCore::EKGException *e) {

				}
				TsDocForm *newSimpleForm;
				try{
					newSimpleForm = new TsDocForm(this->GetParentComponent());
					newSimpleForm->LoadMeasurement(UnicodeString(files[i].c_str()));
					newSimpleForm->refreshWholeWindow(true);
				} catch (LibCore::EKGException *e) {

				}
			}
		}else{
			//TODO tell user that there is no nekge files from s2
		}

		TMainForm *tempForm = dynamic_cast<TMainForm*>(this->GetOwner());
		if(tempForm){
			tempForm->Cascade();
		}
		Close();
		return;
	}

	measurement.loadFromFile(_fileName, fileType);
	UnicodeString location =  measurement.getFileLocation();
	if(findHR && fileType == LibCore::CMeasurement::s2){
		//TODO
	}
    if (measurement.getNumChannels() == 0) {
        // nothing was loaded
        Close();
    } else {

        //load & set window components properties
	    Caption = (measurement.getMeasurementName().Length() > 0 ?
					measurement.getMeasurementName() : AnsiString(gettext("untitled")))
					+ " [" + _fileName + "]";
		xLeft = 0;
	    xRight = measurement.getDuration();
		// in order to keep loading time of very long files to minimum (preparation to draw all the data takes a long time), limit the first view to 60 seconds
		if (xRight > 60)
			xRight = 60;

		refreshWholeWindow();

	    fileName = _fileName;
	}
	dirty = false;
	Caption = measurement.getMeasurementName();
}

void TsDocForm::SaveMeasurement(UnicodeString _fileName, bool automatic)
{
	AnsiString extension = LibCore::extensionOfFilename(_fileName);
	if (!automatic && extension != "nekge")
	{
		_fileName = AnsiString(_fileName.SubString(1, _fileName.Length()-extension.Length()) + "nekge");
		UnicodeString msg(gettext("Data will be saved in Nevro-EKG file format")+" '" + _fileName + "'.");
		if (Application->MessageBox(msg.c_str(), gettext("Confirm save").data(), MB_OKCANCEL) == IDCANCEL)
		{
			Application->MessageBox(gettext("File not saved.").data(), gettext("Warning").data(), MB_OK);
			return;
		}
	}
	measurement.saveToFile(_fileName, LibCore::CMeasurement::nevroEKGeditable);
	Caption = (measurement.getMeasurementName().Length() > 0 ?
				measurement.getMeasurementName() : AnsiString(gettext("untitled")))
				+ " [" + _fileName + "]";
	fileName = _fileName;
    dirty = false;
}

void TsDocForm::refreshWholeWindow(bool automatic)
{
	measurement.refreshAllMinMaxValues();
	repaintGraph();
}

void TsDocForm::repaintGraph(bool genReport){
	//constants for standard grid view
	const double mmPermV = 10.0;
	const double mmPerS = 25.0;
	const int popLine = 5;

	//set bools for selected channel (first visible)
	int iSelected = -1;
	bool selected = false, annSelected = false, eventSelected = false;
	for(int i=0;!selected && i<measurement.getNumChannels(); i++){
		if(measurement[i].viewProps.visible){
			selected = true;
			iSelected = i;
		}
	}
	for(int i=0;!selected && i<measurement.getNumEventChannels(); i++){
		if(measurement.getEventChannel(i).viewProps.visible){
			selected = true;
			eventSelected = true;
			iSelected = i;
		}
	}
	for(int i=0;!selected && i<measurement.getNumAnnotationChannels(); i++){
		if(measurement.getAnnotationChannel(i).viewProps.visible){
			selected = true;
			eventSelected = true;
			annSelected = true;
			iSelected = i;
		}
	}

	if(!selected){
		Application->MessageBox(gettext("At least one channel must be visible! (click on legend to toggle channel visibility)").data(),
				gettext("No channel selected!").data(), MB_OK | MB_ICONWARNING);
		return;
	}

	Chart->AutoRepaint = false;  //This should speed up redraws..
	//draw signals and events
	xLeft = std::max(0.0, xLeft);
	xRight = std::min(measurement.getDuration(), xRight);
	if (xRight < xLeft + 10/measurement.getSamplingRate())
	{
		xRight = std::min(measurement.getDuration(), xLeft + 10/measurement.getSamplingRate());
		xLeft = std::max(0.0, std::min(xLeft, xRight - 10/measurement.getSamplingRate()));
	}
	if (measurement.getDuration() > 0) {
		measurement.refreshGraphValues(Chart, xLeft, xRight,  LibCore::CMeasurement::yScaleModeAll, true,
								eventSelected, annSelected, iSelected, false);
	}


	double yMax = Chart->LeftAxis->Maximum;
	double yMin = Chart->LeftAxis->Minimum;
	if(GridButton->Down && (xRight-xLeft) > (3*60)){
		Chart->BottomAxis->Grid->Visible = false;
		Chart->LeftAxis->Grid->Visible = false;
		Chart->Walls->Back->Visible = true;
		/*if(Application->MessageBox(gettext("Using standard grid view on larger area slows down your computer and might even cause a crash.\nDo you wish to keep standard grid on?").data(),
					gettext("Slow redraw warning!").data(), MB_YESNO | MB_ICONWARNING| MB_DEFBUTTON2) == IDNO){
			GridButton->Down = false;
		}
		*/

	}else if(GridButton->Down){
		Chart->Walls->Back->Visible = false;
		Chart->BottomAxis->Grid->Visible = false;
		Chart->LeftAxis->Grid->Visible = false;
		int numSeries = Chart->SeriesCount(); //save number of currently drawn series and pull them on top later

		int count = -1;
		//draw vetical grid...
		for(double pos = xLeft; pos <= xRight; pos+=(1/mmPerS)){
			count = (count+1) % 5;
			TChartShape *line = new TChartShape(Chart);
			line->HorizAxis = aBottomAxis;
			line->VertAxis = aLeftAxis;

			line->Pen->Color = (count==0)?clDkGray:clLtGray;
			line->Style = chasVertLine;
			line->ParentChart = Chart;
			line->ShowInLegend = false;
			line->Pen->Style = psSolid;

			line->X0 = pos;
			line->X1 = pos;
			line->Y0 = yMax;
			line->Y1 = yMin;
		}

		count = -1;
		//draw horizontal grid
		for(double pos = yMin; pos <= yMax; pos+=(1/mmPermV)){
			count = (count+1) % 5;
			TChartShape *line = new TChartShape(Chart);
			line->HorizAxis = aBottomAxis;
			line->VertAxis = aLeftAxis;

			line->Pen->Color = (count==0)?clDkGray:clLtGray;
			line->Style = chasHorizLine;
			line->ParentChart = Chart;
			line->ShowInLegend = false;
			line->Pen->Style = psSolid;


			line->X0 = xRight;
			line->X1 = xLeft;
			line->Y0 = pos;
			line->Y1 = pos;
		}

        //put series on top of grid (looks nicer)
		int finalNumSeries = max(0, Chart->SeriesCount() - 1 - numSeries);
		for(int i=0; (i < numSeries); i++)  {
			if(i < finalNumSeries){
				Chart->ExchangeSeries(i, finalNumSeries + i);
			}else{
				break;
			}
		}

	}else{
		Chart->Walls->Back->Visible = false;
		Chart->BottomAxis->Grid->Visible = true;
		Chart->LeftAxis->Grid->Visible = true;
	}

    //draw cursor
	if(cursor >= xLeft && cursor <= xRight){
			TChartShape *line = new TChartShape(Chart);
			line->HorizAxis = aBottomAxis;
			line->VertAxis = aLeftAxis;

			line->Pen->Color = clBlue;
			line->Style = chasVertLine;
			line->ParentChart = Chart;
			line->ShowInLegend = false;
			line->Pen->Style = psSolid;

			line->X0 = cursor;
			line->X1 = cursor;
			line->Y0 = yMax;
			line->Y1 = yMin;
	}

	Chart->AutoRepaint = true;
    Chart->Repaint();
}

//---------------------------------------------------------------------------

void __fastcall TsDocForm::FormResize(TObject *Sender)
{
    ToolBar1->Width = ClientWidth;

    Chart->Top = ToolBar1->Height;
	Chart->Width = ClientWidth;
	Chart->Height = ClientHeight - (ToolBar1->Height + 16);
    Chart->Repaint();
	ScrollBar1->Width = ClientWidth;
	ScrollBar1->Top = ClientHeight - 16;
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::ScrollBar1Change(TObject *Sender)
{
	if(scrollEnabled){
		//position changes, zoom stays the same
		double center = measurement.getDuration() * (((ScrollBar1->Position*1.0)+(ScrollBar1->PageSize/2)) / ScrollBar1->Max);
		moveGraphToCenter(center, xRight - xLeft);
	}
}
//---------------------------------------------------------------------------
void TsDocForm::updateScrollbar(){
	double width = ((xRight-xLeft) / measurement.getDuration()) * ScrollBar1->Max ;
	double center = (xLeft+xRight)/2.0;
	int pos =  (ScrollBar1->Max * ( center / measurement.getDuration())) - (width/2);

	scrollEnabled = false;

	ScrollBar1->PageSize = max(0, min(ScrollBar1->Max,(int) width));
	ScrollBar1->Position = pos;
	scrollEnabled = true;
}

void __fastcall TsDocForm::ChartMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta,
		  TPoint &MousePos, bool &Handled)
{
	//if ctrl prssed -> zooom
	if (Shift.Contains(ssCtrl)){
		double delta = 2;
		if(WheelDelta < 0)
			delta = 0.5;
		zoomX(delta, Chart->Series[0]->XScreenToValue(Chart->GetCursorPos().x));
	}
    //just scroll
	else{
		double zoom = xRight-xLeft;
		double delta = 0.1 * zoom; //move 10% to either direction
		if(WheelDelta < 0)
			delta = -delta;

		moveGraphToCenter((xRight+xLeft)/2 - delta, zoom);
	}
	Handled = true;
}

void TsDocForm::zoomX(double factor, double center)
{
    //calculate values
	double zoomValue = xRight-xLeft;
	zoomValue = zoomValue / factor;
    //move and repaint
	moveGraphToCenter(center, zoomValue);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::ChartClickLegend(TCustomChart *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	legendClickOccured = true;
	//TODO http://www.teechart.net/support/viewtopic.php?f=3&t=10858
	int clickedIndex = Chart->Legend->Clicked(X,Y);
	if(clickedIndex != -1){
		if(clickedIndex < measurement.getNumChannels()){
			//signal
            measurement[clickedIndex].viewProps.visible = !measurement[clickedIndex].viewProps.visible;
		}else if(clickedIndex < measurement.getNumChannels() + measurement.getNumEventChannels()){
			//event
			measurement.getEventChannel(clickedIndex - measurement.getNumChannels()).viewProps.visible =
			!measurement.getEventChannel(clickedIndex - measurement.getNumChannels()).viewProps.visible;
		}else{
			//annotation
			measurement.getAnnotationChannel(clickedIndex - measurement.getNumChannels() - measurement.getNumEventChannels()).viewProps.visible =
			!measurement.getAnnotationChannel(clickedIndex - measurement.getNumChannels() - measurement.getNumEventChannels()).viewProps.visible;
        }
	}
	repaintGraph();

}
//---------------------------------------------------------------------------

void TsDocForm::moveGraphToCenter(double center, double width){
	xLeft = max(0.0, center - width/2.0);
	xRight = min(measurement.getDuration(), xLeft + width);
	if(xRight != xLeft + width)
		xLeft = max(0.0, xRight-width);
	//update scroller
	updateScrollbar();

	//repaintGraph
	repaintGraph();
}



void __fastcall TsDocForm::Button2Click(TObject *Sender)
{
	zoomX(2, (xRight+xLeft)/2);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button3Click(TObject *Sender)
{
		if(cursor>=0)
			moveGraphToCenter(cursor, 10);
		else
			moveGraphToCenter((xRight+xLeft)/2, 10);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button4Click(TObject *Sender)
{
	if(cursor>=0)
		moveGraphToCenter(cursor, 30);
	else
		moveGraphToCenter((xRight+xLeft)/2, 30);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button5Click(TObject *Sender)
{
	if(cursor>=0)
		moveGraphToCenter(cursor, 60);
	else
		moveGraphToCenter((xRight+xLeft)/2, 60);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button6Click(TObject *Sender)
{
	if(cursor>=0)
		moveGraphToCenter(cursor, 600);
	else
		moveGraphToCenter((xRight+xLeft)/2, 600);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button7Click(TObject *Sender)
{
	if(cursor>=0)
		moveGraphToCenter(cursor, 60*30);
	else
		moveGraphToCenter((xRight+xLeft)/2, 60*30);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button8Click(TObject *Sender)
{
	if(cursor>=0)
		moveGraphToCenter(cursor, 3600);
	else
		moveGraphToCenter((xRight+xLeft)/2, 3600);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button9Click(TObject *Sender)
{
	if(cursor>=0)
		moveGraphToCenter(cursor, 3600*4);
	else
		moveGraphToCenter((xRight+xLeft)/2, 3600*4);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button10Click(TObject *Sender)
{
	moveGraphToCenter((xRight+xLeft)/2, measurement.getDuration());
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::Button11Click(TObject *Sender)
{
	Generirajporoilo();
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::ChartMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	if(legendClickOccured){
		legendClickOccured = false;
        return;
	}
	double clickedTime = Chart->Series[0]->XScreenToValue(X);
	if(Button == mbRight){
        // if no cursor set -> set cursor where right button on mouse was clicked, and recenter on event
		clickedTime = (cursor>=0)?cursor:clickedTime;
        cursor = clickedTime;
		moveGraphToCenter(clickedTime, xRight-xLeft);
		repaintGraph();

        //query user for annotation text
		UnicodeString NewString = "";
		if(!InputQuery(gettext("Add event."),gettext("Enter annotation text"), NewString))
			return;
		LibCore::Annotation newAnn(clickedTime, NewString, 0.01, 3);

		if(measurement.getNumAnnotationChannels()<1)
			measurement.addAnnotationChannel("Annotation", "");
		LibCore::CAnnotationChannel& chan = measurement.getAnnotationChannel(0);
		chan.append(newAnn);
		sortAllEventChannels();
		if(chan.getNumElems() == 1)
			chan.viewProps.visible = true;
		dirty = true;
        chan.refreshMinMaxValue();
		repaintGraph();
	}else if(Button == mbLeft){
		if(abs(clickedTime-cursor) < 0.1)
			cursor = -1;
		else
			cursor = clickedTime;
		repaintGraph();
	}
}
//---------------------------------------------------------------------------

bool TsDocForm::SaveMeasurement(){
	measurement.saveToFile(measurement.getFileLocation(), LibCore::CMeasurement::FileType::nevroEKGeditable);
}

void TsDocForm::sortAllEventChannels(){
	int n = measurement.getNumEventChannels();
	for(int i= 0; i<n;i++)
		measurement.getEventChannel(i).sortEventsByAscendingTime();
	n = measurement.getNumAnnotationChannels();
	for(int i= 0; i<n;i++)
		measurement.getAnnotationChannel(i).sortEventsByAscendingTime();
}


//Converts to UTF8 and prints out
void printBase64Tag(FILE* file, std::string tag, UnicodeString text){
	RawByteString utf8String = System::UTF8Encode(text);
	if(text.Length() > 0){
		std::string result = base64_encode(utf8String.c_str(), utf8String.Length());
		fprintf(file, "<%s>%s</%s>\n", tag.c_str(),result.c_str(), tag.c_str());
	}
	else{
	  fprintf(file, "<%s></%s>\n", tag.c_str(), tag.c_str());
	}
}

//convert to UTF8 in process
std::string unicode2base64(UnicodeString text){
	RawByteString utf8String = System::UTF8Encode(text);
	return base64_encode(utf8String.c_str(), utf8String.Length());
}

void TsDocForm::Generirajporoilo()
{
	UnicodeString rootReportFolder = rootFolder+"\\reportEvent_"+TDateTime::CurrentDateTime().FormatString("yyyy_mm_dd-hh_nn_ss");
	UnicodeString indexfileloc = rootReportFolder + "\\indexEvent.txt";
	CreateDirectory(rootReportFolder.c_str(), NULL);

	FILE *file = _wfopen(indexfileloc.c_str(), L"wb");
	if (file == NULL)
		return;

	std::string author, user, age, weight, born, sex, electrodePosition, reportLength;

	double timeAround = ((xRight-xLeft)/2)/60;
	double report_ecgLineLength = 15;
	double report_ecgLineMvHeight = 10;
	double report_ecgLineNumMv = 3;
	double report_ecgLineSecondLength = 1.25;

	UnicodeString settingsFname("nastavitve.ini");
	for (int i = Application->ExeName.Length()-1; i >= 0; --i){
		if (Application->ExeName[i] == '\\') {
			settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
			break;
		}
	 }
	Ini::File ini(settingsFname.c_str());
	ini.loadVar(author, L"report author");
	ini.loadVar(user, L"report user");
	ini.loadVar(age, L"report age");
	ini.loadVar(weight, L"report weight");
	ini.loadVar(sex, L"report user_sex");
	ini.loadVar(born, L"report user_born");
	ini.loadVar(electrodePosition, L"ecg position");
	ini.loadVar(reportLength, L"event time length");

	try{
		ini.loadVar(report_ecgLineLength, L"report ecg line length");
		ini.loadVar(report_ecgLineMvHeight, L"report ecg mV height");
		ini.loadVar(report_ecgLineNumMv, L"report ecg mV shown");
		ini.loadVar(report_ecgLineSecondLength, L"report ecg length of second");
	}catch(Exception *e){
	}
	EventreportOnDocSettings->AuthorInp->Text 	= string2WideString(author.c_str());
	EventreportOnDocSettings->UserInp->Text   	= string2WideString(user.c_str());
	EventreportOnDocSettings->AgeInp->Text    	= string2WideString(age.c_str());
	EventreportOnDocSettings->WeightInp->Text 	= string2WideString(weight.c_str());
	EventreportOnDocSettings->timeMinutes->Text 	= AnsiString(timeAround);
	EventreportOnDocSettings->GenderInp->Text 	= string2WideString(sex.c_str());
	EventreportOnDocSettings->BirthInp->Text 		= string2WideString(born.c_str());
	EventreportOnDocSettings->PositionInp->Text 	= string2WideString(electrodePosition.c_str());

	EventreportOnDocSettings->Edit2->Text = UnicodeString(report_ecgLineLength);
	EventreportOnDocSettings->Edit3->Text = UnicodeString(report_ecgLineSecondLength);
	EventreportOnDocSettings->Edit4->Text = UnicodeString(report_ecgLineNumMv);
	EventreportOnDocSettings->Edit5->Text = UnicodeString(report_ecgLineMvHeight);

	while(true){
		if(EventreportOnDocSettings->ShowModal()!=mrOk){
			fclose(file);
			TSearchRec sr;
			if (FindFirst(rootReportFolder+"\\*", faReadOnly, sr) == 0)
			{
				do{
					DeleteFile(rootReportFolder+"\\"+sr.Name);
				} while (FindNext(sr) == 0);
				FindClose(sr);
			}
			RemoveDir(rootReportFolder);
			return;
		}else{
			break;
		}
	}

	author = wideString2String(EventreportOnDocSettings->AuthorInp->Text);
	user = wideString2String(EventreportOnDocSettings->UserInp->Text);

	try {
		sex = 				wideString2String(EventreportOnDocSettings->GenderInp->Text);
		born  = 			wideString2String(EventreportOnDocSettings->BirthInp->Text);
		electrodePosition = wideString2String(EventreportOnDocSettings->PositionInp->Text);
		timeAround = 		std::max(0.25, EventreportOnDocSettings->timeMinutes->Text.ToDouble());
		age = 				wideString2String(EventreportOnDocSettings->AgeInp->Text);
		weight = 			wideString2String(EventreportOnDocSettings->WeightInp->Text);
		reportLength = 		AnsiString(EventreportOnDocSettings->timeMinutes->Text).c_str();
	} catch(Exception *e) {
		// wrong version for parse.. use defaults
		age = "-1";
		weight = "-1";
		reportLength = 4;
	}

	try{
		report_ecgLineLength = 			EventreportOnDocSettings->Edit2->Text.ToDouble();
		report_ecgLineSecondLength = 	EventreportOnDocSettings->Edit3->Text.ToDouble();
		report_ecgLineNumMv = 			EventreportOnDocSettings->Edit4->Text.ToDouble();
		report_ecgLineMvHeight = 		EventreportOnDocSettings->Edit5->Text.ToDouble();
	}catch(Exception *e){

	}

	ini.storeVar(author, L"report author");
	ini.storeVar(user, L"report user");
	ini.storeVar(age, L"report age");
	ini.storeVar(weight, L"report weight");
	ini.storeVar(sex, L"report user_sex");
	ini.storeVar(born, L"report user_born");
	ini.storeVar(electrodePosition, L"ecg position");
	ini.storeVar(report_ecgLineLength, L"report ecg line length");
	ini.storeVar(report_ecgLineMvHeight, L"report ecg mV height");
	ini.storeVar(report_ecgLineNumMv, L"report ecg mV shown");
	ini.storeVar(report_ecgLineSecondLength, L"report ecg length of second");
	ini.save();

	//draw leading images
	//UnicodeString imgloc = rootReportFolder+"\\overview.wmf";
	//UnicodeString imglocdeviation = rootReportFolder+"\\meanAndDev.wmf";
	//GanttChart->SaveToMetafile(imgloc);
	//GanttChart->SaveToMetafile(imglocdeviation);

	// create index file
	nevroreport::ReportPrinter reporter;
    std::string temp;
	//patient
	temp = nevroreport::create_node_borland("name", EventreportOnDocSettings->UserInp->Text);
	temp += nevroreport::create_node_borland("age", EventreportOnDocSettings->AgeInp->Text);
	temp += nevroreport::create_node_borland("weight", EventreportOnDocSettings->WeightInp->Text);
	temp += nevroreport::create_node_borland("born", EventreportOnDocSettings->BirthInp->Text);
	temp += nevroreport::create_node_borland("gender", EventreportOnDocSettings->GenderInp->Text);
	temp += nevroreport::create_node_borland("ECGposition", EventreportOnDocSettings->PositionInp->Text);
	reporter.add_root_node("patient", temp);

	//settings
	temp = nevroreport::create_node_d("ecgLineLength", report_ecgLineLength);
	temp += nevroreport::create_node_d("ecgLineMvHeight", report_ecgLineMvHeight);
	temp += nevroreport::create_node_d("ecgLineNumMv", report_ecgLineNumMv);
	temp += nevroreport::create_node_d("ecgLineSecondLength", report_ecgLineSecondLength);
	reporter.add_root_node("settings", temp);

	reporter.add_root_node_borland("userComment", EventreportOnDocSettings->Memo1->Text);

	int countEvents = 0;

	TDocumentForm * newForm = new TDocumentForm(this);
	newForm->LoadMeasurement(measurement.getFileLocation());
	LibCore::CMeasurement * meas;
    meas = newForm->getMeasurementPointer();

	//event
	UnicodeString folName = rootFolder;
	folName = folName.SubString(1+folName.LastDelimiter("\\"), folName.Length());
	temp = nevroreport::create_node("name","");
	temp += nevroreport::create_node_borland("time", (meas->getDate() + ((xRight+xLeft)/2)/86400).FormatString("yyyy mmm dd; hh:nn:ss"));
	temp += nevroreport::create_node_d("measurementTime", (xRight+xLeft)/2);


	UnicodeString fileName = meas->getFileLocation();
	int locationOfSlash = 1 + fileName.LastDelimiter("\\");
	fileName = fileName.SubString(locationOfSlash, fileName.Length());

	temp += nevroreport::create_node_borland("filename", fileName);
	temp += nevroreport::create_node_borland("fileStart", meas->getDate().FormatString("yyyy mmm dd; hh:nn:ss"), false);

	reporter.add_root_node("event", temp);


	//annotationGraphs

	//graph
	UnicodeString title = "" + (meas->getDate() + ((xRight+xLeft)/2)/86400).FormatString("yyyy mmm dd, hh:nn:ss");
	temp = nevroreport::create_node_borland("title", title);

	AnsiString substr, substr1;
	int lenSubstr;
	AnsiString hw ="", sw="";
	bool certified = false;
	if(meas->getGadgetVersion().Length() > 0){
		hw = meas->getGadgetVersion();
		try{
			hw = hw.SubString(1, hw.Pos("git")-3);
			int x1 =  hw.Pos("fw=")+3, x2 = 5;
			AnsiString fw =  hw.SubString(x1,x2);
			x1 = hw.Pos("hw=")+4;
			x2 = 9;
			AnsiString hw1 =  hw.SubString(x1,x2);
			auto r1 = fw.AnsiCompare("1.0.4"), r2 = hw1.AnsiCompare("Savvy 1.2");
			if( 0 < r1){
				if( 0 < r2){
					certified = true;

				}else{
					certified = false;
				}
			}else{
				certified = false;
            }
		}catch(...){
			certified = false;
		}
		hw = meas->getGadgetMac() + "/" + hw;
		temp += nevroreport::create_node_borland("gadgetVersion", hw);
	}
	if(meas->getMobEcgVersion().Length() > 0){
		sw = meas->getMobEcgVersion();
		try{
			sw = sw.SubString(sw.Pos("MobECG ")+7, 100);
			sw = sw.SubString(1, sw.Pos("-")-1);
		}catch(...){
	        certified = false;
		}

		if(certified && (0 < sw.AnsiCompare("1.8.1"))){
			sw += " [EN ISO 60601-2-47:2012]";
		}
		temp += nevroreport::create_node_borland("MobEcgVersion", sw);
	}

	temp += nevroreport::create_node_d("frequency", meas->getSamplingRate());

	LibCore::CChannel sampleChannel = (*meas)[0];
	temp += nevroreport::create_node_d("errorValue", sampleChannel.getMinValue());
	int startIndex = std::min(sampleChannel.getNumElems(), std::max(0, (int)((((xRight+xLeft)/2) - timeAround*60)*meas->getSamplingRate())));
	int stopIndex =  std::min(sampleChannel.getNumElems(), std::max(0, (int)((((xRight+xLeft)/2) + timeAround*60)*meas->getSamplingRate())));
	temp += nevroreport::create_node_borland("timestamp", (meas->getDate() + ((xRight+xLeft)/2)/86400).FormatString("yyyy mm dd hh:nn:ss"));
	temp += nevroreport::create_node_d("startOffset", meas->sampleIndex2Time(startIndex) - ((xRight+xLeft)/2));

	//bin file
	UnicodeString binFileLocation = rootReportFolder + "\\bin_EVENT_SAMPLES_samples.bin";
	FILE *fileBin = _wfopen(binFileLocation.c_str(), L"wb");
	fwrite(&sampleChannel[startIndex], sizeof(double), stopIndex-startIndex, fileBin);
	fclose(fileBin);


	temp += nevroreport::create_node_borland("data", binFileLocation);

	temp = nevroreport::create_node("graph", temp, false);

    reporter.add_root_node("annotationGraphs", temp);

    newForm->Close();
	delete newForm;

	fseek(file, 0, SEEK_END);
	reporter.print_to_file(file, EventreportOnDocSettings->AuthorInp->Text);

	fclose(file);

    //   Create pdf
	UnicodeString convFilename; //conversion program name
	convFilename = Application->ExeName;
	convFilename.Delete(convFilename.LastDelimiter("\\\\"),100);
	convFilename +="\\resources\\NevroReportToPDF.exe";
	TSearchRec sr;

    SHELLEXECUTEINFO info = {0};
	info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = NULL;
	info.lpVerb = NULL;
	info.lpFile = convFilename.c_str(); //program


	UnicodeString pathToPDF = rootReportFolder + "_" + AnsiString((xRight+xLeft)/2) + ".pdf";

	UnicodeString params = "\"" +rootReportFolder+"\"" + " \""+ pathToPDF + "\"";
	info.lpParameters = params.c_str(); //file to convert

    info.lpDirectory = rootFolder.c_str(); //folder
	info.nShow = SW_HIDE;
	info.hInstApp = NULL;
	ShellExecuteEx(&info);

	WaitForSingleObject(info.hProcess,INFINITE);

	unsigned long exitCode = 0;
	GetExitCodeProcess(info.hProcess, &exitCode);
	TerminateProcess(info.hProcess,0);
	CloseHandle(info.hProcess);

    UnicodeString exitString;
    if(exitCode == 0){

		exitString = gettext("Report successfully created in the file")+ " " + pathToPDF +
			". \n" + gettext("Do you want to open it?");
		if(IDYES == Application->MessageBox(exitString.c_str(), gettext("Completed successfully").data(), MB_YESNO)){
			 ShellExecute(NULL, L"open", pathToPDF.c_str(), NULL, NULL, SW_SHOWDEFAULT);
        }

        TSearchRec sr;

    	if (FindFirst(rootReportFolder+"\\*", faReadOnly, sr) == 0)
	    {
		    do{
				DeleteFile(rootReportFolder+"\\"+sr.Name);
    		} while (FindNext(sr) == 0);
	    	FindClose(sr);
    	}
        RemoveDir(rootReportFolder);
    }else{
		exitString = gettext("Report was not created in folder") + " " +
			rootReportFolder + gettext(" ;") + gettext("exitCode") + ": " + AnsiString(exitCode);
        if(exitCode)
            exitString = gettext("Report not created. Please close applications that may use file!");
		Application->MessageBox(exitString.c_str(), gettext("Error").data(), MB_OK);
    }
}
//----------------------------------------------------

void __fastcall TsDocForm::ToolButton2Click(TObject *Sender)
{
	zoomX(0.5, (xRight+xLeft)/2);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::ToolButton3Click(TObject *Sender)
{
	zoomX(2, (xRight+xLeft)/2);
}
//---------------------------------------------------------------------------

bool __fastcall TsDocForm::CloseQuery(){
	if (dirty)
	{
		switch (Application->MessageBox(
					gettext("Do you want to save changes?").data(),
					gettext("VisECG").data(),
					MB_ICONQUESTION | MB_YESNOCANCEL))
		{
		case IDYES:
			try {
				SaveMeasurement(fileName);
			} catch (LibCore::EKGException *e) {
				Application->MessageBox(e->Message.c_str(),
					gettext("Error while saving file").data(), MB_OK);
				return false;
			}
		case IDNO:
			break;
		default:
			return false;
		}
	}
	return true;
}

void __fastcall TsDocForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	Action = caFree;
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::GridButtonClick(TObject *Sender)
{
	repaintGraph();
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::moveLeftClick(TObject *Sender)
{
	double center = (xRight+xLeft)/2;
	double width = xRight-xLeft;
	moveGraphToCenter(center - width, width);
}
//---------------------------------------------------------------------------

void __fastcall TsDocForm::moveRightClick(TObject *Sender)
{
	double center = (xRight+xLeft)/2;
	double width = xRight-xLeft;
	moveGraphToCenter(center + width, width);
}
//---------------------------------------------------------------------------

