//---------------------------------------------------------------------------

#ifndef eventReportSettingDocFormH
#define eventReportSettingDocFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TEventreportOnDocSettings : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label4;
	TLabel *Label6;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *Label9;
	TLabel *Label5;
	TLabel *Label10;
	TLabel *Label11;
	TLabel *Label12;
	TLabel *Label13;
	TLabel *Label14;
	TLabel *Label15;
	TLabel *Label16;
	TButton *OkButton;
	TButton *CancelButton;
	TEdit *timeMinutes;
	TEdit *AuthorInp;
	TEdit *UserInp;
	TEdit *AgeInp;
	TEdit *WeightInp;
	TEdit *GenderInp;
	TEdit *BirthInp;
	TEdit *PositionInp;
	TEdit *Edit2;
	TEdit *Edit3;
	TEdit *Edit4;
	TEdit *Edit5;
	TMemo *Memo1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall timeMinutesChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TEventreportOnDocSettings(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TEventreportOnDocSettings *EventreportOnDocSettings;
//---------------------------------------------------------------------------
#endif
