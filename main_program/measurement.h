// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


#ifndef _INCLUDE_LIBCORE_MEASUREMENT_H_
#define _INCLUDE_LIBCORE_MEASUREMENT_H_

#include "base.h"
#include <stdarg.h>
#include <VclTee.Chart.hpp>
#include <VclTee.Series.hpp>
#include <VclTee.TeeShape.hpp>
#include <sys/stat.h>
#include <dstring.h>
#include "instantDialog.h"

#include <string>
#include <vector>

namespace LibCore
{

class CMeasurement
//class containing measurement data + a number of double and event channels
//maxNumChannels are always included, whether used or not; unused channels have little allocated memory
{
public:
	static const int maxNumChannels = 100;
	static const double maxMeasurementLength;
	AnsiString markedChName;;

protected:
    int nekgFileVersion;
    AnsiString gadgetVersion;
    AnsiString gadgetMac;
    AnsiString mobEcgVersion;
	CChannel channel[maxNumChannels];
	CEventChannel eventChannel[maxNumChannels];
    CAnnotationChannel annotationChannel[maxNumChannels];

	int numChannels, numEventChannels, numAnnotationChannels;
 	double samplingRate; //all double channels have the same sampling rate

	UnicodeString
		fileLocation,
		measurementName;
	AnsiString comment;

	bool editable;		//if false, original measurement must remain intact
	bool markCut;
	TDateTime date;     //date-time when measurement was taken

    bool nonInteractiveParameterHackOn; // MD: hack for implementing scripted tasks on top of existing interactive functions
    double nonInteractiveParameterD1;
    double nonInteractiveParameterD2;

public:
	CPatientData patientData;
    int numStatData; //number of statistical data saved in file
    TStringList *statDataString;
    double *statDataData;
    static TColor getChannelColor(int channelNum);

    AnsiString electrodePosition; //TODO <- this represents position of the electrode, should be updated when MobECG will support this

	CMeasurement()
	{
        markedChName =  "Marked changes";
        nonInteractiveParameterHackOn = false;
        gadgetVersion ="";
        gadgetMac = "";
        mobEcgVersion = "";
		editable = false;
        markCut = false;
		date = 0;
        nekgFileVersion = 3;
		measurementName = "";
        fileLocation = "";
		comment = "";
		samplingRate = 500;
        numStatData = 0;
		numChannels = 1;
		numEventChannels = 0;
        electrodePosition = "";
		for (int i = 0; i < maxNumChannels; i++)
		{
			char buf[1000];
			sprintf(buf, "channel %d", i+1);
			channel[i].setChannelName(AnsiString(buf));
			sprintf(buf, "event channel %d", i+1);
			eventChannel[i].setChannelName(AnsiString(buf));
		}

	}
	CMeasurement(const int _numChannels, const int _numEventChannels, const double _samplingRate)
	{
			markedChName =  "Marked changes";
		assert(_numChannels > 0 && _numChannels <= maxNumChannels);
		assert(_numEventChannels > 0 && _numEventChannels <= maxNumChannels);
		assert(_samplingRate > 0);
        gadgetVersion ="";
        gadgetMac = "";
        mobEcgVersion = "";
		editable = false;
        markCut = false;
		date = 0;
        nekgFileVersion = 3;
		measurementName = "";
        fileLocation = "";
		comment = "";
		samplingRate = _samplingRate;
		numChannels = _numChannels;
		numEventChannels = _numEventChannels;
        numStatData = 0;
        electrodePosition = "";
		for (int i = 0; i < maxNumChannels; i++)
		{
			char buf[1000];
			sprintf(buf, "channel %d", i+1);
			channel[i].setChannelName(AnsiString(buf));
			sprintf(buf, "event channel %d", i+1);
			eventChannel[i].setChannelName(AnsiString(buf));
		}
	}

    //Ivant 201109
    //it seems I need copy constructor
    CMeasurement(const CMeasurement& orig)
	{
            markedChName =  "Marked changes";
        numChannels=orig.maxNumChannels;
        numEventChannels=orig.maxNumChannels;
        samplingRate=orig.samplingRate;
        numStatData = orig.numStatData;

        nekgFileVersion = orig.nekgFileVersion;
		measurementName=orig.measurementName;
        fileLocation = orig.fileLocation;
		comment=orig.comment;
        electrodePosition = orig.electrodePosition; 
        editable=orig.editable;
        date=orig.date;

    }

    // unfortunately there is no garbage collector :)
    // IvanT201109
    ~CMeasurement()
    {
    }

    UnicodeString getFileLocation() const
	{
		return fileLocation;
	}
	AnsiString getMeasurementName() const
	{
		return measurementName;
	}
	void setMeasurementName(const AnsiString &_name)
	{
		measurementName = _name;
	}
	TDateTime getDate() const
	{
		return date;
	}
	void setDate(TDateTime _date)
	{
        date = _date;
	}
	int getNumChannels()
	{
		return numChannels;
	}                                                                                                     
	int getNumEventChannels() const
	{
		return numEventChannels;
	}
    int getNumAnnotationChannels() const
	{
		return numAnnotationChannels;
	}

    //IvanT
    int getNumSamples()
    {
        return (this->channel[0]).getNumElems();
    }
	double getDuration() const//returns duration in seconds
	{
		return channel[0].getNumElems()/samplingRate;
	}
    double getDurationFromFile()//returns duration in seconds for folder overview
	{
		return channel[0].getNumElemsFile()/samplingRate;
	}
	double getSamplingRate() const
	{
		return samplingRate;
	}
	void setSamplingRate(const double _samplingRate)
	{
		assert(_samplingRate > 0);
		samplingRate = _samplingRate;
	}
    //IvanT
    void setNumChannels(const int _numChannels)
	{
		assert(_numChannels > 0);
		numChannels = _numChannels;
	}
	bool getEditable() const
	{
		return editable;
	}
	AnsiString getComment() const
	{
		return comment;
	}
	void setComment(const AnsiString &_comment, bool timeStamp = true)
	{
      if (timeStamp)
         setCommentWithTimeStamp(comment, _comment);
      else
         comment = _comment;
	}
   void appendComment(const AnsiString &textToAdd, bool timeStamp = true)
	{
      if (timeStamp)
         appendCommentWithTimeStamp(comment, textToAdd);
      else
         comment.Insert(textToAdd, comment.Length()+1);
	}

	int time2SampleIndex(double time)
	{
		assert(time >= 0);
		return int(time*samplingRate+0.5);
	}
	double sampleIndex2Time(int i)
	{
		assert(i >= 0);
		return double(i)/samplingRate;
	}
    double sampleIndex2Time(double d)
	{
		assert(d >= 0.0);
		return d/samplingRate;
	}
	CChannel &operator[](const int channelNum) //address i'th sample of ch. 2 with measurement[2][i]
	{
		assert(channelNum >= 0 && channelNum < numChannels);
		return channel[channelNum];
	}
	CEventChannel &getEventChannel(const int channelNum)//analog to [], but for event channels
	{
		assert(channelNum >= 0 && channelNum < numEventChannels);
		return eventChannel[channelNum];
	}
    CAnnotationChannel &getAnnotationChannel(const int channelNum)//analog to [], but for event channels
	{
		assert(channelNum >= 0 && channelNum < numAnnotationChannels);
		return annotationChannel[channelNum];
	}
	int findChannelByName(AnsiString channelName, int startWith = 0, bool partial = false, bool isEventChan = false)
	{
		if (isEventChan)
			return findEventChannelByName(channelName, startWith, partial);

		for (int i = startWith; i < getNumChannels(); i++)
			if (channel[i].getChannelName() == channelName)
				return i;
		if (partial) {
			for (int i = startWith; i < getNumChannels(); i++)
				if (channel[i].getChannelName().Pos(channelName) == 1)
					return i;
		}
		return -1;
	}
	int findEventChannelByName(AnsiString eventChannelName, int startWith = 0, bool partial = false)
	{
		for (int i = startWith; i < getNumEventChannels(); i++)
			if (eventChannel[i].getChannelName() == eventChannelName)
				return i;

		if (partial) {
			for (int i = startWith; i < getNumEventChannels(); i++)
				if (eventChannel[i].getChannelName().Pos(eventChannelName) == 1)
					return i;
		}
		return -1;
	}

    int findAnnotationChannelByName(AnsiString eventChannelName, int startWith = 0, bool partial = false)
	{
		for (int i = startWith; i < getNumAnnotationChannels(); i++)
			if (annotationChannel[i].getChannelName() == eventChannelName)
				return i;

		if (partial) {
			for (int i = startWith; i < getNumAnnotationChannels(); i++)
				if (annotationChannel[i].getChannelName().Pos(eventChannelName) == 1)
					return i;
		}
		return -1;
	}

    double signalValue(int channelNum, double time) {
        assert(time >= 0);
        assert((channelNum >=0) && (channelNum < numChannels));
        double index = time*samplingRate;
		int i1 = (int)floor(index);
        int i2 = (int)ceil(index);
        assert(i2 < channel[channelNum].getNumElems());
        
        if (i1 == i2) {
            return channel[channelNum][i1];
        } else {
            double v1 = channel[channelNum][i1];
            double v2 = channel[channelNum][i2];
            return v1 * (i2 - index) + v2 * (index - i1);
        }
    }
    
    double eventValue(int channelNum, double time) {
        assert(time >= 0);
        assert((channelNum >=0) && (channelNum < numEventChannels));
        int i1 = eventChannel[channelNum].getIndexClosestToTime(time);
        int i2 = ((eventChannel[channelNum][i1].time < time) || (i1 == 0)) ? i1 + 1 : i1 - 1;
        
        if (eventChannel[channelNum][i1].time == time) {
            return eventChannel[channelNum][i1].value;
        } else {
            assert(i2 < eventChannel[channelNum].getNumElems());
            double v1 = eventChannel[channelNum][i1].value;
            double v2 = eventChannel[channelNum][i2].value;
            return (v1 * (eventChannel[channelNum][i2].time - time) + 
                v2 * (time - eventChannel[channelNum][i1].time)) / 
                (eventChannel[channelNum][i2].time - eventChannel[channelNum][i1].time);
        }
    }

	//deleting channels is an inefficient operation because channels are stored in a table
	void deleteChannel(const int channelNum)
	{
		assert(channelNum >= 0 && channelNum < numChannels);
		for (int i = channelNum; i < numChannels-1; i++)
			channel[i] = channel[i+1];
		channel[numChannels-1].emptyTable(1);
		numChannels--;
	}
	void duplicateChannel(const int channelNum)
	{
		int duplicateIndex = addChannel("", "");
		channel[duplicateIndex] = channel[channelNum];
		channel[duplicateIndex].setChannelName("Kopija " + channel[channelNum].getChannelName());
		channel[duplicateIndex].viewProps.offset = 1;
	}
	void deleteEventChannel(const int channelNum)
	{
        if(channelNum < 0  || channelNum >= numEventChannels)
            int xxxx = 0;
		assert(channelNum >= 0 && channelNum < numEventChannels);
		for (int i = channelNum; i < numEventChannels-1; i++)
			eventChannel[i] = eventChannel[i+1];
		eventChannel[numEventChannels-1].emptyTable(1);
		numEventChannels--;
	}
    void deleteAnnotationChannel(const int channelNum)
	{
		assert(channelNum >= 0 && channelNum < numAnnotationChannels);
		for (int i = channelNum; i < numAnnotationChannels-1; i++)
			annotationChannel[i] = annotationChannel[i+1];
		annotationChannel[numAnnotationChannels-1].emptyTable(1);
		numAnnotationChannels--;
	}
	void duplicateEventChannel(const int channelNum)
	{
		int duplicateIndex = addEventChannel("", "");
		eventChannel[duplicateIndex] = eventChannel[channelNum];
		eventChannel[duplicateIndex].setChannelName("Kopija " + eventChannel[channelNum].getChannelName());
		eventChannel[duplicateIndex].viewProps.offset = 1;
	}
    void duplicateAnnotationChannel(const int channelNum)
	{
		int duplicateIndex = addAnnotationChannel("", "");
		annotationChannel[duplicateIndex] = annotationChannel[channelNum];
		annotationChannel[duplicateIndex].setChannelName("Kopija " + annotationChannel[channelNum].getChannelName());
		annotationChannel[duplicateIndex].viewProps.offset = 1;

	}
	int addChannel(const AnsiString _channelName, const AnsiString _measurementUnit)
		//adds a channel to end of table
	{
		assert(numChannels < maxNumChannels);
		numChannels++;
		channel[numChannels-1].setChannelName(_channelName);
		channel[numChannels-1].setMeasurementUnit(_measurementUnit);
		return numChannels-1;
	}
	int addEventChannel(const AnsiString _channelName, const AnsiString _measurementUnit)
	{
		assert(numEventChannels < maxNumChannels);
		numEventChannels++;
		eventChannel[numEventChannels-1].setChannelName(_channelName);
		eventChannel[numEventChannels-1].setMeasurementUnit(_measurementUnit);
		return numEventChannels-1;
	}

    void replaceEventChannel(int numChannel, CEventChannel * newCh){
        eventChannel[numChannel] = *newCh;
    }

    int addAnnotationChannel(const AnsiString _channelName, const AnsiString _measurementUnit)
	{
		assert(numAnnotationChannels < maxNumChannels);
		numAnnotationChannels++;
		annotationChannel[numAnnotationChannels-1].setChannelName(_channelName);
		annotationChannel[numAnnotationChannels-1].setMeasurementUnit(_measurementUnit);
		return numAnnotationChannels-1;
	}
	AnsiString getChannelDescription(const int channelNum) const //returns "number name [unit]"
	{
		TVarRec channelProperties[3] =
			{channelNum+1, channel[channelNum].getChannelName(),
				channel[channelNum].getMeasurementUnit()};
		return AnsiString::Format("M%d %s [%s]", channelProperties, 2);
	}
	AnsiString getEventChannelDescription(const int channelNum) const //returns "Dnumber name [unit]"
	{
		TVarRec channelProperties[3] =
			{channelNum+1, eventChannel[channelNum].getChannelName(),
				eventChannel[channelNum].getMeasurementUnit()};
		return AnsiString::Format("D%d %s [%s]", channelProperties, 2);
	}
    AnsiString getAnnotationChannelDescription(const int channelNum) const //returns "Dnumber name [unit]"
	{
		TVarRec channelProperties[3] =
			{channelNum+1, annotationChannel[channelNum].getChannelName(),
				annotationChannel[channelNum].getMeasurementUnit()};
		return AnsiString::Format("D%d %s [%s]", channelProperties, 2);
	}
	double &accessByTime(const int channelNum, const double t)
	//returns the sample of a double channel that is closest to time; sample0=startTime
	{
		int sampleNum = time2SampleIndex(t);
		assert(channelNum >= 0 && channelNum < numChannels);
		assert(sampleNum >= 0 && sampleNum <= channel[channelNum].getNumElems());
		if (sampleNum == channel[channelNum].getNumElems())
			sampleNum--;
		return channel[channelNum][sampleNum];
	}

    Annotation &accessAnnotationByTime(const int channelNum, const double t)
	//returns the event closest to time
	{
		return annotationChannel[channelNum][annotationChannel[channelNum].getIndexClosestToTime(t)];
	}

	Event &accessEventByTime(const int channelNum, const double t)
	//returns the event closest to time
	{
		return eventChannel[channelNum][eventChannel[channelNum].getIndexClosestToTime(t)];
	}
	void refreshAllMinMaxValues()
	{
		for (int i = 0; i < getNumChannels(); i++)
			channel[i].refreshMinMaxValue();
		for (int i = 0; i < getNumEventChannels(); i++)
			eventChannel[i].refreshMinMaxValue();
		for (int i = 0; i < getNumAnnotationChannels(); i++)
			annotationChannel[i].refreshMinMaxValue();
	}

	enum BeatAlgorithm {
		beatAlgorithmAM,
		beatAlgorithmAF,
		beatAlgorithmFZ,
		beatAlgorithmFX,
		beatAlgorithmFCOREL,
		beatAlgorithmFRMS
	};

	struct BeatTimesParams {
		BeatAlgorithm 			algorithm;
		int 					sourceChannelNum;
		CursorPositions 		*cursorPos;

		// fcorel
		int 					existBeatNum;
		float					minCorel;

		BeatTimesParams(BeatAlgorithm alg, int chanNum = 0, CursorPositions *cp = 0) :
			algorithm(alg), sourceChannelNum(chanNum), cursorPos(cp),
			existBeatNum(0), minCorel(0.9f) {}
	};

	struct BeatTimesReturn {
		bool					error;
		AnsiString				errorMessage;
		AnsiString				errorCaption;

		BeatTimesReturn() : error(false) {}

		operator bool() const {
			return !error;
		}
	};

    BeatTimesReturn beatTimesAmForEventChan(int sourceChannelNum,double AMparam = -1, bool automatic = false, double AMmaxValue = -1);
    BeatTimesReturn beatTimesAmForEventChanBeatDetection(int sourceChannelNum,double AMparam = -1, double AMmaxValue = -1);

	BeatTimesReturn calculateBeatTimes(const BeatTimesParams& params, double AMparam = -1, bool automatic = false, double AMmaxValue = -1);
	BeatTimesReturn calculateBeatTimesBeatDetection(const BeatTimesParams& params, double AMparam = -1, double AMmaxValue = -1);
	// old calculateBeatTimes function
	inline BeatTimesReturn calculateBeatTimes(BeatAlgorithm algorithm, int sourceChannelNum, CursorPositions *cursorPos) {
		return calculateBeatTimes(BeatTimesParams(algorithm, sourceChannelNum, cursorPos));
	}

	enum DSPWindow {
		windowRectangular,
		windowTriangular
	};

    double getMinimumHeartBeatEvents(int numSelectedChannel);
	void lowPassFilter(DSPWindow windowType, int sourceChannelNum, double cutoffFreq, double errorValue=-1);
	void lowPassFilterBeatDetection(DSPWindow windowType, int sourceChannelNum, double cutoffFreq, double errorValue=-1);    

	void correctBaseline(int signalChannelNum, int existBeatChannelNum, CursorPositions *cursorPos);
	void detrendEventChannel(int channelNum);
	void offsetEventChannel(int channelNum, double offset);
	void assignInterEventTimeAsEventValues(int sourceEventChannelNum);
  	void assignInterEventTimeAsEventValuesSameChannel(int sourceEventChannelNum);
	void assignEventFreqAsEventValues(int sourceEventChannelNum);
	void assignEventFreqAsEventValuesFromRRI(int sourceEventChannelNum);    
	void refineBeatRateWithInterpolation(int signalChannelNum, int existBeatChannelNum);
	void refineBeatRateWithInterpolationSameChannel(int signalChannelNum, int existBeatChannelNum);    
	void calcStatisticsSignal(int signalChannelNum);
    void calcStatisticsSignalBetweenCursors(int signalChannelNum, double startTime, double stopTime); 
	void MaxMin(int eventChannelNum, CursorPositions *cursorPos);
    void signalDifferential(int signalChannelNum, double errorValue);
    void signalDifferentialAbsolute(int signalChannelNum, double errorValue);
    void eventDifferential(int eventChannelNum);
    void eventDifferentialAbsolute(int eventChannelNum);
    void analyzeTWave();
	void resampleEventChannel(int eventChannelNum, double newFrequency);
	void resampleSignalChannel(int signalChannelNum, double newFrequency);
	void createBPEventChannel(int BPSignalChNum, int existBeatChNum);
	void createRTEventChannel(int RRchannel, int TTchannel);
    void createMBPEventChannel(int SBPchannel, int DBPchannel);
	void addEventChannelChangedMarker(double value, AnsiString channelName, double startTime, double endTime, AnsiString comment);

	enum FileType {
		nevroEKG,
		nevroEKGeditable,
        txt,
		dekg_ascii,
		dekg_binary,
		mecg_ascii,
		mecg_binary,
		finapress,
		finapress_pressure,
		finapress_ecg,
		ppi_ascii,
        hl7x,
        s2,
		other
	};
	std::vector<std::string> loadFromFile(const UnicodeString fileName, LibCore::CMeasurement::FileType fileType);
	void loadHeaderFromFile(const UnicodeString fileName, LibCore::CMeasurement::FileType fileType);
	void saveToFile(const UnicodeString fileName, const FileType fileType);
	void exportAllSignalChannels(const AnsiString fileName);
	void exportSignalChannel(const AnsiString fileName, int channelNum);
	void exportEventChannel(const AnsiString fileName, int eventChannelNum);
	bool importEventIntervals(const AnsiString fileName);

	enum YscaleMode {
		yScaleModeAll,
		yScaleModeManual,
		yScaleModeAuto
	};
	void refreshGraphValues(TChart *chart, double xLeft, double xRight, YscaleMode yScaleMode, bool trueYscale,
						bool eventChannelSelected, bool annotationChannelsSelected, int numSelectedChannel, bool genReport = false);
			//selected channel is the one whos values correspond to labels on y-axis
    // deprecated:
	void redrawFourierTransform(TChart *chart, int smoothingWidth, bool coherence);

	struct DFTParams
	{
		double start, end;
		double samplingRate;
        double maxFrequency;

		// for coherence only
        int numberOfWindows;
        double overlay;
        int windowWidth;
	};
	DFTParams fourierTransform(int existBeatChannelNum, double maxFrequency = 0.5);
	DFTParams spectrumCoherence(int eventCh1, int eventCh2, int nwin, double owin, double maxFreq, bool messages = true);


	struct BRSReturn {
		// usage example:
		// BRSReturn is returned from one of different BRS routines
		// 	method is set to one of the predefined values (method_error is only used as default value)
		//	error is set to false
		//		comment is the actual return value that should be printed to screen
		//	error is set to true
		//		error description is included in comment
		//

		enum MethodUsed {
			method_error,
			method_fBRS,
			method_rBRS,
			method_sBRS,
			method_xBRS,
		};

		// general requirements
		int							beatChannelNum, SBPChannelNum;
		UnicodeString				comment;
		bool						error;
		MethodUsed					method;

		// required by sBRS
		double						dRRThreshold, dBPThreshold;
		double						sBRSUp, sBRSDown;
		int							sBRSUpCount, sBRSDownCount;
		// required by xBRS
		double						xBRS;
		int							xBRSCount;
        double                      xBRSAvgSeqElValue;
        double                      xBRSMaxSeqElValue;
		// required by fBRS
		double 						alphaHF, alphaLF;
		// required by rBRS
		double						rBRS, rBRSDelay;
		int							rBRSCount;


		// methods
		BRSReturn(MethodUsed m = method_error, int ekgChan = -1, int pressureChan = -1) :
			beatChannelNum(ekgChan),
			SBPChannelNum(pressureChan),
			error(false),
			method(m)
		{
			// other defaults
			dRRThreshold = dBPThreshold = 0;
			sBRSUp = sBRSDown = 0;
			sBRSUpCount = sBRSDownCount = 0;
			xBRS = 0;
			xBRSCount = 0;
			alphaHF = alphaLF = 0;
			rBRS = rBRSDelay = 0;
			rBRSCount = 0;
		}

		// smart join operator
		void operator+= (const BRSReturn& ret) {
			beatChannelNum = ret.beatChannelNum;
			SBPChannelNum = ret.SBPChannelNum;
			comment = ret.comment;
			error |= ret.error;
			method = ret.method;

			switch(method) {
			case method_sBRS:
				dRRThreshold = ret.dRRThreshold;
				dBPThreshold = ret.dBPThreshold;
				sBRSUp = ret.sBRSUp;
				sBRSDown = ret.sBRSDown;
				sBRSUpCount = ret.sBRSUpCount;
				sBRSDownCount = ret.sBRSDownCount;
				break;
			case method_xBRS:
				xBRS = ret.xBRS;
				xBRSCount = ret.xBRSCount;
                xBRSAvgSeqElValue = ret.xBRSAvgSeqElValue;
                xBRSMaxSeqElValue = ret.xBRSMaxSeqElValue;
				break;
			case method_fBRS:
				alphaHF = ret.alphaHF;
				alphaLF= ret.alphaLF;
				break;
			case method_rBRS:
				rBRS = ret.rBRS;
				rBRSDelay = ret.rBRSDelay;
				rBRSCount = ret.rBRSCount;
				break;
			default:
				break;
			}
		}
	};

	BRSReturn sequentialBRS(int ekgChannelNum, int pressureChannelNum, double dRRThreshold = 0.005, double dBPThreshold = 1);
	BRSReturn correlationxBRS(int beatChannelNum, int systBPChannelNum, double ccThresh, double xBrsByCcThresh, int samplesCount);
	BRSReturn frequencyfBRS(int beatChannelNum, int systBPChannelNum);
    BRSReturn sinhronizedrBRS(int beatChannelNum, int systBPChannelNum, int respChannelNum, CursorPositions *cursorPos);

	void cut(double start, double end)
	{
		assert(start <= end+0.4/samplingRate && start >= -0.4/samplingRate && end <= getDuration()+0.4/samplingRate);

		//delete samples/events from all channels
		for (int chNum = 0; chNum < numChannels; chNum++)
			channel[chNum].deleteElems(time2SampleIndex(start), time2SampleIndex(end));
		for (int eventChNum = 0 ; eventChNum < numEventChannels; eventChNum++)
		{
			int firstToDelete = 0, lastToDelete = eventChannel[eventChNum].getNumElems()-1;
			for (int i = 0; i < eventChannel[eventChNum].getNumElems(); i++)
			{
				if (eventChannel[eventChNum][i].time < start)
					firstToDelete = i+1;
				if (eventChannel[eventChNum][i].time > end)
				{
					lastToDelete = i-1;
					break;
				}
			}
			if (firstToDelete <= lastToDelete) //otherwise, there are no events in the interval [start..end]
			{
				for (int i = lastToDelete+1; i < eventChannel[eventChNum].getNumElems(); i++)
					eventChannel[eventChNum][i].time -= (time2SampleIndex(end)-time2SampleIndex(start)+1)/getSamplingRate();
				eventChannel[eventChNum].deleteElems(firstToDelete, lastToDelete);
			}
		}
        for(int annotationChannelNum = 0; annotationChannelNum < numAnnotationChannels; annotationChannelNum++){
            annotationChannel[annotationChannelNum].deleteElems(start,end,true);
        }

		//delete channels that are now empty
		for (int chNum = numChannels-1; chNum >= 0; chNum--)
			if (channel[chNum].getNumElems() == 0)
				deleteChannel(chNum);
		for (int eventChNum = numEventChannels-1; eventChNum >= 0; eventChNum--)
			if (eventChannel[eventChNum].getNumElems() == 0)
				deleteEventChannel(eventChNum);
        for (int chNum = numAnnotationChannels-1; chNum >= 0; chNum--)
			if (annotationChannel[chNum].getNumElems() == 0)
				deleteAnnotationChannel(chNum);

		char buf[1000];
		sprintf(buf, "\r\nCut all channels from %lf s to %lf s", start, end);
		appendComment(buf);
        markCut = true; 
	}

	void addEventChannels(int chan1, int chan2);

    //IvanT201109
    void calculateECG12(CMeasurement* target) const; //todo: obrni funkcijo source <-> target
    int generate12leadGraph(const CMeasurement* twelveLeadMeasurement);
    int getNekgFileVersion();
    double getMissingSamplesValue(bool evenChannelSelected, int numSelectedChannel);
    double getMulitplierValue(bool eventChannelSelected, int numSelectedChannel);
    //after conversion we ensure files are corrected
    void setNekgFileVersion(int ver);
    void setBitSaving(int bits);
    void calcWindowAverage(int evtCh, int newCh, double windowWidth, double step, double start, double stop, bool strictTime);

    void setGadgetVersion(AnsiString ver)   {gadgetVersion = ver;}
    void setGadgetmac(AnsiString ver)       {gadgetMac = ver;}
    void setMobEcgVersion(AnsiString ver)   {mobEcgVersion = ver;}

    AnsiString getGadgetVersion()   { return gadgetVersion;}
    AnsiString getGadgetMac()       { return gadgetMac;}
    AnsiString getMobEcgVersion()   { return mobEcgVersion;}

	static std::vector<std::string> findConvertedS2Files(UnicodeString path);
	static std::vector<std::string> convertS2File(UnicodeString s2FilePath, long maxLengthPerFile = -1, bool force = false, double missingValue = -1);

}; //end class CMeasurement

}; //end namespace LibCore

#endif
