//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "eventReportSettingDocForm.h"
#include "gnugettext.hpp"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TEventreportOnDocSettings *EventreportOnDocSettings;
//---------------------------------------------------------------------------
__fastcall TEventreportOnDocSettings::TEventreportOnDocSettings(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TEventreportOnDocSettings::FormCreate(TObject *Sender)
{
	TranslateComponent(this);
//	SetWindowLong(timeMinutes->Handle, GWL_STYLE, GetWindowLong(timeMinutes->Handle, GWL_STYLE) | ES_NUMBER);
	SetWindowLong(WeightInp->Handle, GWL_STYLE, GetWindowLong(WeightInp->Handle, GWL_STYLE) | ES_NUMBER);
	SetWindowLong(AgeInp->Handle, GWL_STYLE, GetWindowLong(AgeInp->Handle, GWL_STYLE) | ES_NUMBER);
}
//---------------------------------------------------------------------------

void __fastcall TEventreportOnDocSettings::timeMinutesChange(TObject *Sender)
{
	TEdit* source = dynamic_cast<TEdit*>(Sender);

	if(source == NULL)
        return;

	int caretLocation = source->SelStart;

	UnicodeString numeric = "";
	bool decimalFound = false;
	for(int i=1; i<=source->Text.Length();i++){
		if(source->Text[i] >= '0' && source->Text[i] <= '9'){
			numeric += source->Text[i];
		}else if(!decimalFound && source->Text[i] == FormatSettings.DecimalSeparator){
			decimalFound = true;
			numeric += source->Text[i];
		}else{
            --caretLocation;
        }
	}
	if(numeric.Compare(source->Text) != 0){
		source->Text = numeric;
		source->SelStart = std::max(0, std::min(numeric.Length(), caretLocation));
	}
}
//---------------------------------------------------------------------------


