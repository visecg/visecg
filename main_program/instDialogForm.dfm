object InstantDialogForm: TInstantDialogForm
  Left = 337
  Top = 421
  AutoSize = True
  Caption = 'InstantDialogForm'
  ClientHeight = 129
  ClientWidth = 369
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 353
    Height = 13
    Caption = 'Label1'
    Constraints.MaxWidth = 353
    Constraints.MinWidth = 353
    WordWrap = True
  end
  object Label13: TLabel
    Left = 0
    Top = 0
    Width = 257
    Height = 9
    AutoSize = False
  end
  object Label2: TLabel
    Left = 296
    Top = 80
    Width = 73
    Height = 49
    AutoSize = False
  end
  object Edit1: TEdit
    Left = 8
    Top = 69
    Width = 353
    Height = 21
    TabOrder = 0
  end
  object btnOk: TBitBtn
    Left = 288
    Top = 96
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object btnCancel: TBitBtn
    Left = 208
    Top = 96
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
end
