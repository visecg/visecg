// LICENCE:
// Full License Text
// THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
// BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
// 1. Definitions
// "Adaptation" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image ("synching") will be considered an Adaptation for the purpose of this License. "Collection" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) 
// below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined above) for the purposes of this License. "Distribute" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership. "Licensor" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License. "Original Author" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance 
// the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast. "Work" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cine
// matography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work. "You" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation. "Publicly Perform" means to perform public recitations of the Work and to communicate to the public those public recitations, by any mean
// s or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images. "Reproduce" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium. 2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyrigh
// t protection under copyright law or other applicable laws.
// 3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:
// to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections; to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked "The original work was translated from English to Spanish," or a modification could indicate "The original work has been modified."; to Distribute and Publicly Perform the Work including as incorporated in Collections; and, to Distribute and Publicly Perform Adaptations. The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are h
// ereby reserved, including but not limited to the rights set forth in Section 4(d).
// 4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:
// You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not requi
// re the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested. You may not exercise any of the rights granted to You in Section 3 above in any manner that is primarily intended for or directed toward commercial advantage or private monetary compensation. The exchange of the Work for other copyrighted works by means of digital file-sharing or otherwise shall not be considered to be intended for or directed toward commercial advantage or private monetary compensation, provided there is no payment of any monetary compensation in connection with the exchange of copyrighted works. If You Distribute, or Publicly Pe
// rform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution ("Attribution Parties") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and, (iv) consistent with Section 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., "French trans
// lation of the Work by Original Author," or "Screenplay based on original Work by Original Author"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution 
// Parties. For the avoidance of doubt:
// Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License if Your exercise of such rights is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(b) and otherwise waives the right to collect royalties through any statutory or compulsory licensing scheme; and, Voluntary License Schemes. The Licensor reserves the right to collect royalties, whether individually or, in the event that the Licensor is a member of a
//  collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License that is for a purpose or use which is otherwise than noncommercial as permitted under Section 4(c). Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as a
// ppropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise. 5. Representations, Warranties and Disclaimer
// UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
// 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// 7. Termination
// This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License. Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.
//  8. Miscellaneous
// Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License. Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License. If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable. No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent. This License constitute
// s the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You. The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those tre
// aty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.


//---------------------------------------------------------------------------
#include <fstream>
#include "ini.h"
#include "StringSupport.h"

#include <vcl.h>
#pragma hdrstop

#include "recForm.h"
#include "measurForm.h"
#pragma package(smart_init)
#pragma resource "*.dfm"

#include "gnugettext.hpp"

TrecordForm *recordForm;

void recordAnalog(TrecordForm::RecordThreadParams *pParams)
{
	DWORD bytesInQueue, numRead;
	LibCore::CMeasurement &measurement = *(pParams->pRecordForm->pMeasurement);

	//open device, set up notification event
	pParams->hDevice = LibCore::openAndSetupDevice(pParams->portNumber, 115200, 100, pParams->hFtdMutex);
	HANDLE hEvent = CreateEvent(NULL, false, false, L"");
	ftCheck(FT_SetEventNotification(pParams->hDevice, FT_EVENT_RXCHAR, hEvent), "FT_SetEventNotification");

	if (pParams->portNumber == 1)
	{
		//clear RTS & set event iff this is the thread for port 1; this opens data flow on ports 1 and 2
		ftCheck(FT_ClrRts(pParams->hDevice), "FT_ClrRts");
		Sleep(50);
		SetEvent(pParams->syncEvent);
	}
	//synchronize with other threads
	WaitForSingleObject(pParams->syncEvent, INFINITE);

	if (pParams->portNumber == 1
		&& pParams->channelNumber[0] == -1 && pParams->channelNumber[1] == -1
		&& pParams->channelNumber[2] == -1 && pParams->channelNumber[3] == -1)
		return; //do nothing if port 1 is disabled (and was thus only used to open data flow on port 2)

	//ignore first 260 bytes because they're usually corrupted
	do {
		WaitForSingleObject(hEvent, 50);
		ftCheck(FT_GetQueueStatus(pParams->hDevice, &bytesInQueue), "FT_GetStatus");
	} while (bytesInQueue < 260);
	char *buf = new char[bytesInQueue+10];
	ftCheck(FT_Read(pParams->hDevice, buf, min(int(bytesInQueue),260), &numRead), "FT_Read");
	delete []buf;

	//read in a loop until told to stop
	int carryBytes, availBytes, samplesRead = 0;
	int bufSize = 10000;
	int numErrors = 0;
	buf = new char[bufSize];

    //load constants and factors for transformation of values read from A/D
	UnicodeString settingsFname(L"nastavitve.ini");
	for (int i = Application->ExeName.Length()-1; i >= 0; --i) {
		if (Application->ExeName[i] == '\\') {
			settingsFname = Application->ExeName.SubString(1, i) + settingsFname;
			break;
		}
	}
	Ini::File ini(settingsFname.c_str());

	double EKGoffset[2];
	EKGoffset[0] = -5;
	EKGoffset[1] = -5;
	ini.loadVar(EKGoffset[0], L"EKG kanal 0 offset");
	ini.loadVar(EKGoffset[1], L"EKG kanal 1 offset");

    double therm_a = 1.129241e-3, therm_b = 2.341077e-4, therm_c = 8.775468e-8;
    double therm_R = 9940.0;
	ini.loadVar(therm_a, L"termistor coeficient a");
	ini.loadVar(therm_b, L"termistor coeficient b");
	ini.loadVar(therm_c, L"termistor coeficient c");
    ini.loadVar(therm_R, L"termistor resistance");

	//first, read a byte 01xxxxxx, which marks 1st byte of a sample
	do {
		do
			ftCheck(FT_Read(pParams->hDevice, buf, 1, &numRead), "FT_Read");
		while (numRead == 0);
		assert(numRead == 1);
	} while ((buf[0] & 0xC0) != 0xC0);
	carryBytes = 1;

	do {
		//wait for event & read all available data (break if recording stopped)
		do {
			WaitForSingleObject(hEvent, 50);
			ftCheck(FT_GetQueueStatus(pParams->hDevice, &bytesInQueue), "FT_GetStatus");
		} while (bytesInQueue == 0 && !pParams->pRecordForm->recordingStopped);
		if (pParams->pRecordForm->recordingStopped)
			break;

		if (int(bytesInQueue) > bufSize-10)
		{
			bufSize = bytesInQueue + 1000;
			buf = (char*)realloc(buf, bufSize);
		}
		ftCheck(FT_Read(pParams->hDevice, buf+carryBytes, min(bytesInQueue, DWORD(8*200)), &numRead), "FT_Read");
		availBytes = carryBytes+numRead;

		//decode all samples that were fully read (all 4 channels)
		for (int i = 0; i+7 < int(availBytes); i += 8)
		{
			int lowByte, highByte, sampleValue;
			if ((buf[0] & 0xC0) != 0xC0)
			{
				numErrors++;
				while ((buf[0] & 0xC0) != 0xC0)
					i++;
				if (i+7 >= int(availBytes))
					break;
			}
			if (pParams->portNumber == 2)
			{
				//ch0 = EKG1, ch1 = EKG2, ch2 = battery voltage, ch3 = breathing
				for (int portChannel = 0; portChannel < 4; portChannel++)
				{
					//const double EKGoffset[] = {-4.959, -5.134};
					if (pParams->channelNumber[portChannel] == -1)
						continue;
					highByte  = int(buf[i+2*portChannel] & 0x1F);
					lowByte = int(buf[i+2*portChannel+1] & 0x7F);
					sampleValue = highByte*128 + lowByte;
					switch (portChannel) {
					case 0:
					case 1: //EKG I, EKG II channels; put value into measurement->channel
						measurement[pParams->channelNumber[portChannel]][samplesRead]
							= pParams->pRecordForm->channelGain[pParams->channelNumber[portChannel]]
								*sampleValue + EKGoffset[portChannel];
						break;
					case 2: //battery voltage channel (not used here); calculate lead III EKG from lead I and II
						measurement[pParams->channelNumber[portChannel]][samplesRead]
							= (measurement[pParams->channelNumber[portChannel-1]][samplesRead]
								- measurement[pParams->channelNumber[portChannel-2]][samplesRead]);
						break;
					case 3: //breathing: special formula to obtain temperature in Celsius
						if (sampleValue == 4095)
							measurement[pParams->channelNumber[portChannel]][samplesRead] = -100; //sonda ni prikljucena
						else if (sampleValue == 0)
							measurement[pParams->channelNumber[portChannel]][samplesRead] = -200; //sonda v kratkem stiku
						else {
							/* IZRACUN TEMPERATURE TERMISTORJA
							termistor je prikljucen preko 10k upora na Uref
							z A/D merimo napetost na termistorju, A/D ima obmocje od 0...4095
							4095 je pri Uin=Uref, termistor ima 10k pri 25 stopinj Celzija*/
							//double a = 1.129241e-3, b = 2.341077e-4, c = 8.775468e-8;
							double l = log(therm_R/(4095.0/sampleValue-1)); //9940.0
							measurement[pParams->channelNumber[portChannel]][samplesRead]
								=  1/(therm_a + therm_b*l + therm_c*l*l*l) - 273.15;
						}
					}
				}
			} else {
				//normal port: each channel on port corresponds to 1 channel in measurement
				for (int portChannel = 0; portChannel < 4; portChannel++)
				{
					if (pParams->channelNumber[portChannel] == -1)
						continue;
					highByte  = int(buf[i+2*portChannel] & 0x1F);
					lowByte = int(buf[i+2*portChannel+1] & 0x7F);
					measurement[pParams->channelNumber[portChannel]][samplesRead]
						= pParams->pRecordForm->channelGain[pParams->channelNumber[portChannel]]
							*(highByte*128 + lowByte);
				}
			}
			samplesRead++;
		}
		carryBytes = availBytes % 8;
		MoveMemory(buf, buf+availBytes-carryBytes, carryBytes);
	} while (!pParams->pRecordForm->recordingStopped);

	if (numErrors > 0)
	{
		wchar_t buf[1000];
		wsprintf(buf, L"%d error while saving device data", numErrors);
		MessageBox(NULL, buf, L"Warning", MB_OK);
	}
}

int randRange(int l, int h)
{
	return l+rand()%(h-l);
}

void recordColinDigital(TrecordForm::RecordThreadParams *pParams)
{
	DWORD bytesInQueue, numRead;
	LibCore::CMeasurement &measurement = *(pParams->pRecordForm->pMeasurement);

	//open device; set up notification event
	pParams->hDevice = LibCore::openAndSetupDevice(0, 4800, 1000, pParams->hFtdMutex);
	HANDLE hEvent = CreateEvent(NULL, false, false, L"");
	ftCheck(FT_SetEventNotification(pParams->hDevice, FT_EVENT_RXCHAR, hEvent), "FT_SetEventNotification");

	//synchronize with other threads
	WaitForSingleObject(pParams->syncEvent, INFINITE);

	//re-purge input buffers after synchronization
	ftCheck(FT_Purge(pParams->hDevice, FT_PURGE_RX | FT_PURGE_TX), "FT_Purge");
	//because purging doesn't always work (or I use it incorrectly), manually flush buffers
	ftCheck(FT_GetQueueStatus(pParams->hDevice, &bytesInQueue), "FT_GetQueueStatus");
	char *buf = new char[bytesInQueue+10];
	ftCheck(FT_Read(pParams->hDevice, buf, bytesInQueue, &numRead), "FT_Read");
	delete buf;

	//read in a loop until told to stop
	int carryBytes, samplesRead = 0;
	int sampleSize = 73, bufSize = 2*sampleSize;
	int numErrors = 0;
	int timeCheck, prevTimeCheck;
	buf = new char[2*sampleSize];

	//first, read a CR-LF, which marks end of a sample
	buf[0] = 0;
	do {
		if (buf[0] != 0x0D)
		{
			do {
				ftCheck(FT_Read(pParams->hDevice, buf, 1, &numRead), "FT_Read");
				assert(numRead <= 1);
			} while (buf[0] != 0x0D);
			ftCheck(FT_Read(pParams->hDevice, buf, 1, &numRead), "FT_Read");
		}
	} while (buf[0] != 0x0A);
	carryBytes = 0;
	prevTimeCheck = -1;
	do {
		//wait until at least a sample of data is available; read all available data
		if (carryBytes < sampleSize)
		{
			do {
				WaitForSingleObject(hEvent, 100);
				ftCheck(FT_GetQueueStatus(pParams->hDevice, &bytesInQueue), "FT_GetStatus");
			} while (carryBytes + int(bytesInQueue) < sampleSize && !pParams->pRecordForm->recordingStopped);
			if (pParams->pRecordForm->recordingStopped)
				break;

			if (bufSize < carryBytes + int(bytesInQueue))
			{
				bufSize = carryBytes+bytesInQueue+sampleSize;
				buf = (char*)realloc(buf, bufSize);
			}
			ftCheck(FT_Read(pParams->hDevice, buf+carryBytes, bytesInQueue, &numRead), "FT_Read");
			carryBytes += numRead;
			assert(carryBytes >= sampleSize);
		}
		carryBytes = sampleSize;
		//process all data if this is more than preview
		if (!pParams->pRecordForm->measurementStarted)
		{
			timeCheck = GetTickCount()-pParams->pRecordForm->measurementStartedTime;
			while (carryBytes >= sampleSize)
			{
				LibCore::ColinDigitalData colinData;
				if (timeCheck-prevTimeCheck < 200)
					numErrors++;
				try {
					colinData.readFromString(buf);
				} catch (LibCore::EKGException *) {
					numErrors++;
				}
				measurement.getEventChannel(pParams->channelNumber[0]).appendEventIfChanged(
					timeCheck/1000.0, colinData.systPressure, 0.2);
				measurement.getEventChannel(pParams->channelNumber[0]+1).appendEventIfChanged(
					timeCheck/1000.0, colinData.mapPressure, 0.2);
				measurement.getEventChannel(pParams->channelNumber[0]+2).appendEventIfChanged(
					timeCheck/1000.0, colinData.diastPressure, 0.2);
				measurement.getEventChannel(pParams->channelNumber[0]+3).appendEventIfChanged(
					timeCheck/1000.0, colinData.pulseRate, 0.2);
				MoveMemory(buf, buf+sampleSize, carryBytes-sampleSize);
				carryBytes -= sampleSize;
				samplesRead++;
				prevTimeCheck = timeCheck;
			}
		}
	} while (!pParams->pRecordForm->recordingStopped);

	if (numErrors > 0)
	{
		wchar_t buf[1000];
		swprintf(buf, gettext("%d error while saving device data").data(), numErrors);
		Application->MessageBox(buf, gettext("Warning").data(), MB_OK);
	}
}

DWORD WINAPI recordThreadProc(LPVOID param)
{
    try {
    	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
	    TrecordForm::RecordThreadParams *pParams = reinterpret_cast<TrecordForm::RecordThreadParams*>(param);
    	assert(pParams);

	    if (pParams->portNumber == 0)
    	{
		    recordColinDigital(pParams);
	    	return 0;
    	} else {
		    recordAnalog(pParams);
	    	return 0;
    	}
    }
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
    catch (...) {
        Exception* e = new Exception("Unknown exception occured in one of the record threads");
        Application->ShowException(e);
    } 
    return 0;
}

//---------------------------------------------------------------------------
__fastcall TrecordForm::TrecordForm(TComponent* Owner)
	: TForm(Owner), pMeasurement(NULL)
{
	for (int port = 0; port < LibCore::CDeviceStatus::numPorts; port++)
		hRecordThread[port] = NULL;
	for (int channel = 0; channel < LibCore::CMeasurement::maxNumChannels; channel++)
	{
		oscilloscope[channel] = NULL;
		channelNameLabel[channel] = NULL;
	}
	recordingStopped = true;
}
//---------------------------------------------------------------------------

static int oscLeft = 50, oscSpacing, oscHeight, oscWidth,
	samplesSkip = 6, //draw every n'th sample
	clearRegion = 20; //white region shown in front of current sample


void __fastcall TrecordForm::FormShow(TObject *Sender)
{
	assert(pMeasurement);
	//prepare window content
	Height = GetSystemMetrics(SM_CYMAXIMIZED);
	btnAbort->Top = ClientHeight - 30;
	btnStart->Top = ClientHeight - 30;
	btnStop->Top = ClientHeight - 30;
	btnStop->Visible = false;
	btnStart->Visible = true;
	Caption = "DATA OVERVIEW, press 'Start!' to start measurement.";
	for (int channel = 0; channel < LibCore::CMeasurement::maxNumChannels; channel++)
	{
		if (oscilloscope[channel] != NULL)
		{
			delete oscilloscope[channel];
			oscilloscope[channel] = NULL;
		}
		if (channelNameLabel[channel] != NULL)
		{
			delete channelNameLabel[channel];
			channelNameLabel[channel] = NULL;
		}
	}

	//create threads, start recording & showing
	measurementStarted = false;
	if (measurementType != LibStdMeasurement::stdmNonstandard)
	{
		//initialize standard measurement type properties
		stdMeasurementProps = LibStdMeasurement::getMeasurementProps(measurementType);
		currentProtocolItemIndex = 0;
		previousProtocolItemFinishedTime = 0;
		//show instructions
		if (measurementType != LibStdMeasurement::stdmNonstandard)
			Application->MessageBox(UnicodeString(stdMeasurementProps.measurementInstructions).c_str(), gettext("Instructions for standard measurement").data(), MB_OK);
	}
	//create oscilloscopes (TPaintBox objects)
	oscSpacing = (ClientHeight-80)/pMeasurement->getNumChannels();
	oscHeight = oscSpacing-20;
	oscWidth = ClientWidth-oscLeft-5;
	for (int i = 0; i < pMeasurement->getNumChannels(); i++)
	{
		samplesDrawn[i] = 0;
		oscilloscope[i] = new TPaintBox(this);
		oscilloscope[i]->Parent = this;
		oscilloscope[i]->Left = oscLeft;
		oscilloscope[i]->Width = oscWidth;
		oscilloscope[i]->Top = i*oscSpacing+22;
		oscilloscope[i]->Height = oscHeight;
		oscilloscope[i]->Color = clWhite;
		oscilloscope[i]->Canvas->Pen->Color = clRed;
		oscilloscope[i]->Canvas->Brush->Color = clWhite;
		channelNameLabel[i] = new TLabel(this);
		channelNameLabel[i]->Parent = this;
		channelNameLabel[i]->Left = oscLeft;
		channelNameLabel[i]->Top = i*oscSpacing+5;
		channelNameLabel[i]->Caption = (*pMeasurement)[i].getChannelName();
	}
	recordingStopped = false;
	redrawAll();

	//create threads for recording
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_LOWEST);
	HANDLE syncEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	numActiveThreads = 0;
    HANDLE tempMutex = CreateMutex(NULL, false, NULL);
	for (int port = 0; port < LibCore::CDeviceStatus::numPorts; port++)
		if (portChannel[port][0] != -1 || portChannel[port][1] != -1
			|| portChannel[port][2] != -1 || portChannel[port][3] != -1
			|| port == 1) //port1 always enabled (used to open data flow); others enabled if at least one of their channels enabled
		{
			threadParams[numActiveThreads].channelNumber[0] = (port == 0 ? colinDigitalFirstChannel : portChannel[port][0]);
			threadParams[numActiveThreads].channelNumber[1] = portChannel[port][1];
			threadParams[numActiveThreads].channelNumber[2] = portChannel[port][2];
			threadParams[numActiveThreads].channelNumber[3] = portChannel[port][3];
			threadParams[numActiveThreads].portNumber = port;
			threadParams[numActiveThreads].pRecordForm = this;
            threadParams[numActiveThreads].hFtdMutex = tempMutex;
			threadParams[numActiveThreads].syncEvent = syncEvent;
			hRecordThread[numActiveThreads] = CreateThread(NULL, 0, recordThreadProc, threadParams+numActiveThreads, 0, 0);
			numActiveThreads++;
		}
	previewStartedTime = GetTickCount();
	Timer1->Enabled = true; //enable on-line drawing
}

//---------------------------------------------------------------------------


void __fastcall TrecordForm::btnStartClick(TObject *Sender)
{
	measurementStartedTime = GetTickCount();
	measurementStarted = true;
	pMeasurement->setDate(Time() + Date());
	btnStart->Visible = false;
	btnStop->Visible = true;
}
//---------------------------------------------------------------------------

void TrecordForm::stopRecording()
{
	//stop data (set RTS on port 1)
	for (int thread = 0; thread < numActiveThreads; thread++)
		if (threadParams[thread].portNumber == 1)
			ftCheck(FT_SetRts(threadParams[thread].hDevice), "FT_SetRts");

	recordingStopped = true;  //signal all threads to stop
	Timer1->Enabled = false; //stop on-line drawing
	//wait for threads to finish, then terminate them and close ports
	WaitForMultipleObjects(numActiveThreads, hRecordThread, TRUE, 3000);
	for (int thread = 0; thread < numActiveThreads; thread++)
	{
		if(WaitForSingleObject(hRecordThread[thread], 100) == WAIT_TIMEOUT)
			TerminateThread(hRecordThread[thread], 0);
		hRecordThread[thread] = NULL;
		ftCheck(FT_Close(threadParams[thread].hDevice), "FT_Close");
	}
	numActiveThreads = 0;
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL);

	//if port 2 was used, check battery state at end of measurement
	if (portChannel[2][0] != -1 || portChannel[2][1] != -1 || portChannel[2][2] != -1 || portChannel[2][3] != -1)
	{
		LibCore::CDeviceStatus deviceStatus;
		deviceStatus.getDeviceStatus(false);
		pMeasurement->appendComment(", finalized " + AnsiString(deviceStatus.EKGbatteryVoltage) + " V");
		if (deviceStatus.EKGbatteryVoltage < LibCore::CDeviceStatus::EKGbatteryVoltageLow)
		{
			wchar_t buf[1000];
			swprintf(buf, gettext("Low battery level (%5.2lf V).").data(), deviceStatus.EKGbatteryVoltage);
			Application->MessageBox(buf, gettext("Warning").data(), MB_OK);
		}
	}
}

void TrecordForm::redrawAxes()
{
	if (recordingStopped)
		return;
	for (int channel = 0; channel < pMeasurement->getNumChannels(); channel++)
	{
		Canvas->MoveTo(oscLeft-5, oscilloscope[channel]->Top);
		Canvas->LineTo(oscLeft-5, oscilloscope[channel]->Top+oscHeight);
		Canvas->MoveTo(oscLeft-10, oscilloscope[channel]->Top);
		Canvas->LineTo(oscLeft, oscilloscope[channel]->Top);
		Canvas->MoveTo(oscLeft-10, oscilloscope[channel]->Top+oscHeight);
		Canvas->LineTo(oscLeft, oscilloscope[channel]->Top+oscHeight);
		Canvas->TextOut(5, oscilloscope[channel]->Top+5, AnsiString(channelRangeMax[channel]));
		Canvas->TextOut(5, oscilloscope[channel]->Top+oscHeight-5, AnsiString(channelRangeMin[channel]));
	}
	int timeAxisY = ClientHeight-65;
	Canvas->MoveTo(oscLeft, timeAxisY-5);
	Canvas->LineTo(oscLeft, timeAxisY+5);
	Canvas->MoveTo(oscLeft+oscWidth, timeAxisY-5);
	Canvas->LineTo(oscLeft+oscWidth, timeAxisY+5);
	Canvas->MoveTo(oscLeft, timeAxisY);
	Canvas->LineTo(oscLeft+oscWidth, timeAxisY);
	Canvas->TextOut(oscLeft+oscWidth/2-30, timeAxisY+3,
				AnsiString(oscWidth*samplesSkip/pMeasurement->getSamplingRate())+" s");
}

void TrecordForm::redrawAll()
{
	if (!pMeasurement)
		return;
	for (int channel = 0; channel < pMeasurement->getNumChannels(); channel++)
		if (oscilloscope[channel])
			oscilloscope[channel]->Canvas->FillRect(oscilloscope[channel]->Canvas->ClipRect);
	redrawAxes();
}

void __fastcall TrecordForm::btnAbortClick(TObject *Sender)
{
	stopRecording();
	ModalResult = mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TrecordForm::btnStopClick(TObject *Sender)
{
	stopRecording();
	ModalResult = mrOk;
}
//---------------------------------------------------------------------------
int TrecordForm::scaleSampleValue(int channel, int sample)
{
	double value = (*pMeasurement)[channel][sample];
	return int((value-channelRangeMin[channel])
		/(channelRangeMax[channel]-channelRangeMin[channel])
		*oscHeight);
}

AnsiString minColonSec(double timeInSec) //converts to MIN:SEC
{
	char buf[1000];
	sprintf(buf, "%d:%02d", int(timeInSec/60), int(timeInSec) % 60);
	return AnsiString(buf);
}

void __fastcall TrecordForm::Timer1Timer(TObject *Sender)
{
	int duration = (measurementStarted ? GetTickCount()-measurementStartedTime : 0);
	if (duration > autoStopDuration)
	{
		stopRecording();
		ModalResult = mrOk;
		return;
	}
	if ((GetTickCount()-previewStartedTime)/1000.0 > LibCore::CMeasurement::maxMeasurementLength-10)
	{
		stopRecording();
		Application->MessageBox(gettext("Measurement is finished, due to exceeding the time limit.").data(), gettext("Warning").data(), MB_OK);
		ModalResult = mrOk;
		return;
	}  
	//draw signals on the oscilloscope
	for (int channel = 0; channel < pMeasurement->getNumChannels(); channel++)
	{
		for(; samplesDrawn[channel] < (*pMeasurement)[channel].getNumElems(); samplesDrawn[channel] += samplesSkip)
		{
			double scaledSampleValue = scaleSampleValue(channel, samplesDrawn[channel]);

			if ((samplesDrawn[channel]/samplesSkip+clearRegion) % oscWidth > ((samplesDrawn[channel]/samplesSkip+clearRegion-1) % oscWidth))
				oscilloscope[channel]->Canvas->FillRect(TRect(
					(samplesDrawn[channel]/samplesSkip+clearRegion-1) % oscWidth, 0,
					(samplesDrawn[channel]/samplesSkip+clearRegion) % oscWidth + 10, oscHeight));
			if ((samplesDrawn[channel]/samplesSkip) % oscWidth > ((samplesDrawn[channel]/samplesSkip-1) % oscWidth))
				oscilloscope[channel]->Canvas->LineTo(
					(samplesDrawn[channel]/samplesSkip) % oscWidth,
					oscHeight - scaledSampleValue);
			else //we go back to left edge
			{
				oscilloscope[channel]->Canvas->LineTo(oscWidth, oscHeight - scaledSampleValue);
				oscilloscope[channel]->Canvas->MoveTo(0, oscHeight - scaledSampleValue);
			}
		}
	}   
	//print COLIN digital data
	static bool colinDataAvailable = false;
	if (colinDigitalFirstChannel != -1)
	{
		if (colinDataAvailable)
		{
			int systPressure, mapPressure, diastPressure, pulseRate;
			char buf[1000];
			systPressure = pMeasurement->getEventChannel(colinDigitalFirstChannel)
				[pMeasurement->getEventChannel(colinDigitalFirstChannel).getNumElems()-1].value;
			mapPressure = pMeasurement->getEventChannel(colinDigitalFirstChannel+1)
				[pMeasurement->getEventChannel(colinDigitalFirstChannel+1).getNumElems()-1].value;
			diastPressure = pMeasurement->getEventChannel(colinDigitalFirstChannel+2)
				[pMeasurement->getEventChannel(colinDigitalFirstChannel+2).getNumElems()-1].value;
			pulseRate = pMeasurement->getEventChannel(colinDigitalFirstChannel+3)
				[pMeasurement->getEventChannel(colinDigitalFirstChannel+3).getNumElems()-1].value;
			sprintf(buf, "COLIN: tlak [mmHg] %d/%d (srednji %d); pulz %d/min             ",
					systPressure, diastPressure, mapPressure, pulseRate);
			Canvas->TextOut(oscLeft, ClientHeight-50, buf);
		} else {
			if (pMeasurement->getEventChannel(colinDigitalFirstChannel).getNumElems() > 0
			&& pMeasurement->getEventChannel(colinDigitalFirstChannel+1).getNumElems() > 0
			&& pMeasurement->getEventChannel(colinDigitalFirstChannel+2).getNumElems() > 0
			&& pMeasurement->getEventChannel(colinDigitalFirstChannel+3).getNumElems() > 0)
				colinDataAvailable = true;
		}
	}
	//refresh window caption, which includes protocol item description for standard measurements
	if (measurementStarted)
	{
		AnsiString newCaption;
		if (measurementType == LibStdMeasurement::stdmNonstandard)
			newCaption.printf("Data input - %s", minColonSec(duration/1000.0));
		//if a protocol exists:
		else if (currentProtocolItemIndex < stdMeasurementProps.numProtocolItems)
		{
			//if last item not yet finished, print message announcing current item
			LibStdMeasurement::ProtocolItem currentItem = LibStdMeasurement::getProtocolItem(measurementType, currentProtocolItemIndex);
			double timeRemaining = currentItem.time - (duration - previousProtocolItemFinishedTime)/1000.0;
			AnsiString timeRemainingStr;
			timeRemainingStr.printf( timeRemaining > 0 ? "more %s" : " - too much %s", minColonSec(fabs(timeRemaining)) );
			newCaption.printf("Zajemanje podatkov (%s) - %s; %s ( %s %s)",
							stdMeasurementProps.measurementNamePrefix, minColonSec(duration/1000.0),
							currentItem.instruction,
							currentItem.automatic ? AnsiString("automatic") : AnsiString("pritisni " + AnsiString(currentItem.markerEvent)),
							timeRemainingStr);

			//if current item is auto, advance it iff its time has elapsed
			if (currentItem.automatic && timeRemaining < 0)
			{
				pMeasurement->getEventChannel(markersChannel).append(LibCore::Event(duration/1000.0, currentItem.markerEvent));
				currentProtocolItemIndex++;
				previousProtocolItemFinishedTime = duration;
			}
		} else
			//if last item finished, just announce that fact
			newCaption.printf("Data input (%s) - %s s; protocol is finished",
							stdMeasurementProps.measurementNamePrefix, minColonSec(duration/1000.0));
		Caption = newCaption;
	}
}
//---------------------------------------------------------------------------

void __fastcall TrecordForm::FormPaint(TObject *Sender)
{
	redrawAll();
}
//---------------------------------------------------------------------------

void __fastcall TrecordForm::FormKeyDown(TObject *Sender, WORD &Key,
	  TShiftState Shift)
{
	if (!pMeasurement || recordingStopped || !measurementStarted || markersChannel == -1)
		return;
	//insert marker at current time;
	if (Key >= '0' && Key <= '9')
	{
		int eventType = Key - '0';
		int time = GetTickCount()-measurementStartedTime;
		int numExistingMarkers = pMeasurement->getEventChannel(markersChannel).getNumElems();
		pMeasurement->getEventChannel(markersChannel)[numExistingMarkers]
			= LibCore::Event(time/1000.0, eventType);

		if (measurementType != LibStdMeasurement::stdmNonstandard
					&& currentProtocolItemIndex < stdMeasurementProps.numProtocolItems
					&& LibStdMeasurement::getProtocolItem(measurementType, currentProtocolItemIndex).markerEvent == eventType)
		{
			//advance protocolItemIndex
			currentProtocolItemIndex++;
			previousProtocolItemFinishedTime = time;
		}
	}
}
//---------------------------------------------------------------------------


void __fastcall TrecordForm::FormCreate(TObject *Sender)
{
		 TranslateComponent(this);
}
//---------------------------------------------------------------------------

