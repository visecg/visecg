object BRSForm: TBRSForm
  Left = 252
  Top = 2
  AutoSize = True
  Caption = 'BRSForm'
  ClientHeight = 665
  ClientWidth = 649
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 0
    Top = 0
    Width = 73
    Height = 49
    AutoSize = False
  end
  object WarningLabel: TLabel
    Left = 16
    Top = 644
    Width = 79
    Height = 13
    Caption = 'WarningLabel'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Visible = False
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 212
    Height = 13
    Caption = 'Number of the event channel with RR timing:'
  end
  object Label2: TLabel
    Left = 296
    Top = 8
    Width = 243
    Height = 13
    Caption = 'Number of the event channel with systolic pressure:'
  end
  object Label11: TLabel
    Left = 576
    Top = 608
    Width = 73
    Height = 57
    AutoSize = False
  end
  object sBRSBox: TGroupBox
    Left = 8
    Top = 70
    Width = 633
    Height = 131
    Caption = 'sBRS'
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 40
      Width = 121
      Height = 13
      Caption = 'Threshold for RR interval:'
    end
    object Label4: TLabel
      Left = 8
      Top = 80
      Width = 119
      Height = 13
      Caption = 'Threshold for BP interval:'
    end
    object Label5: TLabel
      Left = 72
      Top = 64
      Width = 13
      Height = 13
      Caption = 'ms'
    end
    object Label6: TLabel
      Left = 72
      Top = 104
      Width = 30
      Height = 13
      Caption = 'mmHg'
    end
    object sBRSComment: TMemo
      Left = 176
      Top = 8
      Width = 449
      Height = 113
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object EditRRThreshold: TEdit
      Left = 8
      Top = 56
      Width = 57
      Height = 21
      TabOrder = 1
      Text = '0'
      OnChange = EditRRThresholdChange
    end
    object EditSBPThreshold: TEdit
      Left = 8
      Top = 96
      Width = 57
      Height = 21
      TabOrder = 2
      Text = '0'
      OnChange = EditSBPThresholdChange
    end
    object sBRSEnabled: TCheckBox
      Left = 8
      Top = 16
      Width = 145
      Height = 17
      Caption = 'Method is enabled'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
  end
  object xBRSBox: TGroupBox
    Left = 8
    Top = 206
    Width = 633
    Height = 131
    Caption = 'xBRS'
    TabOrder = 1
    object Label8: TLabel
      Left = 8
      Top = 40
      Width = 63
      Height = 13
      Caption = 'CC threshold:'
    end
    object Label9: TLabel
      Left = 8
      Top = 80
      Width = 97
      Height = 13
      Caption = 'XBRS/CC threshold:'
    end
    object Label13: TLabel
      Left = 96
      Top = 40
      Width = 58
      Height = 13
      Caption = 'No. samples'
    end
    object xBRSComment: TMemo
      Left = 176
      Top = 8
      Width = 449
      Height = 113
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object xBRSEnabled: TCheckBox
      Left = 8
      Top = 16
      Width = 145
      Height = 17
      Caption = 'Method is enabled'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object EditCCThreshold: TEdit
      Left = 8
      Top = 56
      Width = 57
      Height = 21
      TabOrder = 2
      Text = '0'
      OnChange = EditCCThresholdChange
    end
    object EditXBRSThreshold: TEdit
      Left = 8
      Top = 96
      Width = 57
      Height = 21
      TabOrder = 3
      Text = '0'
      OnChange = EditXBRSThresholdChange
    end
    object EditNumSamples: TEdit
      Left = 96
      Top = 56
      Width = 57
      Height = 21
      TabOrder = 4
      Text = '1'
      OnChange = EditNumSamplesChange
    end
    object EditNumSamplesUpDown: TUpDown
      Left = 153
      Top = 56
      Width = 16
      Height = 21
      Associate = EditNumSamples
      Position = 1
      TabOrder = 5
    end
  end
  object rBRSBox: TGroupBox
    Left = 8
    Top = 342
    Width = 633
    Height = 131
    Caption = 'rBRS'
    TabOrder = 2
    object Label7: TLabel
      Left = 8
      Top = 48
      Width = 151
      Height = 26
      AutoSize = False
      Caption = 'Number of the event channel with respiratory timing'
      WordWrap = True
    end
    object rBRSComment: TMemo
      Left = 176
      Top = 8
      Width = 449
      Height = 113
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object EditRespiratoryChan: TEdit
      Left = 16
      Top = 80
      Width = 113
      Height = 21
      TabOrder = 1
      Text = '1'
      OnChange = EditRespiratoryChanChange
    end
    object EditRespiratoryChanUpDown: TUpDown
      Left = 129
      Top = 80
      Width = 16
      Height = 21
      Associate = EditRespiratoryChan
      Position = 1
      TabOrder = 2
    end
    object rBRSEnabled: TCheckBox
      Left = 8
      Top = 16
      Width = 145
      Height = 17
      Caption = 'Method is enabled'
      TabOrder = 3
    end
  end
  object fBRSBox: TGroupBox
    Left = 8
    Top = 478
    Width = 633
    Height = 131
    Caption = 'fBRS'
    TabOrder = 3
    object fBRSComment: TMemo
      Left = 176
      Top = 8
      Width = 449
      Height = 113
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object fBRSEnabled: TCheckBox
      Left = 8
      Top = 16
      Width = 145
      Height = 17
      Caption = 'Method is enabled'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object EditRRChanNum: TEdit
    Left = 8
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '0'
    OnChange = EditRRChanNumChange
  end
  object EditSBPChanNum: TEdit
    Left = 296
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '0'
    OnChange = EditSBPChanNumChange
  end
  object EditRRChanUpDown: TUpDown
    Left = 129
    Top = 32
    Width = 16
    Height = 21
    Associate = EditRRChanNum
    TabOrder = 6
  end
  object EditSBPChanUpDown: TUpDown
    Left = 417
    Top = 32
    Width = 16
    Height = 21
    Associate = EditSBPChanNum
    TabOrder = 7
  end
  object CalculateButton: TBitBtn
    Left = 520
    Top = 616
    Width = 121
    Height = 25
    Caption = '&Calculate'
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
      33333333333F8888883F33330000324334222222443333388F3833333388F333
      000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
      F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
      223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
      3338888300003AAAAAAA33333333333888888833333333330000333333333333
      333333333333333333FFFFFF000033333333333344444433FFFF333333888888
      00003A444333333A22222438888F333338F3333800003A2243333333A2222438
      F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
      22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
      33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
      3333333333338888883333330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    TabOrder = 8
    OnClick = CalculateButtonClick
  end
  object KeepChannelsCheckBox: TCheckBox
    Left = 8
    Top = 616
    Width = 369
    Height = 17
    Caption = 'Keep the channels calculated by BRS methods'
    TabOrder = 9
  end
end
