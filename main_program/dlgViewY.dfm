object dialogViewY: TdialogViewY
  Left = 260
  Top = 322
  BorderStyle = bsDialog
  Caption = 'Y-axis features'
  ClientHeight = 228
  ClientWidth = 396
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  ShowHint = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 377
    Height = 73
    Caption = 'Y-axis:'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object radioUntrueYscale: TRadioButton
      Left = 16
      Top = 24
      Width = 233
      Height = 17
      Hint = 
        'Other displayed channels show y-zoom and offset: if a channel th' +
        'atis n-times greater than selected y-zoom, will cover n-times gr' +
        'eaterspace; channel with offset 1, with smallest value nand scre' +
        'en will correspond to the previous channel.'
      Caption = 'corresponding to the selected channel'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = radioUntrueYscaleClick
    end
    object radioTrueYscale: TRadioButton
      Left = 16
      Top = 48
      Width = 289
      Height = 17
      Hint = 
        'All visible channels are displayed on y-axis and are overlapped ' +
        'on the screen with their values.'
      Caption = 'corresponding to all channels (y-zoom and offset are ignored)'
      TabOrder = 1
      OnClick = radioTrueYscaleClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 88
    Width = 377
    Height = 97
    Caption = 'X-axis'
    TabOrder = 2
    object radioViewAll: TRadioButton
      Left = 16
      Top = 24
      Width = 289
      Height = 17
      Hint = 
        'Show the topics related to the minimum value of the measurementt' +
        'ill the maximal value of all measurement. Shifting and zoom onx ' +
        'axis do not affect the y-axis.'
      Caption = 'y-range of all channels for all the measurement'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = radioViewAllClick
    end
    object radioViewCurrent: TRadioButton
      Left = 16
      Top = 48
      Width = 345
      Height = 17
      Hint = 
        'Show range from minimal till maximal value in the current displa' +
        'y Shift and zoom along x-axis is automatic  y-axis.'
      Caption = 
        'y-range of all channels in the displayed part of the measurement' +
        's'
      TabOrder = 1
      OnClick = radioViewCurrentClick
    end
    object radioViewManual: TRadioButton
      Left = 16
      Top = 72
      Width = 113
      Height = 17
      Hint = 
        'Manual continue: shift+mouse wheel (shifting), shift+ctrl+mouse ' +
        'wheel (zoom). For other two select manual not possible.'
      Caption = '&manual'
      TabOrder = 2
      OnClick = radioViewManualClick
    end
  end
  object BitBtn1: TBitBtn
    Left = 312
    Top = 192
    Width = 75
    Height = 25
    Kind = bkClose
    NumGlyphs = 2
    TabOrder = 0
  end
end
