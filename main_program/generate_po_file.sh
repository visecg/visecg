#!/bin/bash
# before executing this script, make sure a correct (the right language) visecg.po is in the same directory as this script

# make sure you create po file from the user interface (.dfm files) by clicking "extract translations to template" on windows (requires "gnugettext for Delphi and C++ Builder" to be installed)
# fill in the required data to default.po (change CHARSET to UTF-8)
if [ -f default.po ]; then
    # the file exists, everything is groovy
    echo "using the input file from windows default.po (but converted to unix newline)"
    # convert newlines to linux form
    sed -i 's/\r$//' default.po
    # convert the wrongly inserted newlines where comments should be to normal comments and also rename to pot
    cat default.po | awk '{if ($0 ~ /.*\..*:[0-9]+/) {char1=substr($0,1,1); if (char1==" ") print "#:"$0; else print $0;} else {print $0}}' > default.pot
else
    echo "missing the input file from windows: default.po"
    echo "cannot continue with generation, exiting"
    exit 1
fi

# generate pot from cpp and h files
# TODO: see the --from-code, which is most likely incorrect; probably a different codepage is used in source (use iso-8859-2 instead of windows1251, which is not recognized)
xgettext --keyword=gettext --language=C++ --from-code=iso-8859-2 --add-comments --package-name=VisECG --sort-output -o from_code.pot *.cpp *.h

# merge the two pot files (pretend that windows created pot and not po)
msgcat -s default.pot from_code.pot > visecg.pot

# generate po from pot
if [ -f visecg.po ]; then
    # make backup of the old merged po file
    #rm visecg.po
    DATE=$(date +"%Y%m%d")
    rm -f visecg.po.$DATE
    cp visecg.po visecg.po.$DATE
    
    # the file exists, everything is groovy
    echo "Merging the existing translations (visecg.po)"
    msgmerge --output-file=visecg.po visecg.po visecg.pot
else
    echo "First time the translation has been attempted (there is no visecg.po file)"
    echo "Creating visecg.po from the visecg.pot template"
    msginit --input=visecg.pot --locale=en --output=visecg.po
fi

#
## fix po file (msgcat does something stupid and loses some comments)
#cat visecg.po | awk '{if ($0 ~ /.*\..*:[0-9]+/) {char1=substr($0,1,1); if (char1==" ") print "#:"$0; else print $0;} else {print $0}}' > visecg.po1
#if [ "$?" == "0" ]; then
#    echo "ok"
#    rm visecg.po
#    mv visecg.po1 visecg.po
#else
#    echo "oops, something went wrong, terminating script..."
#fi
#

# visecg.po file will have the description and '' comment repeated, edit these two manually until a better solution is found
echo "Edit vieecg.po manually to fix description"
echo "Also remove the #-#-#-#-#  default.pot (PACKAGE VERSION)  #-#-#-#-# from the msgstr"
geany visecg.po

# generate mo file

# final instructions
echo "now copy visecg.po to locale/xx/LC_MESSAGES/default.po where xx is the locale name, e.g. en"
echo "then execute enter your translations to visecg.po and execute: msgfmt --output-file=visecg.mo visecg.po "
echo "this will create binary translation file visecg.mo"
echo "finally copy visecg.mo to locale/xx/LC_MESSAGES/default.mo where xx is the locale name, e.g. en"
