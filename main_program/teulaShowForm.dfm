object EulaForm: TEulaForm
  Left = 391
  Top = 71
  Caption = 'Legal info'
  ClientHeight = 540
  ClientWidth = 429
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 329
    Height = 13
    Caption = 
      'Please read carefully the text below. Close the program if you d' +
      'isagree'
  end
  object BitBtn1: TBitBtn
    Left = 256
    Top = 504
    Width = 75
    Height = 25
    Caption = 'Yes'
    Kind = bkYes
    NumGlyphs = 2
    TabOrder = 0
  end
  object BitBtn2: TBitBtn
    Left = 88
    Top = 504
    Width = 75
    Height = 25
    Caption = 'No'
    Kind = bkNo
    NumGlyphs = 2
    TabOrder = 1
  end
  object Memo1: TRichEdit
    Left = 16
    Top = 48
    Width = 401
    Height = 441
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HideScrollBars = False
    Lines.Strings = (
      'Memo1')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 2
    Zoom = 100
  end
end
