object FolderOverviewSettings: TFolderOverviewSettings
  Left = 326
  Top = 144
  AutoSize = True
  Caption = 'Folder view settings'
  ClientHeight = 369
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 8
    Width = 69
    Height = 13
    Caption = 'Low limit BPM:'
  end
  object Label2: TLabel
    Left = 0
    Top = 48
    Width = 71
    Height = 13
    Caption = 'High limit BPM:'
  end
  object Label3: TLabel
    Left = 24
    Top = 80
    Width = 167
    Height = 13
    Caption = 'Standard deviation limit of BPM (%):'
  end
  object Label4: TLabel
    Left = 32
    Top = 96
    Width = 108
    Height = 13
    Caption = 'Value displayed in grey'
  end
  object Label5: TLabel
    Left = 32
    Top = 144
    Width = 106
    Height = 13
    Caption = 'Select primary location'
  end
  object Label6: TLabel
    Left = 8
    Top = 192
    Width = 90
    Height = 13
    Caption = 'BPM length (hours)'
  end
  object Label7: TLabel
    Left = 8
    Top = 243
    Width = 131
    Height = 13
    Caption = 'Select default beat detector'
  end
  object Label8: TLabel
    Left = 21
    Top = 291
    Width = 117
    Height = 13
    Caption = 'Measurement part length'
  end
  object Label9: TLabel
    Left = 271
    Top = 291
    Width = 5
    Height = 13
    Caption = 's'
  end
  object lowBPMinput: TEdit
    Left = 96
    Top = 0
    Width = 161
    Height = 21
    TabOrder = 0
    Text = 'lowBPMinput'
    OnChange = lengthBPMChange
  end
  object highBPMinput: TEdit
    Left = 96
    Top = 40
    Width = 161
    Height = 21
    TabOrder = 1
    Text = 'highBPMinput'
    OnChange = lengthBPMChange
  end
  object stdInput: TEdit
    Left = 56
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'stdInput'
    OnChange = lengthBPMChange
  end
  object BitBtn1: TBitBtn
    Left = 168
    Top = 344
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 6
  end
  object BitBtn2: TBitBtn
    Left = 32
    Top = 344
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 7
  end
  object folderLocation: TEdit
    Left = 0
    Top = 160
    Width = 185
    Height = 21
    TabOrder = 3
    Text = 'C.\VisECG\measurements'
  end
  object Button1: TButton
    Left = 192
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Select folder'
    TabOrder = 4
    OnClick = Button1Click
  end
  object lengthBPM: TEdit
    Left = 168
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'lengthBPM'
    OnChange = lengthBPMChange
  end
  object ComboBox1: TComboBox
    Left = 145
    Top = 240
    Width = 168
    Height = 21
    AutoComplete = False
    TabOrder = 8
    OnSelect = ComboBox1Select
    Items.Strings = (
      'No detector(when opening file no detection will take place)'
      'Simple beat detector'
      'Static beat detector'
      'Adaptive beat detector (signal)'
      'Adaptive beat detector (derivative)'
      '5 point beat detector')
  end
  object chunkLength: TEdit
    Left = 144
    Top = 288
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 9
  end
end
